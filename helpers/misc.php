<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

function html_render_select($site, $name, $options, $attrs = array(), $unselected = '-')
{
    $attrs['name'] = $name;
    if (!isset($attrs['id']))
        $attrs['id'] = $name;
    $opts_html = array();
    $cur_val = isset($site->values->$name) ? $site->values->$name : null;

    if (!is_null($unselected)) {
        $opts_html[] = html_render_tag('option', $unselected, array(
            'value' => '',
            'selected' => empty($cur_val)
        ));
    }
    foreach ($options as $val => $text) {
        $opts_html[] = html_render_tag('option', $text, array(
            'value' => $val,
            'selected' => $cur_val == $val
        ));
    }
    return html_render_tag('select', implode("\n", $opts_html), $attrs);
}

function html_render_tag($name, $content = '', $attrs = array())
{
    $ret = array('<'.$name);
    foreach ($attrs as $attr => $val) {
        if (is_bool($val)) {
            if ($val === true)
                $ret[] = $attr;
            continue;
        }
        $ret[] = $attr.'="'.htmlspecialchars($val).'"';
    }
    $ret = implode(' ', $ret);
    if ($content === false)
        $ret .= '/>';
    else
        $ret .= '>'.$content.'</'.$name.'>';
    return $ret;
}

if (!function_exists('array_column')) {
    function array_column($items, $val_field = 'name', $key_field = 'id')
    {
        $ret = array();
        foreach ($items as $item) {
            $val = isset($item[$val_field]) ? $item[$val_field] : null;
            if (isset($key_field) && isset($item[$key_field]))
                $ret[$item[$key_field]] = $val;
            else
                $ret[] = $val;
        }
        return $ret;
    }
}