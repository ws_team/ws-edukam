<?php
/**
 * Защита форм от спама посредством скрытого поля.
 */

class FormSpamProtection {
    private $fieldName;

    public function __construct($fieldName = 'signature')
    {
        $this->fieldName = $fieldName;
    }

    public function isSpamPost($site)
    {
        $site->GetPostValues($this->fieldName);
        return $site->values->{$this->fieldName} !== '';
    }

    public function activate()
    {
        $this->isActive = true;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function protectForms($html)
    {
        $insert = '<input name="'.$this->fieldName.'" value="" title="" />';
        $replacement = "\$0\n$insert\n";

        return
            preg_replace(
                '/<\/body>/i',
                '<style>form input[name="'.$this->fieldName.'"]:first-child {display: none; visibility: hidden;}</style>'.
                "\n".
                '</body>',
                preg_replace('/\<form[^>]*\>/i', $replacement, $html)
            );
    }
}