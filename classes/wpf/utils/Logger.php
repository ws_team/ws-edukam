<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\utils;

require_once('LoggerInterface.php');

class Logger implements LoggerInterface {
    private $file_path;
    private $level;
    private $tags;
    private $exclude;

    private $recordIdPrefix;
    private $recordIdCounter = 0;

    public function __construct($file_path, $level = LOG_INFO, $tags = array(), $exclude = array())
    {
        $dir_path = dirname($file_path);
        if ( ! file_exists($dir_path) && ! mkdir($dir_path, 0700)) {
            $this->level = -100;
            return;
        }
        $this->file_path = $file_path;
        $this->level = $level;
        $this->tags = $tags;
        $this->exclude = (array)$exclude;
    }

    /**
     * @param int $level
     * @param string $sender
     * @param string $message
     * @return string  log record ID
     */
    public function write($level, $sender, $message)
    {
        if ($level > $this->level)
            return;

        if ( ! empty($this->exclude)) {
            foreach ($this->exclude as $prefix) {
                if (strncmp($prefix, $sender, strlen($prefix)) == 0)
                    return;
            }
        }

        $levelTags = array(
            LOG_DEBUG => 'debug',
            LOG_NOTICE => 'notice',
            LOG_INFO => 'info',
            LOG_WARNING => 'warning',
            LOG_ERR => 'error',
            LOG_CRIT => 'crit',
            LOG_ALERT => 'alert',
            LOG_EMERG => 'emerg',
        );

        $recordId = $this->nextRecordId();

        $tags = $this->tags;
        $tags[] = $levelTags[$level];
        $tags[] = $sender;
        array_unshift($tags, $recordId);

        $message = date('Y-m-d H:i:sO ').' ['.implode('][', $tags).'] '.$message."\n";
        file_put_contents($this->file_path, $message, FILE_APPEND);

        return $recordId;
    }

    /**
     * @return string
     */
    protected function nextRecordId()
    {
        if (is_null($this->recordIdPrefix)) {
            $this->recordIdPrefix = mt_rand(1, 1000000).'-'.intval(microtime(true) * 1000000).'-';
        }
        return $this->recordIdPrefix.++$this->recordIdCounter;
    }
}