<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\utils;

require_once('LoggerInterface.php');

class NullLogger implements LoggerInterface {
    public function write($level, $sender, $message)
    {
        return '';
    }
}