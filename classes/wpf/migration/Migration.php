<?php
/**
 * Класс поддержки миграций базы данных.
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\migration;
require_once(WPF_CLASSROOT.'/utils/Logger.php');

class Migration {
    private $site;
    private $logger;

    public function __construct(\iSite $site)
    {
        $this->site = $site;
        $this->logger = $site->getLogger();
    }

    protected function setInputValues($values, $names = null)
    {
        $this->setValuesArray($values, $names, array($this, 'set_site_value'));
    }

    protected function setValuesArray($values, $names, $setter)
    {
        if (is_null($names))
            $names = array_keys($values);
        foreach ($names as $key) {
            $val = isset($values[$key]) ? $values[$key] : '';
            call_user_func($setter, $key, $val);
        }
    }

    protected function set_site_value($key, $val)
    {
        $this->site->values->$key = $val;
    }

    protected function set_request_param($key, $val)
    {
        $_REQUEST[$key] = $val;
    }

    protected function setPostValues($values, $names = null)
    {
        $this->setValuesArray($values, $names, array($this, 'set_request_param'));
    }

    protected function nextDbId($table, $column = 'id', $default = 1)
    {
        $ret = null;

        // @todo use sequence
        $query = 'SELECT MAX("'.$column.'")+1 "sid" FROM "'.$table.'"';

        if (($res = $this->site->dbquery($query)) !== false && count($res))
            $ret = $res[0]['sid'];

        if (empty($ret))
            $ret = $default;

        return $ret;
    }

    public function createTemplate($tpl_info)
    {
        $this->logger->write(LOG_INFO, __METHOD__, $tpl_info['file_name']);
        $this->setInputValues($tpl_info, array('name', 'file_name', 'materiallist', 'content'));

        if (!empty($this->site->values->name) &&
            !empty($this->site->values->file_name) &&
            isset($this->site->values->materiallist) &&
            $this->site->values->materiallist != '') {

            $this->log(LOG_INFO, __METHOD__,
                (!empty($tpl_info['materiallist']) ? 'L' : 'D').' '.$tpl_info['file_name']);

            $this->site->values->materiallist=(int)$this->site->values->materiallist;

            $sid = $this->nextDbId('standart_shablons');
            $tcnt = 0;
            //проверяем на существование шаблона
            $query = 'SELECT count("id") "cnt" FROM "standart_shablons" WHERE "file_name" = $1 AND "materiallist" = $2';
            $res = $this->site->dbquery($query,
                array($tpl_info['file_name'], $tpl_info['materiallist']));

            if ( ! empty($res)) {
                $tcnt=$res[0]['cnt'];
            }

            if ($tcnt == 0) {
                $this->log(LOG_INFO, '.. not exists, adding ', __METHOD__);
                //добавляем шаблон
                $query='INSERT INTO "standart_shablons" ("id","author","name","file_name","content","materiallist")'.
                ' VALUES($1, $2, $3, $4, $5, $6)';
                $params = array(
                    $sid,
                    $this->site->autorize->userlogin,
                    $this->site->values->name,
                    $this->site->values->file_name,
                    $this->site->values->content,
                    $this->site->values->materiallist
                );

                $res = $this->site->dbquery($query, $params);
                if ( ! empty($res)) {
                    $this->message('Шаблон добавлен! - '.$this->site->values->name);
                    $this->log(LOG_INFO, __METHOD__, '.. added');
                } else {
                    $this->message('Не удалось сохранить шаблон - '.$this->site->values->name);
                    $this->log(LOG_WARNING, __METHOD__, '.. failed to add');
                }
            }
            else{
                $this->log(LOG_INFO, __METHOD__, '.. already exists');
                $this->message('Шаблон - '.$this->site->values->name.' уже есть в системе.');
            }
        }
    }

    protected function message($text)
    {
        $this->site->data->errortext .= '<p>'.$text.'</p>';
    }

    public function createMaterialType($mattype_info)
    {
        $this->log(LOG_INFO, __METHOD__, 'id_char='.$mattype_info['id_char']);

        $parent_id = NULL;
        if (!empty($mattype_info['parent_char']))
            $parent_id = findTypeIdByChar($this->site, $mattype_info['parent_char']);

        //создаём типы
        $typeid = createMaterialType(
            $this->site,
            $mattype_info['name'],
            $mattype_info['en_name'],
            NULL,
            $mattype_info['id_char'],
            intval($parent_id),
            isset($mattype_info['ordernum']) ? intval($mattype_info['ordernum']) : 0,
            isset($mattype_info['showinmenu']) ? intval($mattype_info['showinmenu']) : 0
        );

        //сразу его изменяем
        if ($typeid[0]) {
            $this->log(LOG_DEBUG, __METHOD__, '.. created #'.$typeid[0]);
            $mattype_info['template_list'] = $mattype_info['template'] = '';
            if(!empty($mattype_info['template_char'])) {
                $mattype_info['template'] = findTemplateIdByChar(
                    $this->site,
                    $mattype_info['template_char'],
                    'mat'
                );
            }
            if (empty($mattype_info['template']))
                $mattype_info['template'] = 0;

            if ( ! empty($mattype_info['template_list_char'])) {
                $mattype_info['template_list'] = findTemplateIdByChar(
                    $this->site,
                    $mattype_info['template_list_char'],
                    'list'
                );
            }
            if (empty($mattype_info['template_list']))
                $mattype_info['template_list'] = 0;

            $this->setPostValues($mattype_info,
                array(
                    'hide_anons',
                    'hide_date_event',
                    'hide_tags',
                    
                    'hide_parent_id',
                    'hide_extra_number',
                    'hide_contacts',
                    'hide_lang',
                    'hide_files',
                    'hide_conmat',
                    
                    'mat_as_type',
                    'hide_content',
                    'template_list',
                    'template'
                )
            );
            
            //обновляем
            createMaterialType(
                $this->site,
                $mattype_info['name'],
                $mattype_info['en_name'],
                $typeid[0],

                $mattype_info['id_char'],

                intval($parent_id),
                isset($mattype_info['ordernum']) ?
                    intval($mattype_info['ordernum']) :
                    null,
                isset($mattype_info['showinmenu']) ?
                    intval($mattype_info['showinmenu']) :
                    false
            );
        } else {
            $this->log(LOG_ERR, __METHOD__, '.. failed to create');
        }
    }

    public function alterMaterialType($mattype_info)
    {
        $this->log(LOG_INFO, __METHOD__, 'id_char='.$mattype_info['id_char']);
        $this->updateMaterialType($mattype_info);
        $this->addMaterialTypeParams($mattype_info);
    }
    
    protected function updateMaterialType($mattype_info)
    {
        $this->log(LOG_INFO, __METHOD__, 'id_char='.$mattype_info['id_char']);
        if (false === ($typeid=findTypeIdByChar($this->site, $mattype_info['id_char']))) {
            $this->log(LOG_DEBUG, __METHOD__, 'id_char='.$mattype_info['id_char'].' not exists');
            return;
        }

        $template_list = '0';
        $template = '0';

        if ( ! empty($mattype_info['template_char'])) {
            $template = findTemplateIdByChar($this->site, $mattype_info['template_char'], 'mat');
        }

        if ( ! empty($mattype_info['template_list_char'])) {
            $template_list = findTemplateIdByChar(
                $this->site, $mattype_info['template_list_char'], 'list'
            );
        }

        if ($template_list === false) {
            $this->log(LOG_DEBUG, __METHOD__,
                "list template '{$mattype_info['template_list_char']}' does not exist");
            $this->message("Не существует шаблона списка '{$mattype_info['template_list_char']}'");
            $template_list = null;
        }

        if ($template === false) {
            $this->log(LOG_DEBUG, __METHOD__,
                "details template '{$mattype_info['template_char']}' does not exist");
            $this->message("Не существует шаблона детализации '{$mattype_info['template_char']}'");
            $template = null;
        }

        //обновляем шаблон
        $query = 'UPDATE "material_types" SET';
        $params = array();
        $query_set = array();

        $fields = array(
            'parent_id',
            'ordernum',
            'mat_as_type',
            'hide_arent_id',
            'hide_extra_number',
            'hide_lang',

            'hide_anons',
            'hide_ordernum'
        );

        if ( ! is_null($template)) {
            $params[] = $template;
            $query_set[] = '"template"=$'.count($params);
        }
        if ( ! is_null($template_list)) {
            $params[] = $template_list;
            $query_set[] = '"template_list"=$'.count($params);
        }

        foreach ($fields as $fld) {
            if (isset($mattype_info[$fld]) && $mattype_info[$fld] != '') {
                $params[] = intval($mattype_info[$fld]);
                $query_set[] ='"'.$fld.'" = $'.count($params);
            }
        }

        if (empty($query_set))
            return;

        $query .= ' '.implode(',', $query_set);

        $params[] = $typeid;
        $query .= 'WHERE "id" = $'.count($params);
        $res = $this->site->dbquery($query, $params);
        if ( ! empty($res)) {
            $this->addMaterialTypeParams($mattype_info);
            $this->updateMaterialsCache();
        }
    }

    protected function updateMaterialsCache()
    {
        //обновляем кэши
        //перестраиваем кэш массива типов материалов
        $this->site->data->material_types=GepTypeParentTypes($this->site, 0);
        if(!ArrCacheWrite($this->site, $this->site->data->material_types,
            'material_types_'.$this->site->data->language))
        {
            $this->log(LOG_ERR, __METHOD__, 'ArrCacheWrite(current_material_types) failed');
        };

        //перекэшируем массив типов материалов с активными материалами
        $normaltypes=Array();
        $query='SELECT DISTINCT "type_id" FROM "materials" WHERE "status_id" = '.STATUS_ACTIVE;
        if($res=$this->site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    $normaltypes[]=$row['type_id'];
                    //добавляем родительские типы
                    $morearr=GetTypeParents($this->site, $row['type_id']);
                    if(count($morearr) > 0)
                    {
                        foreach($morearr as $ma)
                        {
                            $normaltypes[]=$ma;
                        }
                    }
                }
            }
        }

        if(!ArrCacheWrite($this->site, $normaltypes, 'normal_material_types'))
        {
            $this->log(LOG_ERR, __METHOD__, 'ArrCacheWrite(normaltypes) failed');
        }
    }
    
    public function addMaterialTypeParams($mattype_info)
    {
        if (empty($mattype_info['params']))
            return;

        $this->log(LOG_INFO, __METHOD__, 'type id_char='.$mattype_info['id_char']);
        //получаем id типа материала
        if (false === ($typeid = findTypeIdByChar($this->site, $mattype_info['id_char']))) {
            $this->log(LOG_INFO, __METHOD__, '.. type does not exist');
            return;
        }

        foreach ($mattype_info['params'] as $param) {
            if ( ! empty($param['name'])) {
                $this->log(LOG_DEBUG, __METHOD__, ".. param='{$param['name']}' {$param['valuetype']}");

                $valuetype = isset($param['valuetype']) ? $param['valuetype'] : 'valuename';
                $valuemattype = 0;

                if ( ! empty($param['matchar'])) {
                    $valuemattype = findTypeIdByChar($this->site, $param['matchar']);
                    if ( ! $valuemattype) {
                        $this->log(LOG_WARNING, __METHOD__, "type '{$param['matchar']}' not found'");
                        $this->message("Тип '{$param['matcher']}' параметра {$mattype_info['name']}:{$param['name']} не существует");
                        continue;
                    }
                    $valuetype = 'valuemat';
                }

                //проверяем на существование параметра
                $existingParam = findMatParamByName($this->site, $typeid, $param['name']);

                if ( ! empty($existingParam)) {
                    $this->log(LOG_DEBUG, __METHOD__, '... already exists');
                    if ($valuetype != $existingParam['valuetype']
                        || $valuemattype != $existingParam['valuemattype']) {

                        $this->log(LOG_DEBUG, __METHOD__, '... but it differs');
                        $query = 'UPDATE extra_mattypes SET valuetype = $1, valuemattype = $2 WHERE id = $3';
                        $q_params = array($valuetype, $valuemattype, $existingParam['id']);
                        $this->site->dbquery($query, $q_params);
                    }
                } else {

                    $mid = $this->nextDbId('extra_mattypes');

                    $query = <<<EOS
INSERT INTO "extra_mattypes" ("id", "type_id", "name", "valuetype", "valuemattype")
VALUES($1, $2, $3, $4, $5)
EOS;

                    $q_params = array(
                        $mid,
                        $typeid,
                        $param['name'],
                        $valuetype,
                        intval($valuemattype)
                    );

                    $this->log(LOG_INFO, __METHOD__, 'inserting['.implode(',', $q_params).']');

                    $this->site->dbquery($query, $q_params);
                    $this->log(LOG_INFO, __METHOD__, '... added');
                }
            }
        }
    }

    public function deleteMaterialType($mattype_info)
    {
        $this->message('<!-- OIVDB: ['.$mattype_info['id_char'].'] -->');

        //получаем id типа для скрытия
        if(false === ($typeid=findTypeIdByChar($this->site, $mattype_info['id_char']))) {
            $this->log(LOG_DEBUG, __METHOD__, 'id_char='.$mattype_info['id_char'].' does not exist');
            return;
        }

        $this->log(LOG_INFO, __METHOD__, 'id_char='.$mattype_info['id_char']);

        $this->message('<!-- OIVDB: ['.$mattype_info['id_char'].'] ид типа получен = '.$typeid.' -->');

        //перемещаем материалы
        if (isset($mattype_info['parent_char']) && $mattype_info['parent_char'] != '')
        {
            //получаем тип документа родителя
            if($parentid=findTypeIdByChar($this->site, $mattype_info['parent_char']))
            {
                //перемещаем материалы
                $query='UPDATE "materials" SET "type_id" = $1 WHERE "type_id" = $2';
                $this->site->dbquery($query, array($parentid, $typeid));
            }
        }

        $query='UPDATE "material_types" SET "showinmenu" = 0 WHERE "id" = $1';
        $this->site->dbquery($query, array($typeid));

        //удаляем тип
        $query='DELETE FROM "material_types" WHERE "id" = $1';
        $this->site->dbquery($query, array($typeid));
    }

    public function addMaterial($mat_info)
    {
        if (false !== ($type_id=findTypeIdByChar($this->site, $mat_info['type_char'])))
        {
            $this->log(LOG_INFO, __METHOD__, 'name_char='.$mat_info['name_char'].', type_id='.$type_id);
            $this->site->values->type_id=$type_id;
            $this->site->values->name=$mat_info['name'];
            $this->site->values->id_char=$mat_info['name_char'];
            $this->site->values->status_id=STATUS_ACTIVE;
            $this->site->values->language_id=0;
            $this->site->values->date_event=0;
            $this->site->values->extra_number=0;
            $this->site->values->extra_number2=0;
            $this->site->values->extra_text1='';
            $this->site->values->extra_text2='';
            $this->site->values->anons='';
            $this->site->values->content = isset($mat_info['content']) ? $mat_info['content'] : '';

            //проверяем на наличае материалов
            if (($matid = findMatIdByChar($this->site, $this->site->values->id_char))) {
                $this->log(LOG_DEBUG, __METHOD__, '.. already exists id='.$matid);
                $this->site->dbquery('UPDATE "materials" SET "status_id" = $1 WHERE "id" = $2 AND "status_id" = $3',
                    array(STATUS_ACTIVE, $matid, STATUS_DELETED)
                );
            } else {
                $this->log(LOG_DEBUG, __METHOD__, '.. creating');
                createMaterial($this->site);
                $this->log(LOG_INFO, __METHOD__, '.. created');
            }
        } else {
            $this->log(LOG_DEBUG, __METHOD__, 'type id_char='.$mat_info['type_char'].' not found');
        }
    }

    public function begin()
    {
        $this->log(LOG_DEBUG, __METHOD__);
        if (!GetUnderConstructionFlag($this->site))
            ToggleUnderConstructionFlag($this->site);
        $this->beginOutput();
    }

    protected function beginOutput()
    {
        $this->site->data->iH1='Настройка структуры ОИВ';
        $this->site->data->errortext='';
    }

    protected function getSiteId()
    {
        return $this->site->settings->DB->name;
    }

    protected function log($level, $sender, $msg = '_')
    {
        $this->logger->write($level, $sender, $msg);
    }

    protected function getLogPath()
    {
        return WPF_ROOT.'/runtime/migration.log';
    }

    public function end()
    {
        //обновляем все типы материалов - скрываем отображение языка
        $query = 'UPDATE "material_types" SET "hide_lang" = 1';
        if($res = $this->site->dbquery($query))
            $this->message('У всех типов материала скрыто отображение языка!');

        ToggleUnderConstructionFlag($this->site);
        $this->log(LOG_DEBUG, __METHOD__);
    }
}
