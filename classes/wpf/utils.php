<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\utils;

function in_pathset($path, $set)
{
    $pathLen = mb_strlen($path);
    foreach ($set as $item) {
        $itemLen = mb_strlen($item);
        if ($itemLen > $pathLen)
            continue;
        if (mb_substr($path, -$itemLen) == $item)
            return true;
    }
    return false;
}

function filter_oivlist($oivlist, $pattern)
{
    $ret = array();
    foreach ($oivlist as $oiv)
        if (fnmatch($pattern, $oiv))
            $ret[] = $oiv;
    return $ret;
}