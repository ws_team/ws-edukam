<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\db;

class QueryBuilder {
    private $columns;
    private $from;
    private $orderby;
    private $where = array();
    private $params = array();

    public function select($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    public function from($table)
    {
        $this->from = $table;
        return $this;
    }

    public function where($condition, $params = array())
    {
        $this->where = array();
        $this->params = array();
        $this->andWhere($condition, $params);
        return $this;
    }

    public function andWhere($condition, $params)
    {
        if (is_string($condition)) {
            $rebuilt = array();
            $nparam = count($this->params) + 1;
            $parts = explode('?', $condition);
            $last_part = array_pop($parts);
            foreach ($parts as $i => $cond) {
                $this->params[] = $params[$i];
                $rebuilt[] = $cond . '$'.$nparam++;
            }
            if ($last_part !== '')
                $rebuilt[] = $last_part;

            $rebuilt = '('. implode('', $rebuilt).')';
            $this->where[] = $rebuilt;
        } elseif (is_array($condition)) {
            throw new \RuntimeException('Unimplemented');
        } else {
            throw new \InvalidArgumentException('Unsupported type "'.gettype($condition).'" of $condition');
        }

        return $this;
    }

    public function build()
    {
        $parts = array();
        if ($this->columns) {
            $parts[] = 'SELECT '.$this->buildSelectColumns($this->columns);
        }

        if ( ! empty($this->from))
            $parts[] = 'FROM "'.$this->from.'"';

        if ($this->where)
            $parts[] = 'WHERE '.implode(' AND ', $this->where);

        return array(implode(' ', $parts), $this->params);
    }

    public function buildParamless()
    {
        list($query, $params) = $this->build();
        foreach ($params as $i => $val) {
            if (is_numeric($val))
                $val_esc = $val;
            else
                $val_esc = "'".addslashes(strval($val))."'";
            $query = str_replace('$'.($i + 1), $val_esc, $query);
        }
        return $query;
    }

    protected function buildSelectColumns($columns)
    {
        if (is_scalar($columns))
            return $columns;
        if (is_array($columns)) {
            $protected = array();
            foreach ($columns as $col)
                $protected[] = '"'.$col.'"';
            return implode(',', $protected);
        }

        throw new \InvalidArgumentException('Unsupported type "'.gettype($columns).'" of argument $columns');
    }

    public function hasWhere()
    {
        return ! empty($this->where);
    }
}