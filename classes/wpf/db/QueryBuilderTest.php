<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

require_once('QueryBuilder.php');

class QueryBuilderTest extends \PHPUnit_Framework_TestCase {
    public function test()
    {
        $qb = new \wpf\db\QueryBuilder();
        $qb->select('id')->from('materials')->where('status_id = ?', array(2))
            ->andWhere('date > ?', array('2016-07-18'));
        list($query, $params) = $qb->build();
        $this->assertEquals('SELECT id FROM "materials" WHERE (status_id = $1) AND (date > $2)', $query);
        $this->assertEquals(array(2, '2016-07-18'), $params);
        $query = $qb->buildParamless();
        $this->assertEquals('SELECT id FROM "materials" WHERE (status_id = 2) AND (date > \'2016-07-18\')', $query);
    }
}