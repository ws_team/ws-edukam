<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//класс отсылки EMail сообщений
class iEMail
{
    public $settings;
    public $error;
    public $warning;
    public $email;

    public function __construct($settings)
    {
        $this->settings = $settings;

        $this->error = new stdClass();
        $this->error->flag=0;
        $this->error->text='';

        $this->warning = new stdClass();
        $this->warning->flag=0;
        $this->warning->text='';
    }

    public function Send($to, $subject, $message, $Attachments=Array())
    {
        $cc = array();
        $sendToMany = true;
        $singleTo = null;
        $manyTo = array();

        foreach ((array)$to as $tos) {
            if (!CheckEmail($tos))
                continue;
            if ($sendToMany) {
                $manyTo[] = $tos;
                continue;
            }

            if (is_null($singleTo))
                $singleTo = $tos;
            else
                $cc[] = $tos;
        }

        $to = $sendToMany ? $manyTo : $singleTo;

        if (empty($to)) {
            $this->error->flag = 1;
            $this->error->text = '<br>Ошибка! E-mail адресата не соответствует шаблону!';
            return false;
        }

        $subject = iconv('utf-8', 'cp1251', $subject);
        $message = iconv('utf-8', 'cp1251', $message);
        $signature = iconv('utf-8', 'cp1251', $this->settings->email->signature);

        //$subject=convert_cyr_string($subject, "w","k");
        $subject = '=?koi8-r?B?'.base64_encode(convert_cyr_string($subject, "w", "k")).'?=';

        $mail = new PHPMailer();
        $mail->IsSMTP();

        try {
            if (empty($this->settings->email->pass))
            {
                $ifauth = false;
            } else {
                $ifauth = true;
            }

            if ($this->settings->email->port == 25) {
                if($this->settings->email->smtp != '172.18.130.7'){
                    $SMTPSecure='tls';
                }
            } else {
                $SMTPSecure='ssl';
            }

            $mail->SMTPDebug  = 0;                             // enables SMTP debug information (for testing)
            $mail->SMTPAuth   = $ifauth;                       // enable SMTP authentication
            $mail->SMTPSecure = $SMTPSecure;                   // sets the prefix to the servier
            $mail->Host       = $this->settings->email->smtp;  // sets GMAIL as the SMTP server
            $mail->Port       = $this->settings->email->port;  // set the SMTP port for the GMAIL server
            $mail->Username   = $this->settings->email->mail;  // GMAIL username
            $mail->Password   = $this->settings->email->pass;  // GMAIL password
            if ( ! empty($this->settings->email->self_hostname))
                $mail->Helo = $mail->Hostname = $this->settings->email->self_hostname;

            $mail->SetFrom($this->settings->email->mail, $signature);
            $mail->Subject = $subject;
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
            $mail->ContentType = 'text/plain';
            $mail->CharSet = 'windows-1251';
            //$mail->IsHTML(false);
            //$message = iconv('UTF-8', 'koi8-r//IGNORE', $message);
            $mail->MsgHTML($message);

            if(is_array($Attachments) && count($Attachments) > 0)
            {
                foreach($Attachments as $Attachment)
                {
                    $mail->AddAttachment($Attachment['tmp_name'], $Attachment['name']); // attachment
                }
            }

            $mail->AddReplyTo($this->settings->email->mail, $this->settings->email->mail);

            foreach ($to as $tos)
                $mail->AddAddress($tos, $tos);
            foreach ($cc as $ccc)
                $mail->addCC($ccc, $ccc);

            $mail->Send();

            /*krumo($res);
            exit;*/

            //echo 'Message has been sent';
            // echo $this->settings->email->smtp;
            // echo $this->settings->email->port;
            // echo $this->settings->email->mail;
            
        } catch (phpmailerException $e) {
            $this->error->flag=1;
            $this->error->text='<br>
            '.$e->errorMessage();
            
	    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                        

            
        } catch (Exception $e) {
            $this->error->flag=1;
            $this->error->text.='<br>
            '.$e->getMessage(); //Boring error messages from anything else!
            
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;    
            
        }
        return $this->error->flag == 0;
    }
}