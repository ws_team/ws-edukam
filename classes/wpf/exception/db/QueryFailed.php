<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\exception\db;

class QueryFailed extends \RuntimeException {
    private $query;
    private $params;

    public function __construct($query, $params = array())
    {
        $this->query = $query;
        $this->params = $params;

        parent::__construct('Ошибка выполнения запроса к БД');
    }

    public function __toString()
    {
        return "Запрос:\n{$this->query}\n---\n".
            ( ! empty($this->params) ?
                "Параметры:\n".
                print_r($this->params, true)."\n---\n" :
                ''
            );
    }
}