<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


require_once('auth/User.php');
require_once('auth/InfoProxy.php');
require_once('auth/Manager.php');

require_once('cache/Redis.php');
require_once('geo/LocationService.php');

require_once(__DIR__.'/utils/Logger.php');
require_once(__DIR__.'/utils/NullLogger.php');

require_once(__DIR__.'/base/Hookable.php');
require_once(__DIR__.'/base/Batchable.php');

require_once(__DIR__.'/exception/db/QueryFailed.php');

require_once('mail/Sender.php');

//главный класс - сайт. Решаются все вопросы обработки логики сайта, формирование и вывода информации
class iSite
{
    use \wpf\base\Hookable;
    use \wpf\base\Batchable;

    public $values;
    public $settings;
    public $headers;
    public $data;
    public $html;
    public $htmlprint;
    public $error;
    public $warning;
    public $mysqli;
    public $pgsql;

    public $aregion;

    /**
     * @var \FormSpamProtection
     */
    private $spamProtection;

    private $userInfo;
    private $authInfoProxy;// временный костыль, перехватывает обращения к iSite::$autorize

    /**
     * @var \wpf\auth\Manager
     */
    private $authManager;

    private $redis;
    private $cache;
    private $geoloc;

    /**
     * @var array  Стек имен шаблонов(тем), просматриваемых при поиске файла шаблона (partial)
     */
    private $templateStack = array();
    /**
     * @var string
     */
    private $partialDir;

    private $searchPathList = array();

    /**
     * @var \wpf\utils\LoggerInterface
     */
    private $logger;

    private $meta = array();

    private $sessionActive;

    private $redisKeyPrefix;

    private $headersList = array();
    private $responseSent = false;

    private $flashes;

    private $mailer;

    private $cachePrevented;

    private $templateOptions = array();

    public function isAjaxRequest()
    {
        return (
            isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        );
    }

    public function makeRequestPairs($array) {

        $pairs = array();

        foreach ($array as $key => $value) {
            $pairs[] = $key.'='.$value;
        }

        return $pairs;
    }

    protected function createGetURL($get, $get_params) {

        $url = '/' . $_REQUEST['requeststr'];

        if (is_array($get_params)) {
            foreach ($get_params as $key => $value) {
                if (array_key_exists($key, $get)) {
                    $get[$key] = $value;
                }
            }

            $get = $this->makeRequestPairs($get);

            $url .= '?' . implode('&', $get);

            return $url;
        }

        return '';
    }


    public function AttachJS($url, $place = 'header', $pos = -1)
    {
        global $jsheaderarr;
        if (!is_array($jsheaderarr))
        {
            $jsheaderarr=Array();
        }

        global $jsfooterarr;
        if(!is_array($jsfooterarr))
        {
            $jsfooterarr=Array();
        }

        if($place == 'header')
        {
            if (!in_array($url, $jsheaderarr))
            {
                if ($pos == -1)
                    $jsheaderarr[] = $url;
                elseif ($pos == 0)
                    array_unshift($jsheaderarr, $url);
                else
                    array_splice($jsheaderarr, $pos, 0, $url);
            }
        }
        else {
            if (!in_array($url,$jsfooterarr))
            {
                $jsfooterarr[]=$url;
                if ($pos == -1)
                    $jsfooterarr[]=$url;
                elseif ($pos == 0)
                    array_unshift($jsfooterarr, $url);
                else
                    array_splice($jsfooterarr, $pos, 0, $url);
            }
        }

        $this->data->jsheaderarr = $jsheaderarr;
        $this->data->jsfooterarr = $jsfooterarr;
    }

    public function AttachCSS($url, $place='header')
    {
        global
        $cssheaderarr,
        $cssfooterarr,
        $cssseparr;

        if ( ! is_array($cssheaderarr)) {
            $cssheaderarr = Array();
        }

        if ( ! is_array($cssfooterarr)) {
            $cssfooterarr = Array();
        }

        if ( ! is_array($cssseparr)) {
            $cssseparr = Array();
        }

        $map = array(
            'header' => 'cssheaderarr',
            'footer' => 'cssfooterarr',
            'separate' => 'cssseparr',
        );

        if ( ! isset($map[$place])) {
            $place = 'cssfooterarr';
        }

        $varname = $map[$place];

        if ( ! in_array($url, $GLOBALS[$varname])) {
            if ( ! empty($url)) {
                $GLOBALS[$varname][] = $url;
            }
        }

        foreach ($map as $varname)
            $this->data->$varname = $GLOBALS[$varname];
    }

    public function getSeparateCssList()
    {
        return isset($this->data->cssseparr) ? $this->data->cssseparr : array();
    }

    public function IncludeMinCss($cssarray){

        $uri='';

        if(count($cssarray) > 0){
            foreach($cssarray as $css)
            {

                if($uri == '')
                {
                    $uri='/min/f=';
                }
                else{
                    $uri.=',';
                }

                $uri.=$css;

            }}

        if($uri != '')
        {

            $uri=urldecode($uri);
            return ('<link type="text/css" rel="stylesheet" href="'.$uri.'" />');

        }
        else{
            return false;
        }
    }

    public function IncludeMinJS($jsarray)
    {
        if (!count($jsarray))
            return false;

        $jsarray = array_unique($jsarray);
        $uri = '/min/f='.implode(',', $jsarray);
        $uri = urldecode($uri);

        return '<script type="text/javascript" src="'.$uri.'"></script>';
    }

    public function attachJsSnippet($script)
    {
        $this->data->morejs .= "\n".$script;
    }

    public function modifyGetURL($get_params) {

        if (!isset($this->values->requeststr)) {
            $url = '/';
        } else {
            $url = '/' . $this->values->requeststr;
        }

        $get = $_GET;
        unset($get['requeststr']);
        unset($get['/']);

        if (!count($get)) {
            return $this->createGetURL($get, $get_params);
        }

        if (is_array($get_params)) {
            foreach ($get_params as $key => $value) {
                if($key == 'version') {
                    if($value != 'special')
                    {
                        $value='normal';
                    }
                }

                $get[$key] = $value;
            }

            $get = $this->makeRequestPairs($get);

            $url.= '?' . implode('&', $get);

            return $url;
        }

        return '';
    }

    public function localize_month($monthnum, $rodpad = true, $lang = 'ru') {

        $month = array(
            'ru' => array(
                ($rodpad) ? 'января' : 'январь',
                ($rodpad) ? 'февраля' : 'февраль',
                ($rodpad) ? 'марта' : 'март',
                ($rodpad) ? 'апреля' : 'апрель',
                ($rodpad) ? 'мая' : 'май',
                ($rodpad) ? 'июня' : 'июнь',
                ($rodpad) ? 'июля' : 'июль',
                ($rodpad) ? 'августа' : 'август',
                ($rodpad) ? 'сентября' : 'сентябрь',
                ($rodpad) ? 'октября' : 'октябрь',
                ($rodpad) ? 'ноября' : 'ноябрь',
                ($rodpad) ? 'декабря' : 'декабрь',
            ),
        );

        if ((int)$monthnum) {

            $monthnum--;

            if (array_key_exists($lang, $month)) {
                if (array_key_exists($monthnum, $month[$lang])) {
                    return $month[$lang][$monthnum];
                }
            }

        }

        return false;
    }

    public function get_config_param($name, $default = null)
    {
        return isset($this->settings->$name) ? $this->settings->$name : $default;
    }

    public function trim_text($text, $max_text){
        $trimed='';
        $text = str_replace("  ", " ", $text);
        $string = explode(" ", $text);
        $count = 100;
        for ($wordCounter = 0; $wordCounter <= $count;$wordCounter++) {

            if(isset($string[$wordCounter]))
                $trimed .= $string[$wordCounter];

            if(strlen($trimed) >= $max_text) break;
            if($wordCounter < $count){
                $trimed .= " ";
            }
        }
        $trimed = trim($trimed);
        return $trimed;
    }



    public function GetRegionFromIP($ipAddr='')
    {

        $this->aregion = new stdClass();
        $this->aregion->locator = new stdClass();

        $fres=false;
        if($ipAddr == '')
        {
            $ipAddr=$_SERVER['REMOTE_ADDR'];
            if($ipAddr == '127.0.0.1')
            {
                $ipAddr='194.154.87.138';
            }
        }

        try{

            if($f = file_get_contents('http://ipgeobase.ru:7020/geo/?ip='.$ipAddr))
            {
                $f=iconv('cp1251','utf-8',$f);
                //echo $f.' - '.$ipAddr;
                //пробуем определить город
                if(preg_match("@<city>(.*?)</city>@si", $f, $city))
                {
                    $this->aregion->locator->city=$city[1];
                    $fres=true;
                }

                //пробуем определить код страны
                if(preg_match("@<country>(.*?)</country>@si", $f, $city))
                {
                    $this->aregion->locator->country=$city[1];
                }
                else
                {
                    $this->aregion->locator->country='';
                }
            }

        } catch (Exception $e)
        {
            $fres=false;
            $this->aregion->locator->country='ru';
        }

        return($fres);
    }

    public function DoAutorize()
    {
        if(isset($_REQUEST['autorize_login']) || isset($_REQUEST['autorize_pass']) || isset($_REQUEST['autorize_email']))
        {
            $this->GetPostValues(Array('autorize_login','autorize_pass','autorize_email'));

            if(($this->values->autorize_login != '' || $this->values->autorize_email != '')&&($this->values->autorize_pass != ''))
            {
                //хэш пароля
                $this->values->passhash=md5($this->values->autorize_pass);

                //определяем id или email, формируем запрос
                if($this->values->autorize_email != '')
                {
                    $query='SELECT * FROM rs_users WHERE email IS NOT NULL AND email = '."'".$this->values->autorize_email.
                        "'".' AND pass = '."'".$this->values->passhash."'";
                }
                else
                {
                    $query='SELECT * FROM rs_users WHERE nikname IS NOT NULL AND nikname = '."'".
                        $this->values->autorize_login."'".' AND pass = '."'".$this->values->passhash."'";
                }

                //получаем данные о пользователе
                $res=$this->dbquery($query);
                if ( ! empty($res)) {
                    $this->autorize->answer='Авторизироваться удалось';

                    $this->autorize->autorized=1;
                    $this->autorize->username=$res[0]['fio'];
                    $this->autorize->userlogin=$res[0]['nikname'];
                    $this->autorize->userid=$res[0]['id'];
                    $this->autorize->userrole=$res[0]['role_id'];
                    $this->autorize->email=$res[0]['email'];

                    if( $this->autorize->username == '')
                    {
                        $this->autorize->username='mr. Autorized Unknown';
                    }

                    //получаем разделы
                    $this->autorize->materialtypes=Array();

                    $query='SELECT "type_id" FROM "user_rights" WHERE "goal_id" = '.$this->autorize->userid;
                    if($res=$this->dbquery($query))
                    {
                        if(count($res) > 0)
                        {
                            foreach($res as $row)
                            {
                                $this->autorize->materialtypes=array_merge($this->autorize->materialtypes,
                                    GetSubTypes($this, $row['type_id'], $this->autorize->materialtypes));
                            }
                        }
                    }

                    //записываем данные в php сессию
                    $_SESSION['autorize']['autorized']=$this->autorize->autorized;
                    $_SESSION['autorize']['username']=$this->autorize->username;
                    $_SESSION['autorize']['userlogin']=$this->autorize->userlogin;
                    $_SESSION['autorize']['userid']=$this->autorize->userid;
                    $_SESSION['autorize']['userrole']=$this->autorize->userrole;
                    $_SESSION['autorize']['email']=$this->autorize->email;
                    $_SESSION['autorize']['materialtypes']=$this->autorize->materialtypes;

                }
                else
                {
                    $this->autorize->answer='Ошибка при атворизации - не верное сочетание логина и пароля';
                }

            }
            else
            {
                $this->autorize->answer='Ошибка авторизации - пустой логин или пароль';
            }
        }
    }

    //определяем функцию регистрации
    public function DoRegister($email, $pass, $fio='', $nikname='')
    {
        $pass=md5($pass);
        $aid=false;
        if($email != '')
        {
            $email=strtolower($email);
            //проверяем данные на дублируемость
            $query="SELECT count(id) cid FROM rs_users WHERE email = '$email'";
            $ecnt=0;
            if($res=$this->dbquery($query))
            {
                $ecnt=$res[0]['cid'];
            }
            if($ecnt > 0)
            {
                $this->autorize->answer='Ошибка регистрации - такой email уже есть в системе';
            }
            else
            {
                //формируем случайные фразы для подтверждения мыла и телефона
                $email_approve=random_string(32, 'lower,upper,numbers');
                $emailsec_approve=random_string(32, 'lower,upper,numbers');
                $tel_approve=random_string(5, 'lower,numbers');
                $telsec_approve=random_string(5, 'lower,numbers');


                //если пользователь первый, делаем его главным админом
                $role_id = 0;
                $query="SELECT count(id) cnt FROM users";
                if($res = $this->dbquery($query))
                {
                    if($res['cnt'] == 0)
                    {
                        $role_id=1;
                    }
                }


                //записуЁм пользюка
                $query="INSERT INTO rs_users(id, role_id, pass, email, emailsec, tel, telsec, fio, nikname, isactive, email_approve, emailsec_approve, tel_approve, telsec_approve)
                    VALUES(NULL, $role_id, '$pass', '$email', '', '', '', '$fio', '$nikname', 1, '$email_approve', '$emailsec_approve', '$tel_approve', '$telsec_approve')";
                if($res=$this->dbquery($query))
                {
                    $query="SELECT count(id) aid FROM rs_users WHERE email = '$email'";
                    if($res=$this->dbquery($query))
                    {
                        $aid=$res[0]['aid'];




                    }
                    else
                    {
                        $this->autorize->answer='Ошибка получения идентификатора пользователя - что-то случилось при получении данных из в БД';
                    }
                }
                else
                {
                    $this->autorize->answer='Ошибка регистрации - что-то случилось при записи информации в БД - '.$query;
                }
            }
        }
        else
        {
            $this->autorize->answer='Ошибка регистрации - не передан основной email';
        }
        return($aid);
    }

    //инициализация переменной
    function GetPostValue($valuename, $mode = 0)
    {
        if ($valuename != '') {
            if (isset($_REQUEST[$valuename])) {
                if($mode == 0) {
                    if (is_array($_REQUEST[$valuename])) {
                        $this->values->$valuename = array();
                        foreach ($_REQUEST[$valuename] as $val) {
                            $this->values->$valuename = htmlspecialchars($val, ENT_QUOTES);
                        }
                    } else {
                        $this->values->$valuename = htmlspecialchars($_REQUEST[$valuename], ENT_QUOTES);
                    }
                } else {
                    $this->values->$valuename = $_REQUEST[$valuename];
                }
            } else {
                $this->values->$valuename = '';
            }
        }
    }

    /**
     * @param array|string $valuesnames
     * @param int $mode  0 - преобразование опасных символов из значения, 1 - без преобразования
     */
    public function GetPostValues($valuesnames, $mode = 0)
    {
        if (is_array($valuesnames)) {
            foreach($valuesnames as $valuename)
                $this->GetPostValue($valuename, $mode);
        } else {
            $this->GetPostValue($valuesnames, $mode);
        }
    }

    public function getPostValueAlter($name, $valMain, $valAlter = '')
    {
        $this->getPostValue($name);
        if ($this->values->$name == $valMain)
            return $valAlter;
        return $valMain;
    }

    public function toggleInput($param, $value)
    {
        $this->getPostValue($param);
        if ($this->values->$param == $value)
            return '';
        return $value;
    }

    public function setup($setup, $versions)
    {
        $this->settings = new stdClass();

        $this->settings->versions = new stdClass();
        $this->settings->versions->wpf = '0.94hk';

        if (is_array($versions) && count($versions) > 0) {
            foreach ($versions as $key=>$value) {
                $this->settings->versions->$key = $value;
            }
        }

        if (isset($setup['redis'])) {
            $this->settings->redis = (object)$setup['redis'];
            unset($setup['redis']);
        } else {
            $this->settings->redis = new stdClass();
        }

        if (isset($setup['DB'])) {
            $this->settings->DB = (object)$setup['DB'];
            unset($setup['DB']);
        } else {
            $this->settings->DB = new stdClass();
        }

        $this->settings->DB->oracletomysql = 1;
        if ($this->settings->DB->host == '172.16.201.183') {
            $this->settings->DB->host = '172.18.209.5';
        }

        if (isset($setup['email'])) {
            $this->settings->email = (object)$setup['email'];
            unset($setup['email']);
        } else {
            $this->settings->email = new stdClass();
        }

        if ( ! empty($setup['sms'])) {
            $this->settings->sms = (object)$setup['sms'];

            if (isset($setup['sms']['services']) && is_array($setup['sms']['services'])) {
                foreach ($setup['sms']['services'] as $service) {
                    $this->settings->sms->$service = (object)$service;
                }
            }

            unset($setup['sms']);
        } else {
            $this->settings->sms = new stdClass();
        }

        $this->settings->path = $setup['path'];
        $this->settings->pathglobal = $setup['pathglobal'];

        if (isset($setup['files'])) {
            $this->settings->files = (object)$setup['files'];

            if (isset($setup['files']['index_file_types'])) {
                $this->settings->files->index_file_types = $setup['files']['index_file_types'];
            } else {
                $this->settings->files->index_file_types = Array(
                    // MS office extensions
                    'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx',
                    // Open Office formats
                    'odt', 'odp', 'ods',
                    // portable document formats
                    'pdf',
                    // graphic formats
                    'bmp', 'jpg', 'jpeg', 'png',
                    // audio formats
                    'mp3',
                    // video formats
                    'mp4', 'flv',
                    // text formats
                    'txt', 'csv', 'rtf',
                    // electronic publication format
                    'epub'
                );
            }

            unset($setup['files']);
        } else {
            $this->settings->files = new stdClass();
        }

        if (isset($setup['data'])) {
            $this->data = (object)$setup['data'];
            unset($setup['data']);
        } else {
            $this->data = new stdClass();
        }

        if ( ! isset($this->data->title))
            $this->data->title = '';

        if ( ! isset($this->data->languagename)) {
            $this->data->languagename = isset($this->data->language) ? $this->data->language : null;
            $this->data->language = '';
        }

        if ( ! isset($this->data->language))
            $this->data->language = '';

        $this->data->titlesite = $this->data->title;

        foreach ($setup as $key => $val) {
            $this->settings->$key = $val;
        }
    }

    public function dbclose()
    {
        if ( ! is_null($this->pgsql)) {
            pg_close($this->pgsql);
            $this->pgsql = null;
        }
    }

    //соединение с БД==============================================================
    public function dbconnect()
    {
        if (function_exists('pg_pconnect')) {
            $this->pgsql = pg_connect("host=".$this->settings->DB->host.
                " port=".$this->settings->DB->port.
                " dbname=".$this->settings->DB->name.
                " user=".$this->settings->DB->login.
                " password=".$this->settings->DB->pass.
                " options='--client_encoding=UTF8'"
            );
            if ( ! is_resource($this->pgsql)) {
                $this->error->flag = '1';
                $this->error->text .= "\n<br>Нет возможности установить соединение с БД";

                die('Ведутся технические работы');

                return false;
            }
            return true;
        }

        $this->error->flag = '1';
        $this->error->text .= "\n<br>В системе не установлен модуль для работы PHP с PostgreSQL! Обратитесь к системному администратору";
    }

    public function getDb()
    {
        if (is_null($this->pgsql))
            $this->dbconnect();
        return $this->pgsql;
    }

    //выполнение запроса к БД======================================================
    public function dbquery($query, $params=Array(), $clobs=Array(), $blobs=Array())
    {
        $starttime = microtime(true);

        $fetchResult = false;

        if ($clobs === true) {
            $fetchResult = true;
            $clobs = array();
        }

        if($this->error->flag != '1') {
            // @todo выпилить
            if (empty($params))
                $query = str_replace('"','',$query);

            $query=str_replace('SYSDATE','NOW()',$query);

            $query=str_replace('unix_timestamp(','extract(epoch FROM ',$query);
            $query=str_replace('UNIX_TIMESTAMP(','extract(epoch FROM ',$query);

            $query=str_replace('FROM DUAL','',$query);
            $query=str_replace('(round((date_edit - to_date(\'01011970\',\'ddmmyyyy\'))*86400) - 4*60*60)','(extract(epoch FROM date_edit) - 10*60*60)',$query);
            $query=str_replace('TO_NUMBER','int4',$query);
            $query=str_replace('to_number','int4',$query);
            $query=str_replace('f.extension extension','f.extension "extension"',$query);
            $query=str_replace('t.name name','t.name "name"',$query);
            $query=str_replace('(SELECT round((f.date_event - to_date(\'01-01-1970\',\'DD-MM-YYYY\')) * (86400) - 10*60*60) as dt FROM dual)','(extract(epoch FROM f.date_event) - 10*60*60)',$query);
            $query=str_replace('TO_CHAR(f.id)','f.id',$query);
            $query=str_replace('(SELECT round((mt.validdate - to_date(\'01-01-1970\',\'DD-MM-YYYY\')) * (86400) - 10*60*60) as dt FROM dual)','(extract(epoch FROM mt.validdate) - 10*60*60)',$query);
            $query=str_replace('(SELECT round((m.date_edit - to_date(\'01-01-1970\',\'DD-MM-YYYY\')) * (86400) - 10*60*60) as dt FROM dual)','(extract(epoch FROM m.date_edit) - 10*60*60)',$query);
            $query=str_replace('(SELECT round((mn.date_event - to_date(\'01-01-1970\',\'DD-MM-YYYY\')) * (86400) - 10*60*60) as dt FROM dual)','(extract(epoch FROM mn.date_event) - 10*60*60)',$query);
            $query=str_replace('mt.validdate) valid','mt.validdate) "valid"',$query);
            $query=preg_replace('/\svalid\s/', ' "valid" ',$query);
            $query=str_replace('()','(NULL)',$query);
            $query=str_replace('NOW(NULL)','NOW()',$query);
            $query=str_replace('tb.photo) content','tb.photo) "content"',$query);
            $query=str_replace('parent_id = \'\'','parent_id = 0',$query);
            $query=str_replace('showinmenu = \'\'','showinmenu = 0',$query);
            $query=str_replace('TO_DATE','TO_TIMESTAMP',$query);

            $query=str_replace('m.id value ','m.id "value" ',$query);
            $query=str_replace('e.selector_id IS NULL OR e.selector_id = \'\'','e.selector_id IS NULL OR e.selector_id = 0',$query);
            $query=str_replace('selector_id IS NULL OR selector_id = \'\'','selector_id IS NULL OR selector_id = 0',$query);
            $query=str_replace('valuedate value','valuedate "value"',$query);
            $query=str_replace('valuename value','valuename "value"',$query);
            $query=str_replace('valuetext value','valuetext "value"',$query);

            if(count($blobs) > 0) {
                foreach($blobs as $blob)
                {
                    $blob=pg_escape_bytea($blob);
                    $params[]=$blob;
                }
            }

            if(count($clobs) > 0) {
                foreach($clobs as $clob) {
                    $params[] = $clob;
                }
            }

            $res = pg_query_params($this->pgsql, $query, $params);
            if ($res !== false) {
                if ($fetchResult || preg_match("/^select/i", trim($query))) {
                    $result = array();
                    while ($row = pg_fetch_assoc($res)) {
                        $result[] = $row;
                    }
                    return $result;
                }
                //прочие запросы
                return $res;
            }

            $this->warning->flag = 1;
            $this->warning->text .= "\n<br>Ошибка выполнения запроса: $query";

            $dump = print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), true)."\nDB: ".pg_last_error()."\nQuery:$query\nParams:".serialize($params);
            $this->dump($dump);
        }

        return false;
    }

    public function resetError()
    {
        $this->error->flag = 0;
        $this->error->text = '';
    }

    public function resetValues($preserve = array())
    {
        $rewrite = array();
        foreach ($preserve as $key)
            if (isset($this->values->$key))
                $rewrite[$key] = $this->values->$key;
        $this->values = new stdClass();
        foreach ($rewrite as $key => $val)
            $this->values->$key = $val;
    }

    public function dbqueryExpand($query, $params = array())
    {
        $outParts = array();
        $outParams = array();
        $queryParts = explode('$', $query);
        $outParts[] = array_shift($queryParts);

        foreach ($queryParts as $i => $part) {
            $pnumLen = strspn($part, '1234567890');
            $idx = $pnumLen ? intval(substr($part, 0, $pnumLen)) - 1 : $i;

            $part = substr($part, $pnumLen);

            if (!isset($params[$idx])) {
                throw new RuntimeException(sprintf('Parameter $%d undefined', $idx + 1));
            }

            $pval = $params[$idx];
            if (is_array($pval)) {
                $pholders = array();
                $pnum = count($outParams);
                foreach ($pval as $pvalScalar) {
                    $outParams[] = $pvalScalar;
                    $pholders[] = '$'.++$pnum;
                }
                $outParts[] = implode(',', $pholders).$part;
            } else {
                $outParams[] = $pval;
                $outParts[] = '$'.count($outParams).$part;
            }
        }

        return array(implode('', $outParts), $outParams);
    }

    public function setTemplate($templatename)
    {
        $this->settings->template = $templatename;
        $this->settings->templateurl = '/templates/' . $templatename;
    }

    function wpf_is_dir($str)
    {
        $res=false;

        if(!is_dir($this->settings->path.$str))
        {
            if(is_dir($this->settings->pathglobal.$str))
            {
                $res=true;
            }
        } else {
            $res=true;
        }

        return($res);
    }

    function findModule()
    {
        if ( ! isset($this->values->menu)) {
            $this->values->menu = '';
        }

        //проверянем базовые значения
        switch ($this->values->menu) {
            case '':
                $this->data->amodule = 'main';
                break;
            default:
                //пробуем найти модуль на сервере
                $moduledir = $this->settings->path.'/modules/'.$this->values->menu;
                $modulestr = '/modules/'.$this->values->menu;
                if ($this->wpf_is_dir($modulestr)) {
                    $this->data->amodule = $this->values->menu;
                } else {
                    $this->data->amodule = '404';
                }
                break;
        }
    }

    public function wpf_include($str)
    {
        $path = $this->searchFilepath($str, $this->searchPathList);
        if ($path !== false)
            return include_once($path);
    }

    //подключаем компонент
    public function includeComponent($cname)
    {
        $componentfile=$this->settings->path.'/components/'.$cname.'/'. $cname.'.php';
        $str='/components/'.$cname.'/'. $cname.'.php';

        if (!$this->wpf_file_exists($str)) {
            $this->warning->flag=1;
            $this->warning->text.='
                <br>Не существует подключаемого компонента '.$cname.' по адресу '.$componentfile;
        } else {
            $this->wpf_include($str);
        }
    }

    //проверка существования модуля
    function wpf_file_exists($str)
    {
        $res=false;

        if(!file_exists($this->settings->path.$str))
        {
            //echo $this->settings->path.$str;
            if(file_exists($this->settings->pathglobal.$str))
            {
                $res=true;
            }
        }
        else{
            $res=true;
            //echo $this->settings->path.$str;
        }

        return($res);

    }

    //подключаем модуль============================================================
    function includeModule()
    {
        if (empty($this->data->amodule)) {
            $this->data->amodule = 404;
        }

        $this->includeModuleController($this->data->amodule);

        $GLOBALS['templatetime']=microtime(true)-$GLOBALS["starttime"];

        //echo 'includeModule(2)'.$this->data->amodule; exit;

        $this->includeModuleTemplate($this->data->amodule);


    }

    protected function includeModuleController($module)
    {   
        $controllerPath = $this->locateModulePath('modules', $module);

        if ($controllerPath) {
            $hookPath = dirname($controllerPath).'/hooks.php';
            if (file_exists($hookPath))
                include_once($hookPath);
            
            include($controllerPath);           

        }
    }

    protected function locateModulePath($type, $name)
    {
        $path = $this->searchFilepath(
            $type . '/' . $name . '/' . $name . '.php', $this->searchPathList
        );
        return $path;
    }

    /**
     * @param string $relPath
     * @param array $searchPathList
     * @return bool|string
     */
    protected function searchFilepath($relPath, $searchPathList)
    {
        foreach ($searchPathList as $searchPath) {

            $path = $searchPath.'/'.$relPath;
            
            if (file_exists($path)) {
                return $path;
            }
        }

        return false;
    }

    protected function includeModuleTemplate($module)
    {

        $templateFilepath = $this->locateTemplate('t_'.$module);
        if ( ! $templateFilepath) {
            $this->error->flag = 1;
            $this->error->text .= "<br>Не существует шаблона подключаемого модуля $module";
            return;
        }

        $templatepath = dirname($templateFilepath);
        $this->partialDir = basename($templateFilepath, '.php');

        $this->settings->templatepath = $templatepath;
        $this->settings->templateurl = '/'.implode('/', array_slice(
                explode('/', $this->settings->templatepath), -2, 2
            ));

        ob_start();

        include($templateFilepath);

        $this->html = $this->filterOutputHtml(ob_get_clean());
    }

    public function locatePath($name)
    {
        return $this->searchFilepath($name, $this->searchPathList);
    }

    public function locateTemplate($name)
    {
        // @todo cache

        static $searchPaths;

        if (is_null($searchPaths)) {
            foreach ($this->searchPathList as $path) {
                foreach ($this->templateStack as $tpl) {
                    $searchPaths[] = $path . '/templates/' . $tpl;
                }
            }
        }

        $path = $this->searchFilepath($name . '.php', $searchPaths);

        return $path;
    }

    /**
     * @param string $name
     */
    public function setPartialDir($name)
    {
        $this->partialDir = $name;
    }

    /**
     * @param string $name
     * @return bool|string
     */
    public function partial($name)
    {
        if (substr($name, 0, 1) == '/')
            $path = 'partial'.$name;
        else
            $path = 'partial/'.$this->partialDir.'/'.$name;

        return $this->locateTemplate($path);
    }

    protected function filterOutputHtml($html)
    {
        if ($this->spamProtection && $this->spamProtection->isActive()) {
            $html = $this->spamProtection->protectForms($html);
        }
        return $html;
    }

    protected function sendLanguageCookie()
    {
        $langCookieLifetime = isset($this->settings->lang_cookie_lifetime) ?
            $this->settings->lang_cookie_lifetime :
            3600 * 24 * 30;
        $this->addCookie('language', $this->data->languagename, time() + $langCookieLifetime, '/');
    }

    function setLanguage()
    {

        if ( ! empty($this->values->language)) {
            $query = 'select id FROM languages WHERE name = $1';
            $res = $this->dbquery($query, array($this->values->language));
            if ( ! empty($res)) {
                $this->data->language = $res[0]['id'];
                $this->data->languagename = $this->values->language;
                $_SESSION['data']['language'] = $this->data->language;
                $this->sendLanguageCookie();
            }

            return;
        }

        if ( ! empty($_SESSION['data']['language'])) {
            $query = 'SELECT "id", "name" FROM "languages" WHERE "id" = $1';
            $res = $this->dbquery($query, array($_SESSION['data']['language']));
            if ( ! empty($res)) {
                $this->data->language = $res[0]['id'];
                $this->data->languagename = $res[0]['name'];

                return;
            }
        }

        if (empty($this->data->language)) {
            $this->data->language = LANG_RU;
            $this->data->languagename = 'ru';
        }

        $_SESSION['data']['language'] = $this->data->language;
        $this->sendLanguageCookie();
    }

    //инициализируем обработку сайта================================================
    public function makeData()
    {

        //подключаем нужный модуль
        $this->findModule();
        $this->includeModule();

        global $phperrors;
        global $phpfatalerrors;

        if ($phperrors != '') {
            $this->warning->text.='<br>
                '.$phperrors;

            $this->warning->flag=1;
        }

        if ($phpfatalerrors != '') {
            $this->error->text.='<br>
                '.$phpfatalerrors;
            $this->error->flag=1;
        }
    }

    public function addCookie($name, $value, $expiry = 0, $path = '', $domain = '', $secure = null,
                              $httpOnly = false)
    {
        ob_start();

        $secure=true;
        $httpOnly=true;

        if (is_null($secure))
            $secure = ! empty($this->settings->use_https);

        setcookie($name, $value, $expiry, $path, $domain, $secure, $httpOnly);
        $this->headers .= ( ! empty($this->headers) ? "\r\n" : '') . ob_get_clean();
    }

    public function removeCookie($name)
    {
        $this->addCookie($name, '', time() - 3600 * 24);
    }

    protected function startSession()
    {
        /*if ( ! $this->sessionActive) {
            if (session_status() != PHP_SESSION_ACTIVE) {
                $currentCookieParams = session_get_cookie_params();

                $domain = null;

                if (empty($this->settings->session_restrict_current_domain)) {
                    $domain = isset($this->settings->primary_domain) ?
                        $this->settings->primary_domain :
                        $currentCookieParams['domain'];
                    $domain = str_replace('www.', '', $domain);
                }

                $isSecure = ! empty($this->settings->use_https);

                session_set_cookie_params(
                    $currentCookieParams['lifetime'],
                    $currentCookieParams['path'],
                    $domain,
                    $isSecure,
                    true
                );

                session_start();
            }

            $this->sessionActive = true;

            $this->readFlashes();
        }*/

        //exit('Технические работы. Перезагрузите страницу');
        session_start();


    }

    protected function readFlashes()
    {
        $flashes = array();

        if (isset($_SESSION['flash'])) {
            $flashes = $_SESSION['flash'];
            $_SESSION['flash'] = array();
        }

        $this->flashes = $flashes;
    }

    public function echoHTML()
    {
        //если заголовков нет, вносим стандартные
        if (is_null($this->headers)) {
            ob_start();
            header('expires: mon, 26 jul 2000 05:00:00 GMT'); //Дата в прошлом
            header('cache-control: no-cache, no-store, must-revalidate'); // http/1.1
            header('pragma: no-cache'); // http/1.1
            header('last-modified: '.gmdate('d, d m y h:i:s').' GMT');
            //header('X-Frame-Options: SAMEORIGIN');
            header('X-XSS-Protection: 1; mode=block;');
            header('X-Content-Type-Options: nosniff');
            if (!empty($this->settings->use_https))
                header('Strict-Transport-Security: max-age=31536000; preload');

            $this->headers = ob_get_contents();
            ob_end_clean();
        }

        print $this->headers;
        print $this->html;
    }

    public function ShowError()
    {
        echo '<p style="font-size:18px; color:#ff0000;">'.$this->error->text.'</p>';
    }

    public function ShowWarning($mode=1)
    {
        if($this->warning->flag == 1)
        {
            if($mode == 0)
            {
                echo '<!--Warnings: '.$this->warning->text.' -->';
            }
            else
            {
                echo '<p style="font-size:16px; color:#CC6666;">'.$this->warning->text.'</p>';
            }
        }
    }

    //выводим все данные сайта
    public function ShowSiteInfo()
    {

        if($this->error->flag == 1)
        {
            $this->html='HTML код удален для просмотра метаданных объекта сайта';
        }

        echo '<pre>';
        print_r($this);
        echo '</pre>';
    }

    //создаем таблицы, если их нет
    public function MakeSQLTables()
    {
        //пробуем открыть файлы с SQL таблицами
        $sqldir=$this->settings->path.'/SQL/';
        if(is_dir($sqldir))
        {
            $fdir = array();
            $order=0;
            $mask = '.sql';
            if (false !== ($files = scandir($sqldir, $order)))
            {
                foreach ($files as $file_name)
                {
                    if ($file_name != '.' && $file_name != '..' && mb_strpos($file_name, $mask))
                    {
                        $sqlfilepath=$sqldir.$file_name;
                        //читаем всё из файла
                        $query = file_get_contents($sqlfilepath);
                        //выполняем запрос
                        if($this->dbquery($query))
                        {
                            //print("<br><pre>$query - выполнено</pre>");
                        }
                    }
                }
            }
        }
        else
        {
            $this->warning->flag=1;
            $this->warning->text.='
                <br>Нет директории с SQL скриптами '.$sqldir;
        }

    }

    public function __construct(&$setup, $versions, $ifinstall = 0)
    {

        $this->values = new stdClass();
        $this->error = new stdClass();

        $this->error->flag = '0';
        $this->error->text = '';

        $this->warning = new stdClass();

        $this->warning->flag = '0';
        $this->warning->text = '';

        //записываем переданные настройки в обьект

        $this->setup($setup, $versions);

        $this->registerHooks();

        $this->searchPathList[] = $_SERVER['DOCUMENT_ROOT'];
        if (WPF_ROOT != $_SERVER['DOCUMENT_ROOT'])
            $this->searchPathList[] = WPF_ROOT;

        $this->redisKeyPrefix = (isset($this->settings->redis_key_prefix) ?
                $this->redis_key_prefix :
                $this->settings->DB->name).':';


        $this->startSession();

        //поднимаем соединение с БД
        $this->dbconnect();

        $this->set_general_site_data($setup);

        //получаем базовые переменные
        
        /*
         //определяем главный и вторичный типы материалов (для отрисовки меню), а так-же переменные модреврайта
        // @todo выпилить fontSize


         */
        
        $basevalues = array(
            'menu',
            'imenu',
            'action',
            'id',
            'iid',
            'language',
            'media',
            'maintype',
            'sectype',
            'requeststr',
            'thirdtype',
            'thirdtypename',
            'fontSize'
        );

        $this->GetPostValues($basevalues);

        //настраиваем язык сайта
        $this->setLanguage();

        if($ifinstall == 1) {
            $this->MakeSQLTables();
        }

        $this->detectGeolocation();

        //производим авторизацию
        $this->includeComponent('autorize');

    }

    protected function detectGeolocation()
    {
        //получаем город и страну пользователя
        if (!isset($_SESSION['aregion']['locator']['city'])) {
            $_SESSION['aregion']['locator']['city'] = '';
            $_SESSION['aregion']['locator']['country'] = '';
        } else {
            $this->aregion = new stdClass();
            $this->aregion->locator = new stdClass();
            $this->aregion->locator->city = $_SESSION['aregion']['locator']['city'];
            $this->aregion->locator->country = $_SESSION['aregion']['locator']['country'];
        }
    }

    protected function registerHooks()
    {
        $this->includeHookScripts(WPF_ROOT.'/hooks');

        if ($this->settings->path != WPF_ROOT) {
            $this->includeHookScripts($this->settings->path.'/hooks');
        }
    }

    protected function includeHookScripts($dirPath)
    {
        if (is_dir($dirPath)) {
            $hooks = new \GlobIterator($dirPath.'/hook_*.php');
            foreach ($hooks as $hookScript)
                include_once($hookScript);
        }
    }

    // Получает файлик LESS, смотрит, если для него нет сгенерированной css-ки, то генерит css-файл и возвращает путь к ней
    public function includeLESS($lessfile, $styles_dir = null)
    {
        // Чистое имя переданного файла стилей, без расширения
        $filename = preg_replace("/\.less/", '', $lessfile);

        // Папка стилей темы
        if (is_null($styles_dir))
            $styles_dir = rtrim($this->settings->templatepath, '/') . '/css/';
        else
            $styles_dir = rtrim($this->settings->path, '/') . '/'. trim($styles_dir, '/') .'/';

        $css_path = $styles_dir . $filename . '.css'; // Полный путь CSS файлика с именем
        $less_path = $styles_dir . $filename . '.less';

        // Если разраб балбес и скормил нам кривое имя файла или сунул его не туда
        if (!file_exists($less_path)) {
            // Вернуть вселенскую пустоту
            return '';
        }

        // Если существует собранная css-ка
        if (file_exists($css_path)) {
            // Получить время её изменения
            $css_mtime = filemtime($css_path);
            // Получить время изменения LESS-файла
            $less_mtime = filemtime($less_path);

            // Если LESS новее чем сгенерированный CSS...
            if ($less_mtime > $css_mtime) {
                // Задаём полные права на всякий
                chmod($css_path, 0777);
                // Трём CSS-файл
                unlink($css_path);
            } else {
                // Если нет возвращаем путь до CSS-файла с датой последнего изменения
                $ret = substr($css_path, strlen($this->settings->path));
                if ($ret[0] != '/')
                    $ret = '/'.$ret;
                return $ret;
            }
        }

        $this->wpf_include('classes/less/lessc.inc.php');
        $less = new lessc;

        // Генерим CSS
        $less->checkedCompile($less_path, $css_path);

        $ret = substr($css_path, strlen($this->settings->path));
        if ($ret[0] != '/')
            $ret = '/'.$ret;

        return $ret;
    }

    public function isSpamPost()
    {
        return $this->getSpamProtection()->isSpamPost($this);
    }

    protected function getSpamProtection()
    {
        if (is_null($this->spamProtection)) {
            $this->wpf_include('classes/FormSpamProtection.php');
            $this->spamProtection = new FormSpamProtection();
        }
        return $this->spamProtection;
    }

    public function protectFromSpam()
    {
        $this->getSpamProtection()->activate();
    }

    public function getUser()
    {
        if (is_null($this->userInfo))
            $this->userInfo = new \wpf\auth\User($this->getAuthManager(), $this->getAuthInfoProxy());
        return $this->userInfo;
    }

    public function requireAuthUser()
    {
        if ($this->getUser()->isGuest()) {
            $retUrl = $_SERVER['REQUEST_URI'];
            $loginUrl = '?menu=admin&return_url='.urlencode($retUrl);
            $this->redirect($loginUrl);
        }

        return $this->getUser();
    }

    public function afterUserLogin()
    {
        if (isset($_REQUEST['return_url'])) {
            $this->redirect($_REQUEST['return_url'], 302);
        }
    }

    public function __get($prop)
    {
        if ($prop ==  'autorize')
            return $this->getAuthInfoProxy();
        if ($prop == 'user')
            return $this->getUser();
    }

    protected function getAuthInfoProxy()
    {
        if (is_null($this->authInfoProxy))
            $this->authInfoProxy = new \wpf\auth\InfoProxy();
        return $this->authInfoProxy;
    }

    protected function getAuthManager()
    {
        if (is_null($this->authManager)) {
            $this->authManager = new \wpf\auth\Manager(
                $this->loadAuthRules(),
                $this->getLogger()
            );
        }

        return $this->authManager;
    }

    protected function loadAuthRules()
    {
        $rules = array();

        $rulesFilesList = array(
            WPF_ROOT.'/components/autorize/data/rules-any-top.php',
            $_SERVER['DOCUMENT_ROOT'].'/components/autorize/data/rules.php',
            $_SERVER['DOCUMENT_ROOT'].'/components/autorize/data/rules-local.php',
        );

        if (defined('WPF_OIV') && constant('WPF_OIV'))
            $rulesFilesList[] = WPF_ROOT.'/components/autorize/data/rules-oiv.php';
        if (defined('WPF_OMSU') && constant('WPF_OMSU'))
            $rulesFilesList[] = WPF_ROOT.'/components/autorize/data/rules-omsu.php';

        $rulesFilesList[] = WPF_ROOT.'/components/autorize/data/rules-any.php';

        foreach ($rulesFilesList as $iFile => $rulesFile) {
            if (file_exists($rulesFile)) {
                $appendRules = include($rulesFile);
                foreach ($appendRules as $iRule => $rule) {
                    $appendRules[$iRule]['id'] = basename($rulesFile).':'.($iRule + 1);
                }
                $rules = array_merge($rules, $appendRules);
            }
        }

        return $rules;
    }

    public function __set($prop, $val)
    {
        if ($prop == 'autorize')
            $this->authInfoProxy = new \wpf\auth\InfoProxy($val);
    }

    /**
     * @return \wpf\cache\CacheInterface
     */
    public function getCache()
    {
        if (is_null($this->cache)) {
            $this->cache = new wpf\cache\Redis(
                array(),
                $this->getCacheKeyPrefix()
            );
        }
        return $this->cache;
    }

    protected function getCacheKeyPrefix()
    {
        return $this->getRedisKeyPrefix();
    }

    public function getRedisKeyPrefix()
    {
        return $this->redisKeyPrefix;
    }

    public function getGeoService()
    {
        if (is_null($this->geoloc))
            $this->geoloc = new wpf\geo\LocationService($this->getCache(), $this->getLogger());
        return $this->geoloc;
    }

    public function addTemplate($name)
    {
        if (!in_array($name, $this->templateStack))
            $this->templateStack[] = $name;
    }

    public function validateCsrfToken($paramName = 'csrf', $cookieName = '_csrf')
    {
        $this->getPostValue($paramName);

        if (empty($_COOKIE[$cookieName]))
            return false;
        if ($_COOKIE[$cookieName] != $this->values->$paramName)
            return false;

        return true;
    }

    public function generateCsrfToken($cookieName = '_csrf')
    {
        if ( ! isset($_COOKIE[$cookieName])) {
            $token_secret = isset($this->settings->csrf_token_secret) ?
                $this->settings->csrf_token_secret :
                base64_decode('6f9r33cl9r29iOnZ0UWvWCb+');
            $token = md5(
                pack('N', intval(time() / 3600)).
                substr(inet_pton($_SERVER['REMOTE_ADDR']), 0, 4).
                $token_secret
            );
            $this->addCookie(
                $cookieName,
                $token,
                0,
                '/',
                '',
                null,
                true
            );
        } else {
            $token = $_COOKIE[$cookieName];
        }
        return $token;
    }

    /**
     * @return \wpf\utils\Logger
     */
    public function getLogger()
    {
        if (is_null($this->logger)) {
            $loggerCfg = isset($this->settings->log) ?
                $this->settings->log :
                array('null');

            switch ($loggerCfg[0]) {
                case 'null':
                default:
                    $this->logger = new \wpf\utils\NullLogger();
                    break;
                case 'file':
                    $exclude = isset($loggerCfg['exclude']) ? $loggerCfg['exclude'] : array();
                    $this->logger = new \wpf\utils\Logger(
                        isset($loggerCfg['path']) ? $loggerCfg['path'] : $this->getDefaultLogPath(),
                        isset($loggerCfg['level']) ? $loggerCfg['level'] : LOG_DEBUG,
                        array($this->getSiteId()),
                        $exclude
                    );
                    break;
            }
        }

        return $this->logger;
    }

    public function logWrite($level, $sender, $msg)
    {
        return $this->getLogger()->write($level, $sender, $msg);
    }

    protected function getDefaultLogPath()
    {
        return WPF_ROOT.'/../log/app.log';
    }

    public function getSiteId()
    {
        return $this->settings->DB->name;
    }

    public function setPageMeta($name, $value)
    {
        $this->meta[$name] = $value;
    }

    public function getPageMeta($name)
    {
        return isset($this->meta[$name]) ? $this->meta[$name] : null;
    }

    public function getPageMetaAll()
    {
        return $this->meta;
    }

    /**
     * @param array|string $url  URL в форме строки или маршрут в виде массива array(0 => module_name, query_params...)
     * @param int $statusCode
     */
    public function redirect($url, $statusCode = 301)
    {
        if (is_array($url))
            $url = $this->getRouteUrl($url);

        $this->setResponseHeader('Location', $url);
        $this->endRequest($statusCode);
    }

    /**
     * @param array $route  [0] - модуль, остальные значения не-цифровых ключей добавляются к параметрам URL
     * @return string
     */
    public function getRouteUrl($route)
    {
        $module = isset($route[0]) ? $route[0] : 'materials';
        $path = '/';
        $query = array();
        if ($module != 'materials')
            $query[] = 'menu='.$module;
        foreach ($route as $key => $val) {
            if ( ! is_numeric($key)) {
                $query[] = $key.'='.urlencode($val);
            }
        }

        $query = implode('&', $query);
        return $path . ( ! empty($query) ? '?' . $query : '');
    }

    /**
     * @param string $url
     * @return string
     */
    public function getAbsoluteUrl($url)
    {
        if ( ! preg_match('/https?:\/\//i', $url)) {
            $url = 'http' . ( ! empty($this->settings->use_https) ? 's' : '').'://'.
                (isset($this->settings->primary_domain) ?
                    $this->settings->primary_domain :
                    str_replace('www.', '', $_SERVER['HTTP_HOST'])
                ).$url;
        }

        return $url;
    }

    public function endRequest($statusCode = null)
    {
        $this->sendResponse(null, $statusCode);
        exit;
    }

    public function forbidden()
    {
        $this->endRequest(403);
    }

    public function badRequest()
    {
        $this->endRequest(400);
    }

    public function notFound()
    {
        $this->endRequest(404);
    }

    public function internalServerError()
    {
        $this->sendResponse($this->getUser()->isAdmin() ? $this->error->text : '', 500);
        exit;
    }

    public function sendResponse($body = null, $statusCode = null)
    {
        if ($this->responseSent)
            return;

        $this->responseSent = true;

        if ( ! is_null($statusCode))
            http_response_code($statusCode);

        $this->sendResponseHeaders();

        if ( ! is_null($body)) {
            if (is_string($body)) {
                echo $body;
            } elseif (is_resource($body)) {
                stream_copy_to_stream($body, fopen('php://output', 'wb'));
            }
        }
    }

    public function sendJsonResponse($data)
    {
        $this->setResponseHeader('Content-type', 'application/json');
        $this->sendResponse(json_encode($data), 200);
    }

    public function sendResponseHeaders()
    {
        ob_start();

        foreach ($this->headersList as $name => $content)
            header("$name: $content");

        $headers = ob_get_clean();
        $this->headersList = array();

        if ( ! empty($this->headers)) {
            $headers = $this->headers."\r\n".$headers;
            $this->headers = null;
        }
        echo $headers;
    }

    public function setResponseHeader($name, $content)
    {
        $this->headersList[strtolower($name)] = $content;
    }

    public function resolveCacheKey($name)
    {
        return $this->getCacheKeyPrefix().$name;
    }

    public function isRemoteAvailable($remoteAddr, $skipCache = false)
    {
        $redis = new Yampee_Redis_Client();
        $key = '*:remote:'.str_replace(':', '%3A', $remoteAddr);

        if ( ! $skipCache && $redis->has($key)) {
            return $redis->get($key);
        }

        $retval = $this->doCheckRemoteAvailable($remoteAddr);
        $expire = isset($this->settings->cache) && ! empty($this->settings->cache['check_gosmonitor_lifetime']) ?
            $this->settings->cache['check_gosmonitor_lifetime'] :
            900;
        $redis->set($key, $retval, $expire);

        return $retval;
    }

    protected function doCheckRemoteAvailable($remoteAddr)
    {
        $status = false;
        if (preg_match('/^https?:\/\//i', $remoteAddr)) {
            $ch = curl_init($remoteAddr);
            curl_setopt_array($ch, array(
                CURLOPT_TIMEOUT => 15,
            ));

            if (curl_exec($ch) !== false) {
                $status = curl_getinfo($ch, CURLINFO_CONNECT_TIME);
            }
            curl_close($ch);
            $status = $status !== false;
        }

        return $status;
    }

    public function getState($name, $fallback = null)
    {
        $redis = $this->getRedis();

        $key = 'state.'.$name;
        $fullKey = $this->resolveCacheKey($key);
        if ($redis->has($fullKey))
            return $redis->get($fullKey);

        $fullKey = '*:'.$key;
        if ($redis->has($fullKey))
            return $redis->get($fullKey);

        return $fallback;
    }

    public function setState($name, $value)
    {
        $key = 'state.'.$name;
        $fullKey = $this->resolveCacheKey($key);
        $this->getRedis()->set($fullKey, $value);
    }

    public function setGlobalState($name, $value)
    {
        $key = 'state.'.$name;
        $fullKey = '*:'.$key;
        $this->getRedis()->set($fullKey, $value);
    }

    public function resetGlobalState($name)
    {
        $key = 'state.'.$name;
        $fullKey = '*:'.$key;
        $this->getRedis()->del($fullKey);
    }

    public function resetState($name)
    {
        $key = 'state.'.$name;
        $fullKey = $this->resolveCacheKey($key);
        $this->getRedis()->del($fullKey);
    }

    /**
     * @return Yampee_Redis_Client
     */
    public function getRedis()
    {
        if (is_null($this->redis)) {
            $this->redis = new \Yampee_Redis_Client();
        }
        return $this->redis;
    }

    public function resetActiveMaterialTypes()
    {
        $this->logWrite(LOG_INFO, __FUNCTION__, __LINE__);

        $site = $this;

        $this->batchable(function () use ($site) {
            $site->logWrite(LOG_INFO, __METHOD__, __LINE__);
            MaterialTypesResetActive($site);
        });
    }

    public function setFlash($name, $value)
    {
        $_SESSION['flash'][$name] = $value;
    }

    public function getFlash($name, $fallback = null)
    {
        return isset($this->flashes[$name]) ? $this->flashes[$name] : $fallback;
    }

    public function hasFlash($name)
    {
        return isset($this->flashes[$name]);
    }

    public function preserveFlash()
    {
        foreach ($this->flashes as $key => $val)
            if ( ! isset($_SESSION[$key]))
                $_SESSION[$key] = $val;

        return $this;
    }

    /**
     * @return \wpf\mail\SenderInterface
     */
    public function getMailer()
    {
        if ( ! isset($this->mailer)) {
            $senderClass = isset($this->settings->email->sender) && $this->settings->email->sender == 'stub' ?
                'wpf\mail\StubSender' :
                'wpf\mail\Sender';
            if ( ! class_exists($senderClass)) {
                require_once(WPF_ROOT.'/classes/'.str_replace('\\', '/', $senderClass).'.php');
            }
            $this->mailer = new $senderClass($this->settings);
        }

        return $this->mailer;
    }

    public function preventCache()
    {
        $this->cachePrevented = true;
    }

   
    public function isCachePrevented()
    {
        return $this->cachePrevented;
    }

    public function dump($data)
    {
        $dumpFilename = date('Ymd_Hi');
        $dumpFilepath = WPF_ROOT.'/drop/dump/'.$dumpFilename;

        $boundary = "---".microtime(true);
        $output = "$boundary\n".$data.$boundary."---\n";

        file_put_contents($dumpFilepath, $output, FILE_APPEND);

        return $dumpFilename;
    }

    public function setTemplateOption($name, $value)
    {
        $this->templateOptions[$name] = $value;
    }

    public function getTemplateOption($name, $fallback = null)
    {
        return isset($this->templateOptions[$name]) ?
            $this->templateOptions[$name] :
            $fallback;
    }

    protected function set_general_site_data(&$setup){
        // считываем заранее некоторые общие настройки из базы, потому что выполняется проверка их существования
        // если проверка не пройдется, то будут выставлены настройки из файла settings.php
        $title       = settings_get($this, 'site.title');
        $keywords    = settings_get($this, 'site.keywords');
        $description = settings_get($this, 'site.description');

        $this->data->title              = $title ? $title : $this->data->title;
        $setup['data']['title']         = $this->data->title;
        $this->data->keywords           = $keywords ? $keywords : $this->data->keywords;
        $this->data->description        = $description ? $description : $this->data->description;
        $this->data->yaMetrikaIdCounter = settings_get($this, 'yandex_metrika_id_counter');
        $this->data->feedbackEmail      = settings_get($this, 'email.feedback');
        $this->data->siteColorTheme     = settings_get($this, 'site.colorTheme');
    }
}
