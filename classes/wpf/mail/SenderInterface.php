<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\mail;

interface SenderInterface {
    function send($to, $subject, $body, $attachments = array());
}