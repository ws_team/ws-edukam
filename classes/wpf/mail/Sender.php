<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\mail;

require_once('SenderInterface.php');
require_once(WPF_ROOT.'/classes/wpf/iEmail.php');

class Sender implements SenderInterface {
    private $email;

    public function __construct($settings)
    {
        $this->email = new \iEmail($settings);
    }

    public function send($to, $subject, $body, $attachments = array())
    {
        $this->email->send($to, $subject, $body, $attachments);
    }
}
