<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\geo;

class LocationService {
    private $cache;
    private $logger;

    /**
     * @param wpf\cache\CacheInterface $cache
     * @param wpf\utils\LoggerInterface
     */
    public function __construct($cache, $logger)
    {
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @param string $address
     * @param string $keyPrefix
     * @return Location
     */
    function findAddress($address, $keyPrefix = '')
    {
        $addrhash = md5($address);
        $this->logWrite(LOG_DEBUG, __METHOD__, "addr='$address'($addrhash);keyprefix='$keyPrefix'");

        $geoaddr = str_replace(' ', '+', $address);
        $key = $keyPrefix.$geoaddr;

        if ($this->cache->has($key)) {
            $geoaddr = $this->cache->get($geoaddr);
        } else {
            $geoaddr = $this->fetchGeocodeData($geoaddr);
            $this->cache->set($key, $geoaddr);
        }

        $this->logWrite(LOG_DEBUG, __METHOD__, ".. res=".$geoaddr);

        $geoaddr = json_decode($geoaddr);
        $data = $geoaddr->response->GeoObjectCollection->featureMember;
        if (count($data))
            $geoOb = $data[0]->GeoObject;
        else
            $geoOb = array();
        return new Location($geoOb);
    }

    protected function fetchGeocodeData($geocode)
    {
        return file_get_contents('https://geocode-maps.yandex.ru/1.x/?geocode='.$geocode.'&kind=house&format=json&results=1');
    }

    protected function logWrite()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->logger, 'write'), $args);
    }
}

class Location {
    private $data;

    /**
     * @param object $geoObject
     */
    public function __construct($geoObject)
    {
        $this->data = $geoObject;
    }

    /**
     * @return Coordinates
     */
    function getCoords()
    {
        if (empty($this->data))
            $coords = null;
        else
            $coords = $this->data->Point->pos;
        return new Coordinates($coords);
    }
}

class Coordinates {
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function __toString()
    {
        if (empty($this->data))
            return '';

        $coords = $this->asArray();
        return $coords[0].', '.$coords[1];
    }

    public function asArray()
    {
        if (empty($this->data))
            return array();

        list($c2, $c1) = explode(' ', $this->data);
        return array($c1, $c2);
    }
}