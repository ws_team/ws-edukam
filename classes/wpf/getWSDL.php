<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//класс - получение данных по WSDL (пока по умолчанию в UTF-8)
class getWSDL
{
    public $err;
    public $client;
    public $result;

    public function __construct($wsdlurl, $method, $params, $proxyhost='', $proxyport='', $proxyusername='', $proxypassword='')
    {
        if($wsdlurl != '')
        {
            $this->client = new nusoap_client($wsdlurl, 'wsdl', $proxyhost, $proxyport, $proxyusername, $proxypassword);
            $this->client->setUseCurl(1);
            $this->client->soap_defencoding = 'UTF-8';
            $this->client->decode_utf8 = false;
            //$this->utf8string = array('stuff' => "\xc2\xa9\xc2\xae\xc2\xbc\xc2\xbd\xc2\xbe");

            $this->err = $this->client->getError();
        }
        else
        {
            $this->err=true;
        }

        if(!$this->err)
        {

            if($this->result = $this->client->call($method, array('parameters' => $params)))
            {

            }
            else
            {
                $this->client->call($method, array('parameters' => $params));

                $err = $this->client->getError();
                $fault = $this->client->fault;
                print($err.$fault.'WSDL ERROR:');

                print($this->client->request);
            }

        }

    }
}