<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


require_once('Redis.php');
require_once(dirname(__FILE__).'/../../YampeeRedis/YampeeRedis.php');

class RedisTest extends \PHPUnit_Framework_TestCase {
    function testHash()
    {
        $redis = new \wpf\cache\Redis(array(), '.test');
        $redis->hset('hash', 'key', 'val');
        $redis->hset('hash', 'key2', 'val2');
        $redis->hset('hash', 'key3', 'val3', 1);

        $this->assertEquals('val', $redis->hget('hash', 'key'));
        $this->assertEquals('val2', $redis->hget('hash', 'key2'));
        $this->assertEquals('val3', $redis->hget('hash', 'key3'));
        $this->assertEquals(array(
                'key' => 'val',
                'key2' => 'val2',
                'key3' => 'val3'
            ),
            $redis->hget('hash')
        );

        sleep(1);

        $this->assertNull($redis->hget('hash', 'key3'));
        $this->assertEquals(array(
                'key' => 'val',
                'key2' => 'val2'
            ),
            $redis->hget('hash')
        );

        $redis->hdel('hash', 'key2');
        $this->assertNull($redis->hget('hash', 'key2'));
        $this->assertEquals('val', $redis->hget('hash', 'key'));
        $redis->hdel('hash');
        $this->assertNull($redis->hget('hash', 'key'));
        $this->assertEmpty($redis->hget('hash'));
    }
}