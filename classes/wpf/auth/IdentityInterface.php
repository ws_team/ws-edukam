<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

interface IdentityInterface {
    /**
     * @return mixed
     */
    function getId();

    /**
     * @param string $roleName
     * @return bool
     */
    function hasRole($roleName);

    /**
     * @param string $name
     * @return ?string
     */
    function getAttribute($name);
}