<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

require_once('IdentityInterface.php');

class NullIdentity implements IdentityInterface {
    public function getId()
    {
        return null;
    }

    public function hasRole($name)
    {
        return $name == 'guest';
    }

    public function getAttribute($name)
    {
        return null;
    }
}