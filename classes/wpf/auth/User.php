<?php
/**
 * Инкапсулирует доступ к данным авторизованного пользователя.
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

require_once('IdentityInterface.php');
require_once('NullIdentity.php');
require_once('SimpleIdentity.php');

class User implements IdentityInterface {
    /**
     * @var \wpf\auth\IdentityInterface
     */
    private $identity;

    private $authInfo;

    /**
     * @var ManagerInterface
     */
    private $authManager;
    private $sessionKey = 'autorize';
    private $stateless;

    public function __construct(ManagerInterface $authManager, $authInfo, $stateless = false)
    {
        if ($authInfo) {
            $this->identity = self::createIdentityFromAuthInfo($authInfo);
            $this->authInfo = $authInfo;
        } else {
            $this->identity = new NullIdentity();
        }
        $this->authManager = $authManager;
        $this->stateless = $stateless;
        $this->flushCheckCache();
    }

    public function getAuthInfo()// @todo drop
    {
        return $this->authInfo;
    }

    public function isGuest()
    {
        return is_null($this->identity->getId());
    }

    public function isAdmin()
    {
        return $this->identity->hasRole('admin');
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return self::getAllRoles($this->identity);
    }

    public function isContentManager()
    {
        return $this->identity->hasRole('cmanager');
    }

    public function can($permission)
    {
        $result = $this->getCachedCheck($permission);
        if (is_null($result)) {
            $result = $this->authManager->checkPermission($permission, $this);
            $this->cacheCheckResult($permission, $result);
        }
        return $result;
    }

    protected function getCachedCheck($permission)
    {
        if (isset(
            $_SESSION[$this->sessionKey],
            $_SESSION[$this->sessionKey]['checks'],
            $_SESSION[$this->sessionKey]['checks'][$permission])) {
            return $_SESSION[$this->sessionKey]['checks'][$permission];
        }
    }

    protected function cacheCheckResult($permission, $result)
    {
        if ( ! isset($_SESSION[$this->sessionKey]))
            $_SESSION[$this->sessionKey] = array();
        if ( ! isset($_SESSION[$this->sessionKey]['checks']))
            $_SESSION[$this->sessionKey]['checks'] = array();
        $_SESSION[$this->sessionKey]['checks'][$permission] = $result;
    }

    public function flushCheckCache()
    {
        unset($_SESSION[$this->sessionKey]['checks']);
    }

    /**
     * @param int $type_id
     * @param ?int $id  
     * @param string $action 'create'|'update'|'delete'|'changestatus'
     * @return bool
     */
    public function canEditMaterial($type_id, $id = null, $action = '')
    {
        $permission = array('editmaterials'.($action ? '.'.$action : ''), $type_id);
        if ( ! empty($id))
            $permission[] = $id;
        $permission = implode(':', $permission);

        return $this->can($permission);
    }

    public function logout()
    {
        if (is_null($this->identity->getId()))
            return;

        if ( ! $this->stateless) {
            if ($this->authInfo)
                \wpf\components\autorize\authinfo_reset($this->authInfo);
            unset($_SESSION['autorize']);
        }

        $this->identity = new NullIdentity();
    }

    /**
     * @return ?int
     */
    public function getId()
    {
        return $this->identity->getId();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->identity->getAttribute('login_name');
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getAttribute($name)
    {
        return $this->identity->getAttribute($name);
    }

    public static function createIdentityFromAuthInfo($authInfo)
    {
        if (empty($authInfo->autorized))
            return new NullIdentity();

        $id = $authInfo->userid;
        $roles = array();
        $attributes = array();

        if ( ! empty($authInfo->userrole)) {
            if ($authInfo->userrole == ROLE_ADMIN)
                $roles[] = 'admin';
            if ($authInfo->userrole == ROLE_CMANAGER)
                $roles[] = 'cmanager';
            if ($authInfo->userrole)
                $roles[] = 'registered';
        } else {
            $roles[] = 'guest';
        }

        foreach (array('omsu', 'reg', 'bound_materials', 'materialtypes') as $attr)
            if (isset($authInfo->$attr))
                $attributes[$attr] = $authInfo->$attr;

        foreach (array('email', 'userlogin') as $attr) {
            if ( ! empty($authInfo->$attr)) {
                $attributes['login_name'] = $authInfo->$attr;
                break;
            }
        }

        if ( ! empty($authInfo->username))
            $attributes['display_name'] = $authInfo->username;

        return new SimpleIdentity($id, $roles, $attributes);
    }

    protected static function getLevel(\wpf\auth\IdentityInterface $identity)
    {
        if ($identity->hasRole('admin'))
            return ROLE_ADMIN;
        if ($identity->hasRole('cmanager'))
            return ROLE_CMANAGER;

        return null;
    }

    public function hasRole($roleName)
    {
        return $this->identity->hasRole($roleName);
    }

    public function login(\wpf\auth\IdentityInterface $userId)
    {
        $this->identity = $userId;

        if ( ! $this->stateless)
            $this->updateAuthInfo($userId);

        $this->flushCheckCache();
    }

    protected function updateAuthInfo(\wpf\auth\IdentityInterface $identity)
    {
        if ($this->authInfo) {
            $this->authInfo->autorized = ! is_null($identity->getId());
            $this->authInfo->userid = $identity->getId();
            $this->authInfo->username = $identity->getAttribute('display_name');
            $this->authInfo->userlogin = $identity->getAttribute('login_name');
            $this->authInfo->email = $identity->getAttribute('login_name');
            $this->authInfo->userrole = self::getLevel($identity);
            $this->authInfo->omsu = $identity->getAttribute('omsu');
            $this->authInfo->reg = $identity->getAttribute('reg');
            $this->authInfo->answer = null;
            $this->authInfo->bound_materials = $identity->getAttribute('bound_materials');

            \wpf\components\autorize\authinfo_update($this->authInfo);
        }
    }

}
