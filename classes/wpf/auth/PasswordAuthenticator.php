<?php
/**
 * Идентификация/проверка подлинности по паре логин/пароль.
 * 
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

require_once('AuthenticatorInterface.php');
require_once('User.php');
require_once('SimpleIdentity.php');

class PasswordAuthenticator implements AuthenticatorInterface {
    public function authenticate(\iSite $site)
    {
        $site->wpf_include('components/autorize/functions.php');

        if (isset($_REQUEST['username'], $_REQUEST['password'])) {
            $username = $_REQUEST['username'];
            $password = $_REQUEST['password'];

            $userRecord = \wpf\components\autorize\find_user($site, $username, $password);

            if ($userRecord) {
                $id = isset($userRecord['id']) ? $userRecord['id'] : -1;

                if (isset($userRecord['roles'])) {
                    $roles = $userRecord['roles'];
                } else {
                    $roles = array();
                    if ($userRecord['role_id'] == ROLE_ADMIN)
                        $roles[] = 'admin';
                    if ($userRecord['role_id'] == ROLE_CMANAGER)
                        $roles[] = 'cmanager';
                }

                $attrs = \wpf\components\autorize\load_user_attributes($site, $id);

                $attrs['login_name'] = isset($userRecord['username']) ?  $userRecord['username'] :
                    isset($userRecord['email']) ? $userRecord['email'] : $username;
                $attrs['display_name'] = isset($userRecord['fio']) ? $userRecord['fio'] :
                    isset($userRecord['username']) ? $userRecord['username'] : $username;

                $attrs['omsu'] = isset($userRecord['omsu']) ? $userRecord['omsu'] : null;
                // @todo attribute "reg"

                $attrs['materialtypes'] = \wpf\components\autorize\get_allowed_materialtypes($site, $id);

                return new \wpf\auth\SimpleIdentity($id, $roles, $attrs);
            }
        }

        return false;
    }
}