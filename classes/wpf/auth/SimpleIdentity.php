<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

require_once('IdentityInterface.php');

class SimpleIdentity implements IdentityInterface {
    private $id;
    private $roles;
    private $attributes;

    public function __construct($id, $roles, $attributes)
    {
        $this->id = $id;
        $this->roles = $roles;
        $this->attributes = $attributes;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function hasRole($name)
    {
        return in_array($name, $this->roles);
    }

    public function getAttribute($name)
    {
        return isset($this->attributes[$name]) ?
            $this->attributes[$name] :
            null;
    }
}