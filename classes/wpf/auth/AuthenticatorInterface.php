<?php
/**
 * Интерфейс класса идентификации и проверки подлинности субъекта запроса.
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\auth;

interface AuthenticatorInterface {
    /**
     * В случае успешной проверки возвращает IdentityInterface, при неуспехе false.
     *
     * @return \wpf\auth\IdentityInterface|bool
     */
    function authenticate(\iSite $site);
}