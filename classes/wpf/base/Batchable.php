<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\base;

trait Batchable {
    private $level = 0;
    private $batched = array();

    public function beginBatch()
    {
        $this->level++;
        $this->batched[] = array();
    }

    public function endBatch()
    {
        if ( ! $this->level)
            return;

        if ( ! empty($this->batched[$this->level])) {
            foreach ($this->batched[$this->level] as $cb)
                call_user_func($cb);
        }
        array_pop($this->batched);
        $this->level--;
    }

    /**
     * @param callable $cb
     */
    public function batchable($cb)
    {
        if ( ! $this->level) {
            call_user_func($cb);
        } else {
            if ( ! in_array($cb, $this->batched[$this->level - 1]))
                $this->batched[$this->level - 1][] = $cb;
        }
    }
}
