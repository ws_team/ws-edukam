#!/bin/bash
service=redis

if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
then
echo "$service is running!!!"
else
    service redis start
    service httpd restart
fi