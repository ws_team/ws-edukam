<?php


if (strpos($_SERVER['SERVER_NAME'], 'test.') !== false
    || strpos($_SERVER['SERVER_NAME'], '-test') !== false) {
    define('WPF_DEBUG', true);
    define('WPF_ENV', 'test'); 
    define('WPF_LOCK_ACCESS', true);
}

if (!defined('DB_TIME_OFFSET'))
    define('DB_TIME_OFFSET', 0);// зона БД та же, что и в приложении

if (!defined('SUBSCRIPTION_TRUSTED_IPADDRS'))
    define('SUBSCRIPTION_TRUSTED_IPADDRS',
    '127.0.0.1,172.20.252.1,172.20.252.2,172.20.252.3,172.18.209.3,172.18.209.4,172.18.209.51,172.19.101.131,172.19.101.132,172.19.101.133');

if ( ! defined('MATTYPE_EVENT_IMPORTANT'))
    define('MATTYPE_EVENT_IMPORTANT', 0);

if ( ! defined('MATTYPE_DOCUMENTS'))
    define('MATTYPE_DOCUMENTS', 0);

