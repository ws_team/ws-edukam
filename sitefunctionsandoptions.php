<?php

//вывод сообщения о технических работах
function UnderConstructionBlock($site)
{
    if(GetUnderConstructionFlag($site))
    {
        $out='Сайт находится на техническом обслуживании!';
        return $out;
    }
    else
    {
        return '';
    }
}

//установка флага тех. работ на сайте
function ToggleUnderConstructionFlag(\iSite $site)
{
    $stateName = 'maintenance';
    $state = $site->getState($stateName);
    if ( ! empty($state)) {
        $site->resetState($stateName);
    } else {
        $site->setState($stateName, 1);
    }
}

//получение флага тех. работ
function GetUnderConstructionFlag($site)
{
    return (bool)$site->getState('maintenance');
}

//формирование ссылок Хабкрая
function MakeKhabkraiLinks($site)
{
    $redis = new Yampee_Redis_Client($site->settings->redis->host);
    $key = 'linksarr';

    $site->values->search='';
    $rawLinks = MaterialList($site, 55, 1, 100, STATUS_ACTIVE);
    $links = array();

    if (!empty($rawLinks)) {
        foreach($rawLinks as $link) {
            $query = 'SELECT * FROM "extra_materials" WHERE "material_id" = '.$link['id'].' AND "type_id" = 7';
            if (($res = $site->dbquery($query)) !== false && !empty($res)) {
                $linkurl = $res[0]['valuename'];
                $link['linkurl'] = $linkurl;
                $links[] = $link;
            }
        }
    }

    $redis->set($key, serialize($links));
    unset($redis);

    return $links;
}

//получение ссылок Хабкрая
function GetKhabkraiLinks($site)
{
    $redis = new Yampee_Redis_Client($site->settings->redis->host);
    $key = 'linksarr';

    if ($redis->has($key)) {
        $arr = unserialize($redis->get($key));
    }
    else{
        $arr = MakeKhabkraiLinks($site);
    }

    $i=0;
    $narr=Array();

    $manylinks = Array('khabkrai','forum_khabkrai','khabkraitest_user');

    foreach((array)$arr as $el)
    {
        ++$i;
        if(in_array($site->settings->DB->login, $manylinks) || ($i < 5))
        {
            $narr[]=$el;
        }
    }

    unset($redis);



    return($narr);
}

//получаем подтипы переданного типа с кол-вом материалов в них
function GetSubTypesWithMatCnts($site, $type=15, $omsu='', $datestart='', $dateend='')
{

    $type=(int)$type;

    $morequery='';

    //если передан ОМСУ
    if($omsu != '')
    {
        $morequery.='AND "m"."id" IN

                (

                SELECT "material_child" FROM "material_connections" WHERE "material_parent" = '.$omsu.'

                UNION ALL

                SELECT "material_parent" FROM "material_connections" WHERE "material_child" = '.$omsu.'

                ) ';
    }

    //если передан период дат
    if(($datestart != '')&&($dateend != ''))
    {
        $endstamp=MakeTimeStampFromStr($dateend)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $morequery.=' AND "m"."date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $morequery.=' AND "m"."date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }


    //если передана конкретная дата
    elseif($datestart != '')
    {
        $endstamp=MakeTimeStampFromStr($datestart)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $morequery.=' AND "m"."date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $morequery.=' AND "m"."date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }


    $query='SELECT "t"."id" "id", "t"."name" "name",
    (SELECT COUNT("m"."id") FROM "materials" "m" WHERE "m"."status_id" = 2 AND "m"."type_id" = "t"."id" '.$morequery.') "cnt"
    FROM "material_types" "t"
    WHERE "t"."parent_id" = '.$type.' ORDER BY "t"."name" ASC';

    if($res=$site->dbquery($query))
    {
        return $res;
    }
    else
    {
        return false;
    }

}

//меню для ОМСУ и подразделов
function MakeOMSUMenu($site, $material)
{

    $clear_page_url = preg_replace("/(\?|\#).*$/", "", $_SERVER['REQUEST_URI']);

    ob_start();
    
    ?>
    <div class="menu tabs">
    <?php

    $cityes=Array('732','733');

    if($material['type_id'] == 209) {
        $mainname='О поселении';
    } else {
        if(in_array($material['id'], $cityes)) {
            $mainname='О городском округе';
        } else {
            $mainname='О районе';
        }
    }

    if ($material['type_id'] == 209) {
        $active = ($material['url'] == $clear_page_url) ? ' active' : '';
        ?><a href="<?= $material['url'] ?>" 
            class="temporary-not-preloadable region-menu-link region-menu-link-0<?= $active ?>" 
            data-ajax-container=".content.main"><?= $mainname ?></a>
        <?php
    } else {
        $active = ('/khabarovsk-krai/OMSU/'.$material['id'] == $clear_page_url) ? ' active' : '';
        ?><a href="/khabarovsk-krai/OMSU/<?= $material['id'] ?>"
             class="temporary-not-preloadable region-menu-link region-menu-link-0<?= $active ?>"
             data-ajax-container=".content.main"><?= $mainname ?></a>
        <?php
    }

    $menu_items = GetOneLevelMenu($site, $material['type_id'], $material['id']);

    foreach ($menu_items as $num => $menu_item) {
        $active = ($menu_item['url'] == $clear_page_url) ? ' active' : '';

        ?><a href="<?= $menu_item['url'] ?>"
            class="temporary-not-preloadable region-menu-link region-menu-link-<?= ($num+1).$active ?>"
            data-ajax-container=".content.main"><?= $menu_item['name'] ?></a><?php
    }

    ?></div><?php

    return ob_get_clean();
}

function MakeFileGroupKey($files)
{
    $realkey=false;
    if($filesarr=explode(',',$files)){
        if(count($filesarr) > 0)
        {
            $summ=0;
            $str='';

            foreach($filesarr as $fileid)
            {
                $summ+=$fileid;
                $str.=$fileid;
            }

            //специально для Дена
            $summ+=1488;

            $realkey=md5($summ.$str.'df%$^efDF12h');
        }
    }

    return $realkey;

}

/**
 * Создание CSV-файла реестра открытых данных
 * @param $site
 * Экземпляр сайта
 * @param $type_id
 * ID типа создаваемого или редактируемого материала
 */
function CreateOpenDataCsvList($site, $type_id) {
    $opendata_type_id = 234;

    if($type_id != $opendata_type_id)
        return;


    $csv_delimiter = ','; // разделитель для CSV-файла
    $csv_enclosing = '"'; // Экранирующий символ для CSV-файла
    $goverment_id = 2700000786; // ИНН организации


    $opendata_list_directory = $site->settings->path . '/opendatafiles/'; // папка с файлами для открытых данных

    $list_file_descriptor = fopen($opendata_list_directory. 'list.csv', 'w'); //
    fprintf($list_file_descriptor, chr(0xEF).chr(0xBB).chr(0xBF)); // пишем UTF-8 BOM в начало файла

    // выбираем материалы открытых данных
    $opendata_list_query = 'SELECT "id_char", "name" FROM "materials" WHERE "type_id" = '
        . $opendata_type_id .' AND "status_id" = 2';

    // пишем заголовок
    fputcsv($list_file_descriptor, array("property", "title", "value","format"),
        $csv_delimiter, $csv_enclosing);
    fputcsv($list_file_descriptor, array( "standardversion","Версия методических рекомендаций",
        "http://opendata.gosmonitor.ru/standard/3.0", ""), $csv_delimiter, $csv_enclosing);

    // заполняем ссылки на все паспорта открытых данных
    if($res = $site->dbquery($opendata_list_query)) {
        if($res > 0) {
            foreach($res as $record) {
                $passport_id = $goverment_id . "-" . $record["id_char"];
                $passport_file_path = 'http://'.$_SERVER['SERVER_NAME']."/opendata/" . $passport_id . "/meta.csv";

                fputcsv($list_file_descriptor, array($passport_id, $record["name"], $passport_file_path, "csv"),
                    $csv_delimiter, $csv_enclosing);
            }
        }
    }

    fclose($list_file_descriptor);
}

/**
 * Создание CSV-файла паспорта открытых данных
 * @param $site
 * Экземпляр сайта
 * @param $material_id
 * ID создаваемого / редактируемого материала
 * @param $type_id
 * ID типа создаваемого / редактируемого материала
 */
function CreateOpenDataCsvMaterial($site, $material_id, $type_id) {
    $opendata_type_id = 234;

    if($type_id != $opendata_type_id)
        return;


    $csv_delimiter = ','; // разделитель для CSV-файла
    $csv_enclosing = '"'; // Экранирующий символ для CSV-файла
    $goverment_id = 2700000786; // ИНН организации

    $parse_date_format = 'd-M-y'; // формат дат из MaterialGet
    $csv_date_format = 'Ymd'; // формат для сохранения дат в CSV

    $material = MaterialGet($site, $material_id);
    $opendata_list_directory = $site->settings->path . '/opendatafiles/';
    $passport_id = $goverment_id . "-" . $material["id_char"];

    $descriptor_path = $opendata_list_directory . $passport_id;

    // нет директории для паспорта - создаем ее
    if(!file_exists($descriptor_path)) {
        mkdir($descriptor_path, 0777, true);
    }

    $opendata_file_descriptor = fopen($opendata_list_directory . $passport_id . "/meta.csv", "w");

    fprintf($opendata_file_descriptor, chr(0xEF).chr(0xBB).chr(0xBF)); // пишем UTF-8 BOM в начало файла

    // пишем заголовок
    fputcsv($opendata_file_descriptor, array("propery", "value"), $csv_delimiter, $csv_enclosing);
    fputcsv($opendata_file_descriptor, array("standardversion", "http://opendata.gosmonitor.ru/standard/3.0")
        ,$csv_delimiter, $csv_enclosing);

    // пишем данные паспорта
    fputcsv($opendata_file_descriptor, array("identifier", $passport_id), $csv_delimiter, $csv_enclosing);
    fputcsv($opendata_file_descriptor, array("title", $material["name"]), $csv_delimiter, $csv_enclosing);
    fputcsv($opendata_file_descriptor, array("description", $material["content"]), $csv_delimiter, $csv_enclosing);
    fputcsv($opendata_file_descriptor, array("creator", "Правительство Хабаровского края"),
        $csv_delimiter, $csv_enclosing);
    fputcsv($opendata_file_descriptor, array("format", "csv"), $csv_delimiter, $csv_enclosing);


    // пишем данные об ответственном лице
    if(array_key_exists(42, $material["params"])) {
        $publisher_id = $material["params"][42]["value"];
        $publisher_material = MaterialGet($site, $publisher_id);

        $publisher =  array();
        $publisher["fio"] = $publisher_material["name"];
        $publisher["mail"] = $publisher_material["params"][41]["value"];
        $publisher["phone"] = $publisher_material["params"][40]["value"];

        fputcsv($opendata_file_descriptor, array("publishername", $publisher["fio"]), $csv_delimiter, $csv_enclosing);
        fputcsv($opendata_file_descriptor, array("publisherphone", $publisher["phone"]), $csv_delimiter, $csv_enclosing);
        fputcsv($opendata_file_descriptor, array("publishermbox", $publisher["mail"]), $csv_delimiter, $csv_enclosing);
    }

    // выдергиваем id версий
    $version_ids = array();
    $version_ids = GetParamMaterials($site, $material_id);

    // если набор даннах заполнен, то вытаскиваем инфу по его версиям
    if(count($version_ids) > 0) {

        $versions = array();

        foreach($version_ids as $version_id) {
            array_push($versions, MaterialGet($site, $version_id["id"]));
        }

        $current_version = $versions[0];
        $first_version = $versions[count($versions) - 1];
        $current_structure = MaterialGet($site, $current_version["params"][50]["value"]);
        $structure_id = $current_structure["documents"][0]["name"]; // id для RDFa якорей
        $structure_extension = $current_structure["documents"][0]["extension"]; // расширение файла для ссылки

        // дата создания
        $create_date = DateTime::createFromFormat($parse_date_format, $first_version["date_event"]);

        if($create_date === false)
        {
            //die('wrong date! - '.$parse_date_format.' - '.$first_version["date_event"]);
            fputcsv($opendata_file_descriptor, array("created", ''),
                $csv_delimiter, $csv_enclosing);
        }
        else
        {
            fputcsv($opendata_file_descriptor, array("created", $create_date->format($csv_date_format)),
                $csv_delimiter, $csv_enclosing);
        }



        // дата обновления
        $modified_date = DateTime::createFromFormat( $parse_date_format, $current_version["date_event"]);

        if($modified_date === false)
        {
            fputcsv($opendata_file_descriptor, array("modified", ''),
                $csv_delimiter, $csv_enclosing);
        }
        else
        {
            fputcsv($opendata_file_descriptor, array("modified", $modified_date->format($csv_date_format)),
                $csv_delimiter, $csv_enclosing);
        }



        // дата актуальности
        $valid_date = DateTime::createFromFormat($parse_date_format, $current_version["params"][51]["value"]);

        if($valid_date === false)
        {
            fputcsv($opendata_file_descriptor, array("valid", ''),
                $csv_delimiter, $csv_enclosing);
        }
        else
        {
            fputcsv($opendata_file_descriptor, array("valid", $valid_date->format($csv_date_format)),
                $csv_delimiter, $csv_enclosing);
        }


        // ссылки на версии
        foreach($versions as $version) {
            $version_id = $version["documents"][0]["name"];

            $version_url = "http://".$_SERVER['SERVER_NAME']."/opendata/".
                $passport_id . "/". $version_id . "." . $structure_extension;

            fputcsv($opendata_file_descriptor, array($version_id, $version_url), $csv_delimiter, $csv_enclosing);
        }

        // ссылка на структуру
        fputcsv($opendata_file_descriptor, array($structure_id,
                'http://'.$_SERVER['SERVER_NAME']."/opendata/" . $passport_id . "/" .
                $structure_id. "." .$structure_extension),
            $csv_delimiter, $csv_enclosing);
    }

    fclose($opendata_file_descriptor);
}

//функция получения главы района
function GetOMSUHead($site, $omsuarr)
{

}


function GetRegionTown($site, $town)
{
    //получаем полное инфо по региону
    if($megaarr=MaterialGet($site, $town))
    {

    }
    else{
        return(false);
    }

    //переформатируем массив для удобного отображения


}

//функция получения списка поселений в районе
function GetRegionTowns($site, $region)
{
    //получаем id поселений
    $query='SELECT "id", "name",

    (SELECT "valuename" FROM "extra_materials" WHERE "material_id" = "m"."id" AND "type_id" = 30) "lat",


    (SELECT "valuename" FROM "extra_materials" WHERE "material_id" = "m"."id" AND "type_id" = 31) "long"

    FROM "materials" "m" WHERE "m"."status_id" = 2 AND "m"."type_id" = 209 AND "m"."id" IN

        (
        SELECT "material_parent" FROM "material_connections" WHERE "material_child" = '.$region.'
        UNION ALL
        SELECT "material_child" FROM "material_connections" WHERE "material_parent" = '.$region.'
        )

        ORDER BY "m"."name" ASC

    ';

    //echo '<pre>'.$query.'</pre>';


    if($res=$site->dbquery($query))
    {

        $out=Array();
        //сылка на поселение!!!!!
        foreach($res as $row)
        {
            //$row['url']='#';
            $row['url']=GetMaterialTypeUrl($site, 209, $row['id'], 209);
            $out[]=$row;
        }


        return($out);
    }
    else{
        return false;
    }

}

//функция получения ОКТМО района для ОМСУ
function GetOKTMOforOMSU($site, $omsuarr)
{

    if($omsuarr['type_id'] == 209)
    {
        $pid=32;
    }
    else{
        $pid=12;
    }

    //получаем регион
    if(isset($omsuarr['params'][$pid]['value']))
    {




        if($pid==32)
        {
            $region=$omsuarr['params'][$pid]['value'];
            return $region;
        }


        $region=(int)$omsuarr['params'][$pid]['value'];


        if($region > 0)
        {

            //получаем ОКТМО
            $query='SELECT "valuename" FROM "extra_materials" WHERE "material_id" = '.$region.' AND "type_id" = 28';
            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    return $res[0]['valuename'];
                }
            }

        }
        else{
            return false;
        }

    }
    else{
        return false;
    }

}

//функция получния контанта из кэша
function cache_get_contents(\iSite $site, $url, $seconds)// @todo track usage
{
    $content='';

    $key='url:'.md5($url);

    $redis = $site->getRedis();

    $redis->del($key);

    if ($redis->has($key)) {
        $content = $redis->get($key);
    } else {
        $ch = curl_init($url);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 3,
        ));
        $content = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_status == 200 && $content !== false) {
            $redis->set($key, $content, $seconds);
        } else {
            $content='';
            $lifetime = 600;
            if ($http_status == 410)// GONE
                $lifetime = null;
            $redis->set($key, $content, $lifetime);
        }
    }

    unset($redis);

    return($content);
}

//формирует запрос id-шников (для списка новостей)
function MakeNewsQuery(
    $site,
    $type,
    $types,
    $region,
    $keys,
    $datestart,
    $dateend,
    $qtype='list',
    $subtype='',
    $important = null)
{
    //логика $type и $types - Если не передан регион, то ищим материалы только типом $type, иначе - $types
    //костыль - согласно логике заказчика

    if($region == '')
    {
        $atypes = $type;
    }
    else
    {
        $atypes = $types;
    }

    if($subtype != '')
    {
        $atypes = $subtype;
    }

    if ($qtype == 'list') {
        $qtarget = '"id"';
    } else {
        $qtarget = 'count("id") "cnt"';
    }

    //формируем подзапрос для получания списка id-шников
    $morequery='SELECT '.$qtarget.' FROM "materials"
    WHERE "language_id" = '.$site->data->language.' AND "type_id" IN ('.$atypes.') AND "status_id" = 2';

    //если город президентского внимания
	if($type == 319)
	{



		$morequery='SELECT '.$qtarget.' FROM "materials"
    WHERE "language_id" = '.$site->data->language.' 
    AND (("type_id" IN ('.$atypes.')) OR ("type_id" = 2 AND "id" IN (
    
    SELECT mc.material_parent FROM material_connections mc WHERE mc.material_child = id
    UNION ALL
    SELECT mcc.material_child FROM material_connections mcc WHERE mcc.material_parent = id
    
    ))) 
    AND "status_id" = 2';
	}

    //если переданы ключевые слова, дописываем запрос
    if ($keys != '') {
        $keyarr = explode(',',$keys);

        if (count($keyarr) > 0) {
            foreach($keyarr as $key) {
                $key = trim($key);
                if($key != '') {
                    $morequery.=' AND lower("extra_text1") LIKE lower('."'%".$key."%'".')';
                }
            }
        }
    }

    //если передан период дат
    if(($datestart != '')&&($dateend != ''))
    {
        $endstamp=MakeTimeStampFromStr($dateend)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $morequery.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $morequery.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }
    //если передана конкретная дата
    elseif($datestart != '')
    {
        $endstamp=MakeTimeStampFromStr($datestart)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $morequery.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $morequery.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }

    //если передан регион
    if ($region != '') {
        $morequery.=' AND "id" IN (

        SELECT "material_child" "id" FROM "material_connections"
        WHERE "material_parent" = '.$region.'

        UNION ALL

        SELECT "material_parent" "id" FROM "material_connections"
        WHERE "material_child" = '.$region.'

        )';
    }

    if (!is_null($important)) {
        $morequery .= ' AND "extra_number" = '.intval($important);
    }
    
    return($morequery);
}

//выводим общее кол-во новостей, согласно переданным ограничителям
function coutNews(
    \iSite $site,
    $type='2',
    $types='84,94,164,211,2',
    $region='',
    $keys='',
    $datestart='',
    $dateend='',
    $subtype='',
    $important = null)
{

    //формирование запроса, аналогично функции listNews
    $morequery = MakeNewsQuery($site, $type, $types, $region, $keys, $datestart, $dateend, 'count', '', $important);

    if($res=$site->dbquery($morequery))
    {
        if(count($res) > 0)
        {
            return $res[0]['cnt'];
        }
        else{
            return 0;
        }
    } else {
        return false;
    }

}

//выводим список новостей, согласно переданным ограничителям
function listNews(
    \iSite $site,
    $page=1,
    $perpage=10,
    $type='2',
    $types='84, 94, 164, 211, 2',
    $region = '',
    $keys = '',
    $datestart='',
    $dateend='',
    $subtype='',
    $important = null)
{
    //формирование запроса
    $morequery = MakeNewsQuery(
        $site,
        $type,
        $types,
        $region,
        $keys,
        $datestart,
        $dateend,
        'list',
        $subtype,
        $important
    );

    $site->values->orderby = 'date_event';
    $site->values->ordersort = 'DESC';

    if (($site->settings->DB->login == 'forum_user')&&($type == 10))
    {
        $site->values->orderby = 'name';
        $site->values->ordersort = 'ASC';
    }

    //получаем список материалов
    $materials = MaterialList(
        $site,
        '',
        $page,
        $perpage,
        STATUS_ACTIVE,
        '',
        '',
        $morequery,
        false,
        true
    );

    $showdate=true;

    if ($page > 1) {
        $predpage = $perpage * ($page-1) - 1;

        $predmat = MaterialList($site, '', $predpage, 1, STATUS_ACTIVE, '', '', $morequery, true);

        if(count($predmat) > 0)
        {
            $preddate = date("d.m.Y", $predmat[0]['unixtime']);
            $firstdate = date("d.m.Y", $materials[0]['unixtime']);

            if($preddate == $firstdate)
            {
                $materials[0]['showFirstDate']=false;
            }
        }
    }

    $i =- 1;
    if (!empty($materials)) {
        foreach((array)$materials as $mat)
        {
            ++$i;

            if (empty($mat['id']))
                continue;

            $queryreg='SELECT "m"."id", "m"."name" FROM "materials" "m"
        WHERE "m"."language_id" = '.$site->data->language.' AND "m"."status_id" = 2 AND "m"."type_id" = 21 AND "m"."id" IN (

        SELECT "material_parent" "id" FROM "material_connections" WHERE "material_child" = $1

        UNION ALL

        SELECT "material_child" "id" FROM "material_connections" WHERE "material_parent" = $1

        )';

            $q_params = array($mat['id']);

            $matreg=Array();;

            if($resreg=$site->dbquery($queryreg, $q_params))
            {
                if(count($resreg) > 0)
                {
                    $matreg=$resreg;
                }
            }

            $materials[$i]['region']=$matreg;
        }
    }

    return($materials);
}

//вывести список материалов типа material и колличество связанных материалов типов cntmaterials
function listMatsWithMatCntsByTypes(
    \iSite $site,
    $material='21',
    $cntmaterials='84,94,164,211,2',
    $keys='',
    $datestart='',
    $dateend=''
    )
{
    $materials=Array();

    if (!empty($cntmaterials) && !is_array($cntmaterials))
        $cntmaterials = explode(',', $cntmaterials);

    $morequery='';

    //если переданы ключевые слова, дописываем запрос
    if($keys != '')
    {
        $keyarr=explode(',',$keys);
        if(count($keyarr) > 0)
        {
            foreach($keyarr as $key)
            {
                $key=str_replace(' ','',$key);
                if($key != '')
                {
                    $morequery.=' AND lower("extra_text1") LIKE lower('."'%".$key."%'".')';
                }
            }
        }
    }

    //если передан период дат
    if(($datestart != '')&&($dateend != ''))
    {
        $endstamp=MakeTimeStampFromStr($dateend)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $morequery.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $morequery.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }
    //если передана конкретная дата
    elseif($datestart != '')
    {
        $endstamp=MakeTimeStampFromStr($datestart)+24*60*60-1;
        $dateendn=date('d.m.Y',$endstamp);
        $morequery.=' AND "date_event" >= '."TO_DATE('$datestart','DD.MM.YYYY')";
        $morequery.=' AND "date_event" < '."TO_DATE('$dateendn','DD.MM.YYYY')";
    }


    $query = <<<EOS
SELECT
    "id",
    COALESCE(SUM("extra_number2"),0) "extra_number2",
    "name", SUM("cnt") "cnt"
FROM (
    SELECT
        "m"."id",
        "m"."name",
        "m"."extra_number2",
        0 "cnt"
    FROM "materials" "m"
    WHERE
        "m"."type_id" = $1
        AND "m"."status_id" = $2
        AND "m"."language_id" = $3

    UNION ALL

    SELECT
        "m"."id",
        "m"."name",
        0 "extra_number2",
        COUNT("c"."material_child") "cnt"
    FROM
        "materials" "m",
        "material_connections" "c"
    WHERE
        "m"."type_id" = $1
        AND "m"."status_id" = $2
        AND "m"."language_id" = $3
        AND "m"."id" = "c"."material_parent"
        AND "c"."material_child" IN(
            SELECT "id"
            FROM "materials"
            WHERE "status_id" = $2
            AND "language_id" =  $3
            AND "type_id" IN ($4)
            $morequery
        )
    GROUP BY
        "m"."id",
        "m"."name",
        "m"."extra_number2"
        
    UNION ALL
        
    SELECT
        "m1"."id",
        "m1"."name",
        0 "extra_number2",
        COUNT("c1"."material_parent") "cnt"
    FROM
        "materials" "m1",
        "material_connections" "c1"
    WHERE
        "m1"."type_id" = $1
        AND "m1"."status_id" = $2 
        AND "m1"."language_id" = $3
        AND "m1"."id" = "c1"."material_child"
        AND "c1"."material_parent" IN(
            SELECT "id"
            FROM "materials"
            WHERE
                "status_id" = $2
                AND "language_id" =  $3
                AND "type_id" IN ($4)
                $morequery
            )
        GROUP BY
            "m1"."id",
            "m1"."name",
            "extra_number2"
    ) mt
GROUP BY
    "id",
    "name"
ORDER BY
    "extra_number2" DESC,
    "name" ASC
EOS;

    $q_params = array($material, STATUS_ACTIVE, $site->data->language, $cntmaterials);

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $res = $site->dbquery($query, $q_params);
    if (!empty($res)) {
        if(count($res) > 0)
            $materials = $res;
    }

    return $materials;
}

//массив годов материалов переданных типов
function giveMeAMaterialYears($site, $types, $files=false)
{
    $years=Array();

    if($types != '')
    {
        $query='SELECT DISTINCT TO_NUMBER(TO_CHAR("date_event",'."'YYYY'".')) "myear"
            FROM "materials" WHERE "type_id" IN ('.$types.') AND "status_id" = 2
            ORDER BY "myear" ASC';

        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    if($row['myear'] > 100)
                    {
                        $years[]=$row['myear'];
                    }
                }
            }
        }

        //считаем годы файлов
        if($files)
        {
            $query='SELECT DISTINCT TO_NUMBER(TO_CHAR(date_event,'."'YYYY'".')) myear FROM files f WHERE f.id IN 
                (SELECT c.file_id FROM file_connections c WHERE c.material_id IN 
                    (SELECT m.id FROM materials m WHERE m.type_id IN ('.$types.') AND m.status_id = 2)
                ) ORDER BY myear ASC';

            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {
                        if($row['myear'] > 100)
                        {
                            if(!in_array($row['myear'],$years))
                            {
                                $years[]=$row['myear'];
                            }
                        }
                    }
                }
            }

            sort($years);

        }
    }

    return($years);
}

//Форма аккредитации - сохранение
function AccreditationFormSave($site)
{
    $site->GetPostValues(Array('err_iamrobot','err_iamfox_mulder','event','company_name','employee','phone','email','car_entrance','car_model','car_number','err_content'));

    if($site->values->company_name == '' || $site->values->event =='' || $site->values->employee == '' || $site->values->phone == '')
    {
        return(Array(false,'Не заполнены необходимые поля'));
    }
    else{

        //получаем название события для акредитации

        $site->values->event=(int)$site->values->event;
        if($site->values->event==0){
            return(Array(false,'Не передано мероприятие'));
        }

        $event=MaterialGet($site, $site->values->event);
        //проверяем мероприятие на возможность подать аккредитацию
        if($event['type_id'] != '19')
        {
            return(Array(false,'На переданое мероприятие запистаься не возможно'));
        }

        //krumo($event);

        $enddateparam=43;

        if(isset($event['params'][$enddateparam]['unixtime']))
        {
            if(time() > $event['params'][$enddateparam]['unixtime'])
            {
                return(Array(false,'Закончилось время записи на мероприятие'));
            }
        }

        if(($site->values->car_model != '')&&($site->values->car_number != ''))
        {
            $autopropusk=$site->values->car_model.' номер: '.$site->values->car_number;
        }
        else
        {
            $autopropusk='нет';
        }

        //заносим аккредитацию в материал
        $newsite=$site;
        $newsite->values->id='';
        $newsite->values->type_id=242;
        $newsite->values->name=$event['name'].' - '.$site->values->employee;
        $newsite->autorize->userid=1;
        $newsite->values->status_id=1;
        $newsite->values->anons='ФИО: '.$site->values->employee.'
        <br>Организация: '.$site->values->company_name.'
        <br>e-mail: '.$site->values->email.'
        <br>телефон: '.$site->values->phone.'
        <br>пропуск на авто: '.$autopropusk;
        $newsite->values->content=$site->values->err_content;

        if((!(bool)$site->values->err_iamrobot)&&($site->values->err_iamfox_mulder == 'yep'))
        {
            $newResult=createMaterial($newsite);

            if($newResult[0])
            {
                $newsite->values->id=$newResult[0];

                $newResult[1]='Форма аккредитации успешно отправлена';

                //проверяем прикрепленные скриншеты
                if(isset($_FILES['screen']))
                {
                    foreach ($_FILES["screen"]["error"] as $key => $error) {
                        if ($error == UPLOAD_ERR_OK) {


                            $newsite->values->file_name=$_FILES["screen"]["name"][$key];

                            $_FILES['filedata']['tmp_name']=$_FILES["screen"]["tmp_name"][$key];
                            $_FILES['filedata']['name']=$_FILES["screen"]["name"][$key];
                            $_FILES['filedata']['error']=$_FILES["screen"]["error"][$key];
                            $_FILES['filedata']['type']=$_FILES["screen"]["type"][$key];
                            $_FILES['filedata']['size']=$_FILES["screen"]["size"][$key];


                            $newsite->values->file_type_id=4;


                            $nfile=new MFile($newsite);
                            $resarr=$nfile->DoSaveFile();
                            unset($nfile);

                        }
                    }
                }



                return($newResult);
            }
            else{

                return($newResult);
            }
        }

    }

}

//обратная связь - сохраняем обращение в базу
function callbacksave($site)
{
    $site->GetPostValues(Array('err_fio','err_email','err_content', 'err_iamrobot', 'err_iamfox_mulder', 'err_tel'));

    if(($site->values->err_fio != '')&&($site->values->err_tel != '')&&($site->values->err_content != ''))
    {

        //сохраняем текст обратной связи
        $newsite=$site;
        $newsite->values->id='';
        $newsite->values->type_id=219;
        $newsite->values->name=$site->values->err_fio.' от '.date('d.m.Y H:i').'';
        $newsite->autorize->userid=1;
        $newsite->values->status_id=1;
        $newsite->values->anons='ФИО: '.$site->values->err_fio.'<br>e-mail: '.$site->values->err_tel;
        $newsite->values->content=$site->values->err_content;

        $newResult=true;



        if((!(bool)$site->values->err_iamrobot)&&($site->values->err_iamfox_mulder == 'yep')&&($site->values->err_email == ''))
        {
            $newResult=createMaterial($newsite);

            if($newResult[0])
            {
                $newsite->values->id=$newResult[0];

                $newResult[1]='Спасибо! Ваше сообщение принято.';

                //проверяем прикрепленные скриншеты
                if(isset($_FILES['screen']))
                {
                    foreach ($_FILES["screen"]["error"] as $key => $error) {
                        if ($error == UPLOAD_ERR_OK) {


                            $newsite->values->file_name=$_FILES["screen"]["name"][$key];

                            $_FILES['filedata']['tmp_name']=$_FILES["screen"]["tmp_name"][$key];
                            $_FILES['filedata']['name']=$_FILES["screen"]["name"][$key];
                            $_FILES['filedata']['error']=$_FILES["screen"]["error"][$key];
                            $_FILES['filedata']['type']=$_FILES["screen"]["type"][$key];
                            $_FILES['filedata']['size']=$_FILES["screen"]["size"][$key];


                            $newsite->values->file_type_id=1;


                            $nfile=new MFile($newsite);
                            $resarr=$nfile->DoSaveFile();
                            unset($nfile);

                        }
                    }
                }



                return($newResult);
            }
            else{

                return($newResult);
            }
        }
	    $newResult[0]=1;
	    $newResult[1]='Спасибо! Ваше сообщение принято.';
	    return($newResult);

    }
    else{
        return(Array(false,'Не заполнены необходимые поля'));
    }
}

/**
 * Локализация текстовых строк.
 *
 * @param string $word
 * @param int $lang
 * @param array $arr
 * @return string
 */
function localize($word, $lang, $arr)
{
    if ($lang == LANG_RU)
        return $word;
    if (isset($arr[$word]))
        return $arr[$word];
    return '';
}

//виджет опросов
function vidjet_vote(\iSite $site, $id)
{


    $votehtml='';
    $vote=MaterialGet($site,$id);
    if(count($vote) > 0)
    {
        $votehtml.="<div class=\"main_important_header\">
        &nbsp;&nbsp;&nbsp;&nbsp;Опрос
        </div>
        <div class='vote_header'>$vote[name]</div>
        <div id='votebody'><form method='post' id='vote_form'>";


        //опрос - совершаем запись, если передан
        $site->GetPostValues('option');
        if($site->values->option != '')
        {
            $site->values->option=(int)$site->values->option;

            //обновляем цЫфирку, сохраняем куку
            //$_COOKIE['vote_'.$id]=$site->values->option;
            //$temptext= ob_get_contents();

            //ob_start();


            if($site->settings->DB->name == 'oiv_tek')
            {
                $site->addCookie('vote_'.$id, $site->values->option, 0);
            }
            else
            {
                $site->addCookie('vote_'.$id, $site->values->option, time()+3600000);
            }

            $_COOKIE['vote_'.$id]=$site->values->option;
            //header("expires: mon, 26 jul 2000 05:00:00 Gmt"); //Дата в прошлом
            //header("cache-control: no-cache, must-revalidate"); // http/1.1
            //header("pragma: no-cache"); // http/1.1
            //header("last-modified: ".gmdate("d, d m y h:i:s")." Gmt");
            //$site->headers = ob_get_contents();
            //ob_end_clean();

            //ob_start();
            //echo $temptext;




            $newvalue=1;
            $query='SELECT "valuename" FROM "extra_materials" WHERE "type_id" = 4 AND "material_id" = '.$site->values->option;
            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    $newvalue+=(int)$res[0]['valuename'];

                }
            }


            $query='UPDATE "extra_materials" SET "valuename" = '."'".$newvalue."'".' WHERE "type_id" = 4 AND "material_id" = '.$site->values->option;
            $site->dbquery($query);

        }


        //определяем, голосовал ли пользювзюк
        if(isset($_COOKIE['vote_'.$id]))
        {
            //выводим результаты опороса
            $allvotes=0;
            $color=Array();
            $color[]='#a0f29a';
            $color[]='#e0f29a';
            $color[]='#f2d89a';
            $color[]='#f2b59a';
            $color[]='#f29abd';
            $color[]='#f29af1';
            $color[]='#c19af2';
            $color[]='#9aaaf2';
            $color[]='#9ad4f2';
            $color[]='#9aeff2';
            $color[]='#a0f29a';
            $color[]='#e0f29a';
            $color[]='#f2d89a';
            $color[]='#f2b59a';
            $color[]='#f29abd';
            $color[]='#f29af1';
            $color[]='#c19af2';
            $color[]='#9aaaf2';
            $color[]='#9ad4f2';
            $color[]='#9aeff2';


            if(count($vote['connected_materials']) > 0)
            {
                //получаем сумму ответов
                $answers=Array();
                $maxval=0;
                $i=-1;
                foreach($vote['connected_materials'] as $option)
                {
                    $option=MaterialGet($site, $option['id']);

                    $optionval=0;
                    $optionproc=0;

                    if(isset($option['params'][4]['value']))
                    {
                        $optionval=(int)$option['params'][4]['value'];
                    }

                    if($maxval < $optionval)
                    {
                        $maxval=$optionval;
                    }

                    ++$i;
                    $answers[$i]['votes']=$optionval;
                    $answers[$i]['name']=$option['name'];
                    $allvotes+=$optionval;
                }

                //рисуем ответы
                $i=-1;
                foreach($answers as $option)
                {
                    ++$i;
                    if($option['votes'] > 0)
                    {
                        $option['proc']=round(100/$allvotes*$option['votes']);
                    }
                    else
                    {
                        $option['proc']=0;
                    }

                    $votehtml.='<div style="margin:10px; font-size:12px;">'.$option['name'].'</div>';

                    if($option['votes'] > 0)
                    {
                        $pix=round(200/$maxval*$option['votes']);
                    }
                    else
                    {
                        $pix=0;
                    }

                    if($pix==0)
                    {
                        $pix=2;
                    }

                    $votehtml.="<div style='margin:10px;'>
                        <div style='width: 200px; position: relative; height: 30px;'>
                            <div style='width: $pix"."px; height: 20px; background-color: ".$color[$i]."'></div>
                            <div style='position:absolute; right: -46px; top:0px; font-size:11px; height:20px; line-height:20px;'>$option[votes] ($option[proc]%)</div>
                        </div>
                    </div>";
                }

            }


        }
        else
        {



            //даём гологосунуть
            if(count($vote['connected_materials']) > 0)
            {
                foreach($vote['connected_materials'] as $option)
                {
                    $option=MaterialGet($site, $option['id']);
                    $votehtml.='<div style="margin:10px;"><input class="styler" type="radio" name="option" value="'.$option['id'].'"> - '.$option['name'].'</div>';

                }
            }
            $votehtml.='<div class="widget-news-link-blue link-btn" style="margin: 20px;"><a href="#" onclick="document.getElementById('."'".'vote_form'."'".').submit(); return false;">Ответить</a></div></form>';

        }
        $votehtml.='</div>';
    }

    return($votehtml);
}

//виджет блока картинок
function vjt_print_photos($photos)
{
    ob_start();
    ?><div class="jk_vjt_photo"><?php

    $p=0;
    $closelist = false;
    $scrollwidth = count($photos)*61;
    foreach ($photos as $photo) {
        ++$p;
        //первое фото
        if ($p == 1) {
            ?>
            <div class="jk_vjt_photo_main" id="jk_vjt_photo_main">
                <a href="/photos/<?= $photo['id'] ?>_x950.jpg" class="imggroup xmedia mt-photo s272x199"
                   title="<?= $photo['name'] ?>">
                    <img src="/photos/<?= $photo['id'] ?>_xy272x199.jpg" class="media-content"/>
                    <span class="media-textview">Изображение: <?=
                        htmlspecialchars($photo['name']) ?></span>
                </a>
            </div>
            <?php
        } else {
            if($p == 2) {
                $closelist = true;
                ?><div class="jk_vjt_photo_list">
                    <div id="jk_vjt_photo_listscroll" class="jk_vjt_photo_listscroll"
                         style="width: <?= $scrollwidth ?>px;">
            <?php
            }
            //листинг фото
            ?>
            <div class="jk_vjt_photo_small xnormalblock" style="float:left;">
                <a href="/photos/<?= $photo['id'] ?>_x950.jpg" class="imggroup xmedia s58x49 mt-image"
                   title="<?= $photo['name'] ?>">
                    <img src="/photos/<?= $photo['id'] ?>_xy58x49.jpg" alt="<?=
                        htmlspecialchars($photo['name']) ?>" class="media-content"/>
                    <span class="media-textview">Изображение: <?=
                        htmlspecialchars($photo['name']) ?></span>
                </a>
            </div>
            <?php
        }
    }

    if($closelist)
    {
        ?>
        </div>
    </div>
        <?php
        if ($scrollwidth > 272) {
            ?><div style='position: relative; width: 0px; height:0px'>
                <div class='jk_vjt_photo_rscroll' onclick='jk_vjt_photo_rscroll(300,61);'
                     onmouseover='jk_vjt_photo_rscroll(300,61);'></div>
                <div class='jk_vjt_photo_lscroll' onclick='jk_vjt_photo_lscroll(300,61);'
                     onmouseover='jk_vjt_photo_lscroll(300,61);'></div>
            </div><?php
        }
    }

    ?>
    </div>
    <?php

    return ob_get_clean();
}

/**
 * Группировка документов с одинаковыми именами (и разными типами).
 * 
 * @param $items
 * @return array
 */
function vjt_documents_join_items($items)
{
    $ret = array();

    foreach ($items as $item) {
        $key =
            (isset($item['par_name']) ? $item['par_name'].':' : '').
            $item['name'].':'.
            $item['material_id'];

        if (!isset($ret[$key]))
            $ret[$key] = array();

        $ret[$key][] = $item;
    }

    return $ret;
}

//виджет документов
function vjt_print_documents($docs, $width=885, $ifdate=false)
{
    global $khabkrai;
    $arazd = '';

    $cssWidth = empty($width) ? 'auto' : $width.'px';
    $cssWidth = 'width:'.$cssWidth.';';
    
    ob_start();

    $docs_joined = vjt_documents_join_items($docs);

    $is_specialver = specialver_is_active($khabkrai);

    if (count($docs_joined) > 0) {
        foreach ($docs_joined as $docj) {
            $doc = $docj[0];
            $par_name = $doc['par_name'];

            if ($par_name != $arazd) {
                $arazd = $par_name;
                ?><h3 class="doclist-caption"><?= $arazd ?></h3><?php
            }

            if ($ifdate) {
                $datearr = explode(' ',$doc['date_event']);
                $time = $datearr[1];
                $date = $datearr[0];
                $date = MakeTimeStampFromStr($date);
                $date = russian_datetime_from_timestamp($date, false);
            }

            $url = '/?menu=getfile&id='.$doc['id'];


            $fullmainurl = 'https://'.$_SERVER['SERVER_NAME'].$url;
            $preview_url='https://docs.google.com/viewer?url='.urlencode($fullmainurl);
	        $preview_urlpdf=$fullmainurl.'&view=1';

            ?>
            <div class="file_list_item_small" style="<?= $cssWidth ?>">
                <a href="<?=
                    htmlspecialchars($url)
                ?>" class="item-link"><?= $doc['name'] ?></a><?php

                ob_start();
                $junction = '';
                $cecutient = false;

                foreach ($docj as $doc) {

                    if (array_key_exists('extension', $doc) && array_key_exists('size', $doc)) {

                        if ($doc['size'] != '') {
                            $doc['size'] = intval($doc['size']);

                            $size_item = 'B';
                            $size_amount = $doc['size'];

                            $realdocsize=$doc['size'];

                            if ($doc['size'] > 1024) {
                                $size_item = 'KB';
                                if ($doc['size'] > 1024 * 1024) {
                                    $size_item = 'MB';
                                    $doc['size'] = $doc['size'] / 1024;
                                }
                                $size_amount = round($doc['size'] / 1024);
                            }

                            $doc['size'] = $size_amount . $size_item;
                        }

                        $url = '/?menu=getfile&id='.$doc['id'];

                        ?><?= $junction ?><a href="<?= htmlspecialchars($url) ?>"><span class="type"><?=
                            strtoupper($doc['extension']) ?></span> <span class="size"><?=
                            $doc['size']
                            ?></span></a><?php

                        $junction = ', ';
                    }

                    if ($is_specialver && ! $cecutient && ! empty($doc['cecutient'])) {
                        ob_start();
                        $url_cecu = '/?menu=getfile&id='.$doc['id'].'_sv';
                        ?><?= $junction ?><a href="<?= htmlspecialchars($url_cecu) ?>">версия для слабовидящих</a><?php
                        $cecutient = ob_get_clean();
                    }
                }

                if ( ! empty($cecutient))
                    echo $cecutient;

                $doc_types = trim(ob_get_clean());

                ?> (<?= $doc_types ?>) <?php


                $extarray=Array('DOC','RTF','XLS','DOCX','XLSX','PPT','PPS','PPTX','PPSX');

                if(in_array(strtoupper($doc['extension']), $extarray) && ($realdocsize < 5*1024*1024)){

                ?> <a href="<?= $preview_url ?>" target="_blank"> - просмотр документа online</a> <?php }
                elseif(strtoupper($doc['extension']) == 'PDF'){
	                ?> <a href="<?= $preview_urlpdf ?>" target="_blank"> - просмотр документа online</a> <?php
                } ?>
                <?php

                if (!empty($date)) {
                    ?>
                    <span class="xdate"><?= $time?>, <?= $date ?></span>
                    <?php
                }

                ?>
            </div>
            <?php
        }
    }

    return ob_get_clean();
}

//виджет трансляций
function vjt_translation($url, $width, $height)
{

    if(strpos($url,'streamer=rtmp://vsr.khv.gov.ru'))
    {
        $out='<object type="application/x-shockwave-flash" data="/fl/jwplayer.swf" width="'.$width.'" height="'.$height.'" style="visibility: visible;"><param name="allowFullScreen" value="true"><param name="allowNetworking" value="all"><param name="flashvars" value="'.$url.'"></object>';
    }
    else{
        $out='<object type="application/x-shockwave-flash" data="http://vsr.khv.gov.ru:8080/flu/StrobeMediaPlayback.swf" width="'.$width.'" height="'.$height.'" style="visibility: visible;"><param name="allowFullScreen" value="true"><param name="allowNetworking" value="all"><param name="flashvars" value="'.$url.'"></object>';
    }
    return $out;
}

/* Получение Id видеоматериала из URL на YouTube */
function get_youtube_video_id($youtube_url) {

    $youtube_url=str_replace('https','http',$youtube_url);

    $youtube_video_id = str_replace('https://www.youtube.com/watch?v=','',$youtube_url);
    $youtube_video_id = str_replace('http://www.youtube.com/watch?v=','',$youtube_video_id);
    $youtube_video_id = str_replace('http://www.youtube.com/embed/','',$youtube_video_id);
    $youtube_video_id = str_replace('http://youtu.be/', '', $youtube_video_id);

    // если в URL есть параметры, исключаем их
    $url_parts =  explode('?', $youtube_video_id, 2);
    if(count($url_parts) == 2) {
        $youtube_video_id = $url_parts[0];
    }

    return $youtube_video_id;
}

function vidjet_material_photovideo(\iSite $site)
{
    $material = MaterialGet($site, $site->values->id);

    //получаем фотографии
    //получаем видео

    ob_start();

    if (count($material['videos']) > 0) {
        $res = $material['videos'];
        $i = 0;
        foreach ($res as $row) {
            ++$i;
            if ($i == 1) {
                if ($row['youtubeid']) {
                    $vurl = $row['content'];
                    $ytid = $row['youtubeid'];
                } else {
                    $ytid = null;
                    $vurl = '/?menu=getvideo&id='.$row['id'];
                }

                ?>
                <div class="xmedia mt-video s289x196">
                    <a href='<?= htmlspecialchars($vurl) ?>'
                       class='image_container_middle ximggroupx media-content'
                       style="background-image: url('<?= $row['thumb'] ?>');"
                        target="_blank"<?php if ( ! empty($ytid)) {?>
                            onclick="ShowYoutubeVideo('<?= $ytid ?>');return false;"<?php
                        } ?>>
                        <div class='image_container_icon_middle'>
                            <img class='svg playsmall-icon-middle'
                                alt='Просмотр'
                                 src='/img/play.svg' />
                        </div>
                    </a>
                    <span class="media-textview">Видеоролик: <?=
                        htmlspecialchars($row['name']) ?></span>
                </div>
                <?php
            }
        }
    }
    
    $videos = ob_get_clean();

    ob_start();

    if (count($material['photos']) > 0)
    {
        $res=$material['photos'];
        $i=0;
        $closelist=false;
        $scrollwidth=count($material['photos'])*61;
        
        foreach ($res as $row) {
            if ($row['author'] != '') {
                $row['name'].=' &nbsp;&nbsp; Автор: '.$row['author'];
            }

            ++$i;

            if($i == 1){
                ?>
                <div class='image_container_middle xmedia mt-image s290'>
                    <a href="/photos/<?= $row['id'] ?>_x922.jpg" class="imggroup"
                        title="<?= $row['name'] ?>">
                        <img src='/photos/<?= $row['id'] ?>_x290.jpg' class="media-content"/>
                        <span class="media-textview">Изображение: <?=
                            htmlspecialchars($row['name']) ?></span>
                    </a>
                </div>
                <?php
            } else {
                if($i == 2) {
                    $closelist = true;
                    ?>
                    <div class="jk_vjt_photo_list">
                        <div id="jk_vjt_photo_listscroll" class="jk_vjt_photo_listscroll"
                             style="width: <?= $scrollwidth ?>px;">
                    <?php
                }
                //листинг фото

                ?>
                <div class="jk_vjt_photo_small xnormalblock" style="float: left;">
                    <a href="/photos/<?= $row['id'] ?>_x922.jpg" class="imggroup xmedia mt-image s58x49"
                       title="<?= $row['name'] ?>">
                        <img src="/photos/<?= $row['id'] ?>_xy58x49.jpg" class="media-content">
                        <span class="media-textview">Изображение: <?=
                            htmlspecialchars($row['name']) ?></span>
                    </a>
                </div>
                <?php
            }
        }

        if ($closelist) {
            ?>
            </div>
        </div>
            <?php
            if ($scrollwidth > 290) {
                ?>
                <div style='position: relative; width: 0px; height:0px'>
                    <div class='jk_vjt_photo_rscroll' onclick='jk_vjt_photo_rscroll(<?= $scrollwidth ?>,61);'></div>
                    <div class='jk_vjt_photo_lscroll' onclick='jk_vjt_photo_lscroll(<?= $scrollwidth ?>,61);'></div>
                </div>
                <?php
            }
        }
    }
    
    $photos = ob_get_clean();


    if($site->data->language == LANG_RU)
    {
        $photoname='Фото';
        $videoname='Видео';
    }
    else
    {
        $photoname='Photo';
        $videoname='Video';
    }


    ob_start();
    if (!empty($photos) && empty($videos)) {
        //формируем блок вывода
        ?>
        <div class="vidjet_incontent">
            <div class="vidget_photo_video incontent">
                <div class="option active" id="vidget_photo" onclick="vidget_selectPhoto();"><?=
                    $photoname
                ?></div>
            </div>
            <div class="vidget_photo_content" id="vidget_photo_content">
                <?= $photos ?>
            </div>
        </div>
        <?php
    } elseif (empty($photos) && !empty($videos)) {
        //формируем блок вывода
        ?>
        <div class="vidjet_incontent">
            <div class="vidget_photo_video incontent">
                <div class="option active" id="vidget_video" onclick="vidget_selectVideo();"><?=
                    $videoname
                    ?></div>
            </div>
            <div class="vidget_video_content" id="vidget_video_content">
                <?= $videos ?>
            </div>
        </div>
        <?php
    } elseif (!empty($photos) || !empty($videos)) {
        //формируем блок вывода
        ?>
        <div class="vidjet_incontent">
            <div class="vidget_photo_video incontent">
                <div class="option active" id="vidget_photo" onclick="vidget_selectPhoto();"><?=
                    $photoname
                ?></div>
                <div class="option" id="vidget_video" onclick="vidget_selectVideo();"><?=
                    $videoname
                ?></div>
            </div>
            <div class="vidget_photo_content" id="vidget_photo_content">
                <?= $photos ?></div>
            <div class="vidget_video_content" style="display: none;" id="vidget_video_content">
                <?= $videos ?>
            </div>
        </div>
        <?php
    }

    return ob_get_clean();
}

function vidjet_material_bigphotos($site)
{

    ob_start();

    //получаем фотографии материала
    $query = 'SELECT * FROM "files"WHERE "type_id" = $1 AND "id" IN (
            SELECT "file_id" FROM "file_connections" WHERE "material_id" = $2
        ) ORDER BY "id" ASC';

    $q_params = array(1, $site->values->id);
    $res = $site->dbquery($query, $q_params);
    if (!empty($res)) {
        foreach($res as $row)
        {
            ?>
            <div class="xmedia mt-image s922">
                <img src='/photos/<?= $row['id'] ?>_x922.jpg' border='0' alt='<?= $row['name'] ?>'
                    class="media-content">
                <span class="media-textview">Изображение: <?=
                    htmlspecialchars($row['name']) ?></span>
            </div>
            <?php
        }
    }

    return ob_get_clean();
}


function vidjet_photovideo(\iSite $site, $types = Array(11,12,128), $template = '')
{
    $i = 0;

    $query_parts = array();
    $q_params = array();

    foreach($types as $type)
    {
        ++$i;

        $q_params[] = $type;
        $phType = '$'.count($q_params);

        $query_parts[] = <<<EOS
SELECT *
FROM (
    WITH RECURSIVE temp1(id, parent_id, PATH, LEVEL) as (
	    SELECT
	        t1.id,
	        t1.parent_id,
	        CAST(t1.id as VARCHAR(11)) as PATH,
	        1
        FROM material_types t1
        WHERE t1.id = $phType
        
	    UNION

	    SELECT
	        t2.id,
	        t2.parent_id,
	        CAST ( temp1.PATH || '->' || t2.id AS VARCHAR(11)),
	        LEVEL + 1
        FROM material_types t2
        INNER JOIN temp1 ON(temp1.id = t2.parent_id)
	)
	SELECT *
	FROM temp1
	ORDER BY PATH
) mt$i
EOS;
    }

    $query = implode(' UNION ALL ', $query_parts);
    $res = $site->dbquery($query, $q_params);

    $materialtypes = array();

    if (!empty($res)) {
        foreach($res as $row)
            $materialtypes[] = $row['id'];
    }

    //получаем последние фотоальбомы

    if ($template == 'big') {
        $rnum = 3;
    } elseif ($template == 'mega') {
        $rnum = 6;
    } else {
        $rnum = 4;
    }

    $query = <<<EOS
SELECT * FROM (
    SELECT m.*,
        EXTRACT (EPOCH FROM m.date_event) unixtime,
        (
            SELECT l.name
            FROM languages l
            WHERE l.id = m.language_id
        ) languagename,
        (
            SELECT s.name
            FROM material_statuses s
            WHERE s.id = m.status_id
        ) statusname,
        (
            SELECT i.file_id
            FROM
                file_connections i,
                files f
            WHERE
                f.id = i.file_id
                AND f.type_id = 1
                AND i.material_id = m.id
            ORDER BY f.ordernum ASC
            LIMIT 1 OFFSET 0
        ) photo
        FROM materials m
        WHERE
            m.type_id IN ($1)
            AND m.language_id = $2
            AND "m"."status_id" = $3
        ORDER BY
            m.date_event DESC,
            m.date_edit DESC
) mt
WHERE photo IS NOT NULL
LIMIT $rnum OFFSET 0
EOS;

    $q_params = array($materialtypes, $site->data->language, STATUS_ACTIVE);

    ob_start();

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);
    $res = $site->dbquery($query, $q_params);

    if (!empty($res)) {
        foreach ($res as $row) {
            if (!empty($row['type_id'])) {
                $url = createTypeFullLink($site,$row['type_id']).'/'.$row['id'];
            }
            if ($site->data->language == LANG_RU) {
                $temestr=russian_datetime_from_timestamp($row['unixtime']);
            } else {
                $temestr=en_datetime_from_timestamp($row['unixtime']);
            }

            $photoInfo = FileGetInfo($site, $row['photo']);

            if(($template == 'big')||($template == 'mega')) {
                ?>
                <a class='normalblock'
                     href="<?= $url ?>">
                    <div class="xmedia mt-image s289x196">
                        <img alt='<?= mb_substr($row['name'], 0, 30, 'utf-8') ?>'
                             src='/photos/<?= $row['photo'] ?>_xy289x196.jpg'
                             class="media-content" />
                        <span class="media-textview">Изображение: <?= $photoInfo['name'] ?></span>
                    </div>
                    <p class='material_list_date'><?= $temestr ?></p>
                    <p class='vidget_name'><?= $row['name'] ?></p>
                </a>
                <?php
            } else {
                ?>
                <a href="<?= $url ?>">
                    <div class="xmedia mt-image s210">
                        <img alt='<?= mb_substr($row['name'], 0, 30, 'utf-8') ?>'
                             src='/photos/<?= $row['photo'] ?>_x210.jpg' />
                        <span class="media-textview">Изображение: <?=
                            htmlspecialchars($photoInfo['name']) ?></span>
                    </div>
                    <p class='material_list_date'><?= $temestr ?></p>
                    <p class='vidget_name'><?= $row['name'] ?></p>
                </a>
                <?php
            }
        }
    }
    
    $photos = ob_get_clean();

    //получаем последние трансляции

    $query = <<<EOS
SELECT
    tb.*,
    (
        SELECT v.content
        FROM files v
        WHERE v.id = tb.photo
    ) "content"
FROM
    (
        SELECT
            m.*,
            EXTRACT(EPOCH FROM m.date_event) unixtime,
            (
                SELECT l.name
                FROM languages l
                WHERE l.id = m.language_id
            ) languagename,
            (
                SELECT s.name
                FROM material_statuses s
                WHERE s.id = m.status_id
            ) statusname,
            (
                SELECT MIN(i.file_id)
                FROM
                    file_connections i,
                    files f
                WHERE
                    f.id = i.file_id
                    AND f.type_id = 5
                    AND i.material_id = m.id
            ) photo
        FROM materials m
        WHERE
            m.type_id IN ($1)
            AND "m"."status_id" = $2
            AND m.language_id = $3
    ) tb
WHERE photo IS NOT NULL
ORDER BY
    date_event DESC,
    date_edit DESC
LIMIT $rnum OFFSET 0
EOS;

    $q_params = array($materialtypes, STATUS_ACTIVE, $site->data->language);
    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);
    $res = $site->dbquery($query, $q_params);
    
    ob_start();

    if (!empty($res)) {
        foreach ($res as $row) {
            if ($row['type_id'] != '') {
                $url = createTypeFullLink($site,$row['type_id']).'/'.$row['id'];
            }

            $videoid=$row['content'];

            if ($site->data->language == LANG_RU) {
                $temestr = russian_datetime_from_timestamp($row['unixtime']);
            } else {
                $temestr = en_datetime_from_timestamp($row['unixtime']);
            }

            if ($template == 'big' || $template == 'mega') {
                ?>
                <a class='normalblock'
                     href="<?= $url ?>">
                    <div class='image_container_middle' style="width:289px; height:196px;"><?=
                        vjt_translation($videoid, 289, 196)
                    ?></div>
                    <p class='material_list_date'><?= $temestr ?></p>
                    <p class='vidget_name'><?= $row['name'] ?></p>
                </a>
                <?php
            }
        }
    }

    $onlines = ob_get_clean();

    //получаем последние видео

    $query = <<<EOS
SELECT
    tb.*,
    (
        SELECT v.content
        FROM files v
        WHERE v.id = tb.photo
    ) "content"
FROM
    (
        SELECT
            m.*,
            EXTRACT(epoch FROM m.date_event) unixtime,
            (
                SELECT
                    l.name
                FROM languages l
                WHERE l.id = m.language_id
            ) languagename,
            (
                SELECT s.name
                FROM material_statuses s
                WHERE s.id = m.status_id
            ) statusname,
            (
                SELECT MIN(i.file_id)
                FROM
                    file_connections i,
                    files f
                WHERE
                    f.id = i.file_id
                    AND f.type_id = 2
                    AND i.material_id = m.id
            ) photo
        FROM materials m
        WHERE
            m.type_id IN ($1)
            AND "m"."status_id" = $2
            AND m.language_id = $3
            
    ) tb

WHERE photo IS NOT NULL
ORDER BY
    date_event DESC,
    date_edit DESC
LIMIT $rnum OFFSET 0
EOS;

    $q_params = array($materialtypes, STATUS_ACTIVE, $site->data->language);

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    ob_start();

    $res = $site->dbquery($query, $q_params);

    if (!empty($res)) {
        foreach($res as $row) {
            if (!empty($row['type_id'])) {
                $url = createTypeFullLink($site,$row['type_id']).'/'.$row['id'];
            }

            $videoid = $row['content'];
            $videoid = get_youtube_video_id($videoid);

            if ($site->data->language == LANG_RU) {
                $temestr=russian_datetime_from_timestamp($row['unixtime']);
            } else {
                $temestr=en_datetime_from_timestamp($row['unixtime']);
            }

            $videoInfo = FileGetInfo($site, $row['videoid']);
            
            if ($template == 'big' || $template == 'mega') {
                ?>
                <a class='normalblock'
                     href="<?= $url ?>">
                    <div class="mt-video xmedia s 289x186">
                        <div class='image_container_middle media-content'
                             style="background-image: url('http://img.youtube.com/vi/<?= $videoid ?>/mqdefault.jpg');">
                            <div class='image_container_icon_middle'>
                                <img class='svg playsmall-icon-middle' alt='Просмотр'
                                    src='/templates/khabkrai/img/play.svg' />
                            </div>
                        </div>
                        <div class="media-textview">Видеоролик: <?=
                            htmlspecialchars($videoInfo['name']) ?>
                        </div>
                    </div>
                    <p class='material_list_date'><?= $temestr ?></p>
                    <p class='vidget_name'><?= $row['name'] ?></p>
                </a>
                <?php
            } else {
                ?>
                <a href="<?= $url ?>">
                    <div class="xmedia mt-video s187">
                        <div class='image_container_middle media-content'
                             style="background-image: url('http://img.youtube.com/vi/<?= $videoid ?>/mqdefault.jpg'); background-size: 100%">
                            <div class='image_container_icon_middle'>
                                <img class='svg playsmall-icon-middle' alt='Просмотр'
                                     src='/templates/khabkrai/img/play.svg' />
                            </div>
                        </div>
                        <div class="media-textview">Видеоролик: <?=
                            htmlspecialchars($videoInfo['name']) ?></div>
                    </div>
                    <p class='material_list_date'><?= $temestr ?></p>
                    <p class='vidget_name'><?= $row['name'] ?></p>
                </a>
                <?php
            }
        }
    }

    $videos = ob_get_clean();

    if ($site->data->language == LANG_RU) {
        $photoname = 'Фото';
        $videoname = 'Видео';
        $onlinename = 'Online';
    } else {
        $photoname = 'Photo';
        $videoname = 'Video';
        $onlinename = 'Online';
    }

    ob_start();
    //формируем блок вывода
    if (($template == 'big')||($template == 'mega')) {
        ?>
        <div class="vidget_photo_video">
            <div class="spacer"></div>
            <div class="option active" id="vidget_photo" onclick="vidget_selectPhoto();">
                <?= $photoname ?>
            </div>
            <?php

        if (!empty($videos)) {
            ?>
            <div class="option" id="vidget_video" onclick="vidget_selectVideo();"><?=
                $videoname
            ?>
            </div>
            <?php
        }

        if (!empty($onlines)) {
            ?>
            <div class="option" id="vidget_online" onclick="vidget_selectOnline();"><?=
                $onlinename
            ?>
            </div>
            <?php
        }

        ?>
        </div>
        <div class="vidget_photo_content" id="vidget_photo_content"><?=
            $photos
        ?>
        </div>
        <div class="vidget_photo_content" id="vidget_video_content"><?= $videos ?></div>
        <div class="vidget_online_content" id="vidget_online_content"><?= $onlines ?></div>
        <?php
    } else {
        ?>
        <div class="vidget_photo_video">
            <div class="spacer"></div>
            <div class="option active" id="vidget_photo" onclick="vidget_selectPhoto();"><?=
                $photoname
            ?>
            </div>
            <div class="option" id="vidget_video" onclick="vidget_selectVideo();"><?=
                $videoname
            ?></div>
        </div>
        <div class="vidget_photo_content" id="vidget_photo_content"><?=
            $photos
        ?></div>
        <div class="vidget_video_content" id="vidget_video_content"><?=
            $videos
        ?></div>
        <?php
    }

    return ob_get_clean();
}

//список материалов выбранного типа
function vidjet_right_list($site, $typeid)
{
    $mats = MaterialList($site, $typeid, 1, 4, 2);

    $name = '';
    $query = 'SELECT "name" FROM "material_types" WHERE "id" = $1';
    $res = $site->dbquery($query, array($typeid));
    if ( ! empty($res)) {
        $name=$res[0]['name'];
    }

    ob_start();

    //формируем блок вывода
    ?>
    <div class="doubleright">
        <div class="vidget_right_header">
            <?= $name ?>
        </div>
        <div class="vidget_photo_content" style='margin: 15px;'><?php
            if ( ! empty($mats)) {
                foreach ($mats as $mat) {
                    ?>
                    <p>&nbsp;</p>
                    <p class='material_list_date'><?=
                        russian_datetime_from_timestamp($mat['unixtime'])
                    ?></p>
                    <p><?= $mat['name'] ?></p>
                    <?php
                }
            }

            ?>
        </div>
        <div class='widget-news-link-blue link-btn'>
            <a href="<?= createTypeFullLink($site, $typeid) ?>">Смотреть все</a>
        </div>
    </div>
    <?php

    return ob_get_clean();

}

function vidjet_right_regioninfo($site, $regionchar)
{
    //получаем id региона
    $query = 'SELECT "id" FROM "materials" WHERE "status_id" = $2 AND "id_char" = $1';
    $res = $site->dbquery($query, array($regionchar, STATUS_ACTIVE));

    ob_start();

    $out = 0;

    if (!empty($res)) {
        if (count($res) > 0) {
            $rid=$res[0]['id'];
            //получаем материал региона
            $reg = MaterialGet($site, $rid);

            $rega = '';

            //получаем ОМСУ района
            $querya = 'SELECT "material_id" FROM "extra_materials" WHERE "valuemat" = $1 AND "type_id" = $2';
            $q_params = array($reg['id'], 12);
            $resa = $site->dbquery($querya, $q_params);
            if (!empty($resa)) {
                $rega = MaterialGet($site, $resa[0]['material_id']);
            }

            //блок фото/названия региона

            if (!empty($reg['photos']) && !empty($reg['photos'][0]['id'])) {
                $photo = $reg['photos'][0];
                ?>
                <p style='text-align: center;'>
                    <span class="xmedia mt-image s200">
                        <img src='/photos/<?= $photo['id'] ?>_x370.jpg' width='200'
                             class="media-content"/>
                        <span calss=media-textview">Изображение: <?=
                            htmlspecialchars($photo['name']) ?></span>
                    </span>
                </p>
                <?php
                ++$out;
            }

            ?><p style="font-size:16px; font-weight: bold;"><?= $reg['name'] ?></p><?php

            //блок дополн. материалов
            foreach ($reg['connected_materials'] as $cmat) {
                if ($cmat['type_id'] == '120') {
                    if (!empty($out)) {
                        ?><hr style="margin-top: 10px; margin-bottom: 10px;"><?php
                    }

                    if (!empty($cmat['photo'])) {
                        ?>
                        <p>
                            <img src='/photos/<?= $cmat['photo'] ?>_x100.jpg' width='65' align='left'
                                 style='padding-right: 10px;'>
                        </p>
                        <?php
                    }

                    ?>
                    <p style="font-weight: bold;"><?= $cmat['name'] ?></p>
                    <?= $cmat['content'] ?><br clear="left">
                    <?php
                    ++$out;
                }
            }
            //блок главы. района
            if (!empty($reg['params'][9]['value'])) {
                $glava = MaterialGet($site, $reg['params'][9]['value']);
                if (!empty($out)) {
                    ?><hr style="margin-top: 10px; margin-bottom: 10px;"><?php
                }

                if (!empty($glava['photos']) && !empty($glava['photos'][0]['id'])) {
                    $photo = $glava['photos'][0];
                    ?>
                    <p>
                        <span class="xmedia mt-image s100">
                            <img src='/photos/<?= $photo['id'] ?>_x100.jpg' width='65'
                                 align='left' style='padding-right: 10px;' class="media-content">
                            <span class="media-textview">Изображение: <?=
                                htmlspecialchars($photo['name']) ?></span>
                        </span>
                    </p>
                    <?php
                }

                ?>
                <p style="font-weight: bold;"><?= $glava['name'] ?></p>
                <?= $glava['anons'] ?>
                <br clear="left">
                <?php
                $out++;
            }

            if (is_array($rega)) {
                ?>
                <p>&nbsp;</p>
                <p>
                    <a href="<?= htmlspecialchars($rega['url']) ?>"><?=
                        htmlspecialchars($rega['name'])
                    ?></a>
                </p>
                <?php
            }
        }
    }

    $out = ob_get_clean();

    if (!empty($out)) {
        ob_start();
        ?>
        <div class='doubleright'>
            <div style='padding:30px;'><?= $out ?></div>
        </div>
        <?php
        $out = ob_get_clean();
    }

    return $out;
}

function vidjet_right_photovideo($site, $types='')
{
    if ($types == '')
    {
        //определяем типы в зависимости от характеристик сайта
        if($site->values->maintype == 1)
        {
            $types=Array(11,12);
            $videohref='/events/video';
            $photohref='/events/photo';
        }
        elseif($site->values->maintype == 5)
        {
            $types=Array(30,35);
            $videohref='/governor/performances/governor-video';
            $photohref='/governor/governor-photo';
        }
        else
        {
            $types=Array(11,12);
            $videohref='/events/video';
            $photohref='/events/photo';
        }

    } else {

        $videohref='';
        $photohref='';

        foreach ($types as $type) {
            //проверяем тип
            $query='SELECT "template" FROM "material_types" WHERE "id" = '.$type;

            if($res=$site->dbquery($query))
            {
                if(count($res) > 0)
                {
                    $template=$res[0]['template'];
                    if($template == '4')
                    {
                        if($photohref == '')
                        {
                            $photohref=GetMaterialTypeUrl($site,$type);
                        }
                    }
                    else{
                        if($videohref == '')
                        {
                            $videohref=GetMaterialTypeUrl($site,$type);
                        }
                    }
                }
            }

        }

    }

    //формируем список всех типов материалов фотографий

    $i=0;

    $q_parts = array();
    $q_params = array();

    foreach($types as $type)
    {
        ++$i;

        $q_params[] = $type;
        $phType = '$'.count($q_params);

        $q_parts[] = <<<EOS
SELECT *
FROM (
    WITH RECURSIVE temp1(id, parent_id, PATH, LEVEL) as
    (
        SELECT
            t1.id,
            t1.parent_id,
            CAST(t1.id as VARCHAR(11)) as PATH,
            1 FROM material_types t1
        WHERE t1.id = $phType

        UNION

        SELECT
            t2.id,
            t2.parent_id,
            CAST (temp1.PATH || '->' || t2.id AS VARCHAR(11)),
            LEVEL + 1
        FROM material_types t2
        INNER JOIN temp1 ON(temp1.id = t2.parent_id)
    )
    SELECT *
    FROM temp1
) f$i
EOS;
    }

    $query = implode(' UNION ALL ', $q_parts);
    $res = $site->dbquery($query, $q_params);

    $materialtypes = array();

    if (!empty($res)) {
        foreach($res as $row)
            $materialtypes[] = $row['id'];
    }

    //получаем последние фотоальбомы

    $idsp='';

    $query = <<<EOS
SELECT
    "fc"."material_id"
FROM
    "file_connections" "fc",
    "files" "f",
    "materials" "m"
WHERE
    "fc"."file_id" = "f"."id"
    AND "fc"."material_id" = "m"."id"
    AND "f"."type_id" = $1
    AND "m"."type_id" IN ($2)
EOS;

    $q_params = array(1, $materialtypes);
    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $res = $site->dbquery($query, $q_params);

    if (!empty($res)) {
        foreach($res as $row)
            $idsp[] = $row['material_id'];
    }

    $idsp = implode(',', $idsp);
    $photoarr = MaterialList($site, '', 1, 4, STATUS_ACTIVE, $idsp);
    
    ob_start();

    if (!empty($photoarr)) {
        foreach ($photoarr as $row) {
            if($site->data->language == LANG_RU)
            {
                $temestr = russian_datetime_from_timestamp($row['unixtime']);
            } else {
                $temestr = en_datetime_from_timestamp($row['unixtime']);
            }

            $photourl = $row['url'];

            ?>
            <div class="photo_block xmedia mt-image s210" onclick="document.location.href='<?= $photourl ?>';">
                <span class="media-content">
                    <img src='/photos/<?= $row['photo'] ?>_x210.jpg' />
                    <span class='material_list_date'><?= $temestr ?></span>
                </span>
                <p class='vidget_name media-textview'><?= $row['name'] ?></p>
            </div>
            <?php
        }
    }
    
    $photos = ob_get_clean();


    $query = <<<EOS
SELECT
    "fc"."material_id"
FROM
    "file_connections" "fc",
    "files" "f",
    "materials" "m"
WHERE
    "fc"."file_id" = "f"."id"
    AND "fc"."material_id" = "m"."id"
    AND "f"."type_id" = $1
    AND "m"."type_id" IN ($2)
EOS;

    $q_params = array(2, $materialtypes);
    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $res = $site->dbquery($query, $q_params);
    $idsv = array();

    if (!empty($res)) {
        foreach($res as $row)
            $idsv[] = $row['material_id'];
    }

    $idsv = implode(',', $idsv);

    $videoarr = MaterialList($site, '', 1, 4, STATUS_ACTIVE, $idsv);
    
    ob_start();

    if (!empty($videoarr)) {
        foreach ($videoarr as $row) {
            if ($site->data->language == LANG_RU) {
                $temestr = russian_datetime_from_timestamp($row['unixtime']);
            } else {
                $temestr = en_datetime_from_timestamp($row['unixtime']);
            }

            $videourl = $row['url'];
            $videoid = $row['video'];
            $videoid = str_replace('https://www.youtube.com/watch?v=','',$videoid);
            $videoid = str_replace('http://www.youtube.com/watch?v=','',$videoid);
            $videoid = str_replace('http://www.youtube.com/embed/','',$videoid);

            ?>
            <div class='image_container_middle' onclick="document.location.href='<?= $videourl ?>'"
                 style="background-image: url('http://img.youtube.com/vi/<?= $videoid ?>/mqdefault.jpg'); background-size: 100%">
                <div class='image_container_icon_middle'>
                    <img class='svg playsmall-icon-middle' src='/templates/khabkrai/img/play.svg' />
                </div>
            </div>
            <p class='material_list_date'><?= $temestr ?></p>
            <p class='vidget_name'><?= $row['name'] ?></p>
            <?php
        }
    }

    $videos = ob_get_clean();

    if ($site->data->language == LANG_RU) {
        $photoname='Фото';
        $videoname='Видео';
        $lookall='Смотреть все';
    } else {
        $photoname='Photo';
        $videoname='Video';
        $lookall='more';
    }

    ob_start();

    //формируем блок вывода 
    ?>
    <div class="doubleright">
        <div class="vidget_photo_video">
            <div class="spacer"></div>
            <div class="option active" id="vidget_photo" onclick="vidget_selectPhoto();"><?=
                $photoname
            ?></div>
            <div class="option" id="vidget_video" onclick="vidget_selectVideo();"><?=
                $videoname
            ?></div>
        </div>
        <div class="vidget_photo_content" id="vidget_photo_content">
            <?= $photos ?>
            <div class='widget-news-link-blue link-btn' style='margin: 0px;'>
                <a href='<?= $photohref ?>'><?= $lookall ?></a>
            </div>
        </div>
        <div class="vidget_video_content" id="vidget_video_content" style="display: none;">
            <?= $videos ?>
            <div class='widget-news-link-blue link-btn'>
                <a href='<?= $videohref ?>'><?= $lookall ?></a>
            </div>
        </div>
    </div>
    <?php

    return ob_get_clean();
}

function bigCalendar($site, $date='', $events)
{

    if ($date=='')
        $date = date('d.m.Y');

    $timestamp = MakeTimeStampFromStr($date);
    $fullmonth = date('m.Y',$timestamp);
    $mounth = date('m',$timestamp);

    if($site->data->language == 1)
    {
        switch($mounth)
        {
            case 1: $m='Январь'; break;
            case 2: $m='Февраль'; break;
            case 3: $m='Март'; break;
            case 4: $m='Апрель'; break;
            case 5: $m='Май'; break;
            case 6: $m='Июнь'; break;
            case 7: $m='Июль'; break;
            case 8: $m='Август'; break;
            case 9: $m='Сентябрь'; break;
            case 10: $m='Октябрь'; break;
            case 11: $m='Ноябрь'; break;
            case 12: $m='Декабрь'; break;
        }
    } else {
        switch($mounth)
        {
            case 1: $m='January'; break;
            case 2: $m='February'; break;
            case 3: $m='March'; break;
            case 4: $m='April'; break;
            case 5: $m='May'; break;
            case 6: $m='June'; break;
            case 7: $m='July'; break;
            case 8: $m='August'; break;
            case 9: $m='September'; break;
            case 10: $m='October'; break;
            case 11: $m='November'; break;
            case 12: $m='December'; break;
        }
    }

    $y = date('Y', $timestamp);

    //формируем сам календарь
    $dayofmonth = date('t', $timestamp);
    $day_count = 1;

    // 1. Первая неделя
    $num = 0;
    for ($i = 0; $i < 7; $i++) {
        // Вычисляем номер дня недели для числа
        $dayofweek = date('w',
            mktime(0, 0, 0, date('m',$timestamp), $day_count, date('Y',$timestamp)));
        // Приводим к числа к формату 1 - понедельник, ..., 6 - суббота
        $dayofweek = $dayofweek - 1;
        if($dayofweek == -1) $dayofweek = 6;

        if($dayofweek == $i)
        {
            // Если дни недели совпадают,
            // заполняем массив $week
            // числами месяца
            $week[$num][$i] = $day_count;
            $day_count++;
        }
        else
        {
            $week[$num][$i] = "";
        }
    }

    // 2. Последующие недели месяца
    while(true)
    {
        $num++;
        for($i = 0; $i < 7; $i++)
        {
            $week[$num][$i] = $day_count;
            $day_count++;
            // Если достигли конца месяца - выходим
            // из цикла
            if($day_count > $dayofmonth) break;
        }
        // Если достигли конца месяца - выходим
        // из цикла
        if($day_count > $dayofmonth) break;
    }

    // 3. Выводим содержимое массива $week
    // в виде календаря
    // Выводим таблицу
    if($site->data->language == 1)
    {
        $ctable= "<table class='bigcalend'>
    <tr>
        <th>Понедельник</th>
        <th>Вторник</th>
        <th>Среда</th>
        <th>Четверг</th>
        <th>Пятница</th>
        <th>Суббота</th>
        <th>Воскресенье</th>
    </tr>";
    }
    else{
        $ctable= "<table class='bigcalend'>
    <tr>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thursday</th>
        <th>Friday</th>
        <th>Saturday</th>
        <th>Sunday</th>
    </tr>";
    }

    for($i = 0; $i < count($week); $i++)
    {
        $ctable.="<tr>";
        for($j = 0; $j < 7; $j++)
        {
            if(!empty($week[$i][$j]))
            {
                // Если имеем дело с субботой и воскресенья
                // подсвечиваем их

                $arrayname=$week[$i][$j];
                if(mb_strlen($arrayname,'UTF8') < 2)
                {
                    $arrayname='0'.$arrayname;
                }

                //if((in_array($week[$i][$j],$events))||(in_array('0'.$week[$i][$j],$events)))
                if(isset($events[$arrayname])&&is_array($events[$arrayname]))
                {
                    //$color='#1e64b3';
                    //$astart='<a href="/events/for-mass-media&year='.$site->values->year.'&month='.$site->values->month.'&day='.$week[$i][$j].'">'.$week[$i][$j].'</a>';
                    $eventtext='';

                    if(count($events[$arrayname]) > 1)
                    {
                        $eventtext='Событий: '.count($events[$arrayname]);
                    }
                    else{
                        $eventtext=$events[$arrayname][0]['name'];
                    }


                    //формируем текст всплывающего окна
                    $windowcontent='';
                    foreach($events[$arrayname] as $ev)
                    {

                        //дата события
                        $timestamp=$ev['unixtime'];

                        //кол-во лет до события
                        $yeardate=$arrayname.'.'.$fullmonth;

                        //print($yeardate);

                        $yeartimestamp=MakeTimeStampFromStr($yeardate);

                        $years=$yeartimestamp-$timestamp;

                        $years=round(($years)/(60*60*24*365));

                        if($years > 0)
                        {
                            if($site->data->language=='1')
                            {
                                $yearsstr=get_correct_str($years, 'год', 'года', 'лет');
                            }
                            else{
                                $yearsstr=get_correct_str($years, 'year', 'years', 'years');
                            }

                        }
                        else
                        {
                            $yearsstr='';
                        }

                        if($yearsstr != '')
                        {
                            $windowcontent.="<div class='normalblock bigcalend_window_years'>$yearsstr</div>";
                        }

                        $datestr=russian_datetime_from_timestamp($ev['unixtime'],false);
                        $windowcontent.="<div class='normalblock bigcalend_window_date'>$datestr</div>";


                        //заголовок
                        $windowcontent.="<p>&nbsp;</p><p><strong>$ev[name]</strong></p>";

                        //анонс
                        $windowcontent.="<p style='font-size: 14px; color:#666666;'>$ev[anons]</p><p>&nbsp;</p>";

                    }


                    $astart=$week[$i][$j].'&nbsp;<div onclick="showBigCalendEvent('."'".$arrayname."'".');" class="bigcalend_event">'.$eventtext.'</div><div style="display:none;"><div class="bigcalend_event_content" id="event_'.$arrayname.'">'.$windowcontent.'</div></div>';
                    //$astart=$week[$i][$j].'<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>';
                }
                else
                {
                    //$color='#cbcbcb';
                    $astart=$week[$i][$j].'&nbsp;<div class="bigcalend_event_null">&nbsp;</div>';
                }

                //if($j == 5 || $j == 6)
                $ctable.= "<td>".$astart."</td>";
                //else $ctable.= "<td>".$week[$i][$j]."</td>";
            }
            else $ctable.= "<td>&nbsp;</td>";
        }
        $ctable.= "</tr>";
    }
    $ctable.= "</table>";

    //print_r($site);

    //формируем блок вывода
    $vidjet=$ctable;




    return($vidjet);
}

function vidjet_right_smi_calendar($site, $date='')
{
    if($date=='')
    {
        $date=date('d.m.Y');
    }

    $date='01.'.$site->values->month.'.'.$site->values->year;

    $timestamp=MakeTimeStampFromStr($date);

    //формируем массив дней с анонсами в базе
    $datadays=Array();
    $query='SELECT TO_CHAR("date_event",'."'".'dd'."'".') "day" FROM "materials"
    WHERE TO_CHAR("date_event",'."'".'MM.YYYY'."'".') = '."'".''.$site->values->month.'.'.$site->values->year.''."'".'
    AND "type_id" = '.$site->values->documenttype;
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                $datadays[]=$row['day'];
            }
        }
    }

    $mounth=date('m',$timestamp);
    switch($mounth)
    {
        case 1: $m='Январь'; break;
        case 2: $m='Февраль'; break;
        case 3: $m='Март'; break;
        case 4: $m='Апрель'; break;
        case 5: $m='Май'; break;
        case 6: $m='Июнь'; break;
        case 7: $m='Июль'; break;
        case 8: $m='Август'; break;
        case 9: $m='Сентябрь'; break;
        case 10: $m='Октябрь'; break;
        case 11: $m='Ноябрь'; break;
        case 12: $m='Декабрь'; break;
    }

    $y=date('Y',$timestamp);


    //формируем сам календарь
    $dayofmonth = date('t',$timestamp);
    $day_count = 1;

    // 1. Первая неделя
    $num = 0;
    for ($i = 0; $i < 7; $i++) {
        // Вычисляем номер дня недели для числа
        $dayofweek = date('w',
            mktime(0, 0, 0, date('m',$timestamp), $day_count, date('Y',$timestamp)));
        // Приводим к числа к формату 1 - понедельник, ..., 6 - суббота
        $dayofweek = $dayofweek - 1;
        if($dayofweek == -1) $dayofweek = 6;

        if ($dayofweek == $i) {
            // Если дни недели совпадают,
            // заполняем массив $week
            // числами месяца
            $week[$num][$i] = $day_count;
            $day_count++;
        } else {
            $week[$num][$i] = "";
        }
    }

    // 2. Последующие недели месяца
    while (true) {
        $num++;
        for ($i = 0; $i < 7; $i++) {
            $week[$num][$i] = $day_count;
            $day_count++;
            // Если достигли конца месяца - выходим
            // из цикла
            if($day_count > $dayofmonth) break;
        }
        // Если достигли конца месяца - выходим
        // из цикла
        if($day_count > $dayofmonth) break;
    }

    // 3. Выводим содержимое массива $week
    // в виде календаря
    // Выводим таблицу
    $ctable= "<table class='smallcalend'>
    <tr>
        <th>Пн</th>
        <th>Вт</th>
        <th>Ср</th>
        <th>Чт</th>
        <th>Пт</th>
        <th style='color: #cbcbcb;'>Сб</th>
        <th style='color: #cbcbcb;'>Вс</th>
    </tr>";
    for($i = 0; $i < count($week); $i++)
    {
        $ctable.="<tr>";
        for($j = 0; $j < 7; $j++)
        {
            if(!empty($week[$i][$j]))
            {
                // Если имеем дело с субботой и воскресенья
                // подсвечиваем их

                if(in_array($week[$i][$j],$datadays))
                {
                    $color='#1e64b3';
                    $astart='<a href="./?region='.$site->values->region.'&documenttype='.$site->values->documenttype.'&year='.$site->values->year.'&month='.$site->values->month.'&day='.$week[$i][$j].'">'.$week[$i][$j].'</a>';
                }
                else
                {
                    $color='#cbcbcb';
                    $astart=$week[$i][$j];
                }


                //if($j == 5 || $j == 6)
                $ctable.= "<td style='color:".$color.";'>".$astart."</td>";
                //else $ctable.= "<td>".$week[$i][$j]."</td>";
            }
            else $ctable.= "<td>&nbsp;</td>";
        }
        $ctable.= "</tr>";
    }
    $ctable.= "</table>";

    //print_r($site);

    //формируем блок вывода
    $vidjet="<div class='smallcalendar_body'>
    <p class='smallcalendar_header'>Календарь</p><input type='hidden' name='smallcalendardate' id='smallcalendardate'>
    <div style='position: absolute; width: 230px;'><div class='normalblock'><select id='monthselector' class='styler' onchange='vidget_selectSmiDate();'>";

    //выводим месяцы
    if($site->values->month == '01')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='01' $selected>Январь</option>";

    if($site->values->month == '02')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='02' $selected>Февраль</option>";

    if($site->values->month == '03')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='03' $selected>Март</option>";

    if($site->values->month == '04')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='04' $selected>Апрель</option>";

    if($site->values->month == '05')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='05' $selected>Май</option>";

    if($site->values->month == '06')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='06' $selected>Июнь</option>";

    if($site->values->month == '07')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='07' $selected>Июль</option>";

    if($site->values->month == '08')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='08' $selected>Август</option>";

    if($site->values->month == '09')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='09' $selected>Сентабрь</option>";

    if($site->values->month == '10')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='10' $selected>Октябрь</option>";

    if($site->values->month == '11')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='11' $selected>Ноябрь</option>";

    if($site->values->month == '12')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='12' $selected>Декабрь</option>";

    $vidjet.="</select></div>&nbsp;<div class='normalblock'><select id='yearselector' class='styler' onchange='vidget_selectSmiDate();'>";

    //получаем доступныегоды с анонсами
    $query='SELECT DISTINCT TO_CHAR("date_event",'."'".'YYYY'."'".') "year" FROM "materials" WHERE "type_id" = '.$site->values->documenttype.' ORDER BY "year" ASC';
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                if($row['year'] == $site->values->year)
                {
                    $selected='SELECTED';
                }
                else
                {
                    $selected='';
                }
                $vidjet.="<option value='$row[year]' $selected>$row[year]</option>";
            }
        }
    }

    $vidjet.="</select></div></div><p>&nbsp;</p><p>&nbsp;</p>
    $ctable
    </div>";

    return($vidjet);
}

function vidjet_right_anons_calendar($site, $date='', $type='',$link='')
{
    if($type == '')
    {
        $type=$site->data->atype;
    }

    if($date=='')
    {
        $date=date('d.m.Y');
    }

    if($site->values->month != '')
    {
        $date='01.'.$site->values->month.'.'.$site->values->year;
    }


    $url=GetMaterialTypeUrl($site, $type);

    $timestamp=MakeTimeStampFromStr($date);

    //формируем массив дней с анонсами в базе
    $datadays=Array();
    $query='SELECT TO_CHAR("date_event",'."'".'dd'."'".') "day" FROM "materials"
    WHERE TO_CHAR("date_event",'."'".'MM.YYYY'."'".') = '."'".''.$site->values->month.'.'.$site->values->year.''."'".'
    AND "type_id" = '.$type;

    if ($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                $datadays[]=$row['day'];
            }
        }
    }

    $mounth=date('m',$timestamp);
    switch($mounth)
    {
        case 1: $m='Январь'; break;
        case 2: $m='Февраль'; break;
        case 3: $m='Март'; break;
        case 4: $m='Апрель'; break;
        case 5: $m='Май'; break;
        case 6: $m='Июнь'; break;
        case 7: $m='Июль'; break;
        case 8: $m='Август'; break;
        case 9: $m='Сентябрь'; break;
        case 10: $m='Октябрь'; break;
        case 11: $m='Ноябрь'; break;
        case 12: $m='Декабрь'; break;
    }

    $y=date('Y',$timestamp);

    //формируем сам календарь
    $dayofmonth = date('t',$timestamp);
    $day_count = 1;

    // 1. Первая неделя
    $num = 0;
    for($i = 0; $i < 7; $i++)
    {
        // Вычисляем номер дня недели для числа
        $dayofweek = date('w',
            mktime(0, 0, 0, date('m',$timestamp), $day_count, date('Y',$timestamp)));
        // Приводим к числа к формату 1 - понедельник, ..., 6 - суббота
        $dayofweek = $dayofweek - 1;
        if($dayofweek == -1) $dayofweek = 6;

        if ($dayofweek == $i) {
            // Если дни недели совпадают,
            // заполняем массив $week
            // числами месяца
            $week[$num][$i] = $day_count;
            $day_count++;
        } else {
            $week[$num][$i] = "";
        }
    }

    // 2. Последующие недели месяца
    while (true) {
        $num++;
        for($i = 0; $i < 7; $i++)
        {
            $week[$num][$i] = $day_count;
            $day_count++;
            // Если достигли конца месяца - выходим
            // из цикла
            if($day_count > $dayofmonth) break;
        }
        // Если достигли конца месяца - выходим
        // из цикла
        if($day_count > $dayofmonth) break;
    }

    // 3. Выводим содержимое массива $week
    // в виде календаря
    // Выводим таблицу
    $ctable= "<table class='smallcalend'>
    <tr>
        <th>Пн</th>
        <th>Вт</th>
        <th>Ср</th>
        <th>Чт</th>
        <th>Пт</th>
        <th style='color: #cbcbcb;'>Сб</th>
        <th style='color: #cbcbcb;'>Вс</th>
    </tr>";
    for($i = 0; $i < count($week); $i++)
    {
        $ctable.="<tr>";
        for($j = 0; $j < 7; $j++)
        {
            if(!empty($week[$i][$j]))
            {
                // Если имеем дело с субботой и воскресенья
                // подсвечиваем их

                if(in_array($week[$i][$j],$datadays))
                {
                    $color='#1e64b3';

                    //формируем ссылку по типу материала
                    $astart='<a href="'.$url.'?year='.$site->values->year.'&month='.$site->values->month.'&day='.$week[$i][$j].'">';

                    $aend='</a>';
                }
                else
                {
                    $color='#cbcbcb';
                    $astart='';
                    $aend='';
                }

                $ctable.= "<td style='color:".$color.";'>".$astart.$week[$i][$j].$aend."</td>";
            } else {
                $ctable.= "<td>&nbsp;</td>";
            }
        }
        $ctable.= "</tr>";
    }
    $ctable.= "</table>";

    //print_r($site);

    //формируем блок вывода
    $vidjet="<div class='smallcalendar_body'>
    <p class='smallcalendar_header'>Календарь</p><input type='hidden' name='smallcalendardate' id='smallcalendardate'>
    <div style='position: absolute; width: 230px;'><div class='normalblock'><select id='monthselector' class='styler' onchange='vidget_selectSmiDate(\"$url\");'>";

    //выводим месяцы
    if($site->values->month == '01')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='01' $selected>Январь</option>";

    if($site->values->month == '02')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='02' $selected>Февраль</option>";

    if($site->values->month == '03')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='03' $selected>Март</option>";

    if($site->values->month == '04')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='04' $selected>Апрель</option>";

    if($site->values->month == '05')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='05' $selected>Май</option>";

    if($site->values->month == '06')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='06' $selected>Июнь</option>";

    if($site->values->month == '07')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='07' $selected>Июль</option>";

    if($site->values->month == '08')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='08' $selected>Август</option>";

    if($site->values->month == '09')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='09' $selected>Сентабрь</option>";

    if($site->values->month == '10')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='10' $selected>Октябрь</option>";

    if($site->values->month == '11')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='11' $selected>Ноябрь</option>";

    if($site->values->month == '12')
    {
        $selected='SELECTED';
    }
    else
    {
        $selected='';
    }
    $vidjet.="<option value='12' $selected>Декабрь</option>";

    $vidjet.="</select></div>&nbsp;<div class='normalblock'><select id='yearselector' class='styler smallwidth' onchange='vidget_selectAnonsDate(\"$url\");'>";

    //получаем доступныегоды с анонсами
    $query='SELECT DISTINCT TO_CHAR("date_event",'."'".'YYYY'."'".') "year" FROM "materials" WHERE "type_id" = 3 ORDER BY "year" ASC';
    if($res=$site->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                if($row['year'] == $site->values->year)
                {
                    $selected='SELECTED';
                }
                else
                {
                    $selected='';
                }
                $vidjet.="<option value='$row[year]' $selected>$row[year]</option>";
            }
        }
    }

    $vidjet.="</select></div></div><p>&nbsp;</p><p>&nbsp;</p>
    $ctable
    </div>";



    return($vidjet);
}

function vidjet_right_calendar($site, $date='')
{
    if($date=='')
    {
        $date=date('d.m.Y');
    }

    $timestamp=MakeTimeStampFromStr($date);

    $mounth=date('m',$timestamp);
    switch($mounth)
    {
        case 1: $m='Январь'; break;
        case 2: $m='Февраль'; break;
        case 3: $m='Март'; break;
        case 4: $m='Апрель'; break;
        case 5: $m='Май'; break;
        case 6: $m='Июнь'; break;
        case 7: $m='Июль'; break;
        case 8: $m='Август'; break;
        case 9: $m='Сентябрь'; break;
        case 10: $m='Октябрь'; break;
        case 11: $m='Ноябрь'; break;
        case 12: $m='Декабрь'; break;
    }

    $y=date('Y',$timestamp);


    //формируем сам календарь
    $dayofmonth = date('t',$timestamp);
    $day_count = 1;

    // 1. Первая неделя
    $num = 0;
    for($i = 0; $i < 7; $i++)
    {
        // Вычисляем номер дня недели для числа
        $dayofweek = date('w',
            mktime(0, 0, 0, date('m',$timestamp), $day_count, date('Y',$timestamp)));
        // Приводим к числа к формату 1 - понедельник, ..., 6 - суббота
        $dayofweek = $dayofweek - 1;
        if($dayofweek == -1) $dayofweek = 6;

        if($dayofweek == $i)
        {
            // Если дни недели совпадают,
            // заполняем массив $week
            // числами месяца
            $week[$num][$i] = $day_count;
            $day_count++;
        }
        else
        {
            $week[$num][$i] = "";
        }
    }

    // 2. Последующие недели месяца
    while(true)
    {
        $num++;
        for($i = 0; $i < 7; $i++)
        {
            $week[$num][$i] = $day_count;
            $day_count++;
            // Если достигли конца месяца - выходим
            // из цикла
            if($day_count > $dayofmonth) break;
        }
        // Если достигли конца месяца - выходим
        // из цикла
        if($day_count > $dayofmonth) break;
    }

    // 3. Выводим содержимое массива $week
    // в виде календаря
    // Выводим таблицу
    $ctable= "<table class='smallcalend'>
    <tr>
        <th>Пн</th>
        <th>Вт</th>
        <th>Ср</th>
        <th>Чт</th>
        <th>Пт</th>
        <th style='color: #cbcbcb;'>Сб</th>
        <th style='color: #cbcbcb;'>Вс</th>
    </tr>";
    for($i = 0; $i < count($week); $i++)
    {
        $ctable.="<tr>";
        for($j = 0; $j < 7; $j++)
        {
            if(!empty($week[$i][$j]))
            {
                // Если имеем дело с субботой и воскресенья
                // подсвечиваем их
                if($j == 5 || $j == 6)
                    $ctable.= "<td style='color:#cbcbcb;'>".$week[$i][$j]."</td>";
                else $ctable.= "<td>".$week[$i][$j]."</td>";
            }
            else $ctable.= "<td>&nbsp;</td>";
        }
        $ctable.= "</tr>";
    }
    $ctable.= "</table>";

    //формируем блок вывода
    $vidjet="<div class='smallcalendar_body'>
    <p class='smallcalendar_header'>Календарь</p><input type='hidden' name='smallcalendardate' id='smallcalendardate'>
    <p class='smallcalendar_mainlinks'><span class='link'>$m</span>&nbsp;&nbsp;<span class='link'>$y</span></p>
    $ctable
    <div class='smallcalendar_btn'>Выбрать материалы за период</div>
    </div>";

    return($vidjet);
}

function vidjet_materiallist_pages_2($page, $perpage, $totalItems, $renderType = 'default')
{
    $ret = '';

    if (empty($perpage))
        $perpage = 1;

    if (ceil(floatval($totalItems) / $perpage) <= 1)
        return '';

    $baseUrl = preg_replace('/&?page=\d*/', '', $_SERVER['REQUEST_URI']);

    if ($totalItems) {
        $renderTypes = array('default', 'bootstrap');
        if (!in_array($renderType, $renderTypes))
            $renderType = $renderTypes[0];

        $renderer = 'vidjet_materiallist_pages_render_'.$renderType;

        if (function_exists($renderer))
            $ret = call_user_func($renderer, $page, $perpage, $totalItems, $baseUrl);


        if ($ret)
            $ret = '<div id="pagination" class="pages">'.$ret.'</div>';
    }

    $ret=str_replace('?&amp;','?',$ret);

    $ret=str_replace('&amp;','&',$ret);

    return $ret;
}

/**
 * @param \iSite $site
 * @param string $ids
 * @param string $types
 * @param string $mquery
 * @param string $renderType
 * @return mixed|string
 */
function vidjet_materiallist_pages($site, $ids='', $types='', $mquery='', $renderType='default', $mstatus = 2)
{
    if(!$mstatus){
        $mstatus = 2;
    }

    $pages=0;

    $site->logWrite(LOG_DEBUG,__METHOD__,__LINE__);

    //получаем кол-во страниц
    //если не переданы id-шники, просто получаем кол-во материалов типа
    if(($ids == '')&&($types == '')) {

     
        $site->logWrite(LOG_DEBUG, __METHOD__, __LINE__);

        $query='SELECT count("id") "cid" FROM "materials" WHERE "type_id" = '.$site->data->atype;

        if($res=$site->dbquery($query))
        {
            $pages=$res[0]['cid'];
        }
        
        $site->logWrite(LOG_DEBUG, __METHOD__, 'pages='.$pages);
    } elseif(($ids != '')&&($types == '')) {//переданы id-шники
    
        $site->logWrite(LOG_DEBUG, __METHOD__, __LINE__);
    
        $query='SELECT count("id") "cid" FROM "materials" WHERE "type_id" = '.$site->data->atype.' AND "id" IN ('.$ids.')';

    }
    //переданы типы без id-шников
    elseif(($ids == '')&&($types != ''))
    {
        $site->logWrite(LOG_DEBUG, __METHOD__, __LINE__);
    
        $query='SELECT count("id") "cid" FROM "materials" WHERE "type_id" IN ('.$types.')';

        //krumo($query);


    } elseif(($ids != '')&&($types != '')) {//переданы и типы и id-шники
       
        $site->logWrite(LOG_DEBUG, __METHOD__, __LINE__);
    
        $query='SELECT count("id") "cid" FROM "materials" WHERE "type_id" IN ('.$types.') AND "id" IN ('.$ids.')';

    }

    $langstypes=Array();
    $langstypes[]=124; //новости
    $langstypes[]=96;  //Инвестиционные предложения
    $langstypes[]=97;  //Успешные проекты
    $langstypes[]=35;  //полезные ресурсы

    if(!empty($site->data->language) && $site->data->language > 2 && $site->settings->DB->name == 'invest' && in_array($types, $langstypes)){

        if($mstatus != 'all')
        {
            $query.=' AND "status_id" = '.$mstatus;
        }

        $query.=' AND language_id IN ('.$site->data->language.', 2) AND id NOT IN(
        
            select en.id FROM materials en WHERE en.language_id = 2 AND en.type_id = '.$types.' AND en.date_event IN 
                    (SELECT own.date_event FROM materials own WHERE own.status_id = 2 AND own.type_id = '.$types.' AND own.language_id = '.$site->data->language.')
        
        )';

    }
    else
    {

        if($mstatus != 'all')
        {
            $query.=' AND "status_id" = '.$mstatus;
        }

        $query.=' AND language_id = '.$site->data->language;
    }


    if (!empty($mquery))
        $query .= ' AND "id" IN('.$mquery.')';

    $site->logWrite(LOG_DEBUG, __METHOD__, 'query='.$query);

    if ($res = $site->dbquery($query)) {
        $pages = $res[0]['cid'];
    }

    $site->logWrite(LOG_DEBUG, __METHOD__, 'new pages='.$pages);

    if ($pages <= 1)
        return '';

    $site->logWrite(LOG_DEBUG, __METHOD__,'page='.$site->values->page);
    $site->logWrite(LOG_DEBUG, __METHOD__,'perPage='.$site->values->perpage);
    $site->logWrite(LOG_DEBUG, __METHOD__,'pages='.$pages);

    return vidjet_materiallist_pages_2(
        $site->values->page,
        $site->values->perpage,
        $pages,
        $renderType
    );
}

/**
 * @param int $page
 * @param int $perpage
 * @param int $pages  Общее число элементов
 * @param string $aurl
 * @return string
 */
function vidjet_materiallist_pages_render_default($page, $perpage, $pages, $aurl)
{
    ob_start();

    $allpages = (int)($pages / $perpage);

    if ($allpages < ($pages / $perpage)) {
        ++$allpages;
    }

    if($cntquest=strpos($aurl,'?'))
    {
        $chr='&';
    }
    else
    {
        $chr='?';
    }

    if ($allpages <= 10) {
        for ($allpage=1; $allpage <= $allpages; ++$allpage) {
            if ($allpage == $page) {
                ?><div class='normalblock page active'><?= $allpage ?></div><?php
            } else {
                ?>
                <a href="<?=
                    htmlspecialchars($aurl.$chr.'page='.$allpage) ?>"
                    class='normalblock page'><?=
                    $allpage
                    ?>
                </a>
                <?php
            }
        }
    } elseif ($page < 9) {
        for ($allpage=1; $allpage <= 9; ++$allpage) {
            if ($allpage == $page) {
                ?><span class='normalblock page active'><?= $allpage ?></span><?php
            } else {
                ?>
                <a href="<?=
                    htmlspecialchars($aurl.$chr.'page='.$allpage) ?>" class='normalblock page'><?=
                    $allpage ?></a>
                <?php
            }
        }

        $midPage = intval(($allpage + $allpages - 1) / 2);
        if ($midPage == $allpages - 1)
            $midPage--;
        ?>
        <a href="<?= htmlspecialchars($aurl.$chr.'page='.$midPage) ?>" class='normalblock page'
            title="Страница <?= $midPage ?>">...</a>
        <a href="<?=
            htmlspecialchars($aurl.$chr."page=".($allpages-1)) ?>"
             class='normalblock page'><?= ($allpages-1) ?>
        </a>
        <a href="<?=
            htmlspecialchars($aurl.$chr.'page='.$allpages) ?>"
             class='normalblock page'><?= $allpages
            ?></a>
    <?php
    } elseif ($page == $allpages) {
        for ($allpage=1; $allpage < 8; ++$allpage) {
            ?>
            <a href="<?=
                htmlspecialchars($aurl.$chr.'page='.$allpage) ?>"
                class='normalblock page'><?=
                $allpage
                ?></a>
            <?php

        }
        $midPage = intval(($allpage + $page) / 2);
        if ($midPage == $page)
            $midPage--;

        ?>
        <a href="<?= htmlspecialchars($aurl.$chr.'page='.$midPage) ?>" class='normalblock page'
            title="Страница <?= $midPage ?>">...</a>
        <span class='normalblock page active'><?= $page ?></span>
        <?php

    } else  {
        $lowMidPage = intval($page / 2);
        $highMidPage = intval(($page + 2 + $allpages - 1) / 2);

        if ($highMidPage == $allpages - 1)
            $highMidPage--;
        if ($lowMidPage == 2)
            $lowMidPage++;
        if ($lowMidPage == $page - 1)
            $lowMidPage = null;
        if ($highMidPage == $page + 1)
            $highMidPage = null;
        ?>
        <a href="<?=
            htmlspecialchars($aurl.$chr.'page=1') ?>"
             class='normalblock page'>1</a>
        <?php
        if ($page - 1 != 2) {
            ?>
            <a href="<?= htmlspecialchars($aurl.$chr.'page=2') ?>"
               class='normalblock page'>2</a><?php
        }

        if (isset($lowMidPage)) {
            ?><a href="<?= htmlspecialchars($aurl.$chr.'page='.$lowMidPage) ?>" class='normalblock page'
                style="Страница <?= $lowMidPage ?>">...</a><?php
        }
        ?>
        <a href="<?= htmlspecialchars($aurl.$chr.'page='.($page-1)) ?>"
             class='normalblock page'><?= ($page-1) ?></a>
        <span class='normalblock page active'><?= $page ?></span>
        <a href="<?= htmlspecialchars($aurl.$chr.'page='.($page+1)) ?>"
             class='normalblock page'><?= ($page+1) ?></a>
        <?php
        if (isset($highMidPage)) {
            ?><a href="<?= htmlspecialchars($aurl.$chr.'page='.$highMidPage) ?>" class='normalblock page'
                title="Страница <?= $highMidPage ?>">...</a><?php
        }
        if ($page + 1 != $allpages - 1) {
            ?>
            <a href="<?=
                htmlspecialchars($aurl.$chr.'page='.($allpages-1)) ?>"
                class='normalblock page'><?= ($allpages-1) ?></a><?php
        }
        ?>
        <a href="<?= htmlspecialchars($aurl.$chr.'page='.$allpages) ?>"
             class='normalblock page'><?= $allpages ?></a>
        <?php
    }

    return ob_get_clean();
}

function makeGoodLinkWithSquitz($url)
{

    $out=$url;


    //проверяем на наличае вопроса в строке URL
    if(!mb_strpos($out,'?',null,'utf8'))
    {
        //получаем первое вхождение & в строку URL
        if($firstpos=mb_strpos($out,'&',null,'utf8'))
        {
            $out=mb_substr($out,0,($firstpos),'utf8').'?'.mb_substr($out,($firstpos+1),null,'utf8');
        }
    }

    return $out;
}

function vidjet_materiallist_pages_render_bootstrap($page, $perpage, $pages, $baseUrl)
{
    $perpage=(int)$perpage;

    if($perpage == 0)
    {
        $perpage=10;
    }

    $numPages = intval(ceil($pages / $perpage));
    if ($numPages == 1)
        return '';

    $numLinks = 10;
    $urlTpl = $baseUrl.'&page={{page}}';

    $startPage = $page - intval($numLinks / 2);
    if ($startPage < 1) {
        $startPage = 1;
    } elseif ($startPage > 1 && $numPages > $numLinks) {
        $startPage++;
    }

    $endPage = $startPage + $numLinks - 1;
    if ($endPage > $numPages) {
        $diff = $endPage - $numPages;
        $endPage = $numPages;
        if ($startPage > 1)
            $startPage = max(1, $startPage - $diff);
    } elseif ($endPage < $numPages - 1 && $numPages > $numLinks) {
        $endPage--;
    }

    $items = array();
    if ($startPage > 1) {
        $pageUrl = str_replace('{{page}}', 1, $urlTpl);
        $items[] = '<li><a href="'.htmlspecialchars($pageUrl).'">1</a></li>';
        if ($startPage > 2)
            $items[] = '<li><span>&hellip;</span></li>';
    }
    for ($iPage = $startPage; $iPage <= $endPage; $iPage++) {
        $pageUrl = str_replace('{{page}}', $iPage, $urlTpl);
        if ($iPage == $page)
            $itemClass = ' class="active"';
        else
            $itemClass = '';
        $items[] = '<li'.$itemClass.'><a href="'.htmlspecialchars(makeGoodLinkWithSquitz($pageUrl)).'">'.$iPage.'</a></li>';
    }
    if ($endPage < $numPages) {
        $pageUrl = str_replace('{{page}}', $numPages, $urlTpl);
        if ($endPage < $numPages - 1)
            $items[] = '<li><span>&hellip;</span></li>';
        $items[] = '<li><a href="'.htmlspecialchars(makeGoodLinkWithSquitz($pageUrl)).'" title="Последняя страницы">'.
                $numPages.
            '</a></li>';
    }
    return count($items) ?
        '<nav><ul class="pagination">'.implode('', $items).'</ul></nav>' :
        '';
}

function vidjet_material_region($site, $materilid='')
{
    $vidjet='';


    //определяем id материала
    if($materilid == '')
    {
        if(isset($site->values->id)&&($site->values->id != ''))
        {
            $materilid=$site->values->id;
        }
    }



    if($materilid != '')
    {
        $material=MaterialGet($site, $materilid);
        $tarr=Array(21);
        if(isset($material['connected_materials']))
        {
            foreach($material['connected_materials'] as $conmat)
            {
                if(in_array($conmat['type_id'],$tarr))
                {
                    $vidjet.='&nbsp;<a href="/events/news/&region='.$conmat['id_char'].'">'.$conmat['name'].'</a>';
                }
            }
        }

    }


    if($vidjet != '')
    {
        $vidjet='&nbsp;|'.$vidjet;
    }

    return($vidjet);

}

//Материалы по теме
function vidjet_material_connections($site)
{
    $tarr = Array(21,46,91,100,5);
    
    ob_start();

    if (isset($site->values->id)) {
        $material = MaterialGet($site, $site->values->id);
        if (isset($material['connected_materials'])) {
            foreach ($material['connected_materials'] as $conmat) {
                if (!in_array($conmat['type_id'],$tarr)) {

                    $conmat['date_event'] = russian_datetime_from_timestamp($conmat['unixtime'],
                        false);
                    $conmat['date_edit'] = russian_datetime_from_timestamp($conmat['editunixtime'],
                        false);

                    ?>
                    <a class='material_list item-link' href="<?= $conmat['url'] ?>">
                        <span class='material_list_content'>
                            <span class="date-pub">Опубликовано <?= $conmat['date_edit'] ?></span>
                            <p class='material_list_name'><?= $conmat['name'] ?></p>
                        </span>
                    </a>
                    <?php
                }
            }
        }
    }
    
    return ob_get_clean();
}

function vidjet_material_params($site, $material='')
{
    $out='';

    if($material == '')
    {
        if(isset($site->values->id))
        {
            $material=MaterialGet($site, $site->values->id);
        }
    }

    if(is_array($material))
    {
        if(isset($material['connected_materials']))
        {

            $istochniks=Array();
            $ministries=Array();

            $is_types=Array();
            $min_types=Array(46,91,100);

            foreach($material['connected_materials'] as $cm)
            {
                if(in_array($cm['type_id'],$min_types))
                {
                    $ministries[]=$cm;
                }
            }

            if(count($ministries) > 0)
            {
                $minstr='';
                $out.='<hr style="margin-bottom:10px;" />';
                foreach($ministries as $min)
                {
                    if($minstr != '')
                    {
                        $minstr.=' / ';
                    }
                    $minstr.="<a href='$min[url]'>$min[name]</a>";
                }
                $out.="<div class='ministryi'>$minstr</div>";
            }

        }

    }
    return $out;
}

function vidjet_tags($tagstr, $caption = 'Ключевые слова', $baseUrl = '/tags')
{
    global $khabkrai;
    $tags = '';

    if (strpos($baseUrl, '?') === false)
        $baseUrl .= '?';

    if (!empty($tagstr)) {
        $tagsarr = explode(',', $tagstr);

        foreach ($tagsarr as $tag) {
            $tag = trim($tag);
            if (!empty($tags))
                $tags .= ', ';

            $tags .= '<a href="'.htmlspecialchars($baseUrl.'&tag='.urlencode($tag)).'">'.
                htmlspecialchars($tag).
                '</a>';
        }
    }

    if (!empty($tags)) {
        $tags = "<div class='tags'>".
            (!empty($caption) ?
                localize($caption, $khabkrai->data->language, array()).': ' :
                ''
            ).
            "$tags</div>";
    }

    return($tags);
}

function vidjet_contacts($site, $id)
{
    $i=0;
    $out='';

    ++$i;
    $fullmat=MaterialGet($site,$id);
    $contacts='';
    $sched='';
    $req='';

    if(isset($fullmat['contacts']) && count($fullmat['contacts']) > 0)
    {
        foreach($fullmat['contacts'] as $cont)
        {

            $cname=$cont['name'];

            if(isset($cont['addr'])&&($cont['addr'] != ''))
            {
                $contacts.=$cont['addr'];
            }
            if(isset($cont['building'])&&($cont['building'] != ''))
            {
                $contacts.=', д. '.$cont['building'];
            }
            if(isset($fullmat['contacts'][0]['office'])&&($fullmat['contacts'][0]['office'] != ''))
            {
                $contacts.=', каб. '.$fullmat['contacts'][0]['office'];
            }

            if(isset($fullmat['contacts'][0]['tel'])&&($fullmat['contacts'][0]['tel'] != ''))
            {
                $req.=$fullmat['contacts'][0]['tel'];
            }

            if(isset($fullmat['contacts'][0]['email'])&&($fullmat['contacts'][0]['email'] != ''))
            {
                $req.='<br>'.$fullmat['contacts'][0]['email'];
            }



            if(isset($fullmat['contacts'][0]['schedules'])&&(count($fullmat['contacts'][0]['schedules']) > 0))
            {
                foreach($fullmat['contacts'][0]['schedules'] as $schedule)
                {
                    if($sched != '')
                    {
                        $sched.='<br>';
                    }
                    $sched.=$schedule['name'].': '.$schedule['content'];
                }
            }

            if($i != '1')
            {
                echo '<hr />';
            }




            $out.= '<div class="commissions-element">
                <div class="normalblock option" style="width: 355px; padding:10px;"><p><strong>'.$cname.'</strong></p></div>
                <div class="normalblock option" style="width: 285px; padding:10px;"><p>'.$contacts.'</p><p style="font-size: 12px;">'.$req.'</p></div>
                <div class="normalblock option" style="padding:10px;"><p>&nbsp;</p><p style="font-size: 12px;">'.$sched.'</p></div>
            </div>';
        }

    }

    return($out);
}

function print_select_options($items, $unselected = '--', $selected = null)
{
    $ret = array();
    if (!is_null($unselected))
        $ret[] = '<option>'.htmlspecialchars($unselected).'</option>';
    foreach ($items as $val => $label) {
        $ret[] = '<option value="'.htmlspecialchars($val).'"'.($selected == $val ? ' selected' : '').'>'.
            htmlspecialchars($label).
            '</option>';
    }
    return implode("\n", $ret);
}

function getUserDataByEmail($site, $email){
    $query  = 'SELECT * FROM users WHERE users.email = $1';
    $params = array($email);
    $res    = $site->dbquery($query, $params);

    return $res ? $res[0] : '';
}

function getUnitsList(\iSite $site){
    $units = array();
    $query = <<<EOS
SELECT *
    FROM
        "units"
    ORDER BY
        "id" DESC
EOS;

    $res = $site->dbquery($query);

    if ( ! empty($res)){
        $units = $res;
    }

    return $units;
}

function getAuthorsList(\iSite $site){
    $authors = array();
    $query = <<<EOS
SELECT *
    FROM
        "authors"
    ORDER BY
        "id" DESC
EOS;

    $res = $site->dbquery($query);

    if ( ! empty($res)){
        $authors = $res;
    }

    return $authors;
}

function htmlPrintSelectOptions($items, $selectedId = null){
    $options = array();

    foreach ($items as $row) {
        if ($row['id'] == $selectedId) {
            $selected = 'SELECTED';
        } else {
            $selected = '';
        }

        $options[] = "<option value=\"{$row['id']}\" $selected>{$row['name']}</option>";
    }

    return implode("\n", $options);
}

function printUnitsOptions($site, $unitId = null){

    $query='SELECT * FROM "units" ORDER BY "id" ASC';
    if (($res=$site->dbquery($query)) !== false)
        return htmlPrintSelectOptions($res, $unitId);

    return '';
}

function printAuthorsOptions($site, $selectedAuthorId = null){
    $query='SELECT * FROM "authors" ORDER BY "id" ASC';

    $res = $site->dbquery($query);

    $options = array("<option value=\"\">--- Авторы ---</option>");

    if ($res != false){

        foreach ($res as $row) {

            if ($row['id'] == $selectedAuthorId) {
                $selected = 'SELECTED';
            } else {
                $selected = '';
            }

            $options[] = "<option value=\"{$row['id']}\" $selected>{$row['surname']} {$row['first_name']} {$row['second_name']}</option>";
        }

        return implode("\n", $options);
    }
    else{
        return implode("\n", $options);
    }
}

function printMainMenuLink($site, $matTypeArr){

    if ($matTypeArr['showinmenu'] == '1') {

        $option = '';

        if ($site->values->maintype == $matTypeArr['id']) {
            $option = 'class="disable-link"';
        }
        ?>

        <a
            href="<?php echo $matTypeArr['url'] ?>"
            <?php echo $option ?>
        >
            <?php echo $matTypeArr['name'] ?>
        </a>

        <?php
    }
}

function printFooterMenuLinks($site, $parentMatTypeId){

    $materialTypes = GetTypeChilds($site, $parentMatTypeId);

    foreach ($materialTypes as $matType):

        $matTypeURL = GetMaterialTypeUrl($site, $matType['id'])

        ?>

        <li class="vertical-links-list__item">
            <a href="<?php echo $matTypeURL ?>" class="footer__link"><?php echo $matType['name'] ?></a>
        </li>

    <?php endforeach;
}

function clearPhone( $telephone, $without_plus_sign = false ){
    $pc = preg_replace('~\D+~','',$telephone);
    if ( strlen( $pc ) > 6 ):
        if($without_plus_sign):
            switch (substr($pc, 0,1) ){
                case 8:
                    $pc = '7'.substr($pc, 1);
                    break;
                case 9:
                    $pc = '7'.$pc;
                    break;
            }
        else:
            switch (substr($pc, 0,1) ){
                case 7:
                    $pc = '+'.$pc;
                    break;
                case 8:
                    $pc = '+7'.substr($pc, 1);
                    break;
                case 9:
                    $pc = '+7'.$pc;
                    break;
            }
        endif;
    endif;
    return $pc;
}

function printSocialLinks($site, $socialLinks, $classModificator = '') {

    $iconName = '';

    foreach ($socialLinks as $socialLinksItem):

        $socialLinksItemExtraParams = GetMaterialParams($site, $socialLinksItem['id']);

        foreach ($socialLinksItemExtraParams as $item):

            if($item['name'] ==  'Ссылка'):

                $urlParts = parse_url($item['value']);

                switch ($urlParts['host']){

                    case 'ru-ru.facebook.com':
                        $iconName = 'facebook';
                        break;

                    case 'twitter.com':
                        $iconName = 'twitter';
                        break;

                    case 'vk.com':
                        $iconName = 'vk';
                        break;

                    case 'www.instagram.com':
                        $iconName = 'instagram';
                        break;
                }

                ?>

                <a href="<?php echo $item['value'] ?>" target="_blank" rel="noopener noreferrer">
                    <svg class="social-links__item social-links__item--footer">
                        <use xlink:href="/img/sprite.svg#content--social--icon-<?php echo $iconName ?>"></use>
                    </svg>
                </a>

                <?php

                break;

            endif;

        endforeach;

    endforeach;
}

function formatDateNews($newsDateString) {

    $months = array(
        '01' => 'Января',
        '02' => 'Февраля',
        '03' => 'Марта',
        '04' => 'Апреля',
        '05' => 'Мая',
        '06' => 'Июня',
        '07' => 'Июля',
        '08' => 'Августа',
        '09' => 'Сентября',
        '10' => 'Октября',
        '11' => 'Ноября',
        '12' => 'Декабря',
    );

    $newsDateString = date('d m Y', strtotime($newsDateString));

    $date = explode( ' ', $newsDateString );

    return implode( ' ', array( $date[0], $months[ $date[1] ], $date[2] ) );
}

function formatDateNewsPeriod($newsDateString) {

    $months = array(
        '01' => 'Январь',
        '02' => 'Февраль',
        '03' => 'Март',
        '04' => 'Апрель',
        '05' => 'Май',
        '06' => 'Июнь',
        '07' => 'Июль',
        '08' => 'Август',
        '09' => 'Сентябрь',
        '10' => 'Октябрь',
        '11' => 'Ноябрь',
        '12' => 'Декабрь',
    );

    $newsDateString = date('d m Y', strtotime($newsDateString));

    $date = explode( ' ', $newsDateString );

    return implode( ', ', array( $months[ $date[1] ], $date[2] ) );
}

function printAudioFilePlayer($audioTitle, $audioFileId) {

    if(iconv_strlen($audioTitle, 'UTF-8') > 31){
        $audioTitle = cuteStringWithEllipsis($audioTitle, 31);
    }

    echo <<<end
<div class="audio-files__item">
    <div class="body-3 audio-files__item-title">$audioTitle</div>
    <audio class="audioPlayer" src="/?menu=getfile&id=$audioFileId" preload="auto"></audio>
    <a href="/?menu=getfile&id=$audioFileId" class="link-download link-download--audio-files" title="Скачать">
        <svg class="link-download__icon link-download__icon--audio-files"><use xlink:href="/img/sprite.svg#decor--iconDownload"></use></svg>
    </a>
</div>
end;

}

function cuteStringWithEllipsis($string, $neededWidth) {
    $string = iconv_substr($string, 0, $neededWidth - 3, 'UTF-8');

    return $string . '...';
}

function printMaterialTileItem($site, $material) {

    $firstPhotoId = MaterialGetFirstPhoto($site, $material['id']);

    if(iconv_strlen($material['name'], 'UTF-8') > 82){
        $material['name'] = cuteStringWithEllipsis($material['name'], 82);
    }

    ?>

    <div class="news-tile-item"
        <?php if($firstPhotoId): ?>
            style="background-image: url('/photos/<?php echo $firstPhotoId ?>_xy630x620.jpg');"
        <?php endif; ?>
    >

        <a href="<?php echo $material['url'] ?>">
            <div class="news-tile-item__wrap-title">
                <div class="label-1 news-tile-item__date"><?php echo formatDateNews($material['date_event']) ?></div>
                <div class="subtitle-2 news-tile-item__title"><?php echo $material['name'] ?></div>
            </div>

            <?php if($firstPhotoId): ?>
                <div class="news-tile-item__blackout"></div>
            <?php endif; ?>

            <div class="label-1 news-tile-item__category-label"><?php echo isset($material['typename']) ? $material['typename'] : $material['type_name'] ?></div>
        </a>

    </div>

    <?php
}

function printMaterialListItem($site, $material, $withLabel = true) {

    ?>

    <div class="news-list-item">

        <?php if($withLabel): ?>
            <div class="label-2 news-list-item__label"><?php echo isset($material['typename']) ? $material['typename'] : $material['type_name'] ?></div>
        <?php endif; ?>

        <div class="label-1 news-list-item__date"><?php echo formatDateNews($material['date_event']) ?></div>

        <a href="<?php echo $material['url'] ?>">
            <div class="subtitle-2 news-list-item__title"><?php echo $material['name'] ?></div>
        </a>

        <?php if(isset($material['anons']) && $material['anons']): ?>
            <div class="news-list-item__description">
                <?php echo $material['anons'] ?>
            </div>
        <?php endif; ?>

    </div>

    <?php
}

function printMaterialListItemWithImage($site, $material) {

    $firstPhotoId = MaterialGetFirstPhoto($site, $material['id']);

    ?>

    <div class="materials-list-with-images-item">
        <a href="<?php echo $material['url'] ?>" class="materials-list-with-images-item__link">
            <img src="/photos/<?php echo $firstPhotoId ?>_xy290x290.jpg"
                 alt="<?php echo $material['name'] ?>"
                 class="materials-list-with-images-item__image">
        </a>
        <div class="materials-list-with-images-item__title">
            <a href="<?php echo $material['url'] ?>" class="materials-list-with-images-item__link">
                <?php echo $material['name'] ?>
            </a>
        </div>
        <div class="materials-list-with-images-item__description">
            <?php echo $material['anons'] ?>
        </div>
    </div>

    <?php
}

function toPlugColorThemes(\iSite $site, $path_prefix = '/'){

    if (isset($site->data->siteColorTheme)) {

        switch ($site->data->siteColorTheme){
            case 'green':
                $site->AttachCSS($path_prefix.'css/edukamCSS/greenTheme.min.css');
                break;
            case 'brown':
                $site->AttachCSS($path_prefix.'css/edukamCSS/brownTheme.min.css');
                break;
        }
    }
}