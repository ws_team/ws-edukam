<?php
/**
 * @var \iSite $this
 */
?>
<div class="main_wrapper filtered-materials-list mainpage_content">
    <?php
    include($this->partial('topban'));
    ?>
<div class="doubleblock main">
<div class="doubleleft">
    <?php
    include($this->partial('tabs'));
    ?>
</div>
<div class="doubleright" style="padding-top: 3px;">
    <?php include($this->partial('sidebar')) ?>
</div>
</div>

<?php

include($this->partial('banners_bottom'));

?>
</div>
