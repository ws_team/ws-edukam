<?php
/**
 * Весь функционал стартового скрипта для любых сайтов WPF.
 *
 * @var \iSite $khabkrai
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


// режим локальной разработки (настройки переопределяются в ./config/any.php)
// для работы в данном режиме все базы данных должны принадлежать одному пользователю postgres и иметь те же имена, что и на продакшене
//define('LOCAL_DEVELOP_MODE', true);

//define('WPF_DEBUG', true);



$starttime = microtime(true);

include_once('bootstrap-defines.php');

if (defined('WPF_DEBUG') && constant('WPF_DEBUG')) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
} else {
    ini_set('display_errors', 0);
}

date_default_timezone_set('Asia/Vladivostok');

mb_internal_encoding('utf-8');

require_once('bootstrap-functions.php');

$path_info = parse_path();

if ( ! preg_match('/^[a-z0-9_\/&=.-]*$/i', $path_info['call']) !== false) {
    header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
    exit;
}

require_once(WPF_ROOT.'/classes/less/lessc.inc.php');

require_once(WPF_ROOT.'/classes/krumo/class.krumo.php');

// Поднимаем Blocks API
require_once(WPF_ROOT.'/classes/blocks/class.blocks.php');

//подключаем нужные классы
require_once(WPF_ROOT.'/classes/wpf/wpf.php');//классы фрэймворка
require_once(WPF_ROOT.'/classes/PHPMailer/class.phpmailer.php');
require_once(WPF_ROOT.'/classes/YampeeRedis/YampeeRedis.php');

//получаем настройки
require_once($_SERVER['DOCUMENT_ROOT'].'/settings.php');

if (file_exists(WPF_ROOT.'/config/any.php'))
    include(WPF_ROOT.'/config/any.php');

if (defined('WPF_ENV') && file_exists($_SERVER['DOCUMENT_ROOT'].'/settings-'.constant('WPF_ENV').'.php'))
    include($_SERVER['DOCUMENT_ROOT'].'/settings-'.constant('WPF_ENV').'.php');
if (file_exists(WPF_ROOT.'/config/any-local.php'))
    include(WPF_ROOT.'/config/any-local.php');

if (file_exists($_SERVER['DOCUMENT_ROOT'].'/settings-local.php'))
    include_once($_SERVER['DOCUMENT_ROOT'].'/settings-local.php');

if (isset($setup['primary_domain'])
    && $_SERVER['HTTP_HOST'] != $setup['primary_domain']
    && ! empty($setup['force_primary_domain'])) {
    header($_SERVER['SERVER_PROTOCOL'].' 301 Moved Permanently');
    $primary_url = 'http'.( ! empty($setup['use_https']) ? 's' : '').
        '://'.$setup['primary_domain'].
        $_SERVER['REQUEST_URI'];
    header('Location: '.$primary_url);
    exit;
}

//настройки пути к рабочей директории
$setup['path'] = rtrim($_SERVER['DOCUMENT_ROOT'], '/');
$setup['pathglobal'] = WPF_ROOT;

$versions = array();

if (file_exists(WPF_ROOT.'/versions.php'))
    include_once(WPF_ROOT.'/versions.php');

//подключаем дополнительные функции
require_once(WPF_ROOT.'/functions.php');

//функции модуля материалов
require_once(WPF_ROOT.'/materials_functions.php');

//формируем подгружаем пользовательские настройки и функции
require_once(WPF_ROOT.'/sitefunctionsandoptions.php');
if (WPF_ROOT != $_SERVER['DOCUMENT_ROOT'] && file_exists($_SERVER['DOCUMENT_ROOT'].'/sitefunctionsandoptions.php'))
    require_once($_SERVER['DOCUMENT_ROOT'].'/sitefunctionsandoptions.php');

require_once(WPF_ROOT.'/specialver_functions.php');

//создаем сайт
$khabkrai = new iSite($setup, $versions, 0);

$khabkrai->data->blocks = new Blocks($_SERVER['DOCUMENT_ROOT'].'/blocks/');

//подключаем WSDL
if ($khabkrai->data->needwsdl)
{
    require_once(WPF_ROOT.'/classes/nusoap/lib/nusoap.php');//класс работы по WSDL
}

if (empty($khabkrai->data->language))
    $khabkrai->data->language = LANG_RU;

if ($khabkrai->data->language == LANG_RU) {
    $khabkrai->data->datestr = DayOfWeek().', '.russian_date(date('d.m.Y'));
} else {
    $khabkrai->data->datestr = enDayOfWeek().', '.en_date(date('d.m.Y'));
}

$khabkrai->values->requeststr = $path_info['call'];

if (defined('WPF_LOCK_ACCESS') && constant('WPF_LOCK_ACCESS')) {
    include_once(WPF_ROOT.'/lock_access.php');
}

bootstrap_requestmodule($khabkrai);

include_once(dirname(__FILE__).'/specialver.php');

if ( ! empty($setup['template']))
    $khabkrai->addTemplate($setup['template']);

$khabkrai->addTemplate('edukam');

$GLOBALS['moduletime']=microtime(true)-$GLOBALS["starttime"];

$khabkrai->makeData();

/*if( $_REQUEST['debug'] ){
    krumo($khabkrai);
}*/

if ($khabkrai->error->flag == 0) {
    //выводим сайт
    $khabkrai->echoHTML();
}

if ($khabkrai->getUser()->isAdmin()) {
    $khabkrai->ShowWarning(0);
    if (!empty($khabkrai->error->flag))
        $khabkrai->ShowError();
}
