const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const sassGlob = require('gulp-sass-glob');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const gcb = require('gulp-callback');
const del = require('del');
const cache = require('gulp-cache');
const imageMin = require('gulp-imagemin');
const runSequence = require('run-sequence');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const uglify = require('gulp-uglify');
const $ = require('gulp-load-plugins')();

const PATH = {
    sourceRoot: './source',
    //distRoot: './dist',
    distEdukamTemplateCSS: '../templates/edukam/css/edukamCSS',
    distEdukamTemplateJS: '../templates/edukam/js/edukamJS'
};

const externalCss = [
    'node_modules/normalize.css/normalize.css',
    'node_modules/slick-carousel/slick/slick.css',
    'node_modules/slick-carousel/slick/slick-theme.css',
    'node_modules/magnific-popup/dist/magnific-popup.css'
];

const externalJs = [
    //'node_modules/jquery/dist/jquery.js',
    'node_modules/slick-carousel/slick/slick.js',
    'source/libs/audiojs/audio.min.js',
    'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
    'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
    'node_modules/svgxuse/svgxuse.js'
];

/*gulp.task('addVersions', () => {
    return gulp.src(`${PATH.distRoot}/*.html`)
        .pipe($.replace(new RegExp('.js"', "g"), `.js?v=${new Date().getTime()}"`))
        .pipe($.replace(new RegExp('.css"', "g"), `.css?v=${new Date().getTime()}"`))
        .pipe(gulp.dest(PATH.distRoot));
});*/

//gulp.task('pages', () => {
//    return gulp.src([`${PATH.sourceRoot}/*.html`, `${PATH.sourceRoot}/*.php`])
//        .pipe(gulp.dest(PATH.distRoot));
        // .pipe(gcb(() => addVersion()));
//});

gulp.task('externalCss', () => {
    return gulp.src(externalCss)
        .pipe(concat('external.min.css'))
        .pipe(cleanCSS({ compatibility: 'ie9' }))
        .pipe(gulp.dest(`${PATH.distEdukamTemplateCSS}`));
});

gulp.task('externalJs', () => {
    return gulp.src(externalJs)
        .pipe(concat('external.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(`${PATH.distEdukamTemplateJS}`));
});

gulp.task('sass', () => {
    return gulp.src(`${PATH.sourceRoot}/styles/main.scss`)
        .pipe(plumber({
            errorHandler: notify.onError(error => {
                return {
                    title: 'Styles',
                    message: error.message,
                };
            }),
        }))
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass())
        .pipe(autoprefixer([
            'last 3 versions',
            '> 1%',
            'ie 8',
            'ie 9',
            'Opera 12.1',
        ]))
        .pipe(cleanCSS({ compatibility: 'ie9' }))
        .pipe($.rename({ suffix: '.min' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(`${PATH.distEdukamTemplateCSS}`));
        //.pipe(browserSync.stream());
});

gulp.task('sassSeparate', () => {
    return gulp.src([`${PATH.sourceRoot}/styles/specialVersions/**/*.scss`, `${PATH.sourceRoot}/styles/themes/**/*.scss`])
        .pipe(plumber({
            errorHandler: notify.onError(error => {
                return {
                    title: 'Separate styles',
                    message: error.message,
                };
            }),
        }))
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass())
        .pipe(autoprefixer([
            'last 3 versions',
            '> 1%',
            'ie 8',
            'ie 9',
            'Opera 12.1',
        ]))
        .pipe(cleanCSS({ compatibility: 'ie9' }))
        .pipe($.rename({ suffix: '.min' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(`${PATH.distEdukamTemplateCSS}`));
        //.pipe(browserSync.stream());
});

gulp.task('js', () => {
    return gulp.src(`${PATH.sourceRoot}/js/**/*.js`)
        .pipe(plumber({
            errorHandler: notify.onError(error => {
                return {
                    title: 'JavaScript',
                    message: error.message,
                };
            })
        }))
        .pipe(sourcemaps.init())
        .pipe(concat('bundle.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(`${PATH.distEdukamTemplateJS}`));
});

//gulp.task('images', () => {
//   return gulp.src([`${PATH.sourceRoot}/img/**/*.*`])
//       .pipe(cache(imageMin({
//           optimizationLevel: 5,
//           progressive: true,
//           interlaced: true
//       })))
//       .pipe(gulp.dest(`${PATH.distRoot}/img`));
//});

//gulp.task('svg', () => {
//   return gulp.src(`${PATH.sourceRoot}/img/**/*.svg`)
//       .pipe($.svgmin({
//           js2svg: { pretty: true },
//       }))
//       .pipe($.cheerio({
//           run: $ => {
//               $('[fill]').removeAttr('fill');
//               $('[stroke]').removeAttr('stroke');
//               $('[style]').removeAttr('style');
//           },
//           parserOptions: { xmlMode: true },
//       }))
//       .pipe($.replace('&gt;', '>'))
//       .pipe($.svgSprite({
//           mode: {
//               symbol: { sprite: '../sprite.svg' },
//           },
//       }))
//       .pipe(gulp.dest(`${PATH.distRoot}/img`));
//});

//gulp.task('fonts', () => {
//   return gulp.src(`${PATH.sourceRoot}/fonts/**/*.*`)
//       .pipe(gulp.dest(`${PATH.distRoot}/fonts`));
//});

//gulp.task('files', () => {
//   return gulp.src(`${PATH.sourceRoot}/files/**/*.*`)
//       .pipe(gulp.dest(`${PATH.distRoot}/files`));
//});

//gulp.task('browserSync', [
//       'fonts',
//       'pages',
//       'files',
//       'images',
//       'svg',
//       'externalCss',
//       'sass',
//       'sassSeparate',
//       'externalJs',
//       'js',
//   ], () => {
//       browserSync.init({
//           server: {
//               baseDir: PATH.distRoot ,
//               directory: true
//           },
//       });
//      browserSync.watch([
//          `${PATH.distRoot}/**/*.*`,
//          '!**/*.css',
//      ], browserSync.reload);
//  }
//);

gulp.task('watch', () => {
    //gulp.watch([`${PATH.sourceRoot}/*.html`, `${PATH.sourceRoot}/*.php`], ['pages']);
    gulp.watch(`${PATH.sourceRoot}/**/*.scss`, ['sass', 'sassSeparate']);
    //gulp.watch([
    //    `${PATH.sourceRoot}/img/**/*.*`
    //], ['images']);
    //gulp.watch(`${PATH.sourceRoot}/files/**/*.*`, ['files']);
    gulp.watch(`${PATH.sourceRoot}/**/*.js`, ['js']);
});

gulp.task('clean', () => {
    //return del(PATH.distRoot, { force: true }).then(() => console.log('dist is clean!'));
    del(PATH.distEdukamTemplateCSS, { force: true }).then(() => console.log('dist CSS is clean!'));
    del(PATH.distEdukamTemplateJS, { force: true }).then(() => console.log('dist JS is clean!'));
});

gulp.task('build', callback => {
    runSequence(['clean'], [
        //'fonts',
        //'pages',
        //'files',
        //'images',
        //'svg',
        'externalCss',
        'sass',
        'sassSeparate',
        'externalJs',
        'js',
    ], callback);
});

gulp.task('default', callback => {
    runSequence(['build'], [
        //'browserSync',
        'watch',
    ], callback);
});
