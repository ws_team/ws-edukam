$(document).ready(function() {

    $('#mobileMenuBtn').on('click', function () {
        var $this = $(this);

        if($this.hasClass('header__btn-menu--active')){
            $this.removeClass('header__btn-menu--active');
            $('#mobileMenu').removeClass('header__menu--main-active');
        }
        else{
            $this.addClass('header__btn-menu--active');
            $('#mobileMenu').addClass('header__menu--main-active');
        }
    });
});
