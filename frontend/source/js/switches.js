$(document).ready(function() {

    function toSwitch ($switchable) {
        if($switchable.hasClass('active')){
            $switchable.removeClass('active');
        }
        else{
            $switchable.addClass('active');
        }
    }

    $('[data-role="innerSwitch"]').on('click', function () {
        var $switchable = $(this).parent('[data-role="switchable"]');
        toSwitch($switchable);
    });

    $('[data-role="switch"]').on('click', function () {
        toSwitch($(this));
    });

    $('[data-role="outerSwitch"]').on('click', function () {
        var $switchable = $(this).children('[data-role="switchable"]');
        toSwitch($(this));
    });

});
