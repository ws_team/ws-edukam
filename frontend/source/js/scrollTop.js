$(document).ready(function() {

    var $buttonUpPage = $('#buttonUpPage');

    $(window).on('scroll load', function() {
        if ($(this).scrollTop() > 300 && $(this).width() > 1490) {
            if ($buttonUpPage.is(':hidden')) {
                $buttonUpPage.css({opacity : 1}).fadeIn('fast');
            }
        } else { $buttonUpPage.stop(true, false).fadeOut('fast'); }
    });

    $buttonUpPage.click(function() {
        $('html, body').stop().animate({scrollTop : 0}, 300);
    });
});
