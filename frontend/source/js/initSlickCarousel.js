$(document).ready(function() {

    $('#mainSectionCarousel').slick({
        arrows: false,
        draggable: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 560,
                settings: {
                    dots: true,
                    draggable: true
                }
            }
        ]
    });

    $('#mainSectionAdditionalCarousel').slick({
        draggable: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '#mainSectionCarousel',
        prevArrow: '#carouselNavArrowPrev',
        nextArrow: '#carouselNavArrowNext' ,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 560,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    $('#usefulLinksCarousel').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '#usefulLinksCarouselNavArrowPrev',
        nextArrow: '#usefulLinksCarouselNavArrowNext',
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 980,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 740,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 560,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

});
