<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


php_sapi_name() == 'cli' or die();

require_once(dirname(__FILE__).'/../classes/YampeeRedis/YampeeRedis.php');

$redisClient = new Yampee_Redis_Client();

$keyPrefix = 'sandbox:';
$redisClient->send('hset', array($keyPrefix.'hash', 'k1', 'ok bro'));
$redisClient->send('hset', array($keyPrefix.'hash', 'k2', 'no problem'));
$hash = $redisClient->send('hgetall', array($keyPrefix.'hash'));

$hashKeys = $redisClient->send('hkeys', array($keyPrefix.'hash'));
