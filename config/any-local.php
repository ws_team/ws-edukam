<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


// $setup['email']['sender'] = 'stub';

if ( ! isset($setup['log']))
    $setup['log'] = array();

if (isset($setup['log']['exclude'])) {
    $setup['log']['exclude'] = array_merge(
        array('wpf\\auth\\Manager', 'find', 'Material'),
        (array)$setup['log']['exclude']
    );
}


$setup['log'] = array_merge(array(
        'file',
        'level' => LOG_DEBUG,
        'path' => WPF_ROOT.'/../log/app.log',
    ),
    $setup['log']
);
