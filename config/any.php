<?php
/**
 * @var array $setup
 */

if ( ! isset($setup['email']))
    $setup['email'] = array();

if ( ! isset($setup['email']['mail'])) {
    $setup['email']['mail'] = 'noreply@'.
        (isset($setup['primary_domain']) ?
            $setup['primary_domain'] :
            str_replace('www.', '', $_SERVER['HTTP_HOST'])
        );
}

$setup['email']['port'] = '25';

if ( ! isset($setup['email']['signature']))
    $setup['email']['signature'] = 'Система рассылки сайта khabkrai.ru';

$setup['email']['pass']='';
$setup['email']['smtp']='172.18.130.7';

if ( ! isset($setup['log'])) {
    $setup['log'] = array();
}

$setup['log'] = array_merge(array(
        'file',
        'path' => WPF_ROOT.'/log/app.log',
        'level' => LOG_DEBUG,
        'exclude' => 'wpf\\auth\\Manager',
    ),
    $setup['log']
);

$hostname = $_SERVER['HTTP_HOST'];

/*if (strpos($hostname, 'test.') !== false
    || strpos($hostname, '-test') !== false) {// @todo -> any-test.php
    if (strpos($setup['DB']['name'], '_test') === false)
        $setup['DB']['name'] .= '_test';
}*/



// настройки для режима локальной разработки
if(defined('LOCAL_DEVELOP_MODE')){

    $setup['DB']['name']    = 'temp_edukam_db';
    $setup['DB']['host']    ='127.0.0.1';
    $setup['DB']['port']    ='5432';
    $setup['DB']['login']   ='postgres';
    $setup['DB']['pass']    ='tVVvbq3UB';

    $setup['use_https']     = false;

}



