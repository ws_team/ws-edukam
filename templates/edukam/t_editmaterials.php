<?php


global $type_id;

$templatepath = dirname(__FILE__);

include_once($this->locateTemplate('f_header'));

$user = $this->getUser();

?>

    <div class="container container--admin-title">
        <div class="breadcrumbs"><?php echo $this->data->breadcrumbs; ?></div>
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <p class="errortext"><?php echo $this->data->errortext;  ?></p>
        <?php

        switch($this->values->action)
        {
            //стандартная форма добавления материала
            case 'add':
                include($templatepath.'/partial/t_editmaterials/form_add.php');
                break;

            //изменение материала
            case 'edit':

                //if($this->values->autorize_email == 'desfpc@gmail.com')
                //{
                //    krumo($this);
                //}

                include($templatepath.'/partial/t_editmaterials/form_edit.php');
                break;

            //список материалов
            case 'list':
                include($templatepath.'/partial/t_editmaterials/list.php');
                break;
        }
        ?>
    </div>
<?php

include_once($this->locateTemplate('f_footer'));
