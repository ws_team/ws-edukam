<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

include_once($this->locateTemplate('f_header'));

?>
<div class="content main">
    <div class="contentblock basemargin">
        <?php
        include($this->partial($this->values->action));
        ?>
    </div>
</div>
<?php

include_once($this->locateTemplate('f_footer'));
