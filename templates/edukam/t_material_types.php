<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

global $message, $status;

include_once($this->partial('functions'));

$action_templates = array(
    'add',
    'edit',
    'template',
    'list' => 'template',
);

$imenu = $this->values->imenu;

if (isset($action_templates[$imenu]))
    $tpl = $action_templates[$imenu];
elseif (in_array($imenu, $action_templates))
    $tpl = $imenu;
else
    $tpl = 'template';

$this->wpf_include('components/materials/material_types_array.php');

ob_start();

include($this->partial($tpl));

$content = ob_get_clean();

include($this->partial('/xadmin/page'));
