<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

//die('step2_1');

defined('_WPF_') or die();

global $sites, $stypes, $wrongfiles;

include($this->locateTemplate('f_header'));


$this->data->iH1 = 'Системные настройки';

//die('step2_2');

?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>
<div class="contentblock basemargin">
    <p class="errortext"><?= ! empty($this->data->errortext) ? $this->data->errortext : ''  ?></p>

    <p>&nbsp;</p>
    <table>
		<tr>
			<th>Сайт</th>
			<th>Домен</th>
			<th>Материал</th>
			<th>Создание материала</th>
            <th>Изменение материала</th>
			<th>Файл</th>
			<th>Создание файла</th>
            <th>Изменение файла</th>
            <th>Автор email</th>
            <th>Автор ФИО</th>
		</tr>
		<?php

		if(is_array($wrongfiles) && count($wrongfiles) > 0){
			foreach ($wrongfiles as $file){
                echo '<tr>
                    <td>'.$file['site'].'</td>
                    <td>'.$file['domain'].'</td>
                    <td>'.$file['materialname'].'<br><a href="http://'.$file['domain'].'/?menu=editmaterials&imenu='.$file['materialtypeid'].'&id='.$file['materialid'].'&action=edit">редактировать</a></td>
                    <td>'.$file['materialdate_add'].'</td>
                    <td>'.$file['materialdate_edit'].'</td>
                    <td>'.$file['name'].'<br><a href="http://'.$file['domain'].'/?menu=getfile&id='.$file['id'].'">ссылка</a></td>
                    <td>'.$file['filedatecreate'].'</td>
                    <td>'.$file['filedateedit'].'</td>
                    <td>'.$file['materialuseremail'].'</td>
                    <td>'.$file['materialuserfio'].'</td>
            </tr>';
			}
		}

		?>
	</table><?php die('step2_3'); ?>
	<p>&nbsp;</p>

</div>
<?php

include($this->locateTemplate('f_footer'));

?>