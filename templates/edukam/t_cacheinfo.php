<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

ob_start();

include($this->partial($this->values->action));

$content = ob_get_clean();

include($this->partial('/xadmin/page'));