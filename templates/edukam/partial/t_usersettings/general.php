<?php
/**
 * @var \iSite $this
 * @var string $email
 * @var bool $csrf_fault
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

?>

<div class="usersettings">
    <form id="usersettings_feedback_form" method="post" class="feedback-form">
        <div class="row">
            <label for="colorThemeBlue">Синяя тема</label>
            <input type="radio"
                   name="siteColorTheme"
                   value="blue"
                   id="colorThemeBlue"
                   <?php

                   if(!isset($this->data->siteColorTheme) || $this->data->siteColorTheme == 'blue' ){
                       echo 'checked';
                   }

                   ?>
            >
        </div>
        <div class="row">
            <label for="colorThemeBrown">Коричневая тема</label>
            <input type="radio"
                   name="siteColorTheme"
                   value="brown"
                   id="colorThemeBrown"
                    <?php

                    if($this->data->siteColorTheme == 'brown' ){
                        echo 'checked';
                    }

                    ?>
            >
        </div>
        <div class="row">
            <label for="colorThemeGreen">Зеленая тема</label>
            <input type="radio"
                   name="siteColorTheme"
                   value="green"
                   id="colorThemeGreen"
                    <?php

                    if($this->data->siteColorTheme == 'green' ){
                        echo 'checked';
                    }

                    ?>
            >
        </div>
        <div class="row">
            <label for="site_title">Заголовок сайта (title)</label>
            <input type="text" name="title" id="site_title" value="<?= $this->data->title ?>" />
        </div>
        <div class="row">
            <label for="site_keywords">Ключевые слова (keywords)</label>
            <input type="text" name="keywords" id="site_keywords" value="<?= $this->data->keywords ?>" />
        </div>
        <div class="row">
            <label for="site_description">Описание (description)</label>
            <input type="text" name="description" id="site_description" value="<?= $this->data->description ?>" />
        </div>
        <div class="row">
            <label for="yandex_metrika_id_counter">Идентификатор счетчика Яндекс.Метрика</label>
            <input type="text" name="yandexMetrikaIdCounter" id="yandex_metrika_id_counter" value="<?= $this->data->yaMetrikaIdCounter ?>"/>
        </div>
        <div class="row">
            <label for="feedback_email">E-mail для уведомлений обратной связи</label>
            <input type="email" name="emailFeedback" id="feedback_email" value="<?= $this->data->feedbackEmail ?>" />
        </div>
        <div class="row">
            <button class="styler" type="submit" name="save" value="1">Сохранить</button>
        </div>

    </form>
</div>
