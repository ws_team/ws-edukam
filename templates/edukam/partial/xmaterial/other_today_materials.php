<?php
/**
 * @var \iSite $htis
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ($material['type_id'] == 3) {
    $other_tadays_materials = 0;

    $news_count = coutNews($this, '3', '3', '', '', null, null);
    $news = listNews($this, 1, 999, '3', '3', '', '', date("d.m.Y", $material['unixtime']), null);

    foreach ($news as $mat) {
        if ($mat['id'] != $material['id']) {
            $other_tadays_materials++;
        }
    }

    if ($other_tadays_materials) {

        ?>
        <div class="announces_wrapper filtered-materials-list inner-materials-list">
            <div class="news_inner_wrapper">
                <h2 class="today-announces">Анонсы на день</h2>
                <div class="doubleblock">
                    <?php

                    $filter_values = array();

                    if (isset($this->values->region) && ($this->values->region != '')) {
                        $filter_values['region-filter'] = GetMaterialNameById($this, $this->values->region);
                    }

                    if (isset($this->values->keywords) && ($this->values->keywords != '')) {
                        $filter_values['keywords-filter'] = explode(',', $this->values->keywords);
                    }

                    if (isset($this->values->list_date_filter_datestart)&&($this->values->list_date_filter_datestart != '' && $this->values->list_date_filter_dateend == '')) {
                        $filter_values['single-date-filter'] = $this->values->list_date_filter_datestart;
                    }

                    if ($this->values->list_date_filter_datestart != '' && $this->values->list_date_filter_dateend != '') {
                        $filter_values['list-date-filter'] = 'c '.$this->values->list_date_filter_datestart.' по '.$this->values->list_date_filter_dateend;
                    }

                    $hidden_filter_values = (count($filter_values) == 0) ? ' hidden' : '';

                    ?>
                    <div class="filter-values <?= $hidden_filter_values ?>"><?php

                        foreach ($filter_values as $filter => $value) {
                            if (is_array($value)) {
                                foreach ($value as $num => $subvalue) {
                                    ?>
                                    <div class="filter-value filter-multiple-value <?= $filter ?>" data-value-num="<?= $num ?>">
                                        <span class="value"><?= $subvalue ?></span>
                                        <a href="#" class="filter-remove-link remove-filter-value <?= $filter ?>"></a>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?><div class="filter-value <?= $filter ?>">
                                <span class="value"><?= $value ?></span>
                                <a href="#" class="filter-remove-link reset-filter <?= $filter ?>"></a>
                                </div>
                                <?php
                            }
                        }

                        ?><a href="/events/announces" class="reset-all-filters"></a>
                    </div>
                    <div class="news-list-wrapper">
                        <div class="news-list-inner-wrapper">
                            <?php $current_date = ''; ?>

                            <?php if ($news_count == 0) : ?>
                                <div class="empty-list">
                                    Не найдены материалы соответствующие выбранным критериям
                                </div>
                            <?php endif; ?>

                            <?php foreach($news as $num => $row): // вывод списка новостей ?>

                                <?php if ($row['id'] != $material['id']) : ?>

                                    <?php // вычисляем переменные

                                    $first = ($num === 0) ? ' news-row-first' : '';
                                    $last = ($num === count($news) - 1) ? ' news-row-last' : '';
                                    $zebra = ($num % 2 === 1) ? ' news-row-even' : ' news-row-odd';

                                    $classes = $zebra . $first . $last;

                                    if ($row['photo'] != null) {
                                        $classes .= ' has-photo';
                                        $has_photo = true;
                                    } else {
                                        $has_photo = false;
                                    }

                                    if ($row['video'] != null && $row['photo'] == null) {
                                        $classes .= ' has-video';
                                        $has_video = true;
                                    } else {
                                        $has_video = false;
                                    }

                                    $date = date("d.m.Y", $row['unixtime']);
                                    $human_date = russian_datetime_from_timestamp($row['unixtime'], false, false, true);
                                    $row['extra_text1'] = explode(',', $row['extra_text1']);

                                    $row['video'] = str_replace('https://www.youtube.com/watch?v=','',$row['video']);
                                    $row['video'] = str_replace('http://www.youtube.com/watch?v=','',$row['video']);
                                    $row['video'] = str_replace('http://www.youtube.com/embed/','',$row['video']);
                                    $row['video'] = str_replace('https://youtu.be/', '', $row['video']);

                                    $row['video'] = 'http://img.youtube.com/vi/' . $row['video'] . '/mqdefault.jpg';
                                    ?>

                                    <?php if ($current_date != $date) : // если предыдущая дата отличается от текущей, то вывести плавающий разделитель с датой ?>
                                        <div class="date-splitter attachable"><a href="#" data-dmY-date="<?php print $date ?>" class="single-date-link filter-link single-date-filter"><?php print $human_date; ?></a></div>
                                    <?php endif; ?>
                                    <?php $current_date = $date; ?>



                                    <div id="news-<?php print $row['id']; ?>" class="news-row<?php print $classes; ?>">

                                        <?php if (array_key_exists('unixtime', $row)) : ?>
                                            <div class="time">
                                                <?php print date('H:i', $row['unixtime']); ?>
                                            </div>
                                            <div class="status">
                                                <?php
                                                $event_hour = (int)date("G", $row['unixtime']);
                                                $current_hour = (int)date("G");

                                                switch (true) {
                                                    case ((int)time() < $row['unixtime']): print 'планируется'; break;
                                                    case ((int)time() - $row['unixtime'] >= 0 && (int)time() - $row['unixtime'] <=  3600): print 'идёт'; break;
                                                    case ((int)time() > $row['unixtime']): print 'завершено'; break;
                                                }

                                                ?>
                                            </div>

                                        <?php endif; ?>

                                        <?php if ($has_photo) : // если есть фото - выводим его первым ?>
                                            <a href="/photos/<?php print $row['photo']; ?>_x990.jpg" class="news-photo colorbox"><img src="/photos/<?php print $row['photo']; ?>_x200.jpg" width="151" alt="<?php print $row['name']; ?>" title="Перейти к новости"/></a>
                                        <?php endif; ?>

                                        <?php if ($has_video) : // если есть фото - выводим его первым ?>
                                            <a href="<?php print $row['url']; ?>" class="news-video"><img src="<?php print $row['video']; ?>" width="151" alt="<?php print $row['name']; ?>" title="Перейти к новости"/></a>
                                        <?php endif; ?>

                                        <div class="news-title news-textfield">
                                            <a href="<?php print $row['url']; ?>"><?php print $row['name']; ?></a>
                                        </div>

                                        <div class="news-announce news-textfield">
                                            <?php
                                            $c = $this->trim_text(strip_tags($row['content']), 200);
                                            if (mb_strlen($row['content']) > 200) {
                                                $c .= '...';
                                            }
                                            print $c;
                                            ?>
                                        </div>

                                        <?php if (array_key_exists('contacts', $row) && count($row['contacts'])) : ?>
                                            <div class="news-address news-textfield">
                                                <?php foreach ($row['contacts'] as $num => $contact) : ?>
                                                    <div class="address-line address-line-<?php print $num; ?>">
                                                        <?php
                                                        $addr = $contact['addr'];
                                                        if (array_key_exists('city', $contact) && $contact['city'] != null) { $addr .= ' ' . $contact['city']; }
                                                        if (array_key_exists('building', $contact) && $contact['building'] != null) { $addr .= ', оф.' . $contact['building']; }

                                                        print $addr;
                                                        ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                <?php endif; ?>

                            <?php endforeach; ?>

                        </div>
                    </div>
                    <a href="<?php print $_SERVER['REQUEST_URI']; ?>/../" class="all-news">Все анонсы</a>
                </div>
            </div>
        </div>
        <?php
    }
}