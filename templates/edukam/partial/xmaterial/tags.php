<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

//выводим тэги
if (!empty($material['tags'])) {
    ?>
    <div class="content-tags">
        <?= $material['tags'] ?>
    </div>
    <?php
}