<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ( ! empty($material['photos'])) {
    $has_photo = true;
} else {
    $has_photo = false;
}

if ( ! empty($material['videos'])) {
    $has_video = true;
} else {
    $has_video = false;
}

if(isset($material['widgets']) && isset($material['widgets']['matdetailed_photovideo'])){
	if($material['widgets']['matdetailed_photovideo'] == '1'){
		$has_video = false;
		$has_photo = false;
	}
}

$date = date("d.m.Y", $material['unixtime']);
$human_date = russian_datetime_from_timestamp($material['unixtime'], false, false, true);
$material['extra_text1'] = explode(',', $material['extra_text1']);

if ( ! empty($material['videos'])) {
    foreach ($material['videos'] as $num => $video) {
        $url = $material['videos'][$num]['content'];
        $material['videos'][$num]['image'] = $material['videos'][$num]['thumb'];

        if ($material['videos'][$num]['youtubeid']) {
            $material['videos'][$num]['url'] = $material['videos'][$num]['content'];
        } else {
            $material['videos'][$num]['url'] = '/?menu=getvideo&id='.$material['videos'][$num]['id'];
        }
    }
}

if ($has_photo || $has_video) : ?>
    <div class="news-photos news-photos-microgallery">
    <?php
    
    if ($has_photo) : // если есть фото - выводим его первым
        
        if (count($material['photos']) > 2 || true) :

            if(count($material['photos'] > 0)) {
                $i = 0;
                
                ?><script> 
                    var photos = new Array();
                    var authors = new Array();
                    var aphoto = 1;
                </script><?php

                $photos_count = count($material['photos']);
                $album_size = 0;
                $photo_file_ids = array();

                foreach ($material['photos'] as $photo) {

                    $photo_file_ids[] = $photo['id'];
                    ++$i;
                    $album_size += $photo['size'];

                    //главное большое фото
                    if ($i == 1) {
                        $authortext='';

                        if($photo['author'] != '') {
                            $authortext='Автор: '.$photo['author'];
                        }

                        ?>
                        <script>
                            photos[<?= $i ?>] = <?= $photo['id'] ?>;
                            authors[<?= $i ?>] = '<?= $photo['author'] ?>';
                        </script>
                        <div class="photo_big">
                            <div class="photo_prev" onclick="prevPhoto();"></div>
                            <div class="photo_next" onclick="nextPhoto();"></div>
                            <a href="/photos/<?= $photo['id'] ?>_x922.jpg" class="colorbox">
                                <img id="photo_big" src="/photos/<?= $photo['id'] ?>_xy289x196.jpg"
                                     alt="" width="280"></a>
                        </div>
                        <div class="nullblock">
                            <div class="photo_big_author" id="photo_big_author"><?= $authortext ?></div>
                        </div>
                        <div class="photo_smallphotos">
                            <div>
                                <img src="/photos/<?= $photo['id'] ?>_x290.jpg" onclick="selectPhoto(<?= $i ?>);" alt="">
                            </div>
                            <?php
                    } else {
                        ?>
                        <script>
                            photos[<?= $i ?>] = <?= $photo['id'] ?>;
                            authors[<?= $i ?>] = '<?= $photo['author'] ?>';
                        </script>
                        <div>
                            <a href="/photos/<?= $photo['id'] ?>_x922.jpg" class="colorbox xmedia s289x196"
                               rel="gal">
                                <img src='/photos/<?= $photo['id'] ?>_xy289x196.jpg'
                                     alt="<?= $material['name'] ?>" onclick="selectPhoto(<?= $i ?>);"
                                    class="media-content"/>
                                <span class="media-textview">Изображение: <?= $photo['name'] ?></span>
                            </a>
                        </div>
                        <?php
                    }
                }
            }
            ?>
            </div>
        <?php else : ?>
            <?php

            foreach ($material['photos'] as $num => $photo) : ?>
                <a href="/photos/<?= $photo['id'] ?>_x922.jpg"
                   class="news-photo news-photo-<?= $num ?> colorbox xmedia s280">
                    <img src="/photos/<?= $photo['id'] ?>_x290.jpg" width="280" alt="<?= $photo['name'] ?>"
                        class="media-content" />
                    <span class="media-textview">Изображение: <?= $photo['name'] ?></span>
                </a>
            <?php

            endforeach;
            ?>
        <?php endif; ?>
    <?php endif; ?>

    <?php
    if ($has_video) : // если есть фото - выводим его первым ?>
        <?php
        foreach ($material['videos'] as $num => $video) : ?>
            <a href="<?php print $video['url']; ?>" class="news-video xmedia s280">
                <img src="<?= $video['image'] ?>" width="280" alt="<?php print $video['name']; ?>"
                    class="media-content" />
                <span class="media-textview">Видеоролик: <?= $video['name'] ?></span>
            </a>
        <?php
        endforeach; ?>
    <?php
    endif;
    ?>
    </div>
<?php endif; ?>
