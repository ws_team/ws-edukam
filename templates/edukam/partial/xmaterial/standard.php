<?php
/**
 * @var \iSite $this
 * @var array $material
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

//выводим виджет
echo vidjet_material_photovideo($this);

if (!empty($material)) {
    //выводим контент
    ?>
    <div class="content-text">
        <?= $material['content'] ?>
    </div>
    <?php
}

include($this->partial('/xmaterial/documents'));
include($this->partial('/xmaterial/tags'));

//комитеты, источники,
echo vidjet_material_params($this);

if (!empty($showRelated))
    include($this->partial('/xmaterial/related'));
