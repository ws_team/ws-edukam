<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

?>
<div class="content-extra"><?php

    include($this->partial('/xmaterial/documents'));
    include($this->partial('/xmaterial/tags'));

    //комитеты, источники,
    echo vidjet_material_params($this);

    include($this->partial('/xmaterial/related'));

    ?>
</div>
