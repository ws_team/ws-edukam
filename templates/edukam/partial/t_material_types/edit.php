<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$csrfToken = $this->generateCsrfToken();

?>
<form method="post" action="" class="form-horizontal editform">
    <input type="hidden" name="csrf" value="<?= htmlspecialchars($csrfToken) ?>" />
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label required">Наименование:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $this->data->amattype['name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="en_name" class="col-sm-2 control-label">Наименование (En версия):</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="en_name" name="en_name" value="<?php echo $this->data->amattype['en_name']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="id_char" class="col-sm-2 control-label">Буквенный индентификатор (En):</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="id_char" name="id_char" value="<?php echo $this->data->amattype['id_char']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="parent_id" class="col-sm-2 control-label">Родительский раздел:</label>
        <div class="col-sm-9">
            <select name="parent_id" id="parent_id" class="styler">
                <option value="">---</option><?php
                print(PrintTypeOptions($this->data->material_types, $this->data->amattype['parent_id'], 0, $this->data->amattype['id']));
                ?></select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-9">
            <div class="checkbox">
                <label>
                    <input name="showinmenu" type="checkbox" value="1" <?php if($this->data->amattype['showinmenu'] == 1){echo 'CHECKED'; } ?>> Отображать в меню
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="ordernum" class="col-sm-2 control-label">Вес (для сортировки):</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="ordernum" name="ordernum" value="<?php echo $this->data->amattype['ordernum']; ?>">
        </div>
        <label for="perpage" class="col-sm-2 control-label">Кол-во материалов в списке:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="perpage" name="perpage" value="<?php echo $this->data->amattype['perpage']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="template_list" class="col-sm-2 control-label">Шаблон списка материалов:</label>
        <div class="col-sm-9">
            <select name="template_list" id="template_list" class="styler">
                <option value="0">Собственный шаблон</option>
                <?php echo TemplateSelectorOptions($this,$this->data->amattype['template_list'],1); ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="template" class="col-sm-2 control-label">Шаблон материала:</label>
        <div class="col-sm-9">
            <select name="template" id="template" class="styler">
                <option value="0">Собственный шаблон</option>
                <?php echo TemplateSelectorOptions($this,$this->data->amattype['template'],0); ?>
            </select>
        </div>
    </div><p>&nbsp;</p>
    <p><b>Настройки отображения шаблона редактирования материала:</b></p>
    <div class="form-group">
        <div class="col-sm-3">
            <div class="checkbox">
                <label>
                    <input name="hide_anons" type="checkbox" value="1" <?php if($this->data->amattype['hide_anons'] == 1){echo 'CHECKED';} ?>> спрятать анонс
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="hide_parent_id" type="checkbox" value="1" <?php if($this->data->amattype['hide_parent_id'] == 1){echo 'CHECKED';} ?>> спрятать род. материал
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="hide_files" type="checkbox" value="1" <?php if($this->data->amattype['hide_files'] == 1){echo 'CHECKED';} ?>> спрятать вложения
                </label>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="checkbox">
                <label>
                    <input name="hide_content" type="checkbox" value="1" <?php if($this->data->amattype['hide_content'] == 1){echo 'CHECKED';} ?>> спрятать осн. текст
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="hide_extra_number" type="checkbox" value="1" <?php if($this->data->amattype['hide_extra_number'] == 1){echo 'CHECKED';} ?>> спрятать флаг важности
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="hide_conmat" type="checkbox" value="1" <?php if($this->data->amattype['hide_conmat'] == 1){echo 'CHECKED';} ?>> спрятать прикреп. материалы
                </label>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="checkbox">
                <label>
                    <input name="hide_date_event" type="checkbox" value="1" <?php if($this->data->amattype['hide_date_event'] == 1){echo 'CHECKED';} ?>> спрятать дату
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="hide_contacts" type="checkbox" value="1" <?php if($this->data->amattype['hide_contacts'] == 1){echo 'CHECKED';} ?>> спрятать контакты
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="mat_as_type" type="checkbox" value="1" <?php if($this->data->amattype['mat_as_type'] == 1){echo 'CHECKED';} ?>> материал как тип материала
                </label>
            </div>
            <!--<div class="checkbox">
                <label>
                    <input name="ifgood" type="checkbox" value="1" <?php if(isset($this->data->amattype['ifgood']) && $this->data->amattype['ifgood'] == 1){echo 'CHECKED';} ?>> раздел каталога товаров
                </label>
            </div>-->
        </div>
        <div class="col-sm-2">
            <div class="checkbox">
                <label>
                    <input name="hide_tags" type="checkbox" value="1" <?php if($this->data->amattype['hide_tags'] == 1){echo 'CHECKED';} ?>> спрятать тэги
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input name="hide_lang" type="checkbox" value="1" <?php if($this->data->amattype['hide_lang'] == 1){echo 'CHECKED';} ?>> спрятать языки
                </label>
            </div>
            <!--<div class="checkbox">
                <label>
                    <input name="ifstore" type="checkbox" value="1" <?php if(isset($this->data->amattype['ifstore']) && $this->data->amattype['ifstore'] == 1){echo 'CHECKED';} ?>> раздел магазина/склада
                </label>
            </div>-->
        </div>
    </div><input type="hidden" name="menu" value="material_types"><input type="hidden" name="action" value="edit"><input type="hidden" name="id" value="<?php echo $this->data->amattype['id']; ?>">
    <button type="submit" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="сохранить изменения типа материала">
        <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Сохранить изменения!</button>
</form>

<p>&nbsp;</p><p>&nbsp;</p>
<h2 class="adminSubtitle">Дополнительные характеристики материала:</h2>
<?php

if(count($this->data->extratypes) > 0)
{
    echo '<table width="100%" class="table table-striped table-hover table-condensed table-bootstrap">
                <tr>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Тип материала</th>
                    <th width="100">Действия</th>
                </tr>';

    foreach($this->data->extratypes as $etype)
    {
        $form_id = 'edit_mattypeextra_form'.$etype['id'];

        echo "<tr>
            <td>
            <form method=post action='' id=\"$form_id\">
                <input type=\"hidden\" name=\"iid\" value='".$etype['id']."'>
                <input type=text name='name' class='form-control' style='width:250px;' value='".$etype['name']."'>
                <input type=\"hidden\" name=\"menu\" value=\"material_types\">
                <input type=\"hidden\" name=\"imenu\" value=\"edit\">
                <input type=\"hidden\" name=\"action\" value=\"editextratype\">
                <input type=\"hidden\" name=\"id\" value='".$this->data->amattype['id']."'>
            </form>
        </td>"; ?>
        <td>
            <select name="valuetype" class="styler width-200" form="<?= $form_id ?>">
                <option value="valuename"<?php if($etype['valuetype'] == 'valuename'){?> selected <?php } ?>>Строка</option>
                <option value="valuetext"<?php if($etype['valuetype'] == 'valuetext'){?> selected <?php } ?>>Текст</option>
                <option value="valuedate"<?php if($etype['valuetype'] == 'valuedate'){?> selected <?php } ?>>Дата</option>
                <option value="valuemat"<?php if($etype['valuetype'] == 'valuemat'){?> selected <?php } ?>>Материал</option>
            </select>
        </td>
        <td>
            <select name="valuemattype" class="styler width-200" form="<?= $form_id ?>">
                <option value="">---</option>
                <?= PrintTypeOptions($this->data->material_types, $etype['valuemattype'], 0) ?>
            </select>
        </td>
        <td>
            <button type=submit class="btn btn-default showtooltip small" data-toggle="tooltip" data-placement="top" data-original-title="изменить доп. характеристику" title="" value='изменить' form="<?= $form_id ?>">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
            </form>

            <button type="button" class="btn btn-danger showtooltip small" data-toggle="tooltip" data-placement="bottom" title="" onclick='doDelExtraParam(<?= $etype['id'] ?>, <?= $this->data->amattype['id'] ?>);' data-original-title="Удалить доп. характеристику">
                <span class="glyphicon glyphicon-remove-sign"></span></button>
        </td>
        <?php echo "
        </tr>";
    }

    echo '</table>';
}
else
{
    echo '<p><i>Доп. характеристик нет</i></p>';
}

?>
<p>&nbsp;</p><p>&nbsp;</p>
    <h2 class="adminSubtitle">Добавить дополнительную характеристику:</h2>
    <form method="post" action="" class="form-horizontal editform">
        <div class="form-group">
            <label for="hname" class="col-sm-2 control-label required">Наименование характеристики:</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="hname" name="name" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="valuetype" class="col-sm-2 control-label required">Тип данных:</label>
            <div class="col-sm-9">
                <select name="valuetype" id="valuetype" class="styler">
                    <option value="valuename">Строка</option>
                    <option value="valuetext">Текст</option>
                    <option value="valuedate">Дата</option>
                    <option value="valuemat">Материал</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="valuemattype" class="col-sm-2 control-label">Тип материала (если тип данных - материал):</label>
            <div class="col-sm-9">
                <select name="valuemattype" id="valuemattype" class="styler">
                    <option value="">---</option>
                    <?php

                    print(PrintTypeOptions($this->data->material_types, '', 0));

                    ?>
                </select>
            </div>
        </div><input type="hidden" name="menu" value="material_types"><input type="hidden" name="imenu" value="edit">
        <input type="hidden" name="action" value="addextratype"><input type="hidden" name="id" value="<?php echo $this->data->amattype['id']; ?>">
        <button type="submit" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Добавить характеристику материала">
            <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить характеристику</button>
    </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<!--<h2>Типы-материалов характеристик:</h2>
<?php

if (count($this->data->extraselectors) > 0) {
    ?>
    <table>
        <tr>
            <th>№</th>
            <th>Тип материала</th>
            <th>Действия</th>
        </tr>
        <?php

        foreach ($this->data->extraselectors as $selector) {
            $del_url = "/?menu=material_types&imenu=edit&id=".$this->values->id.
                "&action=delextraselect&esid=".$selector['id'];

            ?>
            <tr>
                <td><?= $selector['id'] ?></td>
                <td><?= $selector['mattypename'] ?></td>
                <td><a href='<?= htmlspecialchars($del_url) ?>'>удалить</a></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}
?>
<p>&nbsp;</p>
<h2>Добавить тип-материала характеристик:</h2>
<form method="post" action="">
    <input type="hidden" name="csrf" value="<?= htmlspecialchars($csrfToken) ?>" />
    <table>
        <tr>
            <th>Тип материала:</th>
            <td>
                <select name="type_id" class="styler">
                    <?= PrintTypeOptions($this->data->material_types, '', 0) ?>
                </select>
            </td>
        </tr>
    </table>
    <input type="submit" value="Добавить" class="styler" style="margin-top: 3px;">
    <input type="hidden" name="menu" value="material_types">
    <input type="hidden" name="imenu" value="edit">
    <input type="hidden" name="action" value="addextraselect">
    <input type="hidden" name="id" value="<?= $this->data->amattype['id'] ?>">
</form>-->
<?php
