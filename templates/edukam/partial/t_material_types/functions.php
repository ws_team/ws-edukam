<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//функция выводит типы в табличной форме
function PrintTypeRows($types, $level = 0)
{
    ob_start();

    //цикл по массиву
    foreach ($types as $mattype) {

        $fsize = 8 + (10 - ($level * 2));
        $otst = '';
        if ($level > 0) {
            for ($i=0; $i<$level; ++$i)
            {
                $otst.='&nbsp;&nbsp;&nbsp;-&nbsp;';
            }
        }

        $url_edit = '/?menu=material_types&imenu=edit&id='.$mattype['id'];

        $hasChildsSign = !empty($mattype['childs']);

        ?>

        <div class="dropdown-list <?php echo $hasChildsSign ? 'dropdown-list_parent' : '' ?>" data-role="dropdownList">
            <div class="material-type-columns">
                <div class="material-type-columns__item material-type-columns__item_opener" data-role="dropdownListOpener"></div>
                <div class="material-type-columns__item material-type-columns__item_number">
                    <?php echo $mattype['id'] ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_char-id">
                    <?php echo $mattype['id_char'] ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_name" style="font-size: <?php echo $fsize ?>px;">
                    <?php echo $otst.$mattype['name'] ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_name-en">
                    <?php echo $mattype['en_name'] ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_parent-type-id">
                    <?php echo $mattype['parent_name'] ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_nesting">
                    <?php echo $level ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_weight">
                    <?php echo $mattype['ordernum'] ?>
                </div>
                <div class="material-type-columns__item material-type-columns__item_actions">
                    <a href="#" onclick="deleteMatType(<?= $mattype['id'] ?>);return false;">
                        <button
                                type="button"
                                class="btn btn-danger showtooltip small"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                data-original-title="Удалить тип материала">
                            <span class="glyphicon glyphicon-remove-sign"></span>
                        </button>
                    </a>&nbsp;
                    <a href="<?= htmlspecialchars($url_edit) ?>">
                        <button
                                type="button"
                                class="btn btn-default showtooltip small"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                data-original-title="Форма редактирования типа материала">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </a>
                </div>
            </div>

            <?php

            if ($hasChildsSign) {
                echo PrintTypeRows($mattype['childs'], $level + 1);
            }

            ?>

        </div>

        <?php
    }

    return ob_get_clean();
}