<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

?>
<script>

    $(document).ready(function () {

       /* audiojs.events.ready(function() {
            var as = audiojs.createAll();
        });*/

        jQuery('ul.sf-menu').superfish();

        $('input.styler, select.styler').styler();

        //колорбокс будет тут:
        $(".imggroup").colorbox({rel:'popout', maxWidth:'95%', maxHeight:'95%'});
        <?php
        if(isset($this->data->jsdocready)&&($this->data->jsdocready != ''))
        {
            echo $this->data->jsdocready;
        }
        ?>
        jQuery('img.svg, img.svgsome').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');
        });
    });
</script>
