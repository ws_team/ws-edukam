<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


// Объединяемые (через сборщик-минификатор) скрипты
$attach_js = array(
    'js/jquery-3.3.1.min.js',
    'js/jquery.colorbox-min.js',
    'js/jquery.formstyler.min.js',
    'js/jquery-ui/jquery-ui.min.js',
    'js/jquery.form.min.js',
    'js/jquery.datetimepicker.js',
    'js/datepicker.js',
    'js/date-dialog-vidget.js',
    'js/hoverIntent.js',
    'js/superfish.js',
    'classes/blocks/blocksAPI.js',
    'js/visual_assist.js',
    'bootstrap/js/bootstrap.js',
    'js/newJS/commonScripts.js',
    'js/edukamJS/external.min.js',
    'js/edukamJS/bundle.min.js',
);

// Необъединяемые скрипты
$external_js = array(
    'https://api-maps.yandex.ru/2.1.23/?lang=ru_RU',
);

// Объединяемые CSS
$attach_css = array(
    'css/superfish.css',
    'css/superfish-vertical.css',
    'js/jquery-ui/jquery-ui.min.css',
    'css/jquery.datetimepicker.css',
    'css/colorbox.css',
    'css/datepicker.css',
    'css/jquery.formstyler.css',
    'css/jquery.formstyler_hacks.css',
    'bootstrap/css/bootstrap_sm.css',
    'newCSS/commonStyles.less',
    'newCSS/adminPanelStyles.less',
    'css/style.css',
    'css/edukamCSS/external.min.css',
    'css/edukamCSS/main.min.css',
);



foreach ($attach_js as $js) {
    if (substr($js, 0, 1) == '@')
        continue;
    $js = str_replace('js', 'js', $js);
    $this->AttachJs($js);
}

if (isset($this->data->blocks))
    print $this->data->blocks->css();

foreach ($attach_css as $icss => $css) {
    if (substr($css, 0, 1) == '@')
        break;
    $css = str_replace(
        array('css', 'js'),
        array('css', 'js'),
        $css
    );
    if (substr($css, -5, 5) == '.less')
        $css = $this->IncludeLESS($css, 'css/');
    $this->AttachCSS($css);
}

toPlugColorThemes($this);

$this->wpf_include('specialver_functions.php');
specialver_select_stylesheets($this);

if ($icss < count($attach_css) - 1) {
    foreach (array_slice($attach_css, $icss + 1) as $css) {
        if (substr($css, 0, 1) == '@')
            continue;
        $css = str_replace(
            array('css', 'js'),
            array('css', 'js'),
            $css
        );
        if (substr($css, -5, 5) == '.less') {
            $css = $this->IncludeLESS($css, 'css/');
        }
        $this->AttachCSS($css);
    }
}

echo $this->IncludeMinJS($jsheaderarr);
echo $this->IncludeMinCSS($cssheaderarr);

foreach ($external_js as $src) {
    $src = str_replace('js', 'js', $src);
    ?>
    <script src="<?=htmlspecialchars($src)?>"></script>
    <?php
}
