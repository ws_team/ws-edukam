<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


if (!empty($this->data->iH1))
    $this->setPageMeta('title', $this->data->iH1);

foreach ($this->getPageMetaAll() as $name => $value) {
    ?>
    <meta property="og:<?= $name ?>" content="<?= $value ?>" />
    <meta name="vk:<?= $name ?>" value="<?= $value ?>" />
    <?php
    if ($name == 'image') {
        ?><link rel="image_src" href="<?= $value ?>" />
        <?php
    }
}
