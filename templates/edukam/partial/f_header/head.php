<?php
/**
 * @var \iSite $this

 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

?>

<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?= strip_tags($this->data->title) ?></title>


<META name="keywords" content="<?php

    if(isset($material) && isset($material['keywords']) && $material['keywords'] != '')
        echo $material['keywords'];
    else
        echo $this->data->keywords;
?>" />
<META name="description" content="<?php

if(isset($material) && isset($material['description']) && $material['description'] != '')
    echo $material['description'];
else
    echo $this->data->description;

?>" />


<?php

if(specialver_is_active($this)){
    echo '<meta name="viewport" content="width=1300px">';
}
else{
    echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
}

?>

<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" type="image/x-icon" href="/img/decor/favicon.png">
<link rel="shortcut icon" type="image/x-icon" href="/img/decor/favicon.png">



<?php

include($this->partial('/f_header/resources'));

$this->getPostValues('media');

$this->wpf_include('components/materials/material_types_array.php');

if (!empty($this->data->morejs))
    echo $this->data->morejs;


include($this->partial('/f_header/inline_js'));
include($this->partial('/f_header/meta'));

?>
