<?php
/**
 * @var \iSite $this
 * @var string $content
 * @var string $message
 * @var string $status
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $message, $status;

if ( ! isset($message)) {
    $message = '';

    if ( ! empty($error))
        $message = $error;
    elseif ( ! empty($this->data->errortext))
        $message = $this->data->errortext;
    elseif ($this->hasFlash('message'))
        $message = $this->getFlash('message');
}

if ( ! isset($status)) {
    $status = $this->getFlash('status');
}

$msg_class = 'info';
if ( ! empty($error))
    $msg_class = 'error';
elseif ( ! empty($status))
    $msg_class = $status;// 'success' | 'error'

if ( ! empty($message)) {
    $message = explode("\n", $message);
    $message = '<p>'.implode('</p><p>', $message).'</p>';
}

include_once($this->locateTemplate('f_header'));
?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>
<div class="contentblock basemargin">
    <?php
    if ( ! empty($message)) {
        ?>
        <div class="errortext admin-message <?= $msg_class ?>"><?= $message ?></div>
        <?php
    }

    echo $content;

    ?>
</div>
<?php

include_once($this->locateTemplate('f_footer'));