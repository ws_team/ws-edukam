<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->getPostValues(array('page', 'perpage'));

$page = intval($this->values->page);
if ($page <= 0)
    $page = 1;

$perpage = intval($this->values->perpage);
if ($perpage <= 0)
    $perpage = 20;

$items = settings_get_all($this, $page, $perpage);

$baseNumber = ($page - 1) * $perpage + 1;

$csrfToken = $this->generateCsrfToken();
$csrfTokenEnc = urlencode($csrfToken);

$canEdit = $this->getUser()->can('admin.settings.edit');
$canAdd = $this->getUser()->can('admin.settings.add');
$canDelete = $this->getUser()->can('admin.settings.delete');

$urlEditTpl = '?menu=settings&action=edit&name={name}&_csrf='.$csrfTokenEnc;
$urlDelTpl = '?menu=settings&action=delete&name={name}&_csrf='.$csrfTokenEnc;
$urlPageTpl = '?menu=settings&action=list&page={page}';

if (!empty($this->values->perpage))
    $urlPageTpl .= '&perpage='.$perpage;

?>
<table class="settings-table" id="settings_table">
    <caption>Параметры</caption>
    <thead>
        <tr>
            <th>№</th>
            <th>Параметр</th>
            <th>Значение</th>
            <th>Действия</th>
        </tr>
    </thead>
    <tbody>
    <?php
    foreach ($items as $i => $item) {
        ?>
        <tr class="item">
            <td><?= $i + $baseNumber ?></td>
            <td class="param-name"><?= htmlspecialchars($item['name']) ?></td>
            <td class="param-value"><?= htmlspecialchars($item['value']) ?></td>
            <td>
                <?php if ($canEdit) {?>
                    <a href="#setting_edit_popup" class="edit">редактировать</a>
                    <?php
                }
                if ($canDelete) {
                ?>
                    <a href="<?= htmlspecialchars(str_replace('{name}', $item['name'], $urlDelTpl))
                        ?>" class="delete">удалить</a>
                <?php
                }
                ?>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <?php
            $startPos = ($page - 1) * $perpage + 1;
            $endPos = $startPos + count($items) - 1;
            $totalCount = settings_get_count($this);
            ?>
            <tr colspan="4">
                <td ><?= $totalCount ?> [<?= $startPos ?> &ndash; <?= $endPos ?>]
                </td>
                <td colspan="3">
                    <?php
                    if ($totalCount > $perpage) {
                        ?>
                        <div class="pagination">
                            <span class="label">Страницы</span>:
                            <span class="pages">
                            <?php
                            $numPages = intval(ceil(floatval($totalCount) / $perpage));
                            for ($nPage = 1; $nPage <= $numPages; $nPage++) {
                                if ($nPage == $page) {
                                    ?><b><?= $nPage ?></b><?php
                                } else {
                                    $pageUrl = str_replace('{page}', $nPage, $urlPageTpl);
                                    ?><a href="<?= htmlspecialchars($pageUrl) ?>"><?= $nPage ?></a>
                            <?php
                                }
                            }
                            ?>
                            </span>
                        </div>
                <?php
                }
                ?>
                </td>
        </tr>
        <?php
        if ($canAdd) {
        ?>
        <tr>
            <td colspan="4"><a href="#setting_add_popup" class="add">Добавить</a></td>
        </tr>
        <?php
        }
        ?>
    </tfoot>
</table>

<div style="display:none;">
    <div id="settings_popup" class="settings-popup">
        <form action="" method="post">
            <input type="hidden" name="menu" value="settings"/>
            <input type="hidden" name="action" value=""/>
            <input type="hidden" name="csrf" value="<?= htmlspecialchars($csrfToken) ?>" />
            <input type="text" name="name" value="" placeholder="параметр" required />
            <input type="text" name="value" value="" placeholder="значение" />
            <button type="submit" name="add" value="1" class="styler">Сохранить</button>
        </form>
    </div>
</div>
<div style="display:none;">
    <div id="settings_delete_popup" class="settings-popup">
        <form action="" method="post">
            <input type="hidden" name="menu" value="settings"/>
            <input type="hidden" name="action" value="delete"/>
            <input type="hidden" name="csrf" value="<?= htmlspecialchars($csrfToken) ?>" />
            <input type="hidden" name="name" value=""" />
            <div style="padding:1em 0;text-align:center;">
                <p>Вы собираетесь удалить параметр <span class="param-name-display" style="font-weight:bold;"></span>.
                    Подтверждаете?</p>
                <div class="buttons">
                    <button type="submit" class="styler">Удалить</button>
                    <button type="button" name="cancel" value="0" class="styler">Отмена</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    jQuery(function($) {
        window.console&&console.log('[] ok bro');
        var popup = $('#settings_popup');
        var popupDelete = $('#settings_delete_popup');

        popupDelete.find('button[name="cancel"]').on('click', function (evt) {
            evt.preventDefault();
            $.colorbox.close();
        });

        $('#settings_table').on('click', 'a.edit', function (evt) {
            evt.preventDefault();

            window.console&&console.log('[settings] edit clicked');

            var anc = $(evt.target);
            var row = anc.closest('.item');
            var itemName = row.children('.param-name').text();
            var itemVal = row.children('.param-value').text();

            window.console&&console.log('[settings] edit: name=%s value=%s', itemName, itemVal);

            popup.find('input[name="name"]').prop('readonly', true).val(itemName);
            popup.find('input[name="action"]').val('edit');
            popup.find('input[name="value"]').val(itemVal);

            $.colorbox({
                width: '400px',
                inline: true,
                href: '#settings_popup',
                title: 'Изменение параметра'
            });
        });

        $('#settings_table').on('click', 'a.delete', function (evt) {
            evt.preventDefault();
            window.console&&console.log('[settings] delete clicked');
            var anc = $(evt.target);
            var row = anc.closest('.item');
            var paramName = row.children('.param-name').text();
            popupDelete.find('input[name="name"]').val(paramName);
            popupDelete.find('.param-name-display').text(paramName);
            $.colorbox({
                width: '400px',
                inline: true,
                href: '#settings_delete_popup',
                title: 'Удаление параметра'
            })
        });

        $('#settings_table a.add').click(function (evt) {
            evt.preventDefault();

            window.console&&console.log('[settings] add clicked');

            popup.find('input[name="name"]').removeProp('readonly').val('');
            popup.find('input[name="value"]').val('');
            popup.find('input[name="action"]').val('add');

            $.colorbox({
                width: '400px',
                inline: true,
                href: '#settings_popup',
                title: 'Добавление параметра'
            });
        });
    });
</script>
<style>
    .settings-popup {
        min-height: 100px;
        padding: 2em 1em;
    }
    .settings-popup .buttons {
        text-align: center;
        padding: 1em 0 0;
    }
    .settings-popup input {
        display: block;
        height: 30px;
        padding:5px 8px;
        margin: 0.5em 0;
        width: 100%;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
</style>
