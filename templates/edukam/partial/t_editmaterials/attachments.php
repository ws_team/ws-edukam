<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


global
    $file_pars,
    $file_par_selector,
    $file_type_selector,
    $material;

if($this->data->mattype['hide_files'] != '1'){
    ?>
    <p>&nbsp;</p><a name="files"></a>
    <h2 class="adminSubtitle">Вложения:</h2>
    <?php

    $fileconnter=0;
    if (!empty($material['allfiles']))
        $fileconnter += count($material['allfiles']);

    //заголовки таблицы
    ?>
    <form method="post" action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php
    echo $this->values->id; ?>&amp;action=edit">
        <table width="100%" class="table table-striped table-hover table-condensed table-bootstrap">
            <tbody data-role="materialFilesList">
            <tr>
                <th width='42'></th>
                <th>Наименование / Раздел</th>
                <th width='85'>Тип</th>
                <th><small>Версия для<br>Слабовидящих</small></th>
                <th width='24'>Вес</th>
                <th style="width: 100px;">Действия</th>
                <th width='12'></th>
            </tr>
            <?php
            if (!empty($material['allfiles']))
            {
                foreach($material['allfiles'] as $file)
                {
                    //проверяем на колличество связей файла
                    $ccnt=0;
                    $query='SELECT count("material_id") "ccnt" FROM "file_connections" WHERE "file_id" = '.$file['id'];
                    if($res=$this->dbquery($query))
                    {
                        if(count($res) > 0)
                        {
                            $ccnt=$res[0]['ccnt'];
                        }
                    }

                    //иконка

                    $ilink='';

                    $iclass='class="img-thumbnail"';

                    switch($file['type_id'])
                    {
                        case FILETYPE_IMAGE:
                            $icon='/photos/'.$file['id'].'_xy64x64.jpg?' . time('u') ;
                            $ilink=" href='/photos/$file[id]_x600.jpg? " . time('u')  .  "' class='imggroup'";
                            //$iclass='class="img-thumbnail"';
                            break;
                        case FILETYPE_VIDEO:
                            $icon='/images/filetypes/video.png';
                            break;
                        case FILETYPE_AUDIO:
                            $icon='/images/filetypes/audio.png';
                            break;
                        case FILETYPE_DOCUMENT:
                            $icon='/images/filetypes/document.png';
                            $ilink=" href='/?menu=getfile&amp;id={$file['id']}'";
                            break;
                        case FILETYPE_BROADCAST:
                            $icon='/images/filetypes/broadcast.png';
                            break;
                        default:
                            $icon='/images/filetypes/file.png';
                            break;
                    }

                    $icon="<img src='$icon' width=\"42\" height=\"42\" $iclass border='0'/>";

                    if($ilink != '')
                    {
                        $icon="<a $ilink>".$icon.'</a>';
                    }

                    $actions='';

                    $imenu=$this->values->imenu;
                    $id=$this->values->id;

                    $file['name']=str_replace("'",'',$file['name']);

                    /*$actions.="<a href=\"#files\" onclick=\"deleteMatFile($file[id],$ccnt,$imenu,$id);\"><img class='svg delete-icon' src='/images/trash.svg' /></a>";
                    $actions.="&nbsp;<a onclick=\"editFileForm($file[id],'$file[typename]','$file[name]','$file[name_en]','$file[content]','$file[date_event]','$file[author]','$file[ordernum]','$file[par_id]','$file[name_off]');\"><img class='svg edit-icon' src='/images/edit.svg' /></a>";
                    */

                    $actions="
<button type=\"button\" 
class=\"btn btn-danger showtooltip small\" 
data-toggle=\"tooltip\" 
data-placement=\"bottom\" 
title=\"Удалить вложение\" 
onclick=\"if(checkEditInputs()){ deleteMatFile($file[id],$ccnt,$imenu,$id) } else{ showEditWarning(event, this) }\">
<span class=\"glyphicon glyphicon-remove-sign\"></span></button>";

                    $actions.="
&nbsp;<button 
onclick=\"if(checkEditInputs()){ editFileForm($file[id],'$file[typename]','$file[name]','$file[name_en]','$file[content]','$file[date_event]','$file[author]','$file[ordernum]','$file[par_id]','$file[name_off]') } else{ showEditWarning(event, this) }\" 
type=\"button\" 
class=\"btn btn-default showtooltip small\" 
data-toggle=\"tooltip\" 
data-placement=\"bottom\" 
title=\"Форма редактирования вложения\" >
<span class=\"glyphicon glyphicon-pencil\"></span></button>";

                    $typename = $file['typename'];
                    if ($file['type_id'] == FILETYPE_DOCUMENT)
                        $typename .= ' ('.$file['fextension'].')';

                    ?>
                    <tr>
                        <td><?=$icon?></td>
                        <td><?=$file['name']?> / <?=$file['par_name']?></td>
                        <td><?=$typename?></td>
                        <td><?php if(isset($file['cecutient']) && $file['cecutient']){echo 'загружен';}else{echo 'нет';}
                            echo "<div style='display: none;'>"; print_r($file); echo "</div>"; ?></td>
                        <td><?=$file['ordernum']?></td>
                        <td><?=$actions?></td>
                        <td><input type='checkbox' value='1' name='dfile_<?=$file['id']?>' /></td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table><p>&nbsp;</p>
        <button type="button"
                onclick='if(checkEditInputs()){ addFileForm() } else{ showEditWarning(event, this) }'
                class="btn btn-success showtooltip"
                data-toggle="tooltip"
                data-placement="bottom"
                title="Загрузить вложение (документ, изображение, видео...)">
            <span class="glyphicon glyphicon-link" ></span>&nbsp;Загрузить вложение</button>&nbsp;
        <button type="button"
                onclick='if(checkEditInputs()){ connectFileForm() } else{ showEditWarning(event, this) }'
                class="btn btn-success showtooltip"
                data-toggle="tooltip"
                data-placement="bottom"
                title="Связать уже загруженный файл">
            <span class="glyphicon glyphicon-link" ></span>&nbsp;Связать уже загруженный файл</button>&nbsp;
        <input type="hidden" name='deletefilegroup' value='1'>
        <button type="submit"
                data-action="checkEditInputsBeforeAction"
                class="btn btn-infobtn btn-danger showtooltip"
                data-toggle="tooltip"
                data-placement="bottom"
                title="Удалить/отвязать отмеченные вложения">
            <span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Удалить отмеченные</button>
    </form>
    <?php
    //выводим файлы
    ?>
    <div style="display: none;">
        <div id="connectfileform" style="height: 400px; padding: 30px; width: 800px; position: relative;">
            <h2>Связать уже загруженный файл:</h2>
            <p>&nbsp;</p>
            <form method="post" action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php
            echo $this->values->id; ?>&amp;action=edit">
                <div class="form-group">
                    <label for="mattypeforconnectfile" class="control-label required" style="width: 150px; text-align: right;">Тип материала:</label>
                    <select id="mattypeforconnectfile" onchange="MaterialEditSelectTypeFile();"
                            class="styler maxheight width-200">
                        <option>---</option>
                        <?php echo PrintTypeOptions($this->data->material_types, '', 0) ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="matforconnectfile" class="control-label required" style="width: 150px; text-align: right;">Материал:</label>
                    <select id="matforconnectfile" onchange="MaterialEditSelectMatFile();" class="styler maxheight width-200"></select>
                </div>
                <div class="form-group">
                    <label for="fileforconnect" class="control-label required" style="width: 150px; text-align: right;">Файлы:</label>
                    <select id="fileforconnect" name="fileforconnect" class="styler maxheight width-200"></select>
                </div>
                <input type="hidden" name="doconnectfile" value="1">
                <button type="submit" class="btn btn-success">Связать</button>
            </form>
        </div>
    </div>
    <div style="display: none;">
        <div id="editfileform">
            <form method="post" enctype="multipart/form-data" action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php echo $this->values->id;
            ?>&amp;action=edit">
                <table>
                    <tr>
                        <th colspan="2"><strong>Изменить файл:</strong></th>
                    </tr>
                    <tr>
                        <th><sup>*</sup>Тип файла:</th>
                        <td><input type="text" class="styler" readonly disabled id="file_typename" /></td>
                    </tr>
                    <tr>
                        <th>Раздел вложения</th>
                        <td><select id="file_par_sel" name="file_par" class="styler">
                                <?php

                                echo $file_par_selector;

                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <th><sup>*</sup>Наименование: (<input type="checkbox" id="name_off" name="nameoff" value="1"> выкл)</th>
                        <td><input type="text" id="file_name" name="file_name" style="width: 705px;" class="styler" value=""></td>
                    </tr>
                    <tr id="addfile_file_sv">
                        <th>Версия для слабовидящих (документы):</th>
                        <td><input id="addfile_fileinput_sv" type="file" class="styler" name="filedata_sv">
                            <input id="addfile_size_sv" type="hidden" name="MAX_FILE_SIZE" value="20885760" /></td>
                    </tr>
                    <!--<tr>
                        <th>Наименование (en):</th>
                        <td><input type="text" id="file_name_en" name="file_name_en" style="width: 705px;" class="styler" value=""></td>
                    </tr>-->
                    <tr>
                        <th>Ссылка на внешний ресурс (для видео):</th>
                        <td><input type="text" id="file_content" name="file_content" style="width: 705px;" class="styler" value=""></td>
                    </tr>
                    <tr>
                        <th>Дата публикации:</th>
                        <td><input type="text" class="styler dateinput" id="file_date_event" name="date_event" value=""></td>
                    </tr>
                    <tr>
                        <th>Автор:</th>
                        <td><input type="text" class="styler" id="file_author" name="author" value=""></td>
                    </tr>
                    <tr>
                        <th>Вес:</th>
                        <td><input type="text" class="styler" id="file_ordernum" name="ordernum" value=""></td>
                    </tr>
                </table>
                <input type="hidden" name="file_id" value="" id="file_id">
                <input type="hidden" name="doeditfile" value="1">
                <input type="submit" value="Изменить" class="styler" style="margin-top: 3px;">
            </form>
        </div>
    </div>
    <div style="display: none;">
        <div id="addfileform" style="height: 400px;">
            <form
                id="addfileformform"
                class="add-file-form can-be-refreshed"
                method="post"
                enctype="multipart/form-data"
                data-role="ajaxForm"
                action="/?menu=editmaterials&amp;imenu=<?php echo $this->values->imenu; ?>&amp;id=<?php echo $this->values->id; ?>&amp;action=edit"
            >
                <table>
                    <tr>
                        <th colspan="2"><strong>Загрузить файл </strong>(Размер файла не должен превышать 80Мб):</th>
                    </tr>
                    <tr>
                        <th><sup>*</sup>Тип файла:</th>
                        <td><select id="addfile_type" style="width: 200px;" class="styler" name="file_type_id" onchange="showhidefileparams();">
                                <?php

                                echo $file_type_selector;

                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><sup>*</sup>Наименование: (<input type="checkbox" name="nameoff" value="1"> выкл)</th>
                        <td><input type="text" name="file_name" style="width: 705px;" class="styler" value=""></td>
                    </tr>
                    <!--<tr>
                                <th>Наименование (en):</th>
                                <td><input type="text" name="file_name_en" style="width: 705px;" class="styler" value="<?php echo $this->values->file_name_en; ?>"></td>
                            </tr>-->
                    <tr id="addfile_url">
                        <th>Сcылка на внешний ресурс (для видео):</th>
                        <td><input type="text" name="file_content" style="width: 705px;" class="styler" value=""></td>
                    </tr>
                    <tr id="addfile_file">
                        <th>Файл к загрузке:</th>
                        <td><input id="addfile_fileinput" type="file" class="styler" name="filedata">
                            <input id="addfile_size" type="hidden" name="MAX_FILE_SIZE" value="20885760" /></td>
                    </tr>
                    <tr id="addfile_file_sv">
                        <th>Версия документа для слабовидящих:</th>
                        <td><input id="addfile_fileinput_sv" type="file" class="styler" name="filedata_sv">
                            <input id="addfile_size_sv" type="hidden" name="MAX_FILE_SIZE" value="20885760" /></td>
                    </tr>
                    <tr id="addfile_par">
                        <th>Раздел вложения</th>
                        <td>
                            <select name="file_par" class="styler">
                                <?php echo $file_par_selector; ?>
                            </select>
                        </td>
                    </tr>
                    <tr id="addfile_date">
                        <th>Дата публикации:</th>
                        <td>
                            <input type="text" class="styler dateinput" name="date_event" value="<?php
                            echo date('d.m.Y H:i'); ?>">
                        </td>
                    </tr>
                    <tr id="addfile_author">
                        <th>Автор:</th>
                        <td><input type="text" class="styler" name="author" value=""></td>
                    </tr>
                    <tr>
                        <th>Вес:</th>
                        <td><input type="text" class="styler" name="ordernum"></td>
                    </tr>
                </table>
                <div data-role="containerResponseMessage"></div>

                <input type="hidden" name="doaddfile" value="1">
                <input type="hidden" name="action" value="ajaxSaveFileEditMaterial">
                <input type="hidden" name="id" value="<?php echo $this->values->id; ?>">
                <input type="hidden" name="current_imenu" value="<?php echo $this->values->imenu; ?>">
                <input type="hidden" name="formName" value="addFileEditMaterial">
                <button type="submit" class="styler" style="margin-top: 3px;">Загрузить</button>
                <div class="preloader"><img src="/img/preloader.svg" class="preloader__image"></div>
            </form>
        </div>
    </div><?php if(file_exists($this->settings->path.'/addfile.php')){ ?>
	<p>&nbsp;</p>
	<h2>Загрузка группы изображений:</h2>
		<div class="form-group">
			<p>Перетащите файлы изображений на поле, либо нажмите на него, для выбора файлов из диалогового окна.<br>Размер файла должен быть менее 8 Мб.</p>
			<div style="display: none;"><input id="fileupload" type="file" name="files[]" data-url="/addfile.php" multiple></div>
			<div id="filepreviews">
				<div id="dropzone" onclick="performClick('fileupload');"></div>
			</div>
			<div id="progress" style="display: none;">
				<div class="bar">
					<img src="/img/loading.gif" width="106" height="12" />
				</div>
			</div>
		</div>
		<button
                type="button"
                id="AddGroupFilesBtn"
                class="styler"
                style="clear: both;"
                onclick="if(checkEditInputs()){ doAddGroupFiles(<?php echo $this->values->id; ?>) } else{ showEditWarning(event, this) }">
            Добавить загруженные изображения к материалу
        </button>
	<?php } ?>
    <p style="clear: both;">&nbsp;</p>
    <p>&nbsp;</p>
    <h2 class="adminSubtitle">Разделы вложений:</h2>
    <table width="100%" class="table table-striped table-hover table-condensed table-bootstrap">
        <tr>
            <th>Наименование</th>
            <th width='100'>Действия</th>
        </tr>
        <?php
        echo $file_pars;
        ?>
    </table>
    <div style="display: none">
        <div id="addfileparform" style="padding: 30px; min-height: 80px;">
            <form id="addfileparformform" method="post" enctype="multipart/form-data" action="/?menu=editmaterials&imenu=<?php echo $this->values->imenu; ?>&id=<?php echo $this->values->id; ?>&action=edit">
                <input type="hidden" name="doaddfilepar" value="1">
                <p>Название раздела/параграфа: <input type="text" class="styler" value="" name="filepar"> Вес:
                    <input type="text" class="styler" value="0" name="orderby"><br>
                    <input type="submit" value="Создать" class="styler">
            </form>
        </div>
        <div id="editfileparform" style="padding: 30px; min-height: 80px;">
            <form id="editfileparformform" method="post" enctype="multipart/form-data" action="/?menu=editmaterials&imenu=<?php echo $this->values->imenu; ?>&id=<?php echo $this->values->id; ?>&action=edit">
                <input type="hidden" name="doeditfilepar" value="1"><input type="hidden" name="fileparid" id="fileparid"><p>Название раздела/параграфа: <input type="text" class="styler" value="" name="filepar" id="filepar">Вес: <input type="text" id="fileparorder" class="styler" value="" name="orderby"><br><input type="submit" value="Изменить" class="styler">
            </form>
        </div>
    </div><p>&nbsp;</p>
    <button type="button" onclick="if(checkEditInputs()){ addFileParForm() } else{ showEditWarning(event, this) }" class="btn btn-success showtooltip" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Добавить новый раздел для вложений">
        <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;Добавить новый раздел вложений
    </button>
    <?php
}
