<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


global $type_id,
       $status_selector,
       $language_selector;

?>
<form method="post" action="">
    <table>
        <tr>
            <th><sup><font color="#ff7f50">*</font></sup>Тип материала:</th>
            <td><input type="hidden" name="type_id" value="<?php echo $type_id; ?>"><?php
                echo htmlspecialchars($this->values->typename); ?></td>
        </tr>
        <tr>
            <th><sup><font color="#ff7f50">*</font></sup>Заголовок:</th>
            <td><input class="styler" style="width: 705px;" type="text" name="name" value="<?php
                echo htmlspecialchars($this->values->name); ?>"></td>
        </tr>
        <tr>
            <th>Букв. идентификатор (en):<br><sup>(Заполн. автоматич.)</sup></th>
            <td><input class="styler" style="width: 705px;" type="text" name="id_char" value="<?php
                echo htmlspecialchars($this->values->id_char); ?>"></td>
        </tr>
        <tr>
            <th>Язык:</th>
            <td>
                <select name="language_id" class="styler"><?php
                    echo $language_selector;
                    ?>
                </select>
            </td>
        </tr>

        <tr>
            <th>Статус:</th>
            <td>
                <select name="status_id" class="styler" disabled><?php echo $status_selector; ?></select>
            </td>
        </tr>
        <tr><?php

            $this->values->parent_id_name='';
            $this->values->matforconnect_name='';

            if ($this->values->parent_id != '')
            {
                $query = 'SELECT "name" FROM "materials" WHERE "id" = '.$this->values->parent_id;
                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $this->values->parent_id_name=$res[0]['name'];
                    }
                }
            }

            if($this->values->matforconnect != '')
            {
                $query='SELECT "name" FROM "materials" WHERE "id" = '.$this->values->matforconnect;
                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $this->values->matforconnect_name=$res[0]['name'];
                    }
                }
            }
            ?>
            <th>Родительский материал (ID):</th>
            <td>
                <input type="text" class="styler" name="parent_id" id="parent_id" value="<?php
                    echo $this->values->parent_id;
                    ?>">&nbsp;Название:&nbsp;<input type="text" id="parent_id_name" class="styler" value="<?php echo $this->values->parent_id_name; ?>" readonly></td>
        </tr>
        <tr>
            <th>При добавлении связать с материалом:</th>
            <td>
                <input type="hidden" name="doconnectmaterial" value="1">
                <input type="text" class="styler" name="matforconnect" id="matforconnect"
                       value="<?php echo $this->values->matforconnect; ?>">&nbsp;Название:&nbsp;<input type="text" id="matforconnect_name" class="styler" value="<?php echo $this->values->matforconnect_name; ?>" readonly></td>
        </tr>
    </table>
    <input type="submit" value="Создать" class="styler" style="margin-top: 3px;">
</form>