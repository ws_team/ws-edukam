<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$site = $this;

return array(
    'editmaterials' => array(
        'Управление материалами',
        'permission' => 'editmaterials',
    ),
    'material_types' => array(
        'Типы материалов',
        'permission' => 'admin.material_types',
        'children' => array(
            'list' => array(
                'Список типов материалов',
                'url' => array(
                    'material_types',
                )
            ),
            'add' => array(
                'Добавить тип материала',
                'url' => array(
                    'material_types',
                    'imenu' => 'add',
                ),
            ),
        ),
    ),
    'usersettings' => array(
        'Настройки',
        'permission' => 'usersettings',
        'children' => array(
            'general' => array(
                'Общие настройки сайта',
                'url' => array(
                    'usersettings',
                    'action' => 'general',
                )
            ),
            'units' => array(
                'Подразделения',
                'url' => array(
                    'units',
                )
            ),
            'authors' => array(
                'Авторы',
                'url' => array(
                    'authors',
                )
            ),
        ),
    ),
    'admin' => array(
        'Администрирование',
        'permission' => 'admin.*',
        'children' => array(
            'users' => array(
                'Пользователи',
                'permission' => 'admin.users',
            ),
            'caches' => array(
                'Кэши',
                'children' => array(
                    'imagecaches' => array(
                        'Кэши изображений',
                        'permission' => 'admin.imagecaches',
                    ),
                    'cacheinfo' => array(
                        'Кэши данных',
                        'permission' => 'admin.cacheinfo',
                    ),
                ),
            ),
            'standarttemplates' => array(
                'Стандартные шаблоны',
                'permission' => 'admin.standarttemplates',
            ),
        ),
    ),
    'stat' => array(
        'Статистика',
        'permission' => 'admin.stat',
        'children' => array(
            'authorsStatistic' => array(
                'Статистика авторских материалов',
                'url' => array(
                    'authorsStatistic',
                    'action' => 'getAuthorsStatistic',
                ),
            )
        )
    ),
    'exit' => array(
        'Выход',
    ),
);