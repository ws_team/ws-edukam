<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$counter_id = $this->data->yaMetrikaIdCounter ? $this->data->yaMetrikaIdCounter : 27406256;

?>
<span id="sputnik-informer"></span>
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=<?php echo $counter_id ?>&amp;from=informer"
   target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/<?php echo $counter_id ?>/3_1_FFFFFFFF_FFFFFFFF_0_pageviews"
                                       style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" /></a>
<!-- /Yandex.Metrika informer -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter<?php echo $counter_id ?> = new Ya.Metrika({id:<?php echo $counter_id ?>});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/<?php echo $counter_id ?>" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->