<?php

$videostr='';

if($this->values->controls != 'no')
{
    $videostr.=' controls="controls" ';
}

if($this->values->autoplay == 'yes')
{
    $videostr.=' autoplay loop';
}

?><video id="toservideo" poster="/photos/<?php echo $this->values->id; ?>_thumb.png" <?php echo $videostr; ?> tabindex="0" style="width: 100%; height: 95%;">
    <source src="/?menu=playvideo&id=<?php echo $this->values->id; ?>" type='video/mp4' />
</video>