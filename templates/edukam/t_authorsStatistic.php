<?php

if(($this->autorize->autorized == 1)&&($this->autorize->userrole == 1)) {

    // include_once($this->settings->path.$this->settings->templateurl.'/f_header.php');
    include_once($this->locateTemplate('f_header'));


//page body===========================================================================
?>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <p class="errortext"><?php echo $this->data->errortext;  ?></p>

        <p>&nbsp;</p>

        <form method="get" action="/">
            c <input type="text" name="startdate" class="styler" id="startdate" value="<?php echo $this->values->startdate; ?>"> до <input type="text" name="enddate" id="enddate" class="styler" value="<?php echo $this->values->enddate; ?>">

            <div class="input-row">
                <select name='unit_id' class='styler input-row__item'>
                    <option value="">--- Подразделения ---</option>
                    <?php echo printUnitsOptions($this, $this->values->unit_id) ?>
                </select>
            </div>

            <div class="input-row">
                <select name="author_id" class="styler input-row__item">
                    <?php echo printAuthorsOptions($this, $this->values->author_id); ?>
                </select>
            </div>

            <div class="input-row">
                <input type="submit" value="Обновить" class="styler">
            </div>

            <input type="hidden" name="menu" value="authorsStatistic">
            <input type="hidden" name="action" value="getAuthorsStatistic">
        </form>

        <p>&nbsp;</p>

        <?php

        if(count($this->data->stat) > 0)
        {
            echo "<a href='
/?menu=authorsStatistic&action=exportAuthorsStatistic&startdate={$this->values->startdate}&enddate={$this->values->enddate}&unit_id={$this->values->unit_id}&author_id={$this->values->author_id}
'>Скачать результаты в формате Excel</a>";
            echo '<table class="statistic-table">
            <tr>
                <th>Дата</th>
                <th>ФИО</th>
                <th>Подразделение</th>
                <th>Название материала</th>
                <th>Ссылка на материал</th>
            </tr>';
            foreach($this->data->stat as $row)
            {
                echo "<tr>
                    <td>$row[material_date_add]</td>
                    <td>$row[author_surname] $row[author_first_name] $row[author_second_name]</td>
                    <td>$row[author_unit]</td>
                    <td>$row[material_name]</td>
                    <td><a href='$row[material_url]' target='_blank' rel='noopener noreferrer'>$row[material_url]</a></td>
                </tr>";
            }
            echo '</table>';
        }else{
            echo '<p>Нет записей, соответствующих заданным условиям</p>';
        }

        ?>
    </div>

    <?php

    //include_once($this->settings->path.$this->settings->templateurl.'/f_footer.php');
    include_once($this->locateTemplate('f_footer'));

}