<?php


defined('_WPF_') or die();

if ( ! empty($this->data->iH1)) {
    ?>

    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>

    <?php
}
?>
<div class="contentblock basemargin">
    <?php
    if ( ! empty($this->data->errortext)) { ?>
        <p class="errortext"><?= $this->data->errortext ?></p>
        смотри ошибки!
        <?php
    }

    if (strlen($this->html)) {
        ?>
        <pre>
        <?= $this->html ?>
        </pre>
        <?php
    }

    ?>
</div>
