<?php

defined('_WPF_') or die();

include_once($this->locateTemplate('f_header'));


//получаем id шаблона формы обращений
$query="SELECT id FROM standart_shablons WHERE file_name = 'appeals'";

$typesstr='';

$otypes=Array();

if($res=$this->dbquery($query))
{

    //krumo($query);

    if(count($res) > 0)
    {

        //krumo($res);

        $templateid = $res[0]['id'];

        //получаем типы материалов с шаблоном обращений
        $query='SELECT id, name, id_char FROM material_types WHERE template_list = '.$templateid;


        //krumo($query);


        if($res=$this->dbquery($query))
        {

            //krumo($res);

            if(count($res) > 0)
            {
                $otypes=$res;

                foreach ($otypes as $otype)
                {
                    if($typesstr != '')
                    {
                        $typesstr.=',';
                    }

                    $typesstr.=$otype['id'];
                }

            }
        }

    }
}

$this->getPostValues(array('datestart','dateend'));

$ocnt=0;

//получаем кол-во обращений
if($typesstr != '')
{
    $query='SELECT count(id) ocnt FROM materials WHERE type_id IN ('.$typesstr.')';

    if($this->values->datestart != '')
    {
        $datestart=MakeTimeStampFromStr($this->values->datestart);
        $query.=' AND date_add >= '."'".date('Y-m-d 00:00:00',$datestart)."'";
    }
    if($this->values->dateend != '')
    {
        $dateend=MakeTimeStampFromStr($this->values->dateend);
        $query.=' AND date_add <= '."'".date('Y-m-d 00:00:00',$dateend)."'";
    }

    if($res=$this->dbquery($query))
    {
        if(count($res) > 0)
        {
            $ocnt=$res[0]['ocnt'];
        }
    }

}


?>
<div class="content main">
    <div class="contentblock basemargin">
        <h1>Статистика отправки обращений на сайте</h1>

        <?php

        if(isset($otypes) && count($otypes) > 0)
        {
            echo '<p>Разделы с отправкой обращений: ';

            foreach ($otypes as $otype)
            {
                echo '&nbsp;<a href="?menu=editmaterials&imenu='.$otype['id'].'&action=list">'.$otype['name'].'</a>';
            }

            echo '</p>


            <p>&nbsp;</p>
            <p><b>Кол-во обращений:</b></p>
            <p>&nbsp;</p>
            <form method="get" action="/">
            <input type="hidden" name="menu" value="appealsstat">
            <p>отображать обращения с <input type="text" name="datestart" id="datestart" value="'.$this->values->datestart.'"> по <input type="text" name="dateend" id="dateend" value="'.$this->values->dateend.'"></p>
            <p>&nbsp;</p>
            <input type="submit" value="Показать кол-во обращений">
            </form>
            <p>&nbsp;</p>
            <p>Обращений за выбранный период: <b>'.$ocnt.'</b></p>';


        }
        else
        {
            echo '<p>На текущем сайте нет разделов с отправкой обращений</p>';
        }

        ?>

    </div>
</div>
    <script>
        $(document).ready(function () {

            $('#datestart').datetimepicker({
                lang:'ru',
                timepicker:true,
                format:'d.m.Y'
            });
            $('#dateend').datetimepicker({
                lang:'ru',
                timepicker:true,
                format:'d.m.Y'
            });

        });
    </script>
<?php

include_once($this->locateTemplate('f_footer'));