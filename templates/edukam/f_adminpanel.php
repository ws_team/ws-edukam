<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

$is_khabkrai = preg_match('/^habkrai(_test\d*)?$/', $this->settings->DB->name);

/*function MaterialTypesAdminMenuNew($site,$materials, $level=0, $action)
{

    $menustr='';

    //получаем список материалов пользолвателя
    //print_r($site->autorize);
    $userrights=$site->autorize->materialtypes;



    if(is_array($materials))
    {
        $menustr.="<ul class=\"dropdown-menu\">";

        foreach($materials as $material)
        {



            if(mb_strlen($material['name'],'utf8') > 60)
            {
                $material['name']=mb_substr($material['name'],0,60,'utf8').'...';
            }

            if((count($userrights) == 0)||(in_array($material['id'],$userrights))){
                //рисуем тип

                $menustr.="<li";

                if(is_array($material['childs']))
                {
                    if(count($material['childs']) > 0){
                        $menustr.=' class="dropdown-submenu"';
                    }
                }

                $menustr.="><a href='/?menu=editmaterials&imenu=$material[id]&action=$action'>$material[name]</a>";}
            else{
                $menustr.= "<li><a style='color: #795456;'>$material[name]</a>";}



            //проверяем его дочерний тип
            if(is_array($material['childs']))
            {
                if(count($material['childs']) > 0){
                    $menustr.=MaterialTypesAdminMenuNew($site, $material['childs'], $level+1, $action);}
            }


            $menustr.="</li>";
        }

        $menustr.="</ul>";

    }

    return($menustr);
}*/

include($this->partial('/f_adminpanel/functions'));
$panel_structure = include($this->partial('/f_adminpanel/structure'));

?>
	<script src="/js/jquery.iframe-transport.js"></script>
	<script src="/js/jquery.fileupload.js"></script>
	<script src="/js/admin.js?ver=2"></script>
<!--<div id="admin_panel" class="admin-panel">
    <?php

    //echo adminpanel_render($this, $panel_structure);

    ?>
    <div style="float: left; height: 40px; line-height: 33px; padding-right: 10px;">
        <small>[<?= $this->autorize->email?> UID<?= $this->autorize->userid ?>]</small>
    </div>
    <small>Фрэймворк: v.<?php echo $this->settings->versions->wpf; ?>;
        Модуль материалов: v.<?php echo $this->settings->versions->materials; ?>;
        Сайт: v.<?php echo $this->settings->versions->site;?></small>
</div>-->
    <nav class="navbar navbar-inverse" role="navigation" style="position: fixed; z-index: 1000; width: 100%;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-0">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-0">
                    <?php echo adminpanel_render($this, $panel_structure); ?>
                    <!--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Управление материалами <span class="caret"></span></a>
                        <?php  echo MaterialTypesAdminMenuNew($this,$this->data->material_types, 0, 'list'); ?>
                    </li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Типы материалов <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/?menu=material_types">Список типов материалов</a></li>
                            <li><a href="/?menu=material_types&imenu=add">Добавить тип материала</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Администрирование <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/?menu=users">Пользователи</a></li>
                            <li><a href="/?menu=imagecaches">Кэши изображений</a></li>
                            <li><a href="/?menu=standarttemplates">Стандартные шаблоны</a></li>
                        </ul>
                    </li>
                    <li><a href="/?menu=exit">Выход</a></li>
                </ul>-->
                <div class="admin-nav-bar-site-version">
                    <small>[<?= $this->autorize->email?> UID<?= $this->autorize->userid ?>]
                    <br><span style="color: #d9f3ff;">
                        Модуль материалов: v.<?php echo $this->settings->versions->materials; ?>;
                            Сайт: v.<?php echo $this->settings->versions->site;?></span></small>
                </div>
            </div>
        </div>
    </nav>
    <div style="position: relative; height: 60px;">&npsp;</div>
    <style>
        .secmenu.fixed {top: 50px!important;}
    </style>
<?php

if ($this->getState('upcoming_release')) {
    ?>
    <div class="maintenance-message" id="maintenance_message">
        <p>
        Внимание!
        Сегодня в течении дня будет загружен релиз. Во время обновления возможны сбои, пожалуйста, проверяйте корректность
        сохранения добавляемых данных</p>
    </div>
    <?php
}