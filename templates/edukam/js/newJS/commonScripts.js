function checkEditInputs() {
    return !$('#checkEditMaterialField').val();
}

function showEditWarning(event, activeElem) {
    event.preventDefault();
    event.stopPropagation();

    $.colorbox({width:"500px", inline:true, href:"#warningEditForm"});

    $('#warningEditFormAnswerSave').click(function() {
        $.colorbox.close();
        $('[data-role="saveDataForm"]').submit();
    });

    $('#warningEditFormAnswerNotSave').click(function() {
        let $activeElem = $(activeElem),
            href        = $activeElem.attr('href');

        $('#checkEditMaterialField').val('');

        if(href){
            window.location = href;
        }
        else{
            $activeElem.click();
        }
    });
}

function changeFormStylerSelectObj($selectObj, action){

    if(!($selectObj instanceof jQuery)){
        $selectObj = $($selectObj);
    }

    switch (action) {
        case 'destroy':
            $selectObj.off('.styler refresh').removeAttr('style').parent().before($selectObj).remove();
            break;
        case 'refresh':
            $selectObj.off('.styler refresh').removeAttr('style').parent().before($selectObj).remove();
            $selectObj.styler();
            break;
    }
}

function resetAjaxPaginator(paginatorBtnText, pageNumber, remainingAmount){
    let $paginatorBtn       = $('#ajaxPaginatorBtn'),
        $paginatorHtml      = $('#ajaxPaginatorHtml');

    $paginatorHtml.html('');

    if(paginatorBtnText){
        $paginatorBtn.html(paginatorBtnText);
        $paginatorBtn.attr("data-ajaxPaginatorRemainingAmount", remainingAmount);
        $paginatorBtn.attr("data-ajaxPageNumber", pageNumber);
        $paginatorBtn.show();
    }
    else{
        $paginatorBtn.hide();
    }
}

function formHandlerAfterAjaxRequest($form, ajaxResponse){

    if(!($form instanceof jQuery)){
        $form = $($form);
    }

    switch ($form.find('input[name="formName"]').val()) {

        case 'addFileEditMaterial':

            let $responseMessageClass,
                $containerResponseMessage,
                $responseMessage;

            switch (ajaxResponse.status) {
                case 'success':
                    $responseMessageClass = 'add-file-form__response-message add-file-form__response-message_success';

                    var $orderNumInput = $form.find(':input[name=ordernum]'),
                        nextOrderNum   = Number($orderNumInput.val()) + 1;

                    $form[0].reset();

                    showhidefileparams();

                    $orderNumInput.val(nextOrderNum);

                    break;
                case 'fail':
                    $responseMessageClass = 'add-file-form__response-message add-file-form__response-message_fail';
                    break;
            }

            $containerResponseMessage = $form.find('[data-role="containerResponseMessage"]');
            $responseMessage          = $(
                    '<div>',
                    {
                        class: $responseMessageClass,
                        text: ajaxResponse.message
                    }
                );

            $containerResponseMessage.html($responseMessage);
            setTimeout(function(){ $responseMessage.remove() }, 4000);

            $('[data-role="materialFilesList"]').append(ajaxResponse.html);

            break;

        case 'regulationsSearch':
            if(ajaxResponse.html){
                $('#ajaxFormResult').html(ajaxResponse.html);
            }
            resetAjaxPaginator(ajaxResponse.paginatorBtnText, ajaxResponse.pageNumber, ajaxResponse.remainingAmount);
            break;
    }
}

$(document).ready(function(){

    $('#search-category-select').styler({
        selectSearch: false,
    });

    $('[data-role="dropdownListOpener"]').on('click', function(){
        $dropDownList = $(this).closest('[data-role="dropdownList"]');

        if($dropDownList.hasClass('dropdown-list_parent-open')){
            $dropDownList.removeClass('dropdown-list_parent-open');
        }
        else{
            $dropDownList.addClass('dropdown-list_parent-open');
        }
    });

    $('[data-role="saveDataInput"]').on('change', function(){
        $('#checkEditMaterialField').val('edited');
    });

    $('[data-action="checkEditInputsBeforeAction"]').on('click', function(event){
        if(!checkEditInputs()){
            showEditWarning(event, this);
        }
    });

    $('[data-role="inputAjaxFormSubmitOnChange"]').on('change', function(){
        let $ajaxForm = $(this).closest('[data-role="ajaxForm"]');
        $ajaxForm.submit();
    });

    $('[data-role="ajaxForm"]').on('submit', function(event){

        event.preventDefault();

        let $form               = $(this),
            $refreshingWrapper  = $form.closest('.can-be-refreshed'),
            $requestData        = new FormData($form[0]),
            ajaxHandler         = '/?menu=ajax&imenu=' + $requestData.get('action');

        $refreshingWrapper.addClass('can-be-refreshed_refreshing');

        $.ajax({
            type: 'POST',
            url: ajaxHandler,
            data: $requestData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            success: function(response) {
                if (response.status == 0) {
                    console.log('Сервер вернул пустой ответ');
                } else {
                    formHandlerAfterAjaxRequest($form, response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Произошла ошибка. Попробуйте еще раз или немного позже.');
                console.log(textStatus + ': ' + errorThrown);
            },
            complete: function() {
                $refreshingWrapper.removeClass('can-be-refreshed_refreshing');
            }
        });
    });

});