jQuery.fn.myAddClass = function (classTitle)
{
    return this.each(function ()
    {
        var oldClass = jQuery(this).attr("class");
        oldClass = oldClass ? oldClass : '';
        jQuery(this).attr("class", (oldClass + " " + classTitle).trim());
    });
};

jQuery.fn.myRemoveClass = function (classTitle)
{
    return this.each(function ()
    {
        var oldClass = jQuery(this).attr("class");
        if (oldClass != undefined)
        {
            var startpos = oldClass.indexOf(classTitle);
            var endpos = startpos + classTitle.length;
            var newClass = oldClass.substring(0, startpos).trim() + " " + oldClass.substring(endpos).trim();
            if (!newClass.trim())
                jQuery(this).removeAttr("class");
            else
                jQuery(this).attr("class", newClass.trim());
        }
    });
};

var colors = ['#D3D3D3','#DF4848', '#D38660', '#C9A771', '#AAA756','#79A12D'];

var mapItems = ['#g-khab path', '#g-koms circle', "#_x31_-amurskii", '#_x32_-ayano-maiskii', '#_x33_-bikinskii',
    '#_x34_-vanins', '#_x35_-verhneburiinskii', '#_x36_-vyazemskii', '#_x37_-komsomolskii',
    '#_x31_1-lozo', '#_x38_-nanaiskii', '#_x39_-nikolaevskii', '#_x31_0-ohotsk', '#_x31_2-polina-osipenko',
    '#_x31_3-sov-gav', '#_x31_4-solnechnii', '#_x31_5-tuguro-chumikan', '#_x31_6-ylchskii','#_x31_7-khabarovskii'];

var mapItemsTownsId = ['#g-khab', '#g-koms'];

var mapItemsIds = {'g-khab': 'Khabarovsk', 'g-koms': 'Komsomolsk-on-Amur', '_x31_7-khabarovskii': 'Khabarovsk-region',
    '_x31_6-ylchskii': 'Ulchsky-region', '_x31_5-tuguro-chumikan': 'Tuguro-Chumikansky-region', '_x31_4-solnechnii': 'Solnechnyj-region',
    '_x31_3-sov-gav': 'Sovetskaya-Gavan-region', '_x31_2-polina-osipenko': 'Polina-Osipenko-region', '_x31_1-lozo': 'Lazo-region',
    '_x31_0-ohotsk': 'Okhotsk-region', '_x39_-nikolaevskii': 'Mykolayiv-region', '_x38_-nanaiskii': 'Nanai-region',
    '_x37_-komsomolskii': 'Komsomol-region', '_x36_-vyazemskii': 'Vyazemskyi-region', '_x35_-verhneburiinskii': 'Verkhnebureinsky-region',
    '_x34_-vanins': 'Vaninskiy-region', '_x33_-bikinskii': 'Bikin-region', '_x32_-ayano-maiskii': 'Ayano-Maisky-region', "_x31_-amurskii": 'Amur-region'};

var mapItemsNames = {'g-khab': 'Urban District of Khabarovsk', 'g-koms': 'Urban District of Komsomolsk-na-Amure', '_x31_7-khabarovskii': 'Khabarovsky municipal district',
    '_x31_6-ylchskii': 'Ulchsky municipal district', '_x31_5-tuguro-chumikan': 'Tuguro-Chumikansky municipal district', '_x31_4-solnechnii': 'Solnechny municipal district',
    '_x31_3-sov-gav': 'Sovetsko-gavansky municipal district', '_x31_2-polina-osipenko': 'Imeni Polini Osipenko municipal district', '_x31_1-lozo': 'Imeni Lazo municipal district',
    '_x31_0-ohotsk': 'Okhotsky municipal district', '_x39_-nikolaevskii': 'Nikolaevsky municipal district', '_x38_-nanaiskii': 'Nanaisky municipal district',
    '_x37_-komsomolskii': 'Komsomolsky municipal district', '_x36_-vyazemskii': 'Vyazemsky municipal district', '_x35_-verhneburiinskii': 'Verkhnebureinsky municipal district',
    '_x34_-vanins': 'Vaninsky municipal district', '_x33_-bikinskii': 'Bikinsky municipal district',
    '_x32_-ayano-maiskii': 'Ayano-Maysky municipal district', '_x31_-amurskii': 'Amursky municipal district'};

var mapItemsBlazonry = {'g-khab': 'g-khab', 'g-koms': 'g-koms', '_x31_7-khabarovskii': 'khab',
    '_x31_6-ylchskii': 'ulchskiy', '_x31_5-tuguro-chumikan': 'tuguro_chumikanskiy', '_x31_4-solnechnii': 'solnechniy',
    '_x31_3-sov-gav': 'sov_gav', '_x31_2-polina-osipenko': 'osipenko', '_x31_1-lozo': 'lazo',
    '_x31_0-ohotsk': 'ohotskiy', '_x39_-nikolaevskii': 'nikolaevskiy', '_x38_-nanaiskii': 'nanaiskiy',
    '_x37_-komsomolskii': 'koms', '_x36_-vyazemskii': 'vyazemskiy', '_x35_-verhneburiinskii': 'verhnebur',
    '_x34_-vanins': 'vaninskiy', '_x33_-bikinskii': 'bikinskiy',
    '_x32_-ayano-maiskii': 'ayano_mayskiy', '_x31_-amurskii': 'amursk'};

$(window).load(function ()
{
    //map_colorAllMap("#b4b4b4");

    map_colorAllMap('#b4b4b4');

});

//функция красит регион
function map_colorAllMap(color)
{
    var svgObject = document.getElementById("map");
    if ("contentDocument" in svgObject)
    {
        var svgMap = $(svgObject.contentDocument);
        var plusOffsetX = -50;
        var plusOffsetY = -70;

        for (var i = 0; i < mapItems.length; i++)
        {
        $(mapItems[i], svgMap).attr("fill", color)
            .myRemoveClass("area-0").myRemoveClass("area-1").myRemoveClass("area-2").myRemoveClass("area-3").myRemoveClass("area-4").myRemoveClass("area-5");


        //назначаем функции при событиях мыши
            $(mapItems[i], svgMap).mouseenter(function (e)
            {
                //map_colorRegion(this.id, '#00ff00');

                if(this.id)
                {
                    var itemId=this.id;
                }
                else
                {
                    var itemId=this.parentNode.id;
                }

                if(itemId == 'g-khab')
                {
                    itemId='g-khab path';
                }
                if(itemId == 'g-koms')
                {
                    itemId='g-koms circle';
                }

                //alert(itemId);

                map_colorRegion(itemId, '#0071aa');
                map_activeRegionName(itemId);
                map_changeContent(itemId);
                map_showRegionInfo();

            });

            $(mapItems[i], svgMap).mouseleave(function (e)
            {
                //map_colorRegion(this.id, '#00ff00');

                if(this.id)
                {
                    var itemId=this.id;
                }
                else
                {
                    var itemId=this.parentNode.id;
                }

                if(itemId == 'g-khab')
                {
                    itemId='g-khab path';
                }
                if(itemId == 'g-koms')
                {
                    itemId='g-koms circle';
                }

                //alert(itemId);

                map_colorRegion(itemId, '#b4b4b4');
                map_deactiveRegionName(itemId);
                map_hideRegionInfo();

            });

            $(mapItems[i], svgMap).mousemove(function (e)
            {
                var relX = e.pageX + plusOffsetX;
                var relY = e.pageY + plusOffsetY;

                map_moveRegionInfo(relX,relY);
            });

            $(mapItems[i], svgMap).click(function (e)
            {

                if(this.id)
                {
                    var itemId=this.id;
                }
                else
                {
                    var itemId=this.parentNode.id;
                }

                //alert(itemId);

                //var ahref='/events/news/&region='+mapItemsIds[itemId];

                //document.location.href=ahref;

                if(itemId == 'g-khab path')
                {
                    itemId='g-khab';
                }
                if(itemId == 'g-koms circle')
                {
                    itemId='g-koms';
                }

                //itemId selected_region
                var rname=mapItemsNames[itemId];
                $('#selected_region').html('<h3>'+rname+'</h3>');


                //alert($('#map_'+itemId).html());
                if(($('#map_'+itemId).html() == '')||($('#map_'+itemId).html() == undefined))
                {
                    $('#selected_region').html('<h3>'+rname+'</h3><p>no maps for the area</p>');
                }

                $('.maps_blocks').hide();
                $('#map_'+itemId).show();


            });


        }

    }
}

//функуия меняет контент блока
function map_changeContent(region)
{
    $('#SmallRegionInfo').html(contentarr[region]);
}

//функция перемещает/отображает окно информации по региону
function map_moveRegionInfo(x,y)
{

    var nx=x-40;
    var ny=y-120;

    $('#SmallRegionInfo').css('left',nx);
    $('#SmallRegionInfo').css('top',ny);
}


function map_showRegionInfo()
{
    $('#SmallRegionInfo').show(0);
}

function map_hideRegionInfo()
{
    $('#SmallRegionInfo').hide(0);
}

//функция красит конкретный регион
function map_colorRegion(region, color)
{

    var svgObject = document.getElementById("map");
    if ("contentDocument" in svgObject)
    {
        var svgMap = $(svgObject.contentDocument);
        $('#'+region, svgMap).attr("fill", color);
    }

    if(region == 'g-khab path')
    {
        region='g-khab';
    }
    if(region == 'g-koms circle')
    {
        region='g-koms';
    }

    var rname=mapItemsNames[region];

    if(color == '#b4b4b4')
    {
        $('#region_sel').html('');
    }
    else
    {
        $('#region_sel').html('Select a map of the district: <b>'+rname+'</b>');
    }


}



//функция активирует боковое меню
function map_activeRegionName(region){
    //var rid = backarr[region];

    //$('#option_region_'+rid).addClass('active');

}

//функция деактивирует боковое меню
function map_deactiveRegionName(region){
    //var rid = backarr[region];

    //$('#option_region_'+rid).removeClass('active');

}