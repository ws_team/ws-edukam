// Функция-конструктор нашего API
DateDialogAPI = function(e) {


    this.container_id = "date_dialog";
    this.opener = e.target;
    this.openerInitText = "выбрать период";


    this.container = $("#"+this.container_id);

    this.getFields();

    // Задаём дату с которой мы хотим начать просмотр календаря

    // Если поле начальной даты пустое
    if (this.dateStartField.val() == "") {
        // Генерируем значение сегодняшней даты в формате dd.mm.YYYY

        // var showDate = date + '.' + month + '.' + tmp.getFullYear();
        // var showDate = tmp.getFullYear() + '-' + month + '-' + date;
        var showDate = this.timestampToNormal(Date.now());
    // Иначе
    } else {
        // Берём значение из поля
        var showDate = this.dateStartField.val();
    }

    var preSelectedDates =  (this.dateStartField.val() != "" && this.dateEndField.val() != "") ? [this.dateStartField.val(), this.dateEndField.val()] : [];

    // Собираем календарик из jQuery-плагина
    this.container.DatePicker({
        flat: true, // хз
        date: preSelectedDates,   // предварительно отжатые даты
        current: showDate, // текущая дата?
        calendars: this.calendarsCount, // количество отображемых месяцев
        mode: this.selectType, // режим выбора: (одиночное значение/множественное значение/диапазон)
        start: 1, // начало недели (1 = понедельник)
        format: 'd.m.Y', // юзаемый формат данных
        locale: { // локализация
            daysShort : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            daysMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            months : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthsShort : ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
            week : "Нед"
        },
        // коллбэки-чебуреки
        onShow: $.proxy(this.onShow, this),
        onHide: $.proxy(this.onHide, this),
        onChange: $.proxy(this.onChange, this),
        onRender: $.proxy(this.onRender, this)
    });

}

// Функция для сохраниения ссылок на поля формы в виде свойств объекта API
DateDialogAPI.prototype.getFields = function() {
    $opener = $(this.opener);

    // Если указан тип выбора даты
    if (typeof $opener.attr("data-select-type") != "undefined" && $opener.attr("data-select-type").match(/(single|multiple|range)/)) {
        this.selectType = $opener.attr("data-select-type");
    } else {
        this.selectType = "single";
    }

    // Если указано количество отображаемых календарей
    if (typeof $opener.attr("data-calendars") != "undefined" && !isNaN(parseInt($opener.attr("data-calendars")))) {
        this.calendarsCount = $opener.attr("data-calendars");
    } else {
        this.calendarsCount = 1;
    }


    // Ищем поле начальной даты

    // Если имеется аттрибут указывающий ид поля начальной даты
    if (typeof $opener.attr("data-datestart-field-id") != "undefined") {
        // Сохраняем jQuery-объект найденного по id поля в переменную
        this.dateStartField = $("input#"+$opener.attr("data-datestart-field-id"));
        // Перепроверяем не случилось ли какое-нибудь недоразумение в виде не найденного поля,
        // Если случилось - громко материмся и валим функцию
        if (!this.dateStartField.size()) {
            throw("DateDialog: datestart field id recieved, but field is missing");
            return false;            
        }
    // Если аттрибут указывающий id отсутствует
    } else {
        // Смотрим есть ли аттрибут указывающий класс поля начальной даты
        if (typeof $opener.attr("data-datestart-field-class") != "undefined") {
            // Если есть - сохраняем jQ-obj первого попавшегося поля с таким классом
            this.dateStartField = $("input."+$opener.attr("data-datestart-field-class")+":first");
            // Перепроверяем не случилось ли какое-нибудь недоразумение в виде не найденного поля,
            // Если случилось - громко материмся и валим функцию
            if (!this.dateStartField.size()) {
                throw("DateDialog: datestart field class recieved, but field is missing");
                return false; 
            }
        } else {
            // Если не передан ни один из аттрибутов поля
            throw("DateDialog: can't find datestart field, no attributes for search");
            return false;
        }
    }

    // Ищем поле конечной даты

    // Если имеется аттрибут указывающий ид поля конечной даты
    if (typeof $opener.attr("data-dateend-field-id") != "undefined") {
        // Сохраняем jQuery-объект найденного по id поля в переменную
        this.dateEndField = $("input#"+$opener.attr("data-dateend-field-id"));
        // Перепроверяем не случилось ли какое-нибудь недоразумение в виде не найденного поля,
        // Если случилось - громко материмся и валим функцию
        if (!this.dateStartField.size()) {
            throw("DateDialog: dateend field id recieved, but field is missing");
            return false;            
        }
    // Если аттрибут указывающий id отсутствует
    } else {
        // Смотрим есть ли аттрибут указывающий класс поля конечной даты
        if (typeof $opener.attr("data-dateend-field-class") != "undefined") {
            // Если есть - сохраняем jQ-obj первого попавшегося поля с таким классом
            this.dateEndField = $("input."+$opener.attr("data-dateend-field-class")+":first");
            // Перепроверяем не случилось ли какое-нибудь недоразумение в виде не найденного поля,
            // Если случилось - громко материмся и валим функцию
            if (!this.dateStartField.size()) {
                throw("DateDialog: dateend field class recieved, but field is missing");
                return false; 
            }
        } else {
            // Если не передан ни один из аттрибутов поля
            throw("DateDialog: can't find dateend field, no attributes for search");
            return false;
        }
    }    
}


// Конвертит timestamp в dd.mm.YYYY
DateDialogAPI.prototype.timestampToNormal = function(timestamp) {
        var timestamp = new Date(timestamp);
        var date = timestamp.getDate();
        var month = timestamp.getMonth()+1;

        if (date < 10) { date = "0" + date; }
        if (month < 10) { month = "0" + month; }

        var showDate = date + '.' + month + '.' + timestamp.getFullYear();

        return showDate;
}

// Сallback на создание календаря
DateDialogAPI.prototype.onShow = function() {
    console.log("DateDialog: onShow event triggered");
}

// Сallback на скрытие календаря
DateDialogAPI.prototype.onHide = function() {
    console.log("DateDialog: onHide event triggered");
}

// Сallback на отрисовку календаря
DateDialogAPI.prototype.onRender = function(date) {
    console.log("DateDialog: onRender event triggered >> " + this.timestampToNormal(date));

    return {
        // 'selected': [],
        // 'disabled': [],
        'className': (this.timestampToNormal(date.valueOf()) == this.timestampToNormal(Date.now())) ? 'today' : ''
    }
}

// Сallback на изменение календаря
DateDialogAPI.prototype.onChange = function(formated, dates) {
    console.log(formated);
    console.log(dates);

    // Если всё правильно и нам пришло 2 даты, то записываем их в сообщение о выборе и в поля формы
    if (formated.length == 2) {
        $("#date_dialog").find(".range-caption").show();
        $("#date_dialog").find(".range-caption").find(".datestart").text(formated[0]);
        $("#date_dialog").find(".range-caption").find(".dateend").text(formated[1]);
        $(this.dateStartField).val(formated[0]);
        $(this.dateEndField).val(formated[1]);
        $(this.opener).text("c " + formated[0] + " по " + formated[1]);
    }
}

// Функция сброса выбранных значений календаря
DateDialogAPI.prototype.reset = function() {
    $(this.container).DatePickerClear();
    $(this.dateStartField).val('');
    $(this.dateEndField).val('');
    $("#date_dialog").find(".range-caption").find(".datestart").text('--.--.----');
    $("#date_dialog").find(".range-caption").find(".dateend").text('--.--.----');    
    $("#date_dialog").find(".range-caption").hide();
    $(this.opener).text(this.openerInitText);
}


// Функция показа окна диалога
DateDialogAPI.prototype.open = function() {
    this.showModal("#"+this.container_id);
}

// Этот метод пустой потому-что он переопределяется в vidget.js
DateDialogAPI.prototype.showModal = function() {

}

// Функция создающая объект API в глобальном пространстве для последующей работы с ним
function callDateDialog(e) {
    // Если APIшечка ещё не поднята - поднимаем её
    if (typeof DateDialog == "undefined") {
        DateDialog = new DateDialogAPI(e);
    }
    // Открываем окно диалога
    DateDialog.open();

    e.preventDefault();
    return DateDialog;
}