<?php

/*
 * Blocks API class
 *
 *	@author 	:	V.Biryukov
 *	@date 		:	14.01.2015
 *
 */

class Blocks {

		var $blocks_dir = null;
		var $block_types = array();
		var $rendered_blocks = array();
		var $blocks_js = array();
		var $blocks_css = array();
		
		private $ignore_css;

		/*
		 *	@TODO: тут нужно будет реализовать механизм опроса для определения доступных типов блоков, ...?
		 *
		 *
		 */
		function __construct($blocks_dir = null, $ignore_css = false) {
			global $khabkrai;
			
			$this->ignore_css = $ignore_css;

			$khabkrai->AttachJS('/classes/blocks/blocksAPI.js', 'footer');

			// Адрес папки с блоками
			//$blocks_dir = __DIR__ . "/../../blocks";

			if ($blocks_dir == null) {
	            $blocks_dir = __DIR__;
	            $blocks_dir = str_replace('/classes/blocks','',$blocks_dir).'/blocks';
			}

			$this->blocks_dir = $blocks_dir;

			// Сканируем подпапки
			$subfolders = $this->scandir($blocks_dir);
			if ($subfolders) {
				// С каждой предполагаемой папкой блока
				foreach ($subfolders as $subfolder) {
					// Сканим содержимое папки
					$block_files = $this->scandir($blocks_dir.'/'.$subfolder);
					if(is_array($block_files)){
                        // Если есть php-файл блока
                        if (in_array('block.php', $block_files)) {
                            // Регистрируем тип блока по имени папки
                            $this->block_types[] = $subfolder;
                        }
                        // Если есть js-файл блока
                        if (in_array('block.js', $block_files)) {
                            // Регистрируем тип блока по имени папки
                            $this->blocks_js[$subfolder] = '/blocks/'.$subfolder.'/block.js';
                        }
                        // Если есть css-файл блока
                        if (in_array('block.css', $block_files)) {
                            // Регистрируем тип блока по имени папки
                            $this->blocks_css[$subfolder] = '/blocks/'.$subfolder.'/block.css';
                        }
                    }
				}
			} else {
				$this->error("Missing blocks main folder!", E_USER_WARNING);
				return;
			}
			return $this;
		}

		// Обёртка стандартной функции
		private function scandir($dir) {
			// Получаем содержимое папки
			if(is_dir($dir)){
            $filelist = scandir($dir);
			// Если есть стандартные "." и ".."			
			if (is_array($filelist) && $filelist[0] = '.') {
				// Убираем их
				$filelist = array_slice($filelist, 2);
			}

			return $filelist;	}else{
                return false;
            }
		}

		// Обёртка функции trigger_error выводящая сообщение с префиксом
		public function error($msg = "", $type = E_USER_WARNING) {
			trigger_error("Blocks API: " . $msg, $type);
		}

		/*
		 *	Метод получающий новый порядковый номер для блока указанного типа
		 *
		 *	@return: (int) новый порядковый номер для блока указанного типа
		 *
		 */
		function get_block_delta($block_type) {
			if ($this->is_rendered($block_type)) {
				return $this->rendered_blocks[$block_type] + 1;
			} else {
				return 1;
			}
		}

		/*
		 *	Метод ...
		 *
		 *	@return: (bool) существует ли такой тип блока
		 *
		 */
		function block_type_exists($block_type) {
			return (in_array($block_type, $this->block_types));
		}

		/*
		 *	Метод ...
		 *
		 *	@return: (bool)	был-ли отрендерен блок до этого	 
		 *
		 */
		function is_rendered($block_type) {
			return (array_key_exists($block_type, $this->rendered_blocks) && $this->rendered_blocks[$block_type] !== 0);
		}

		/*
		 *	Метод ...
		 *
		 *	@return: (string) html-код блока		 
		 *
		 */
		function render() {

            global $khabkrai;

			// Получаем все аргументы переданные методу...
			$args = func_get_args();
			// Если вообще они есть
			if (count($args)) {
				// Первый аргумент является типом блока который нужно отрисовать (да потому-что так задумано!)
				$block_type = $args[0];
				// Остальные аргументы это аргументы, которые нужно будет пробросить в функцию самого блока
				$additional_args = array_slice($args, 1);

				// Подчищаем память, а то мало-ли сколько там чего
				unset($args);

				// Если вызываемый тип блока существует
				if ($this->block_type_exists($block_type)) {
					// Строим путь до php-файла блока
					// $block_file_path = __DIR__ . "/../../blocks/" . $block_type . "/block.php";
					$block_file_path = $this->blocks_dir . '/' . $block_type . "/block.php";
					// Если файл блока никуда не пропал
					if (file_exists($block_file_path)) {
						$block_delta = $this->get_block_delta($block_type);
						$block_id = 'block-' . preg_replace("/\_/", "-", $block_type) . '--' . $block_delta;
						$block_name = 'block_' . preg_replace("/\-/", "_", $block_type) . '__' . $block_delta;
						$block_class = 'block-' . preg_replace("/\_/", "-", $block_type);
						// Подключаем его
						include_once $block_file_path;
						// Вызываем функцию-конструктор блока и пробрасываем в неё массив полученных в этом методе аргументов 
						// (кроме типа блока естественно)
						$block_html = call_user_func_array($block_type."\\block_content", $additional_args);
						// Если блок вернул нам контент
						if ($block_html) {
							$output = '<div id="' . $block_id . '" class="block '.$block_class.'">';
							$output.= $block_html;
							$output.= '</div><!-- /' . str_replace('--', '- -', $block_id) . ' -->';

							// Отмечаемся во всех наших учётных свойствах API что мы запилили ещё один блок 
							if (!array_key_exists($block_type, $this->rendered_blocks)) {
								$this->rendered_blocks[$block_type] = 1;
							} else {
								$this->rendered_blocks[$block_type]++;
							}

?><!-- cbaj:<?= __LINE__ ?> --><?php

                            $khabkrai->AttachJS('/classes/blocks/blocksAPI.js', 'footer');
                            // $khabkrai->AttachJS('/blocks/'.$block_type.'/block.js', 'footer');

                            if (array_key_exists($block_type, $this->blocks_js)) {
                            	$khabkrai->AttachJS($this->blocks_js[$block_type], 'footer');
                            }

							return $output;
						} else {
							// $this->error($block_id . ' rendering failed!', E_USER_WARNING);
                            //$khabkrai->AttachJS($block_type, 'footer');
							return '';
						}
					}
				} else {
					$this->error('block type "' . $block_type . '" doesn\'t exists!', E_USER_WARNING);
				}
			} else {
				$this->error('missing block type argument', E_USER_WARNING);
				return '';
			}

		}

		/*
		 *	Метод ...
		 *
		 *	@return: (array) простой (не ассоциативный) массив демострирующий количество аргументов и их структуру если они являются объектами или массивами параметров
		 *
		 */
		function explain($block_type) {
			// Если вызываемый тип блока существует
			if ($this->block_type_exists($block_type)) {
				// Строим путь до php-файла блока
				$block_file_path = __DIR__ . "/../../blocks/" . $block_type . "/block.php";
				// Если файл блока никуда не пропал
				if (file_exists($block_file_path)) {
					// Подключаем его
					include_once $block_file_path;
					return call_user_func($block_type . "\\block_arguments");
					 // block_arguments();
				}
			} else {
				$this->error('block type "' . $block_type . '" doesn\'t exists!', E_USER_WARNING);
			}
		}

		/*
		 *	Метод ...
		 *
		 *	@return: (string) html-код подключения css-файлов подключеных блоков		 
		 *
		 */
		function css() {
			global $khabkrai;

			if ($this->ignore_css)
				return;

			if (!count($this->blocks_css)) { return ''; }

			// $output = "<!-- Blocks styles -->\n";
			

			foreach ($this->blocks_css as $block_type => $css_path) {
				// $output .= '<link href="'.$css_path.'" rel="stylesheet" type="text/css" />' . "\n";
				$khabkrai->AttachCSS($css_path);
			}

			// $output.= "<!-- /Blocks styles -->\n";

			// return $output;
			// return $this->blocks_css;
		}

		/*
		 *	Метод ...
		 *
		 *	@return: (string) html-код подключения js-файлов подключеных блоков		 
		 *
		 */
		function js() {
			global $khabkrai;

			if (!count($this->blocks_js)) { return ''; }

			// $khabkrai->AttachJS('/classes/blocks/blocksAPI.js', 'footer');

			foreach ($this->blocks_js as $block_type => $js_path) {
				if ($this->is_rendered($block_type)) {
					// $output .= '<script src="'.$js_path.'" type="text/javascript"></script>' . "\n";
					// $khabkrai->AttachJS($js_path, 'footer');
				}
			}
		}


	}

?>
