<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

include($this->locateTemplate('/f_header'));

$view_map = array();

if ( ! isset($status))
    $status = '';

?>

    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1 ?></h1>
    </div>

    <div class="contentblock basemargin">

    <?php
    if ( ! empty($message)) {
        ?><div class="message <?php if ( ! empty($status)) {?> <?= $status ?><?php } ?>"><?= $message ?></div>
        <?php
    }

    if ($status == 'error' && ! empty($errorInfo)) {
        ?>
        <div class="error-info">Технические детали:<br/><pre><code><?= $errorInfo ?></code></pre></div>
        <?php
    }

    $action = $this->values->action;
    $action_view = isset($view_map[$action]) ? $view_map[$action] : $action;

    include($this->partial($action_view));

    ?>
</div>
<style>
    .usersettings .feedback-form label {
        font-weight: bold;
        display: block;
        width: 300px;
        float: left;
        margin-right: 20px;
        line-height: 32px;
        text-align: right;
    }
    .usersettings .feedback-form label,
    .usersettings .feedback-form .row {
        line-height: 32px;
    }
    .usersettings .feedback-form label,
    .usersettings .feedback-form .row {
        height: 33px;
    }
    .usersettings .feedback-form button,
    .usersettings .feedback-form input[type="text"],
    .usersettings .feedback-form input[type="email"] {
        height: 32px;
    }
    .usersettings .feedback-form .row {
        overflow: hidden;
        margin: 1em 0;
    }
    .usersettings .feedback-form input[type="text"],
    .usersettings .feedback-form input[type="email"] {
        line-height: 26px;
    }
    .usersettings .feedback-form input[type="text"],
    .usersettings .feedback-form input[type="email"],
    .usersettings .feedback-form textarea {
        box-sizing: border-box;
        padding: 2px 8px;
        border: 1px solid silver;
        margin: 0;
        width: 520px;
    }

    .usersettings .feedback-form textarea {
        line-height: 20px;
    }

    .usersettings .feedback-form button {
        padding: 4px 12px;
        line-height: 20px;
        margin-left: 320px;
    }
</style>
<?php

include($this->locateTemplate('/f_footer'));
