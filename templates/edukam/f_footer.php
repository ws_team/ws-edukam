<?php if ( ! $this->isAjaxRequest()) : ?>

</div>
<footer class="footer">
    <div class="footer__top">
        <div class="container">
            <div class="body-2 footer__menu">
                <div class="footer__menu-column">

                    <div class="subtitle-1 footer__menu-title">
                        <a href="<?php echo GetMaterialTypeUrl($this, 116) ?>" class="footer__link">Сведения об организации</a>
                    </div>

                    <ul class="vertical-links-list">
                        <?php printFooterMenuLinks($this, 116); ?>
                    </ul>

                    <div class="subtitle-1 footer__menu-title">
                        <a href="<?php echo GetMaterialTypeUrl($this, 144) ?>" class="footer__link">Профилактика терроризма</a>
                    </div>

                </div>

                <div class="footer__menu-column">

                    <div class="subtitle-1 footer__menu-title">
                        <a href="<?php echo GetMaterialTypeUrl($this, 132) ?>" class="footer__link">Учащимся/воспитанникам</a>
                    </div>

                    <ul class="vertical-links-list">
                        <?php printFooterMenuLinks($this, 132); ?>
                    </ul>

                    <div class="subtitle-1 footer__menu-title">
                        <a href="<?php echo GetMaterialTypeUrl($this, 108) ?>" class="footer__link">Родителям</a>
                    </div>

                    <ul class="vertical-links-list">
                        <?php printFooterMenuLinks($this, 108); ?>
                    </ul>

                </div>

                <div class="footer__menu-column">

                    <div class="subtitle-1 footer__menu-title">
                        <a href="<?php echo GetMaterialTypeUrl($this, 112) ?>" class="footer__link">Педагогам</a>
                    </div>

                    <ul class="vertical-links-list">
                        <?php printFooterMenuLinks($this, 112); ?>
                    </ul>

                    <div class="subtitle-1 footer__menu-title">
                        <a href="<?php echo GetMaterialTypeUrl($this, 2) ?>" class="footer__link">События</a>
                    </div>

                    <ul class="vertical-links-list">
                        <?php printFooterMenuLinks($this, 2); ?>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <?php

    $headerInfo             = MaterialList($this, 148, 1, 1);
    $headerInfoLogoId       = MaterialGetFirstPhoto($this, $headerInfo[0]['id']);
    $footerInfo             = MaterialList($this, 146, 1, 1);
    $footerInfoExtraParams  = GetMaterialParams($this, $footerInfo[0]['id']);
    $copyrightLabel         = '';
    $socialLinks            = MaterialList($this, 145, 1, 1000);

    foreach ($footerInfoExtraParams as $item){
        if($item['name'] == 'Подпись правообладателя'){
            $copyrightLabel = $item['value'];
            break;
        }
    }

    ?>

    <div class="footer__bottom">
        <div class="container footer__bottom-wrap-content">
            <div class="copyright">
                <div class="copyright__wrap-logo">

                    <?php if($headerInfoLogoId): ?>

                        <img src="/photos/<?php echo $headerInfoLogoId ?>" class="copyright__logo" alt="">

                    <?php else: ?>

                        <img src="/img/content/logo.png" class="copyright__logo" alt="">

                    <?php endif; ?>

                </div>
                <div class="body-2 copyright__text"><?php echo $copyrightLabel ?></div>
            </div>

            <?php if($socialLinks): ?>

                <div class="social-links social-links--footer">

                    <?php printSocialLinks($this, $socialLinks, 'social-links__item--footer'); ?>

                </div>

            <?php endif; ?>

            <div class="wrap-developer-link">
                <a href="https://thewhite.ru/" class="developer-link" target="_blank" rel="noopener noreferrer">
                    <svg class="developer-link__logo"><use xlink:href="/img/sprite.svg#decor--wsLogoWhite"></use></svg>
                    <div class="body-1 developer-link__text">создано<br>2019</div>
                </a>
            </div>
        </div>
    </div>
</footer>



<?php

$this->wpf_include('specialver_functions.php');

//include($this->partial('/f_footer/ya_metrika'));

if (isset($this->data->blocks)) {
    $this->data->blocks->js();
}

//$this->AttachJS( $this->settings->templateurl .'/js/vidgets.js', 'footer');

echo $this->IncludeMinJS($jsfooterarr);

?>


</body>
</html>

<?php endif;
