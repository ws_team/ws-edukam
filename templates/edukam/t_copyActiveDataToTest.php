<?php

// include_once($this->settings->path.$this->settings->templateurl.'/f_header.php');
include_once($this->locateTemplate('f_header'));

?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>
<div class="contentblock basemargin">
    <p class="errortext"><?php echo $this->data->errortext;  ?></p>
</div>
<?php

//include_once($this->settings->path.$this->settings->templateurl.'/f_footer.php');
include_once($this->locateTemplate('f_footer'));

?>