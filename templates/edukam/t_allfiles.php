<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

//die('step2_1');

defined('_WPF_') or die();

global $sites, $stypes, $wrongfiles;

include($this->locateTemplate('f_header'));


$this->data->iH1 = 'Системные настройки';

//die('step2_2');

?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>

<div class="contentblock basemargin">
    <p class="errortext"><?= ! empty($this->data->errortext) ? $this->data->errortext : ''  ?></p>

    <p>&nbsp;</p>
    <table>
		<tr>
			<th>Путь</th>
			<th>Файл</th>
			<th>Создание файла</th>
            <th>Размер предполагаемый</th>
            <th>Размер реальный</th>
		</tr>
		<?php

		if(is_array($wrongfiles) && count($wrongfiles) > 0){
			foreach ($wrongfiles as $file){
                echo '<tr>
                    <td>'.$file['patch'].'</td>
                    <td>'.$file['name'].'</td>
                    <td>'.$file['filedatecreate'].'</td>
                    <td>'.$file['size'].'</td>
                    <td>'.$file['filesize'].'</td>
            </tr>';
			}
		}

		?>
	</table><?php die('step2_3'); ?>
	<p>&nbsp;</p>

</div>
<?php

include($this->locateTemplate('f_footer'));

?>