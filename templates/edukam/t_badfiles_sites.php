<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

//die('step2_1');

defined('_WPF_') or die();

global $sites, $stypes, $wrongfiles, $outarr, $days, $sitesFiles;

include($this->locateTemplate('f_header'));


$this->data->iH1 = 'Системные настройки';

//die('step2_2');

ksort($days);
//uksort($a, "cmp");

?>
<div class="container container--admin-title">
    <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
</div>
<div class="contentblock basemargin">
    <p class="errortext"><?= ! empty($this->data->errortext) ? $this->data->errortext : ''  ?></p>

    <p>&nbsp;</p>
    <table>
		<tr>
			<th>Сайт</th>
            <?php

            foreach ($days as $day)
            {
                $daystr=date('d.m.Y',$day);
                echo '<th>'.$daystr.'</th>';
            }

            ?>
		</tr>
		<?php

            //список сайтов
            foreach ($sitesFiles as $site)
            {
                echo '<tr><th>'.$site['domain'].'<br>'.$site['file_folder'].'</th>';
                    foreach ($days as $dayz)
                    {
                        if(isset($outarr[$site['id']][$dayz]))
                        {
                            $countall=count($outarr[$site['id']][$dayz]);
                            $counwrong=0;
                            foreach ($outarr[$site['id']][$dayz] as $files)
                            {
                                if($files['wrong'])
                                {
                                    ++$counwrong;
                                }
                            }
                        }
                        else
                        {
                            $countall=0;
                            $counwrong=0;
                        }

                        if($dayz < $site['folderdate'])
                        {
                            $color='#ffdddd';
                        }
                        else
                        {
                            $color='#ddffdd';
                        }

                        if($counwrong > 0)
                        {
                            $color='#ffaaaa';
                        }

                        echo '<td style="background-color:'.$color.';">'.$countall.'<br>'.$counwrong.'</td>';

                    }

                echo '</tr>';
            }

		?>
	</table><?php die('step2_3'); ?>
	<p>&nbsp;</p>

</div>
<?php

include($this->locateTemplate('f_footer'));

?>