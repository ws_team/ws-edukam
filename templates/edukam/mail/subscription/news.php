<?php
/**
 * Шаблон письма.
 *
 * @var array $items
 * @var string $unsubscribeUrl
 * 
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


$layout = 'layout';

$hostname = ! empty($site->settings->primary_domain) ?
    $site->settings->primary_domain :
    getenv('HTTP_HOST');

$siteName = subscription_settings($site, 'hostname', $hostname);

if (!isset($title))
    $title = 'Последние события на '.$siteName;

ob_start();

foreach ($items as $item) {
    ?>
    <article>
        <div>
            <time datetime="<?=$item['time']->format('c')?>"><?=
                $item['time']->format('d.m.Y')?></time>
            <?php
            if (!empty($item['category'])) {
                ?>
                <a href="<?=htmlspecialchars($item['category']['url'])?>"><?=
                    htmlspecialchars(
                        $item['category']['name']
                    )
                ?></a>
                <?php
            }
            ?>
        </div>
        <h1><a href="<?= htmlspecialchars($item['url']) ?>"><?=
            $item['title'] ?></a></h1>
        <?php
        if (!empty($item['image'])) {
            ?><a href="<?=htmlspecialchars($item['url'])?>">
                <img src="<?=htmlspecialchars($item['image'])?>" alt="" width="200" />
            </a>
            <?php
        }
        ?>
        <p><?=nl2br($item['text'])?></p>
        <a href="<?=htmlspecialchars($item['url'])?>"><?=htmlspecialchars($item['url'])?></a>
    </article>
    <?php
}

$content = ob_get_clean();

ob_start();
include($site->locateTemplate('mail/subscription/footer'));
$footer = ob_get_clean();

include($layout.'.php');
