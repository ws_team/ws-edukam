<div class="breadcrumbs"><?php echo $this->data->breadcrumbs; ?></div>
<h1><?php echo $this->data->iH1; ?></h1>
<?php

$this->GetPostValues('region');

if(($this->values->maintype == 1)&&($this->values->sectype == 2))
{

    //формируем новый список
    if($this->values->region != '')
    {
        //все новости регионов
        if($this->values->region == 'all')
        {
            $ids='';
            $query='SELECT "material_child" "mid" FROM "material_connections" WHERE "material_parent" IN (SELECT "id" FROM "materials" WHERE "type_id" = 21)
UNION ALL
SELECT "material_parent" "mid" FROM "material_connections" WHERE "material_child" IN (SELECT "id" FROM "materials" WHERE "type_id" = 21)';

            if ($res=$this->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {
                        if($ids != '')
                        {
                            $ids.=',';
                        }

                        $ids.=$row['mid'];

                    }
                }
            }

            if($ids == '')
            {
                $ids=-1;
            }

            $materials=MaterialList($this, '', $this->values->page, $this->values->perpage, 2, $ids, 2);

        }
        //новости конкретного региона
        else
        {
            $ids='';
            $query='SELECT "material_child" "mid" FROM "material_connections" WHERE "material_parent" IN (SELECT "id" FROM "materials" WHERE "id_char" = '."'".$this->values->region."'".')
UNION ALL
SELECT "material_parent" "mid" FROM "material_connections" WHERE "material_child" IN (SELECT "id" FROM "materials" WHERE "id_char" = '."'".$this->values->region."'".')';

            //print($query);

            if($res=$this->dbquery($query))
            {
                if(count($res) > 0)
                {
                    foreach($res as $row)
                    {
                        if($ids != '')
                        {
                            $ids.=',';
                        }

                        $ids.=$row['mid'];

                    }
                }
            }

            if($ids == '')
            {
                $ids=-1;
            }

            $materials=MaterialList($this, '', $this->values->page, $this->values->perpage, 2, $ids, 2);

        }
    }


    //echo $this->values->maintype.' - '.$this->values->sectype;
    ?>
    <select class="styler" onchange="document.location.href='/events/news/&region='+$(this).val();">
        <option value=""><?php echo localize('Все новости',$this->data->language,$localize); ?></option>
        <?php
        //рисуем опции - регионы сайта
        $regions=MaterialList($this, 21, 1, 1000, 2);
        if(count($regions) > 0)
        {
            foreach($regions as $reg)
            {
                $selected='';

                if($reg['id_char'] == $this->values->region)
                {
                    $selected='selected';
                }


            echo '<option '.$selected.' value="'.$reg['id_char'].'">'.$reg['name'].'</option>';
            }
        }
        ?>
    </select><p>&nbsp;</p>
<?php
}
elseif($this->values->sectype == 23){
    $query = 'SELECT "id" FROM "materials" WHERE "type_id"=2 AND "language_id" = '.$this->data->language.' AND "status_id" = 2 AND "id" IN
(

SELECT "c"."material_parent" FROM "material_connections" "c" WHERE "c"."material_child" = 1222

UNION ALL

SELECT "cc"."material_child" FROM "material_connections" "cc" WHERE "cc"."material_parent" = 1222

)
';

    $ids='';

    if($res=$this->dbquery($query))
    {
        if(count($res) > 0)
        {
            foreach($res as $row)
            {
                if($ids != '')
                {
                    $ids.=',';
                }
                $ids.=$row['id'];
            }
        }
    }

    if($ids != '')
    {
        $materials = MaterialList($this, 2, $this->values->page, $this->values->perpage, 2, $ids);
    }
    else{
        $materials = Array();
    }


}

?>
<div class="doubleblock">
    <div class="doubleleft">
        <div class="normalblock">
            <div class="rss">
                <a href="/?menu=rss" target="_blank">RSS <img align="right" src="/templates/khabkrai/img/rss.png" width="16" height="16"></a>
            </div>
        </div>
        <?php

        $mcount=count($materials);
        $mi=0;

        if (count($materials[0]) > 0) {
            foreach ($materials as $material) {
                ++$mi;

                $stylewidth='';

                echo "<a class='material_list' href=\"document.location.href='/".$this->values->maintypechar."/".$this->values->sectypechar."/$material[id_char]'\">";

                if (!empty($material['video'])) {
                    $videoid = $material['video'];
                    $videoid = get_youtube_video_id($videoid);
                    $videoInfo = FileGetInfo($this, $material['videoid']);

                    ?>
                    <div class='material_list_image xmedia mt-video s158'>
                        <div class='image_container media-content'
                             style="background-image: url('http://img.youtube.com/vi/<?= $videoid ?>/mqdefault.jpg'); background-size: 100%">
                            <div class='image_container_icon'>
                                <img class='svg playsmall-icon' src='/templates/khabkrai/img/play.svg' />
                            </div>
                        </div>
                        <div class="media-textview">Видеоролик: <?=
                            htmlspecialchars($videoInfo['name']) ?></div>
                    </div>
                    <?php

                    $stylewidth="style='width: 432px;'";

                } elseif(!emptu($material['photo'])) {
                    $photoInfo = FileGetInfo($this, $material['photo']);

                    ?>
                    <div class='material_list_image xmedia s158 mt-image'>
                        <div class='image_container'>
                            <img src='/photos/<?= $material['photo'] ?>_x158.jpg' class="media-content">
                        </div>
                        <div class="media-textview">Изображение: <?=
                            htmlspecialcahrs($photoInfo['name']) ?></div>
                    </div>
                    <?php

                    $stylewidth="style='width: 432px;'";
                }

                if ($this->data->language == LANG_RU) {
                    $material['date_event']=russian_datetime_from_timestamp($material['unixtime']);
                } else {
                    $material['date_event']=en_datetime_from_timestamp($material['unixtime']);
                }

                ?>
                <div class='material_list_content' <?= $stylewidth ?>>
                    <p class='material_list_date'><?= $material['date_event'] ?></p>
                    <p class='material_list_name'><?= $material['name'] ?></p>
                    <p class='material_list_anons'><?= $material['anons'] ?></p>
                    <?= $material['tags'] ?>
                </div>
            </a>
            <?php
            if($mi != $mcount) {
                ?><div class='material_list_line'></div><?php
            }
        }
    }

    echo vidjet_materiallist_pages($this, $ids);
    ?>
</div>



    if(($this->values->region == '')||($this->values->region == 'all'))
    {
        echo  vidjet_right_photovideo($this);
    }
    else{
        echo  vidjet_right_regioninfo($this, $this->values->region);
    }

    ?>
</div>