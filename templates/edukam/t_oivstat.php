<?php

if (($this->autorize->autorized == 1)&&($this->autorize->userrole == 1)) {
    include_once($this->settings->path.$this->settings->templateurl.'/f_header.php');
    include_once($this->locateTemplate('f_header'));



    function oiv_get_select_item($dir) { 
        global $khabkrai;
         
        if (file_exists($dir . '/' . 'settings.php')) {

            // получаем настройки
            include($dir . '/' . 'settings.php');

            return array(
                'dbname' => $setup['DB']['login'],
                'title' => $setup['data']['title'],
                );

        } else {

            $khabkrai->data->errortext .= 'Отсутствует файл <i>'.$dir . '/' . 'settings.php</i>';

        }    

        if (isset($setup)) {
            unset($setup);
        }
    } 

//page body===========================================================================

    $oivcat = '/oiv/';

    // Список ОИВов для копирования
    $oivlist = array(
        'gku',
        'komcit',
        'komgrz',
        'komgzk',
        'kominv',
        'kompimk',
        'komsud',
        'komtek',
        'komtrud',
        'minec',
        'minfin',
        'minio',
        'minit',
        'minjkh',
        'minkult',
        'minobr',
        'minpt',
        'minsh',
        'minsport',
        'minstr',
        'minszn',
        'minzdrav',
        'mpr',
        'predstav',
        'uprarch',
        'uprles',
        'uprovr',
        'uprvet',
        'uprzags',
        );

    $oivcat = $this->settings->pathglobal . $oivcat;

    // код для работы с оивами

    ini_set('max_execution_time', 3000);
    ini_set('memory_limit', 5205987900);

    // chmod($oivcat, 0777);

    $oivs_select_options = array();

    if (chdir($oivcat)) {

        foreach ($oivlist as $oiv_machine_name) {
            $oivs_select_options[] = oiv_get_select_item($oivcat . $oiv_machine_name);
        }
    }


?>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin">
        <p class="errortext"><?php echo $this->data->errortext;  ?></p>

        <p>&nbsp;</p><form method="get" action="/">
        c <input type="text" name="startdate" class="styler" id="startdate" value="<?php echo $this->values->startdate; ?>"> по <input type="text" name="enddate" id="enddate" class="styler" value="<?php echo $this->values->enddate; ?>">
        <input type="submit" value="Обновить" class="styler" style="margin-top: 0px; margin-left: 10px;"><input type="hidden" name="menu" value="oivstat">
        <br><select name="oiv" class="styler">
            <option value="">-</option>
            <?php 

                foreach ($oivs_select_options as $option) {
                    print '<option value="'.$option['dbname'].'" '.(($this->values->oiv == $option['dbname']) ? ' selected' : '').'>'.$option['title'].'</option>';
                }

            ?>
        </select></form>
        <p>&nbsp;</p>

        <?php

        if(count($this->data->stat) > 0)
        {

            if($this->values->mode == 'accesslogs')
            {
                echo '<p><a href="/?menu=oivstat">Статистика заполнения контента</a></p><p>&nbsp;</p>';

                echo '<table>
            <tr>
                <th>Действие</th>
                <th>Пользователь</th>
                <th>Время</th>
            </tr>';
                foreach($this->data->stat as $row)
                {
                    echo "<tr>
                    <td>$row[actionname]</td>
                    <td>id: $row[userid]
                    <br>имя: <b>$row[username]</b>
                    <br>логин: <b>$row[userlogin]</b></td>
                    <td>".date('d.m.Y H:i:s',$row['actiondate'])."</td>
                </tr>";
                }
                echo '</table>';
            }
            else
            {
                echo '<p><a href="/?menu=oivstat&mode=accesslogs">Логи действий</a></p><p>&nbsp;</p>';

                echo '<table>
            <tr>
                <th>Сайт</th>
                <th>Действие</th>
                <th>Пользователь</th>
                <th>Кол-во действий</th>
            </tr>';
                foreach($this->data->stat as $row)
                {
                    echo "<tr>
                    <td>$row[site]</td>
                    <td>$row[actionname]</td>
                    <td>id: $row[userid]
                    <br>имя: <b>$row[username]</b>
                    <br>логин: <b>$row[userlogin]</b></td>
                    <td>$row[ocnt]</td>
                </tr>";
                }
                echo '</table>';
            }


        }else{
            echo '<p>За выбранный период данных нет</p>';
        }

        ?>
    </div>
    <?php

    //include_once($this->settings->path.$this->settings->templateurl.'/f_footer.php');
    include_once($this->locateTemplate('f_footer'));

}
?>