<?php
/**
 * @var \iSite $this
 */


global
    $jsheaderarr,
    $jsfooterarr,
    $cssheaderarr;

$is_auth_user =  ! $this->getUser()->isGuest();

//krumo($this);

// Если это не AJAX-запрос то выводим всю шапку, стили, скрипты
if (!$this->isAjaxRequest()):

    include($this->partial('/f_header/head'));

    ?>

</head>

    <?php

    if(!isset($_SESSION['viewtime']))
    {
        $_SESSION['viewtime'] = time();
    }

    //проверяем сколько прошло времени
    $ntime = time();
    if ($ntime - $_SESSION['viewtime'] > 60*10)
    {
        if ( ! empty($this->data->atype) && $this->data->atype == 1)
        {
            unset($_SESSION['underconstructionbanner']);
        }
    }

    $_SESSION['viewtime'] = time();

    $this->GetPostValues(Array('err_fio'));

    $callbackresanswer = '';

    if (!empty($this->values->err_fio)) {
        $sucessresult=callbacksave($this);

        if ($sucessresult[0]) {
            $callbackresanswer=$sucessresult[1];

            ?>
            <script>
                $(document).ready(function() {
                    showModal("callbackresult");
                });
            </script>
            <?php
        }
    }

    $bodyClassModifier = '';

    if(isset($_REQUEST['media']) == 'print'){
        $bodyClassModifier .= ' body--print';
    }

    if(specialver_is_active($this)){
        $bodyClassModifier .= ' body--special-version';
    }

    $explodeURIArray = explode('?',$_SERVER["REQUEST_URI"]);

    ?>

    <body class="body body--stretch-height <?php echo $bodyClassModifier ?>">

    <div class="global-wrap">

    <?php

    //подключаем админ-меню для админа
    if ($is_auth_user){
        include_once($this->locateTemplate('f_adminpanel'));
    }

    $headerInfo             = MaterialList($this, 148, 1, 1);
    $headerInfoLogoId       = MaterialGetFirstPhoto($this, $headerInfo[0]['id']);
    $headerInfoExtraParams  = GetMaterialParams($this, $headerInfo[0]['id']);
    $headerPhone            = '';

    foreach ($headerInfoExtraParams as $item){
        if($item['name'] == 'Телефон'){
            $headerPhone = $item['value'];
            break;
        }
    }

    ?>

    <button type="button" id="buttonUpPage" class="btn-up-page">
        <svg class="btn-up-page__icon"><use xlink:href="/img/sprite.svg#decor--arrowUp"></use></svg>
    </button>

    <header class="header <?php echo $is_homepage ? 'header--frontpage' : '' ?>">
        <div class="header__top">
            <div class="header__left-side">
                <a href="/">
                    <div class="header__wrap-logo">
                        <div class="header__mount-logo"><img src="/img/decor/logoMount.png"></div>

                        <div class="header__logo">

                            <?php if($headerInfoLogoId): ?>

                                <img src="/photos/<?php echo $headerInfoLogoId ?>" alt="">

                            <?php else: ?>

                                <img src="/img/content/logo.png" alt="">

                            <?php endif; ?>
                        </div>
                    </div>
                </a>

                <?php

                if (isset($this->data->material_types)) {

                    echo '<ul class="body-2 header__menu header__menu--main" id="mobileMenu">';

                    foreach($this->data->material_types as $matType) {

                        echo '<li class="header__menu-item">';

                        printMainMenuLink($this, $matType);

                        if(isset($matType['childs'])) {

                            echo '<ul class="header__menu header__menu--submenu">';

                            foreach($matType['childs'] as $childMatType) {

                                echo '<li>';

                                printMainMenuLink($this, $childMatType);

                                if(isset($childMatType['childs'])) {

                                    echo '<ul class="header__menu header__menu--submenu">';

                                    foreach($childMatType['childs'] as $subChildMatType) {

                                        echo '<li>';

                                        printMainMenuLink($this, $subChildMatType);

                                        echo '</li>';

                                    }

                                    echo '</ul>';

                                }

                                echo '</li>';

                            }

                            echo '</ul>';

                        }

                        echo '</li>';

                    }

                    if(specialver_is_active($this)):
                        ?>

                        <li class="header__impaired-version-menu-link">
                            <a href="<?php echo $explodeURIArray[0] ?>?version=normal">Обычная версия сайта</a>
                        </li>

                    <?php else: ?>

                        <li class="header__menu-item header__impaired-version-menu-link">
                            <a href="<?php echo $explodeURIArray[0] ?>?version=special">Версия сайта для слабовидящих</a>
                        </li>

                    <?php

                    endif;

                    $socialLinks = MaterialList($this, 145, 1, 1000);

                    if($socialLinks):

                    ?>

                        <li class="social-links social-links--mobile-menu-item">

                            <?php printSocialLinks($this, $socialLinks, 'social-links__item--header'); ?>

                        </li>

                    <?php

                    endif;

                    echo '</ul>';

                }

                ?>

            </div>
            <div class="header__right-side">

                <?php if($headerPhone): ?>

                    <a href="tel:<?php echo clearPhone($headerPhone) ?>" class="header__phone">
                        <svg class="header__icon-phone"><use xlink:href="/img/sprite.svg#decor--iconPhone"></use></svg>
                        <span class="label-2 header__phone-text">

                            <?php echo $headerPhone; ?>

                        </span>
                    </a>

                <?php endif; ?>

                <?php

                if(specialver_is_active($this)){
                    echo '<a href="' . $explodeURIArray[0] . '?version=normal" title="Обычная версия" class="header__impaired-version-switch header__impaired-version-switch--active">';
                }
                else{
                    echo '<a href="' . $explodeURIArray[0] . '?version=special" title="Версия для слабовидящих" class="header__impaired-version-switch">';
                }

                ?>


                    <svg class="header__impaired-version-icon"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-eye"></use></svg>
                </a>
                <button type="button" class="header__btn-menu" id="mobileMenuBtn"></button>
            </div>
        </div>
        <div class="header__bottom header__bottom--frontpage">
            <div class="header__left-side">
                <ul class="header__menu header__menu--additional">
                    <li><a href="/events/Novosti">Новости</a></li>
                    <li><a href="/events/Anonsy">Анонсы</a></li>
                    <li><a href="/events/Nashi-dostizheniya">Наши достижения</a></li>
                    <li><a href="/events/Tvorchestvo-uchaschihsya">Творчество учащихся</a></li>
                    <li><a href="/Uchaschimsya/Uchebnaya-deyatelnost/Raspisanie-zanyatij">Расписание</a></li>
                    <li><a href="/Uchaschimsya/Vneurochnaya-deyatelnost">Внеурочная деятельность</a></li>
                </ul>
            </div>

            <?php if(specialver_is_active($this)): ?>

                <div class="header__right-side">
                    <div class="font-size-switches">
                        <div class="label-1 font-size-switches__label">Размер шрифта:</div>
                        <a href="<?php echo $explodeURIArray[0] ?>?version=special&special_fontsize=1x"
                           title="Обычный"
                           class="font-size-switch font-size-switch--1x <?php echo (!isset($_SESSION['special_fontsize']) || $_SESSION['special_fontsize'] == '1x') ? 'font-size-switch--active disable-link' : '' ?>"
                        >
                            <svg class="font-size-switch__balloon"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-balloon"></use></svg>
                            <svg class="font-size-switch__symbol"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-A"></use></svg>
                        </a>
                        <a href="<?php echo $explodeURIArray[0] ?>?version=special&special_fontsize=2x"
                           title="Увеличить в 2 раза"
                           class="font-size-switch font-size-switch--2x <?php echo (isset($_SESSION['special_fontsize']) && $_SESSION['special_fontsize'] == '2x') ? 'font-size-switch--active disable-link' : '' ?>"
                        >
                            <svg class="font-size-switch__balloon"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-balloon"></use></svg>
                            <svg class="font-size-switch__symbol"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-A"></use></svg>
                        </a>
                        <a href="<?php echo $explodeURIArray[0] ?>?version=special&special_fontsize=3x"
                           title="Увеличить в 3 раза"
                           class="font-size-switch font-size-switch--3x <?php echo (isset($_SESSION['special_fontsize']) && $_SESSION['special_fontsize'] == '3x') ? 'font-size-switch--active disable-link' : '' ?>"
                        >
                            <svg class="font-size-switch__balloon"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-balloon"></use></svg>
                            <svg class="font-size-switch__symbol"><use xlink:href="/img/sprite.svg#decor--spec-version--icon-A"></use></svg>
                        </a>
                    </div>
                </div>

            <?php endif; ?>

        </div>
    </header>

    <?php

endif; // !$this->isAjaxRequest()
