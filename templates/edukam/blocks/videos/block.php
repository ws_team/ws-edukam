<?php
namespace videos { // имя namespace должно соответствовать типу блока (названию папки)

    // Функция которая возвращает
    function block_arguments() {
        return array(
            1 => 'Количество выводимых фотоальбомов',
            2 => '(Необязательный) типы материалов к которым относятся фотоальбомы (???)',
            );
    }

    // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
    function block_content($videos_count, $types='') {
        global $khabkrai;

        ob_start();

        if ($types == '') {

            //определяем типы в зависимости от характеристик сайта
            if($khabkrai->values->maintype == 1)
            {
                $types=Array(11,12);
                $videohref='/events/video';
            }
            elseif($khabkrai->values->maintype == 5)
            {
                $types=Array(30,35);
                $videohref='/governor/performances/governor-video';
            }
            else
            {
                $types=Array(11,12);
                $videohref='/events/video';
            }

        } else {
            $videohref='';
            $photohref='';

            foreach ($types as $type) {
                //проверяем тип
                $query='SELECT "template" FROM "material_types" WHERE "id" = '.$type;

                if($res=$khabkrai->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $template=$res[0]['template'];
                        if($template == '4')
                        {
                            if($photohref == '')
                            {
                                $photohref=GetMaterialTypeUrl($khabkrai,$type);
                            }
                        } else {
                            if($videohref == '')
                            {
                                $videohref=GetMaterialTypeUrl($khabkrai,$type);
                            }
                        }
                    }
                }
            }
        }

        $materialtypes = '';

        $query='';

        $i=0;

        foreach ($types as $type) {

            ++$i;

            if($query != '')
            {
                $query.="
                 UNION ALL
                ";
            }

            if($khabkrai->settings->DB->type == 'postgresql')
            {
                $query.='SELECT * FROM (WITH RECURSIVE temp1(id, parent_id, PATH, LEVEL) as
(
select t1.id, t1.parent_id, CAST(t1.id as VARCHAR(11)) as PATH, 1 FROM material_types t1 WHERE t1.id = '.$type.'

UNION

SELECT t2.id, t2.parent_id, CAST ( temp1.PATH ||\'->\'|| t2.id AS VARCHAR(11)), LEVEL + 1 FROM material_types t2
INNER JOIN temp1 ON(temp1.id = t2.parent_id)
) SELECT * FROM temp1) f'.$i;
            }
            else
            {
                $query.='SELECT "id", "parent_id" FROM "material_types" START WITH "id" = '.$type.'
        CONNECT BY PRIOR "id" = "parent_id"';
            }
        }

        if($res=$khabkrai->dbquery($query))
        {
            foreach($res as $row)
            {
                if($materialtypes != '')
                {
                    $materialtypes.=',';
                }

                $materialtypes.=$row['id'];
            }
        }

        $idsv='';

        $query='SELECT "fc"."material_id" FROM "file_connections" "fc", "files" "f", "materials" "m"
        WHERE "fc"."file_id" = "f"."id" AND "fc"."material_id" = "m"."id"  AND "f"."type_id" = 2 AND "m"."type_id" IN ('.$materialtypes.')';

        if($res=$khabkrai->dbquery($query))
        {
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    if($idsv != '')
                    {
                        $idsv.=',';
                    }
                    $idsv.=$row['material_id'];
                }
            }
        }

        $videoarr = MaterialList($khabkrai, '', 1, $videos_count, 2, $idsv);

        if((count($videoarr) > 0)&&($videoarr[0]['thumb'] != '')&&($videoarr[0]['type_id'] != $khabkrai->data->atype))
        {
            ?><h3>Видео</h3><?php

            foreach($videoarr as $row)
            {
                if($khabkrai->data->language == LANG_RU)
                {
                    $album_date=russian_datetime_from_timestamp($row['unixtime'], false, false, false);
                }
                else{
                    $album_date=en_datetime_from_timestamp($row['unixtime']);
                }

                $videourl=$row['url'];
                $videoimg=$row['thumb'];

                if (mb_strwidth($row['name']) > 55) {
                    $row['name'] = mb_substr($row['name'], 0, 55, 'utf-8') . '...';
                }

                $videoInfo = FileGetInfo($khabkrai, $row['videoid']);

                ?>
                <a href="<?= $videourl ?>" class="video">
                    <span class="xmedia mt-video s276x155">
                        <img alt="<?= $row['name'] ?>" class="video_cover media-content"
                             src="<?= $videoimg ?>" width="276"/>
                        <span class="media-textview">Видеоролик: <?= $videoInfo['name'] ?></span>
                    </span>
                    <span class="video_info">
                        <span class="video_info_count"></span>
                        <span class="video_info_date"><?= $album_date ?></span>
                        <span class="video_info_name"><?= $row['name'] ?></span>
                    </span>
                </a>
                <a href="<?= $videohref ?>" class="link-btn videos_all">Все видео</a>
                <?php
            }
        }

        return ob_get_clean();
    }

} // /namespace