<?php
namespace single_news { // имя namespace должно соответствовать типу блока (названию папки)

    // Функция которая возвращает
    function block_arguments() {
        return array();
    }

    // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
    function block_content($types='') {
        global $khabkrai;

        ob_start();
        
        ?><h3>Новость</h3><?php

        if($types == '')
        {
            $types='2, 84, 94, 164, 211';
        }

        $typesarr=str_replace(' ','',$types);
        $typesarr=explode(',',$typesarr);
        $maintype=$typesarr[0];

        $news = listNews($khabkrai, 1, 1, $maintype, $types);

        foreach($news as $num => $row): // вывод списка новостей

            $classes = '';

            if ($row['photo'] != null) {
                $classes .= ' has-photo';
                $has_photo = true;
            } else {
                $has_photo = false;
            }

            $date = date("d.m.Y", $row['unixtime']);
            $human_date = russian_datetime_from_timestamp($row['unixtime'], false, false, true);
            $row['extra_text1'] = explode(',', $row['extra_text1']);

            ?>
            <div class="news_wrapper filtered-materials-list">
                <div class="news_inner_wrapper">
                    <div id="news-<?= $row['id'] ?>" class="news-row' <?= $classes ?>">
                        <?php

                        if ($has_photo) { // если есть фото - выводим его первым
                            $photoInfo = FileGetInfo($khabkrai, $row['photo']);
                            ?>
                            <a href="<?= $row['url'] ?>" class="news-photo xmedia mt-image s85">
                                <img src="/photos/<?= $row['photo'] ?>_x100.jpg" width="85"
                                    alt="<?= $row['name'] ?>" title="Перейти к новости"
                                    class="media-content"/>
                                <span class="media-textview">Изображение: <?=
                                    htmlspecialchars($photoInfo['name'])
                                    ?></span>
                            </a>
                            <?php
                        }

                        ?>
                        <div class="news-title news-date">
                            <?= $human_date ?>
                        </div>

                        <div class="news-title news-textfield">
                        <a href="<?= $row['url'] ?>"><?= $row['name'] ?></a>
                    </div>
                    <div class="news-announce news-textfield">
                        <?= $row['anons'] ?>
                    </div>
                </div>
            </div>
        </div>
        <?php

        endforeach;

        $url=GetMaterialTypeUrl($khabkrai, $maintype);

        ?><a href="<?= $url ?>" class="news_all link-btn">Все новости</a><?php

        return ob_get_clean();
    }
} // /namespace