<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 * oiv:newinvest
 */


namespace xform {
    function block_arguments()
    {
        return array(
            1 => '(array) fields config',
            2 => '(array) form options'
        );
    }

    /**
     * @param array $fields_config
     * @param array $form_options
     * @return string
     */
    function block_content($fields_config, $form_options)
    {
        global $khabkrai;

        $khabkrai->getPostValues(array_keys($fields_config));

        if ( ! isset($form_options['id']))
            $form_options['id'] = 'form_'.str_replace('.php', '', $khabkrai->data->shabname);

        $form_id = $form_options['id'];

        ob_start();

        ?>

        <input type="hidden" name="csrf" value="<?=
            htmlspecialchars($khabkrai->generateCsrfToken()) ?>" />
        <?php

        foreach ($fields_config as $field_name => $field) {
            $field_type = isset($field['type']) ? $field['type'] : 'text';

            if ( ! isset($field['name']))
                $field['name'] = $field_name;

            if ( ! isset($field['id']))
                $field['id'] = $form_id.'_'.$field_name;

            $field_renderer = 'render_field_'.$field_type;
            $field_renderer = __NAMESPACE__.'\\'.$field_renderer;

            if ( ! function_exists($field_renderer)) {
                $field_renderer = 'render_field_text';
                $field_renderer = __NAMESPACE__.'\\'.$field_renderer;
            }

            ?>
            <div class="row row-field-<?= $field_type ?>">
                <?php
                call_user_func($field_renderer, $field, $khabkrai->values->$field_name);
                ?>
            </div>
            <?php
        }

        if ( ! empty($form_options['buttons'])) {
            ?>
            <div class="row buttons">
                <?php

                render_buttons($form_options['buttons']);
                ?>
            </div>
            <?php
        }

        $form_contents = ob_get_clean();
        $form_attrs = array(
            'method' => 'post',
            'action' => '',
        );

        ob_start();

        foreach (array('id', 'class', 'action', 'enctype', 'method') as $attr)
            if (isset($form_options[$attr]))
                $form_attrs[$attr] = $form_options[$attr];

        if ( ! empty($form_options['description'])) {
            ?><p class="description"><?=
                $form_options['description']
            ?></p><?php
        }

        render_html_tag('form', $form_attrs, $form_contents, false);

        $ret = ob_get_clean();

        return $ret;
    }

    function render_field_text($field, $value)
    {
        render_html_label($field);
        render_html_input('text', $value, $field);
    }

    function render_html_label($field, $content = null)
    {
        if (is_null($content))
            $content = htmlspecialchars(isset($field['label']) ? $field['label'] : $field['name']);

        render_html_tag('label', array('for' => $field['id']), $content);
    }

    function render_field_textarea($field, $value)
    {
        render_html_label($field);
        $node_attrs = array();
        foreach (array('name', 'id') as $attr)
            if (isset($field[$attr]))
                $node_attrs[$attr] = $field[$attr];

        render_html_tag('textarea', $node_attrs, htmlspecialchars($value));
    }

    function render_field_checkbox($field, $value)
    {
        ob_start();
        render_html_input('checkbox', $value == $field['value'], $field);
        $checkbox = ob_get_clean();

        render_html_label($field, $checkbox);
    }

    function render_field_radio($field, $value)
    {
        $field_label = isset($field['label']) ? $field['label'] : '';
        $field_id = $field['id'];

        ?>
        <div class="radio-group-label"><?= htmlspecialchars($field_label) ?></div>
        <div class="radio-group">
            <?php
            foreach ($field['options'] as $i => $opt) {
                ?><span><?php

                ob_start();

                $radio_val = isset($opt['id']) ? $opt['id'] : $i;
                $radio_id = $field_id.'-'.$radio_val;
                $radio_label = isset($opt['name']) ? $opt['name'] : $opt['id'];

                $radio_cfg = $field;
                $radio_cfg['id'] = $radio_id;
                $radio_cfg['value'] = $radio_val;

                render_html_input('radio', $value == $radio_val, $radio_cfg);

                $radio_btn = ob_get_clean();

                render_html_label($radio_id, $radio_label.' '.$radio_btn);

                ?></span><?php
            }
            ?>
        </div>
        <?php
    }

    function render_field_select($field, $value)
    {
        render_html_label($field);

        $options = isset($field['options']) ? $field['options'] : array();

        ob_start();

        foreach ($options as $opt) {
            ?><option value="<?= $opt['id'] ?>"<?php
                if ($opt['id'] == $value) {?> selected<?php } ?>><?=
                htmlspecialchars($opt['name']) ?></option>
            <?php
        }

        $options = ob_get_clean();

        $node_attrs = array();
        foreach (array('name', 'id') as $attr) {
            if (isset($field[$attr]))
                $node_attrs[$attr] = $field[$attr];
        }

        render_html_tag('select', $node_attrs, $options, false);
    }

    /**
     * @param $name
     * @param $id
     * @param $type
     * @param $value
     * @param array $field_config
     * @return string
     */
    function render_html_input($type = 'text', $value = '', $field_config = array())
    {
        $attrs = array();

        foreach (array('id', 'name', 'class') as $attr)
            if (isset($field_config[$attr]))
                $attrs[$attr] = $field_config[$attr];

        $attrs['type'] = $type;

        if ($type == 'checkbox' || $type == 'radio') {
            if ($value === true)
                $attrs['checked'] = true;
        } else {
            if ($value !== '')
                $attrs['value'] = $value;
        }

        render_html_tag('input', $attrs, '', true);
    }

    function render_html_tag($tag_name, $attrs = array(), $inner_html = '', $self_closed = false)
    {
        $html = array();
        $html[] = '<'.$tag_name;

        foreach ($attrs as $a => $v) {
            if (is_bool($v)) {
                if ($v === true) {
                    $html[] = $a;
                }
                continue;
            }
            $html[] = $a.'="'.htmlspecialchars($v).'"';
        }
        $html = implode(' ', $html);
        if ($self_closed) {
            $html .= ' />';
        } else {
            $html .= '>'.$inner_html.'</'.$tag_name.'>';
        }

        echo $html;
    }

    function render_buttons($buttons)
    {
        foreach ($buttons as $btn_name => $button) {
            if (is_numeric($btn_name))
                $btn_name = 'send'.strval($btn_name);

            ?><button class="button" type="submit" name="<?= $btn_name ?>" value="1"><?=
            htmlspecialchars($button['text']);
            ?></button><?php
        }
    }

    function render_field_hidden($field, $value)
    {
        render_html_input('hidden', $field['value'], $field);
    }
}