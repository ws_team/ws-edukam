<?php
    namespace list_date_filter { // имя namespace должно соответствовать типу блока (названию папки)

        // Функция которая возвращает
        function block_arguments() {
            return array(
                1 => 'см. описание аргументов типа блока "date-dialog-opener"');
        }

        // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
        function block_content($text, $settings, $tag = 'a') {
            global $khabkrai;

            $content = '';

            // Формируем массив месяцев, чтобы в блоке одиночного календаря у нас были доступны все месяцы
            $months = array(
                'Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябрь',
                'Октябрь',
                'Ноябрь',
                'Декабрь',
                'Весь год',
                );  
                
            // $years = array(
            //     2011, 
            //     2012, 
            //     2013, 
            //     2014, 
            //     2015,
            //     ); 

            // а вот года будут доступны только те для которых есть материалы с типом относящимся к новостям
            if (!isset($types) || $types == null) {
                $types = '84,94,164,211,2';
            }        
                
            $years = \giveMeAMaterialYears($khabkrai, $types);

            // Вызываем блок который выводит одиночный блок
            $content.= $khabkrai->data->blocks->render('single_calendar', $settings, $months, $years);
            // И блок выбора диапазона дат
            $content.= $khabkrai->data->blocks->render('date_dialog_opener', $text, $settings, $tag);

            // Творим какую-то ещё хуйню по разбору параметров
            $datestart_id = (array_key_exists('data-datestart-field-id', $settings)) ? ' id="' . $settings['data-datestart-field-id'] . '"' : '';
            $datestart_class = (array_key_exists('data-datestart-field-class', $settings)) ? ' class="datestart-field ' . $settings['data-datestart-field-id'] . '"' : ' class="datestart-field"';
            $datestart_value = (array_key_exists('list_date_filter_datestart', $_GET)) ? strip_tags($_GET['list_date_filter_datestart']) : '';

            // Рисуем невидимое поле начальной даты для формы фильтров новостей
            $content.= '<input type="hidden" name="list_date_filter_datestart"' . $datestart_id . $datestart_class . ' value="' . $datestart_value . '">';

            // .....
            $dateend_id = (array_key_exists('data-dateend-field-id', $settings)) ? ' id="' . $settings['data-dateend-field-id'] . '"' : '';
            $dateend_class = (array_key_exists('data-dateend-field-class', $settings)) ? ' class="dateend-field ' . $settings['data-dateend-field-id'] . '"' : ' class="dateend-field"';
            $dateend_value = (array_key_exists('list_date_filter_dateend', $_GET)) ? strip_tags($_GET['list_date_filter_dateend']) : '';

            // Рисуем невидимое поле конечной даты для формы фильтров новостей
            $content.= '<input type="hidden" name="list_date_filter_dateend"' . $dateend_id . $dateend_class . ' value="' . $dateend_value . '">';

            return $content;
        }

    } // /namespace
?>