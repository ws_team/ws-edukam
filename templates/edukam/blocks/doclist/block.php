<?php
namespace doclist { // имя namespace должно соответствовать типу блока (названию папки)

    // Функция которая возвращает
    function block_arguments() {
        return array(
            1 => 'Список документов для вывода',
            2 => 'Заголовок',
            3 => 'Количество выводимых документов',
            4 => array(
                'month' => '(int) фильтр документов по месяцу',
                'year' => '(int [YYYY]) фильтр документов по году'
                ),
            5 => array(
                'url' => 'Адрес ссылки в днище',
                'text' => 'Текст ссылки в днище',
                )
            );
    }

    // Функция, которая генерирует контент блока и возвращает его при вставке на страницу через Blocks API
    function block_content($docs, $header = 'Прикреплённые файлы', $limit = 999999, $datefilter = null, $link = null) {
        global $khabkrai;

        $file_ids = array();
        $apar = '';

        ob_start();

        $docsj = vjt_documents_join_items($docs);

        $num = 0;

        $is_specialver = specialver_is_active($khabkrai);

        foreach ($docsj as $name => $docj) {

            $doc = $docj[0];
            if ($doc['type_id'] != FILETYPE_DOCUMENT)
                continue;

            $num++;
            if ($doc['par_id'] != $apar) {
                $apar = $doc['par_id'];
                if (!empty($doc['par_name'])) {
                    ?>
                    <p>&nbsp;</p>
                    <h4 style="font-size: 18px; border-top: 1px solid #ccc; padding: 10px 0; border-bottom: 1px solid #ccc;"><?= $doc['par_name'] ?></h4><?php
                }
            }

            $main_url = '/?menu=getfile&id='.$doc['id'];
            $name = $doc['name'];

            ob_start();

            $junction = '';

            foreach ($docj as $doc) {
                $cecutient = false;

                $url_view='';

                if ($doc['type_id'] == FILETYPE_DOCUMENT) {
/*
                if (!preg_match("/youtube\.com/", $doc['content'])
                    && $doc['type_id'] != FILETYPE_IMAGE
                    && $doc['type_id'] != FILETYPE_VIDEO) {
*/
                    $file_ids[] = $doc['id'];

	                $extarray=Array('DOC','RTF','XLS','DOCX','XLSX','PPT','PPS','PPTX','PPSX');

                    if ($num < $limit) {
                        $url = '/?menu=getfile&id='.$doc['id'];
                        $view_url = null;
                        if ($doc['extension'] == 'pdf') {
                            $url_view = '/?menu=getfile&id='.$doc['id'].'&view=1';
                        }
                        elseif(in_array(strtoupper($doc['extension']), $extarray))
                        {
	                        $url = '/?menu=getfile&id='.$doc['id'];
	                        $fullmainurl = 'https://'.$_SERVER['SERVER_NAME'].$url;
	                        $url_view = 'https://docs.google.com/viewer?url='.urlencode($fullmainurl);
                        }

                        $doc_size = '';

                        if ($doc['size'] != '') {
                            $doc['size'] = intval($doc['size']);

                            $size_item = 'B';
                            $size_amount = $doc['size'];

                            if ($doc['size'] > 1024) {
                                $size_item = 'KB';
                                if ($doc['size'] > 1024 * 1024) {
                                    $size_item = 'MB';
                                    $doc['size'] = $doc['size'] / 1024;
                                }
                                $size_amount = round($doc['size'] / 1024);
                            }

                            $doc_size = $size_amount . $size_item;
                        }

                        $doc_date = '';

                        if ( ! empty($doc['date_event'])) {
                            ob_start();
                            $date = explode(' ', $doc['date_event']);
                            $date = $date[0];
                            $tm = strptime($date, '%d.%m.%Y');
                            $unixtime = 0;

                            if ($tm) {
                                $unixtime = mktime(0, 0, 0, $tm['tm_mon'] + 1, $tm['tm_mday'],
                                    $tm['tm_year'] + 1900);
                            }
                            ?><time datetime="<?= date('c', $unixtime) ?>" class="doc-date"><?= $date ?></time><?php
                            $doc_date = ob_get_clean();
                        }

                        ?><?= $junction ?><a href="<?= htmlspecialchars($url) ?>"><span class="type"><?=
                            strtoupper($doc['extension'])?></span> <span class="size"><?=
                            $doc_size
                            ?></span> <?= $doc_date ?></a><?php
                        if ( ! empty($url_view)) {
                            ?> / <a href="<?= htmlspecialchars($url_view) ?>" class="view-link"
                                    target="_blank">открыть в браузере</a>
                            <?php
                        }
                        $junction = ', ';
                    }
                }

                if ($is_specialver && ! $cecutient && ! empty($doc['cecutient'])) {
                    ob_start();
                    $url_cecutient = '?menu=getfile&id='.$doc['id'].'_sv';
                    echo $junction;
                    ?><a href="<?= htmlspecialchars($url_cecutient) ?>">версия для слабовидящих</a><?php

                    $cecutient = ob_get_clean();
                }
            }

            if ( ! empty($cecutient))
                echo $cecutient;

            $doc_types = ob_get_clean();

            ?>
            <div class="doc doc-<?= $num ?> document-row">
                <a href="<?= htmlspecialchars($main_url) ?>" class="download">
                    <span class="icon"></span>
                    <span class="document-name document-textfield" style="font-size: 15px; color: #515151;"><?= $name ?></span>
                </a>
                <span class="size-extension">(<?= $doc_types ?>)</span>
            </div>
            <?php
        }

        $filelist = ob_get_clean();

        if ($filelist == '') {
            return false;
        }

        if ($datefilter != null
            && array_key_exists('month', $datefilter) && array_key_exists('year', $datefilter)) {
            
            $month_names = array(
                1 => 'Январь ',
                2 => 'Февраль ',
                3 => 'Март ',
                4 => 'Апрель ',
                5 => 'Май ',
                6 => 'Июнь ',
                7 => 'Июль ',
                8 => 'Август ',
                9 => 'Сентябрь ',
                10 => 'Октябрь ',
                11 => 'Ноябрь ',
                12 => 'Декабрь ',
            );
            
            ob_start();

            $month_name = isset($month_names[$datefilter['month']]) ?
                $month_names[$datefilter['month']] :
                '';

            ?><p class="date-filtered">на <?= $month_name.$datefilter['year'] ?></p><?php

            $header = ob_get_clean();
        }

        ob_start();

        ?>
        <p>&nbsp;</p>
        <h3><?= $header ?></h3>
        <div class="filelist">
            <div class="omsu_documents_wrapper"><?= $filelist ?></div>
        </div>
        <?php

        if (count($file_ids) > 1) {
            $file_ids = implode(',', $file_ids);

            $archive_url = '/?menu=getfile&files='. $file_ids . '&mode=group&key=' . MakeFileGroupKey($file_ids);

            if ($link != null && array_key_exists('url', $link) && array_key_exists('text', $link)) {
                if ($link['url'] == '{{download_all}}') {
                    $link['url'] = $archive_url;
                }
                ?><a class="bottom-link link-btn" href="<?= $link['url'] ?>"><?= $link['text'] ?></a><?php
            }
        }

        return ob_get_clean();
    }

} // /namespace
