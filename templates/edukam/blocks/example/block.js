/* 
 *	Весь JavaScript используемый в блоке необходимо писать как функцию передаваемую аргументом в метод BlocksAPI.attachJS();
 *	Это позволяет объединить и унифицировать процесс подключения скриптов	
 */

BlocksAPI.attachJS(function() {
	// code...
	if (typeof console != "undefined") {
		console.log("Example block JS: executed!");
	}
});