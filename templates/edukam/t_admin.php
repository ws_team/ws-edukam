<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

if ($this->getUser()->isAdmin() || $this->getUser()->isGuest()) {

    include_once($this->locateTemplate('f_header'));

    ?>
    <div class="container container--admin-title">
        <h1 class="adminTitle"><?php echo $this->data->iH1; ?></h1>
    </div>
    <div class="contentblock basemargin"><?php

        if ( ! empty($this->autorize->answer)) {
            ?><p style="color:#ff0000; text-align:center;margin:0 0 1em;"><?=$this->autorize->answer?></p><?php
        }

        if ($this->getUser()->isGuest()) {// @todo вынести, сделать настройку логики
            //форма смены пароля
            if(isset($_SESSION['changepass']) && ($_SESSION['changepass'] != '') &&
                isset($_SESSION['changepasslogin']) && ($_SESSION['changepasslogin'] != '')) {
                ?>
                <div id='admin_form'>
                    <form method='post' action=''  class="formAuth">
                        <table class="formAuth__table" cellspacing='3' cellpadding='3' border='0'>
                            <tr>
                                <th>новый пароль (минимум 6 символов):</th>
                                <td>
                                    <input type='password' name='new_autorize_pass' value=''>
                                </td>
                            </tr>
                            <tr>
                                <th>введите новый пароль повторно:</th>
                                <td>
                                    <input type='password' name='new_autorize_pass2' value=''>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="styler formAuth__submitBtn">Сменить пароль</button>
                    </form>
                </div>
                <?php
            } else {
                ?>
                <div id='admin_form'>
                    <form method='post' action='' class="formAuth">
                        <table class="formAuth__table" cellspacing='3' cellpadding='3' border='0'>
                            <tr>
                                <th>email:</th>
                                <td><input type='text' name='autorize_email' value=''></td>
                            </tr>
                            <tr>
                                <th>пароль:</th>
                                <td>
                                    <input type='password' name='autorize_pass' value=''>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="styler formAuth__submitBtn">Войти</button>
                    </form>
                </div>
                <?php
            }

        } else { ?>
            <div style='padding: 20px 0;'><p style='text-align: center;'>Добро пожаловать!</p></div>
            <?php
        }
        ?>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        Модуль материалов: v.<?php echo $this->settings->versions->materials; ?>; <br><big>Сайт: v.<?php echo $this->settings->versions->site;?></big>
    </div>
    <?php

    include_once($this->locateTemplate('f_footer'));

} else { // content manager
    $this->redirect('/');
}