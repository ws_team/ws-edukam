<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$mattypes_path = $this->settings->path.'/components/materials/material_types_array.php';
if (file_exists($mattypes_path))
	include_once($mattypes_path);

if ( ! isset($callbackresanswer))
    $callbackresanswer = '';

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Запрашиваемая страница не найдена</title>
		<link href='<?= str_replace('//', '/', $this->includeLESS("main.less")) ?>'
              rel='stylesheet' type='text/css' />
		<link href='<?= str_replace('//', '/', $this->includeLESS("404.less")) ?>'
              rel='stylesheet' type='text/css' />
	    <script src="/js/jquery-1.11.1.min.js"></script>
	    <script src="<?=  $this->settings->templateurl ?>/js/vidgets.js"></script>
	</head>
	<body>
        <div id="modal-overlay" style="display: none;">
            <div class="centerer" style="display: table-cell; text-align: center; vertical-align: middle;">
                <div id="under-construction" class="modal">
                    <button type="button" class="custom-closer popup-closer">закрыть</button>
                    <h1>Внимание!</h1>

                    <div class="uc-image"></div>
                    <div class="grey">
                        <h2>Портал работает в тестовом режиме!</h2>
                        <p class="message">Если у вас появились предложения по улучшению<br>
                        портала или вы нашли ошибку, <a href="#" onclick="clallbackform();">свяжитесь с нами</a>.</p>
                        <!-- <p><a href="http://old.khabkrai.ru">Архивная версия сайта 2004-2014</a></p> -->
                        <!-- <p><a href="http://old.gov.khabkrai.ru">Архивная версия gov.khabkrai.ru</a></p> -->
                        <p class="hint">
                            Форма обратной связи расположена в верхней навигационной панели.
                        </p>
                </div></div>

                <div id="callbackform" class="modal">
                    <button type="button" class="custom-closer popup-closer">закрыть</button>
                    <div class="icon"></div>

                    <h2>Обратная связь</h2>
                    <p class="desc">Предложения и замечания по работе портала направляйте<br>по адресу: <a href="mailto:info@adm.khv.ru">info@adm.khv.ru</a>.</p>

                    <form id="callback" method="post" action="" enctype="multipart/form-data">
                        <div class="textfields">
                            <div class="field-wrapper required"><input type="text" class="textfield" name="err_fio" placeholder="Представьтесь"></div>
                            <div class="field-wrapper right required"><input type="text" class="textfield" name="err_email" placeholder="Ваш e-mail"></div>
                        </div>
                        <div class="field-wrapper required"><textarea class="styler" name="err_content" placeholder="Описание ошибки"></textarea></div>
                        <div class="screenshooter">
                            <p class="add-first-screen">Вы можете <a href="#" class="screenshot-first">прикрепить скриншот</a> с ошибкой</p>
                            <div class="screenfields">
                                <div class="screen-field" data-screen-number="0" style="display: none;">
                                    <input type="file" name="screen[0]" class="file autoclone">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="sumbiterror" value="1">
                        <div class="required-explain">
                            <div class="field-wrapper required"></div> <span>Обязательно для заполнения</span>
                        </div>
                        <input type="submit" value="Отправить" class="submit">
                    </form>
                </div>

                <div id="callbackresult" class="modal">
                    <button type="button" class="custom-closer popup-closer">закрыть</button>
                    <div class="icon"></div>

                    <h2>Обратная связь</h2>
                    <p class="desc"><?php echo $callbackresanswer; ?></p>
                    <p>&nbsp;</p>
                    <p class="desc">Предложения и замечания по работе портала направляйте<br>по адресу: <a href="mailto:info@adm.khv.ru">info@adm.khv.ru</a>, телефон: 8 (4212) 40-21-20.</p>


                </div>

                <div id="error_popup" class="modal">
                    <button type="button" class="custom-closer popup-closer">закрыть</button>
                    <div class="icon"></div>

                    <h2>Ошибка!</h2>
                    <p class="desc">Неизвестная ошибка. Пожалуйста свяжитесь с нами и опишите последовательность действий которые привели к данному сообщению.</p>
                </div>

                <div id="date_dialog" class="modal">
                    <button type="button" class="custom-closer popup-closer">закрыть</button>
                    <button type="button" class="apply-filter">выбрать</button>
                    <div class="helper">Вы можете выбрать как один день, так и период (отметив его начальную и конечную дату)</div>
                    <?php
                    $visibility = '';
                    if (array_key_exists('datestart', $_GET) && array_key_exists('dateend', $_GET)) {
                        if ($_GET['datestart'] != '' && $_GET['dateend'] != '') {
                            $visibility = '';
                        } else {
                            $visibility = ' hidden';
                        }
                    }

                    ?>
                    <div class="range-caption <?php print $visibility; ?>">
                        Вы выбрали материалы с <span class="dates datestart">--.--.----</span> по <span class="dates dateend">--.--.----</span><br><a href="#" class="clear-range">Очистить</a>
                    </div>
                </div>

                <div id="some-test-modal" class="modal">
                </div>
            </div>
        </div>		
		<div class="main-wrapper">
			<div class="content not-found">

				<div class="info-wrapper">
					<div class="info">
						<h1 class="red">404</h1>
						<h2>Запрашиваемая страница не найдена</h2>
						<p class="desc">
							Уважаемый посетитель, если вы уверены, что такой страницы нет по нашей вине, пожалуйста, <a href="#" class="callback">сообщите нам</a> об этом и не забудьте указать адрес, с которого вы сюда попали.
						</p>
						<a href="/" class="btn block-red">Перейти на главную страницу</a>
						<div class="menu">
							<h2>Перейти в раздел:</h2>
							<div class="columns">
			                <?php

			                if(isset($this->data->material_types))
			                {

			                    //print_r($this->data->material_types);

			                	$column_rows_count = 3;
			                	$menu_item_num = 1;

			                	print '<div class="column">';

				                foreach($this->data->material_types as $mtype)
				                {


				                    if(($mtype['parent_id'] == '')&&($mtype['showinmenu'] == '1'))
				                    {
				                        if(($this->data->language == 1)||($mtype['id'] == 4)||($mtype['id'] == 5)){

				                        //активный главный тип
				                        if($this->values->maintype == $mtype['id'])
				                        {
				                            $styleclass='option active';
				                        }
				                        else
				                        {
				                            $styleclass='option';
				                        }

				                        print '<a class="'.$styleclass.'" href="'."/".$mtype['id_char'].'">'.$mtype['name'].'</a><br>';

					                	if ($menu_item_num % $column_rows_count == 0) {
					                		print '</div><div class="column">';
					                	}
	
					                    $menu_item_num++;
				                    }}

				                }

				                print '</div>';
			                }

			                ?>
							</div>
						</div>
					</div>
				</div>
				<!-- <object data="/templates/khabkrai/img/404.svg" type="image/svg+xml" width="100%" id="bg_svg"></object> -->
			</div>
		</div>
    <!-- v:<?php var_dump($this->values); ?> -->
	</body>
</html>