<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$materials = MaterialList($this, $this->data->atype, 1, $this->values->perpage, 2);

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>

<div class="container">
    <div class="wrap-content wrap-content--double">
        <div class="main-section">

            <?php

            include($this->partial('/xpage/title'));

            foreach ($materials as $materialsItem):

                $firstPhotoId   = MaterialGetFirstPhoto($this, $materialsItem['id']);
                $materialParams = GetMaterialParams($this, $materialsItem['id']);
                $post           = '';
                $contacts       = '';

                foreach ($materialParams as $materialParamsItem){
                    switch ($materialParamsItem['name']){
                        case 'Должность':
                            $post = $materialParamsItem['value'];
                            break;
                        case 'Контакты':
                            $contacts = $materialParamsItem['value'];
                            break;
                    }
                }

            ?>

                <div class="person-item">
                    <img src="/photos/<?php echo $firstPhotoId ?>_xy150x150.jpg" alt="" class="person-item__image">
                    <div class="person-item__wrap-text">
                        <div class="subtitle-2 person-item__title"><?php echo $materialsItem['name'] ?></div>

                        <?php if($post): ?>

                            <div class="label-1 person-item__post"><?php echo $post ?></div>

                        <?php endif; ?>

                        <?php if($materialsItem['content']): ?>

                            <div class="body-2 person-item__description"><?php echo $materialsItem['content'] ?></div>

                        <?php endif; ?>

                        <?php if($contacts): ?>

                            <div class="body-2 person-item__contacts"><?php echo $contacts ?></div>

                        <?php endif; ?>

                    </div>
                </div>

            <?php endforeach; ?>

            <!-- <div class="subtitle-3 person-section-title">Руководство</div> -->

        </div>

        <?php include($this->partial('/xpage/sidebar'));  ?>

    </div>
</div>
