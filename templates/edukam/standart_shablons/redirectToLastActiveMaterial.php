<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$materials = MaterialList($this, $this->data->atype, 1, 1, 2);

if(count($materials) > 0) {
    $this->redirect($materials[0]['url']);
}

$this->redirect('/');