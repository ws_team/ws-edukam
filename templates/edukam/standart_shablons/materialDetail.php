<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

//krumo($material);

$countOwnVideo = 0;

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>

<div class="container">
    <div class="wrap-content wrap-content--double">
        <div class="main-section">

            <?php

            include($this->partial('/xpage/title'));

            $subtypes = GetTypeChilds($this, $this->data->atype);

            if($subtypes):

            ?>

                <div class="child-pages">
                    <div class="subtitle-2 child-pages__title">Подробнее</div>
                    <ul class="child-pages-list">

                        <?php

                        foreach ($subtypes as $subtypesItem):

                            $url = GetMaterialTypeUrl($this, $subtypesItem['id']);

                            ?>

                            <li class="child-pages-list__item">
                                <a href="<?php echo $url ?>" class="child-pages-list__link"><?php echo $subtypesItem['name'] ?></a>
                            </li>

                        <?php endforeach; ?>

                    </ul>
                </div>

            <?php endif; ?>

            <?php if($material['photos']): ?>

            <div class="wrap-main-section-carousel">
                <div class="carousel-nav-arrow carousel-nav-arrow--prev" id="carouselNavArrowPrev"></div>
                <div class="main-section-carousel" id="mainSectionCarousel">

                    <?php foreach ($material['photos'] as $photo): ?>

                        <div class="main-section-carousel-item">
                            <img
                                src="/photos/<?php echo $photo['id'] ?>"
                                alt="<?php echo $photo['name'] ?>"
                                class="main-section-carousel-item__img"
                            >
                        </div>

                    <?php endforeach; ?>

                </div>
                <div class="carousel-nav-arrow carousel-nav-arrow--next" id="carouselNavArrowNext"></div>
            </div>
            <div class="main-section-additional-carousel" id="mainSectionAdditionalCarousel">

                <?php foreach ($material['photos'] as $photo): ?>

                    <div class="main-section-additional-carousel-item">
                        <img
                            src="/photos/<?php echo $photo['id'] ?>_xy170x101.jpg"
                            alt="<?php echo $photo['name'] ?>"
                            class="main-section-additional-carousel-item__img"
                        >
                    </div>

                <?php endforeach; ?>

            </div>

            <?php endif; ?>

            <?php if($material['content'] || $material['videos']): ?>

                <div class="main-content">

                    <?php

                    if($material['content']){
                        echo $material['content'];
                    }

                    ?>


                    <?php

                    if($material['videos']):

                        foreach ($material['videos'] as $video):

                            if($video['youtubeid']):
                                ?>

                                <iframe
                                        src="<?php echo $video['content'] ?>"
                                        width="100%"
                                        height="500px"
                                        frameborder="0"
                                        allowfullscreen=""
                                >

                                </iframe>

                            <?php

                            else:

                                $countOwnVideo++;
                            ?>

                                <video id="toservideo"
                                       poster="/img/decor/videoThumb.svg"
                                       controls="controls"
                                       tabindex="0"
                                >
                                    <source src="<?php echo $video['content'] ?>" type="video/mp4">
                                </video>

                    <?php endif; endforeach; endif; ?>

                </div>

            <?php endif; ?>


            <?php if($material['documents']): ?>

                <div class="accordion" data-role="switchable">
                    <div class="accordion__title" data-role="innerSwitch">
                        <div class="subtitle-2 accordion__title-text">Документы
                            <span class="accordion__content-count">
                                <?php echo count($material['documents']) ?>
                            </span>
                        </div>
                        <svg class="accordion__arrow-down"><use xlink:href="/img/sprite.svg#decor--arrowDown"></use></svg>
                    </div>
                    <div class="accordion__content">

                        <?php foreach ($material['documents'] as $document): ?>

                            <div class="attachment">
                                <div class="attachment-image attachment-image--document">
                                    <div class="attachment-image__extension"><?php echo $document['extension'] ?></div>
                                </div>
                                <div class="attachment__wrap-title">
                                    <div class="label-2 attachment__title"><?php echo $document['name'] ?></div>
                                    <a href="/?menu=getfile&id=<?php echo $document['id'] ?>" class="link-download link-download--attachment">
                                        <svg class="link-download__icon link-download__icon--attachment"><use xlink:href="/img/sprite.svg#decor--iconDownload"></use></svg>
                                        <div class="body-2 link-download__text">Скачать</div>
                                    </a>
                                </div>
                            </div>

                        <?php endforeach;?>

                    </div>
                </div>

            <?php endif; ?>


            <?php if($material['photos']): ?>

                <div class="accordion" data-role="switchable">
                    <div class="accordion__title" data-role="innerSwitch">
                        <div class="subtitle-2 accordion__title-text">Изображения
                            <span class="accordion__content-count">
                                <?php echo count($material['photos']) ?>
                            </span>
                        </div>
                        <svg class="accordion__arrow-down"><use xlink:href="/img/sprite.svg#decor--arrowDown"></use></svg>
                    </div>
                    <div class="accordion__content">

                        <?php foreach ($material['photos'] as $photo): ?>

                            <div class="attachment">
                                <div class="attachment-image attachment-image--image">
                                    <div class="attachment-image__extension"><?php echo $photo['extension'] ?></div>
                                </div>
                                <div class="attachment__wrap-title">
                                    <div class="label-2 attachment__title"><?php echo $photo['name'] ?></div>
                                    <a href="/?menu=getfile&id=<?php echo $photo['id'] ?>" class="link-download link-download--attachment">
                                        <svg class="link-download__icon link-download__icon--attachment"><use xlink:href="/img/sprite.svg#decor--iconDownload"></use></svg>
                                        <div class="body-2 link-download__text">Скачать</div>
                                    </a>
                                </div>
                            </div>

                        <?php endforeach;?>

                    </div>
                </div>

            <?php endif; ?>


            <?php if($countOwnVideo != 0): ?>

                <div class="accordion" data-role="switchable">
                    <div class="accordion__title" data-role="innerSwitch">
                        <div class="subtitle-2 accordion__title-text">Видео
                            <span class="accordion__content-count">
                                <?php echo $countOwnVideo ?>
                            </span>
                        </div>
                        <svg class="accordion__arrow-down"><use xlink:href="/img/sprite.svg#decor--arrowDown"></use></svg>
                    </div>
                    <div class="accordion__content">

                        <?php

                        foreach ($material['videos'] as $video):

                            if($video['youtubeid']){
                                continue;
                            }

                            ?>

                            <div class="attachment">
                                <div class="attachment-image attachment-image--video">
                                    <div class="attachment-image__extension"><?php echo $video['extension'] ?></div>
                                </div>
                                <div class="attachment__wrap-title">
                                    <div class="label-2 attachment__title"><?php echo $video['name'] ?></div>
                                    <a href="/?menu=getfile&id=<?php echo $video['id'] ?>" class="link-download link-download--attachment">
                                        <svg class="link-download__icon link-download__icon--attachment"><use xlink:href="/img/sprite.svg#decor--iconDownload"></use></svg>
                                        <div class="body-2 link-download__text">Скачать</div>
                                    </a>
                                </div>
                            </div>

                        <?php endforeach; ?>

                    </div>
                </div>

            <?php endif; ?>


            <?php if($material['audios']): ?>

                <div class="accordion" data-role="switchable">
                    <div class="accordion__title" data-role="innerSwitch">
                        <div class="subtitle-2 accordion__title-text">Аудио
                            <span class="accordion__content-count">
                                <?php echo $countOwnVideo ?>
                            </span>
                        </div>
                        <svg class="accordion__arrow-down"><use xlink:href="/img/sprite.svg#decor--arrowDown"></use></svg>
                    </div>
                    <div class="accordion__content">
                        <div class="audio-files">

                            <?php

                            foreach ($material['audios'] as $audio){
                                printAudioFilePlayer($audio['name'], $audio['id']);
                            }

                            ?>

                        </div>
                    </div>
                </div>

            <?php endif; ?>

        </div>

        <?php include($this->partial('/xpage/sidebar'));  ?>

    </div>
</div>
