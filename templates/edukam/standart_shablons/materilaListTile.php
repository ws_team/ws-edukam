<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

$materials = MaterialList($this, $this->data->atype, 1, $this->values->perpage, 2);

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>


<div id="ajaxPaginatorDestinationHtml" class="container">
    <div class="wrap-content">

        <?php

        include($this->partial('/xpage/titleWithAdditionalButtons'));

        $i = 1;
        $j = 0;
        $materialsCount = count($materials);
        $preventDateNewsPeriod = '';

        foreach ($materials as $materialsItem):

            $currentDateNewsPeriod = formatDateNewsPeriod($materialsItem['date_event']);

            // плитка состоит из 7 материалов, каждые семь материалов плитка выводится заново

            // также новая плитка выводится в случае, если текущая новость опубликована в месяце,
            // который не совпадает с месяцем предыдущей новости

            if($preventDateNewsPeriod != $currentDateNewsPeriod){
                $preventDateNewsPeriod = $currentDateNewsPeriod;
                echo '</div>';
                echo '<div class="subtitle-4">'. $preventDateNewsPeriod .'</div>';
                echo '<div class="news-tile">';
                $i = 1;
            }
            elseif($i == 1) {
                echo '<div class="news-tile">';
            }

            $firstPhotoId = MaterialGetFirstPhoto($this, $materialsItem['id']);

            if(iconv_strlen($materialsItem['name'], 'UTF-8') > 82){
                $materialsItem['name'] = cuteStringWithEllipsis($materialsItem['name'], 82);
            }

            printMaterialTileItem($this, $materialsItem);

            if($i == 7 || ($j + 1) == $materialsCount) {
                echo '</div>';
                $i = 0;
            }

            $i++;
            $j++;

        endforeach;

        ?>

    </div>
</div>
<div class="container can-be-refreshed">

    <?php

    $allMaterials   = MaterialList($this, $this->data->atype, 1, 10000, 2);
    $materialsCount = count($allMaterials);

    print $this->data->blocks->render(
        'ajaxUniversalPaginator',
        'ajaxGetMaterialListTile',
        $this->data->atype,
        $this->values->perpage,
        1,
        $materialsCount
    );
    ?>

    <div class="preloader"><img src="/img/preloader.svg" class="preloader__image"></div>

</div>
