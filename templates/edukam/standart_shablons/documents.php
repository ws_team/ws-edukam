<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>

<div class="container">
    <div class="wrap-content wrap-content--double">
        <div class="main-section">

            <?php include($this->partial('/xpage/title'));  ?>

            <?php foreach ($material['documents'] as $document): ?>

                <div class="attachment">
                    <div class="attachment-image attachment-image--document">
                        <div class="attachment-image__extension"><?php echo $document['extension'] ?></div>
                    </div>
                    <div class="attachment__wrap-title">
                        <div class="label-2 attachment__title"><?php echo $document['name'] ?></div>
                        <a href="/?menu=getfile&id=<?php echo $document['id'] ?>" class="link-download link-download--attachment">
                            <svg class="link-download__icon link-download__icon--attachment"><use xlink:href="/img/sprite.svg#decor--iconDownload"></use></svg>
                            <div class="body-2 link-download__text">Скачать</div>
                        </a>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>

        <?php include($this->partial('/xpage/sidebar'));  ?>

    </div>
</div>
