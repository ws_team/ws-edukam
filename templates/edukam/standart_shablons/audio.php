<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>

<div class="container">
    <div class="wrap-content">

        <?php include($this->partial('/xpage/titleWithAdditionalButtons'));  ?>

        <div class="audio-files">

            <?php

            foreach ($material['audios'] as $audio){
                printAudioFilePlayer($audio['name'], $audio['id']);
            }

            ?>

        </div>

    </div>
</div>
