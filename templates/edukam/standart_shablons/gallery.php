<?php
/**
 * @var \iSite $this
 */

defined('_WPF_') or die();

//$photosPerPage = 3;

?>

<?php include($this->partial('/xpage/breadcrumbs'));  ?>

<div class="container">
    <div class="wrap-content wrap-content--double">
        <div class="main-section">

            <?php include($this->partial('/xpage/title'));  ?>

            <div class="main-content" data-role="gallery-grid">

                <?php

                $i = 1;
                foreach ($material['photos'] as $photo):

                    ?>

                    <div class="grid-item">
                        <div class="gallery-item">
                            <div class="gallery-item__wrap-img">
                                <a href="/photos/<?php echo $photo['id'] ?>"
                                   class="image-popup-no-margins"
                                   title="<?php echo $photo['name'] ?>"
                                >
                                    <img src="/photos/<?php echo $photo['id'] ?>_x435.jpg" alt="<?php echo $photo['name'] ?>">
                                    <span class="gallery-item__overlay-img"></span>
                                </a>
                            </div>
                            <div class="gallery-item__title"><?php echo $photo['name'] ?></div>
                        </div>
                    </div>

                <?php

                /*if($i++ >= $photosPerPage){
                    break;
                }*/

                endforeach;

                ?>

            </div>
        </div>

        <?php include($this->partial('/xpage/sidebar'));  ?>

    </div>
</div>

