<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

$frontpageMaterial = getFirstActiveMaterial($this, 'Glavnaya');

$promoSectionTitle          = '';
$promoSectionDescription    = '';
$quoteSectionTitle          = '';
$quoteSectionText           = '';
$quoteSectionAuthorName     = '';
$quoteSectionAuthorPost     = '';

foreach ($frontpageMaterial['params'] as $item){
    switch ($item['name']){
        case 'Главный заголовок страницы':
            $promoSectionTitle = $item['value'];
            break;
        case 'Подпись под главным заголовком':
            $promoSectionDescription = $item['value'];
            break;
        case 'Заголовок блока с цитатой':
            $quoteSectionTitle = $item['value'];
            break;
        case 'Текст цитаты':
            $quoteSectionText = $item['value'];
            break;
        case 'Имя автора цитаты':
            $quoteSectionAuthorName = $item['value'];
            break;
        case 'Должность автора цитаты':
            $quoteSectionAuthorPost = $item['value'];
            break;
    }
}

$promoSectionImageId        = '';
$quoteSectionAuthorImageId  = '';

foreach ($frontpageMaterial['photos'] as $item){
    switch ($item['par_name']){
        case 'Фоновое изображение первого блока':
            $promoSectionImageId = $item['id'];
            break;
        case 'Фотография автора цитаты':
            $quoteSectionAuthorImageId = $item['id'];
            break;
    }
}


?>

<div class="promo-section"
     <?php

     if($promoSectionImageId && !specialver_is_active($this)){
         echo 'style="background-image: url(\'?menu=getfile&id=' . $promoSectionImageId . '}\');"';
     }

     ?>
>
    <div class="promo-section__blackout"></div>
    <div class="container promo-section__wrap-content">
        <div class="title promo-section__title"><?php echo $promoSectionTitle ?></div>
        <div class="promo-section__split-description"></div>
        <div class="body-4 promo-section__description"><?php echo $promoSectionDescription ?></div>
    </div>
</div>

<?php

$advertisementsMatTypeIdArray = getMaterialTypeIdByCharId($this, 'Obyavleniya');
$advertisementsMaterialList   = MaterialList($this, $advertisementsMatTypeIdArray[0]['id'], 1, 4, STATUS_ACTIVE);

if($advertisementsMaterialList):

?>

<div class="adverts-section">
    <div class="container">
        <div class="subtitle-3 adverts-section__title">Объявления</div>
        <div class="adverts-list">

            <?php foreach ($advertisementsMaterialList as $item): ?>

                <div class="adverts-item adverts-item--frontpage">
                    <div class="subtitle-2 adverts-item__title">
                        <a href="<?php echo $item['url'] ?>" class="adverts-item__link"><?php echo $item['name'] ?></a>
                    </div>
                    <div class="body-2 adverts-item__description"><?php echo $item['anons'] ?></div>
                </div>

            <?php endforeach; ?>

        </div>
        <a href="/events/Obyavleniya" class="label-1 show-more-link show-more-link--frontpage">Все объявления</a>
    </div>
</div>

<?php endif; ?>

<?php

$newsMatTypeIdArr  = getMaterialTypeIdByCharId($this, 'Novosti');
$newsMaterialArr   = MaterialList($this, $newsMatTypeIdArr[0]['id'], 1, 3, STATUS_ACTIVE);

$announcementFirstMaterial  = getFirstActiveMaterial($this, 'Anonsy');
$achievementsFirstMaterial  = getFirstActiveMaterial($this, 'Nashi-dostizheniya');
$galleryFirstMaterial       = getFirstActiveMaterial($this, 'Galereya');
$creativityFirstMaterial    = getFirstActiveMaterial($this, 'Tvorchestvo-uchaschihsya');

if(specialver_is_active($this)){
    $cssClassMatList = 'news-list';
    $printMaterialItemFunctionName = 'printMaterialListItem';
}
else{
    $cssClassMatList = 'news-tile';
    $printMaterialItemFunctionName = 'printMaterialTileItem';
}

?>

<div class="container section-last-events">
    <div class="subtitle-3">Последние события</div>
    <div class="<?php echo $cssClassMatList ?>">

        <?php

        // ВЫВОД НОВОСТЕЙ

        foreach ($newsMaterialArr as $materialsItem){
            $printMaterialItemFunctionName($this, $materialsItem);
        }

        // ВЫВОД АНОНСА

        if($announcementFirstMaterial){
            $printMaterialItemFunctionName($this, $announcementFirstMaterial);
        }

        // ВЫВОД НАШИХ ДОСТИЖЕНИЙ

        if($achievementsFirstMaterial){
            $printMaterialItemFunctionName($this, $achievementsFirstMaterial);
        }

        // ВЫВОД ГАЛЕРЕИ

        if($galleryFirstMaterial){
            $printMaterialItemFunctionName($this, $galleryFirstMaterial);
        }

        // ВЫВОД ТВОРЧЕСТВА УЧАЩИХСЯ

        if($creativityFirstMaterial){
            $printMaterialItemFunctionName($this, $creativityFirstMaterial);
        }

        ?>

    </div>
    <a href="/events/" class="btn-show-more">Все события</a>
</div>

<?php

$eduProgsIdArr        = getMaterialTypeIdByCharId($this, 'Nashi-obrazovatelnye-programmy-i-napravleniya-deyatelnosti');
$eduProgsMaterialList = MaterialList($this, $eduProgsIdArr[0]['id'], 1, 10000, STATUS_ACTIVE);

if($eduProgsMaterialList):

?>

<div class="section-education-programs">
    <div class="container">
        <div class="subtitle-3">Наши образовательные программы и направления деятельности</div>
        <div class="section-education-programs__wrap-content">

            <?php

            foreach ($eduProgsMaterialList as $eduProgsMaterial):

                $eduProgsMaterial = MaterialGet($this, $eduProgsMaterial['id']);

            ?>

            <div class="section-education-programs__column">
                <div class="label-2">
                    <?php echo $eduProgsMaterial['name'] ?>
                </div>
                <ul class="vertical-links-list vertical-links-list--edu-programs">

                    <?php

                    foreach ($eduProgsMaterial['connected_materials'] as $item):

                        $item = MaterialGet($this, $item['id']);
                        $url  = '';

                        foreach ($item['params'] as $itemParam){
                            if($itemParam['name'] == 'Ссылка'){
                                $url = $itemParam['value'];
                            }
                        }

                        ?>

                        <li class="vertical-links-list__item">
                            <a href="<?php echo $url ?>" class="section-education-programs__link"><?php echo $item['name']?></a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

            <?php endforeach; ?>

        </div>
    </div>
</div>

<?php endif; ?>

<?php if($quoteSectionTitle): ?>

<div class="section-admission-places">
    <div class="container section-admission-places__wrap-content">
        <div class="section-admission-places__left-side">
            <div class="subtitle-3 section-admission-places__title"><?php echo $quoteSectionTitle ?></div>
            <div class="body-4 section-admission-places__quote"><?php echo $quoteSectionText ?></div>
            <div class="quote-author">

                <?php if($quoteSectionAuthorImageId): ?>

                    <img src="/photos/<?php echo $quoteSectionAuthorImageId ?>_xy150x150.jpg" alt="<?php echo $quoteSectionAuthorName ?>" class="quote-author__img">

                <?php endif; ?>

                <div class="quote-author__wrap-title">
                    <div class="body-4 quote-author__title"><?php echo $quoteSectionAuthorName ?></div>
                    <div class="label-1 quote-author__post"><?php echo $quoteSectionAuthorPost ?></div>
                </div>
            </div>
        </div>
        <div class="section-admission-places__right-side">

            <?php

            $examplesProgsIdArr        = getMaterialTypeIdByCharId($this, 'Primery-obrazovatelnyh-programm');
            $examplesProgsMaterialList = MaterialList($this, $examplesProgsIdArr[0]['id'], 1, 4, STATUS_ACTIVE);

            foreach ($examplesProgsMaterialList as $item):

            $item = MaterialGet($this, $item['id']);

            $unit        = '';
            $countPlaces = '';

            foreach ($item['params'] as $paramsItem){
                switch ($paramsItem['name']){
                    case 'Класс':
                        $unit = $paramsItem['value'];
                        break;
                    case 'Количество мест':
                        $countPlaces = $paramsItem['value'];
                        break;
                }
            }

            switch ($countPlaces){
                case '11':
                case '12':
                case '13':
                case '14':
                    $countPlaces .= ' мест';
                    break;
                default:
                    switch (substr($countPlaces, -1)){
                        case '1':
                            $countPlaces .= ' место';
                            break;
                        case '2':
                        case '3':
                        case '4':
                            $countPlaces .= ' места';
                            break;
                        default:
                            $countPlaces .= ' мест';
                            break;
                    }
            }

            ?>

                <div class="count-admission-places-item">
                    <div class="subtitle-5 count-admission-places-item__unit"><?php echo $unit ?></div>
                    <div class="count-admission-places-item__wrap-title">
                        <div class="body-4 count-admission-places-item__title"><?php echo $item['name'] ?></div>
                        <div class="label-1 count-admission-places-item__count"><?php echo $countPlaces ?></div>
                    </div>
                </div>

            <?php endforeach; ?>

            <a href="/Svedeniya-ob-organizacii/Obrazovanie/Obrazovatelnye-programmy" class="body-3 show-more-link--admission-places">Подробнее</a>
        </div>
    </div>
</div>

<?php endif; ?>

<?php

$usefulInfoMatIdArr     = getMaterialTypeIdByCharId($this, 'Slajder-s-poleznoj-informaciej');
$usefulInfoMaterialList = MaterialList($this, $usefulInfoMatIdArr[0]['id'], 1, 10000, STATUS_ACTIVE);

if($usefulInfoMaterialList):

?>

<div class="container">
    <div class="wrap-useful-links-carousel">
        <div class="useful-links-carousel-arrow useful-links-carousel-arrow--prev" id="usefulLinksCarouselNavArrowPrev">
            <svg class="useful-links-carousel-arrow__icon"><use xlink:href="img/sprite.svg#decor--arrow-left"></use></svg>
        </div>
        <div class="useful-links-carousel" id="usefulLinksCarousel">

            <?php

            foreach ($usefulInfoMaterialList as $item):

                $itemParams = MaterialParamGetAll($this, $item['id']);
                $url = '';

                foreach ($itemParams as $param){
                    if($param['name'] == 'Ссылка'){
                        $url = $param['value'];
                    }
                }

            ?>

            <div class="useful-links-carousel-item">
                <a href="<?php echo $url ?>" class="useful-links-carousel-item__link" target="_blank" rel="noopener noreferrer">
                    <div class="useful-links-carousel-item__wrap-img">
                        <img src="/photos/<?php echo $item['photo'] ?>_x160.jpg" alt="<?php echo $item['name'] ?>">
                    </div>
                    <div class="label-2 useful-links-carousel-item__title"><?php echo $item['name'] ?></div>
                </a>
            </div>

            <?php endforeach; ?>

        </div>
        <div class="useful-links-carousel-arrow useful-links-carousel-arrow--next" id="usefulLinksCarouselNavArrowNext">
            <svg class="useful-links-carousel-arrow__icon"><use xlink:href="img/sprite.svg#decor--arrow-right"></use></svg>
        </div>
    </div>
</div>

<?php endif; ?>
