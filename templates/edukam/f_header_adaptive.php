<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $this->data->title; ?></title>
    <META name="keywords" content="<?php echo $this->data->keywords; ?>" />
    <META name="description" content="<?php



    echo strip_tags($this->data->description);


    ?>" />
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/bootstrap/js/bootstrap-select.js"></script>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/bootstraphacks.css">
    <script src="/js/yamaps2.1.js"></script>
    <script src="/js/jquery.colorbox-min.js"></script>
    <script src="/js/jquery.formstyler.min.js"></script>
    <link href='<?php echo $this->settings->templateurl ?>/css/colorbox.css' rel='stylesheet' type='text/css' />
    <link href='<?php echo $this->settings->templateurl ?>/css/jquery.datetimepicker.css' rel='stylesheet' type='text/css' />
    <?php if($this->data->special == 1)
    {
        ?><link href='<?php echo $this->settings->templateurl ?>/css/style_lite.css' rel='stylesheet' type='text/css' /><?php
    }
    else
    {
        ?><link href='<?php echo $this->settings->templateurl ?>/css/style.css' rel='stylesheet' type='text/css' /><?php
    }?>

    <link href='/css/wpf.css?ver=1' rel='stylesheet' type='text/css' />
    <link href='/css/jquery.formstyler.css' rel='stylesheet' type='text/css' />
    <script src="<?php echo $this->settings->templateurl ?>/js/jquery.datetimepicker.js"></script>
    <script src="<?php echo $this->settings->templateurl ?>/js/vidgets.js"></script>
    <?php

    if($this->data->language == 1)
    {
        $searchplaceholder='Поиск';
        $logotext='Официальный портал Губернатора и Правительства  Хабаровского края';
        $menuname='name';
    }
    else
    {
        $searchplaceholder='Search';
        $logotext='Official portal of the Governor and Government of Khabarovsk Krai';
        $menuname='en_name';
    }

    if($this->data->amodule == 'materials')
    {
    if(($this->values->maintype == 1)&&($this->values->sectype==''))
        {
        echo '<script src="'.$this->settings->templateurl.'/js/map.js"></script>';
        }
    if(($this->values->maintype == 4)&&($this->values->sectype==''))
        {
            echo '<script src="'.$this->settings->templateurl.'/js/map.js"></script>';

        }
    if(($this->values->maintype == 4)&&($this->values->sectype == '70'))
    {
        echo '<script src="'.$this->settings->templateurl.'/js/map2.js"></script>';

    }
    }
    ?>
    <script src="<?php echo $this->settings->templateurl ?>/js/hoverIntent.js"></script>
    <script src="<?php echo $this->settings->templateurl ?>/js/superfish.js"></script>
    <script src="/templates/khabkrai/audiojs/audio.min.js"></script>
    <link rel="stylesheet" media="screen" href="<?php echo $this->settings->templateurl ?>/css/superfish.css" />
    <link rel="stylesheet" media="screen" href="<?php echo $this->settings->templateurl ?>/css/superfish-vertical.css" />

        <?php

        include_once($this->settings->path.'/components/materials/material_types_array.php');

        if(isset($this->data->morejs)&&($this->data->morejs != ''))
        {
            echo $this->data->morejs;
        }

        ?>
        <script>


            $(document).ready(function () {


                /*audiojs.events.ready(function() {
                    var as = audiojs.createAll();
                });*/

                jQuery('ul.sf-menu').superfish();

                $('input.styler, select.styler').styler();

                //колорбокс будет тут:
                $(".imggroup").colorbox({rel:'imggroup'});

                <?php

                 if(isset($this->data->jsdocready)&&($this->data->jsdocready != ''))
                 {
                 echo $this->data->jsdocready;
                 }

                 ?>


                jQuery('img.svg, img.svgsome').each(function(){
                    var $img = jQuery(this);
                    var imgID = $img.attr('id');
                    var imgClass = $img.attr('class');
                    var imgURL = $img.attr('src');

                    jQuery.get(imgURL, function(data) {
                        // Get the SVG tag, ignore the rest
                        var $svg = jQuery(data).find('svg');

                        // Add replaced image's ID to the new SVG
                        if(typeof imgID !== 'undefined') {
                            $svg = $svg.attr('id', imgID);
                        }
                        // Add replaced image's classes to the new SVG
                        if(typeof imgClass !== 'undefined') {
                            $svg = $svg.attr('class', imgClass+' replaced-svg');
                        }

                        // Remove any invalid XML tags as per http://validator.w3.org
                        $svg = $svg.removeAttr('xmlns:a');

                        // Replace image with new SVG
                        $img.replaceWith($svg);

                    }, 'xml');

                });


                });

        </script>
    </head>
    <body><?php
    //подключаем флаг края

    if($this->values->sectype == '')
    {
        $ifflag = true;
    }
    else
    {
        $ifflag = false;
    }

    if($ifflag)
    {
        ?><div class="kraiflag"></div><?php
    }


    //подключаем админ-меню для админа
    if(($this->autorize->autorized == 1)&&($this->autorize->userrole > 0))
    {
        include_once($this->settings->path.$this->settings->templateurl.'/f_adminpanel.php');
    }
    ?>
<div class="mainmenu" style="height: auto;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="menu" style="width: auto; height: auto;">
                <?php

                if(isset($this->data->material_types))
                {

                    //print_r($this->data->material_types);

                    foreach($this->data->material_types as $mtype)
                    {
                        if(($mtype['parent_id'] == '')&&($mtype['showinmenu'] == '1'))
                        {
                            if(($this->data->language == 1)||($mtype['id'] == 4)||($mtype['id'] == 5)){

                                //активный главный тип
                                if($this->values->maintype == $mtype['id'])
                                {
                                    $styleclass='option active';
                                    $sectypearr=$mtype;
                                    $mainname=$mtype['name'];
                                }
                                else
                                {
                                    $styleclass='option';
                                }



                                echo '<div class="'.$styleclass.'" style="white-space: nowrap;" onclick="document.location.href='."'/".$mtype['id_char']."'".'">'.$mtype[$menuname].'</div>';
                            }}
                    }
                }

                ?>
                    </div>
            </div>
            <div class="col-md-3" style="text-align: right;">
                <div style="line-height: 33px; padding: 0 8px 0 8px; font-size: 11px; color: #4991c7;">
                <?php echo $this->data->datestr; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="nullblock" style="width: 99px; height: 115px;">
                <div class="logo"><a href='/'><img src="/templates/khabkrai/img/logo3.png" width="99" height="115" border="0"></a></div>
                <div class="logo_text"><?php echo $logotext; ?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="nullblock" style="min-width: 278px; width: auto; height: 135px; text-align: right;">
            <div onclick="document.location.href='/?language=en'" class="lang en <?php if($this->data->language == 2) echo 'active'; ?>">EN</div>
            <div onclick="document.location.href='/?language=ru'" class="lang ru <?php if($this->data->language == 1) echo 'active'; ?>">РУ</div>
            <div class="globalsearch"><?php $this->GetPostValues('search');

                $this->values->search=trim($this->values->search,'utf8');



                ?>
                <input class="globalsearch_str" id="globalsearch" type="text" placeholder="<?php echo $searchplaceholder; ?>" value="<?php echo $this->values->search; ?>"/>
                <div class="globalsearch_btn" onclick="doSearch();"></div>
            </div>
            <div class="viewversion" <?php if($this->data->special == 1)
            {
            ?>onclick="document.location.href='/?version=normal'" <?php
                 }else
                 {
                 ?>onclick="document.location.href='/?version=special'" <?php
            } ?>><?php if($this->data->language == 1)
                {
                    if($this->data->special == 1){
                        echo 'Обычная версия сайта';
                    }
                    else
                    {
                        echo 'Версия для слабовидящих';
                    }
                }
                else
                {

                    if($this->data->special == 1){
                        echo 'Normal site version';
                    }
                    else
                    {
                        echo 'Version for Visually Impaired';
                    }
                }?></div>
                </div>
        </div>
    </div>
</div>
<div class="container">
    <nav class="navbar navbar-default" role="navigation">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Навигация</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php

                    if(isset($sectypearr))
                    {
                        $mc=0;

                        foreach($sectypearr['childs'] as $mtype)
                        {
                            if($mtype['showinmenu'] == '1'){
                                //если активный раздел
                                if($mtype['id'] == $this->values->sectype)
                                {
                                    $styleclass='class="active';
                                    $secname=$mtype['name'];
                                }
                                else
                                {
                                    $styleclass='';
                                }

                                //определяем, есть или нет выпадашки
                                $pi=0;
                                $submenu='';
                                if(count($mtype['childs']) > 0)
                                {
                                    foreach($mtype['childs'] as $smtype)
                                    {
                                        $styleclasss='';

                                        if($smtype['showinmenu'] == '1'){

                                            ++$pi;
                                            if($smtype['id'] == $this->values->thirdtype)
                                            {
                                                $styleclasss='class="active"';
                                                $thirdname=$smtype['name'];
                                            }
                                            else
                                            {
                                                $styleclasss='';
                                            }

                                            if($pi == 1)
                                            {


                                                $submenu.="<li class='link-toggle collapsed'><a href='/".$sectypearr['id_char'].'/'.$mtype['id_char']."'>".$mtype[$menuname]."</a></li>";
                                            }

                                            $submenu.='<li '.$styleclasss.'><a href="/'.$sectypearr['id_char'].'/'.$mtype['id_char']."/".$smtype['id_char']."/".'">'.$smtype[$menuname].'</a></li>';

                                        }

                                    }
                                }

                                if($pi > 0)
                                {
                                    if($styleclass != '')
                                    {
                                        $styleclass.=' dropdown"';
                                    }
                                    else
                                    {
                                        $styleclass.='class="dropdown"';
                                    }

                                    $dropa='class="dropdown-toggle" data-toggle="dropdown"';


                                    $caret='<span class="caret"></span>';

                                }
                                else
                                {
                                    if($styleclass != '')
                                    {
                                        $styleclass.='"';
                                    }

                                    $dropa='';

                                    $caret='';

                                }

                                echo "<li $styleclass><a href='/".$sectypearr['id_char'].'/'.$mtype['id_char']."' $dropa>".$mtype[$menuname]."$caret</a>";

                                if($pi > 0)
                                {
                                    echo '<ul class="dropdown-menu" role="menu">
                                    '.$submenu.'
                                    </ul>';
                                }

                                echo "</li>";

                            }
                        }
                    }

                    ?>
                </ul>
            </div>

    </nav>
</div>

    <div class="wrapper">


        <div class="content">