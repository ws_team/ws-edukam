<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

global
    $normaltypes,
    $sectypearr;

//echo 'material_types_array begin';exit;

$this->data->material_types = ArrCacheRead($this, 'material_types_'.$this->data->language);

if (empty($this->data->material_types)) {
    $this->data->material_types = GepTypeParentTypes($this);
    ArrCacheWrite($this, $this->data->material_types, 'material_types_'.$this->data->language);
}

//выводим главные типы материалов
if (isset($this->data->material_types)) {
    foreach ($this->data->material_types as $mtype) {
        if (($mtype['parent_id'] == '0') && ($mtype['showinmenu'] == '1'))
        {
            //активный главный тип
            if (($this->values->maintype == $mtype['id'])&&($this->data->amodule == 'materials')) {
                $sectypearr = $mtype;
                $mainname = $mtype['name'];
            }
        }
    }
}


$normaltypes = ArrCacheRead($this, 'normal_material_types');
//определяем список разделов, у которых есть связка материалов

/*if ($_REQUEST['debug']){
    krumo($normaltypes);
}*/

if ( ! is_array($normaltypes)) {
    $normaltypes = array();
    $query = 'SELECT DISTINCT "type_id" FROM "materials" WHERE "status_id" = $1';
    $res = $this->dbquery($query, array(STATUS_ACTIVE));
    if ( ! empty($res)) {
        foreach ($res as $row) {

            $normaltypes[] = $row['type_id'];
            //добавляем родительские типы
            $morearr = GetTypeParents($this, $row['type_id']);
            if (count($morearr) > 0) {
                foreach ($morearr as $ma) {
                    $normaltypes[] = $ma;
                }
            }
            
        }
    }

    ArrCacheWrite($this, $normaltypes, 'normal_material_types');
}

