<?php
/**
 * @var \iSite $this
 */

namespace wpf\components\autorize;

defined('_WPF_') or die();

require_once(__DIR__.'/functions.php');

//количество попыток авторизации
if ( ! isset($_SESSION['authtryes'])) {
    $_SESSION['authtryes'] = 0;
}

if ( ! isset($_SESSION['authblock'])) {
    $_SESSION['authblock'] = 0;
}

authinfo_reset($this->autorize);

function KhabkraiDoAutorize(\iSite $site)
{
    //функция-костыль - для авторизации на всех сайтах
    if (auth_change_password($site) === false)
        return false;
    if (auth_login($site) === false)
        return false;

    $site->afterUserLogin();
}

//производим авторизацию
KhabkraiDoAutorize($this);

//получаем данные из сессии
if (isset($_SESSION['autorize'])) {
    authinfo_restore($this);
}
