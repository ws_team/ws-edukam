<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

return array(
    array(
        'allow',
        'admin.sysoptions',
        'user' => '1',
    ),
    array(
        'deny',
        'admin.sysoptions',
    ),
    array(
        'allow',
        // anything
        'role' => 'admin',
    ),
);