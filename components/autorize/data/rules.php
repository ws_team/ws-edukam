<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

return array(
    array(
        'deny',
        'editmaterials',
        'callback' => '\wpf\components\autorize\editmaterials_restrict_bound',
        'callback_params' => array(
            'site' => $this,
        ),
    ),
);