<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$site = $this;

$site->hook('materialdelete', function ($hook_state) use ($site) {
    $id = $hook_state->id;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "id=$id");

    delMatCache($site, $id);
    $site->getLogger()->write(LOG_INFO, __FILE__, __LINE__);
    solr_DeleteMaterial($site, $id);
    $site->getLogger()->write(LOG_INFO, __FILE__, __LINE__);
    $site->resetActiveMaterialTypes();
    $site->getLogger()->write(LOG_INFO, __FILE__, __LINE__);
});

$site->hook('materialsave', function ($hook_state) use ($site) {
    $id = $hook_state->id;
    $type_id = $hook_state->type_id;
    $attributes = $hook_state->attributes;
    $status_id = isset($attributes['status_id']) ? $attributes['status_id'] : null;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "id=$id, type_id=$type_id, status_id='$status_id'");

    delMatCache($site, $id);

    MaterialBind($site, $id);

    if ( ! is_null($status_id)) {
        //индексируем материал
        if ($status_id == STATUS_ACTIVE) {
            //одновляем/создаём индекс
            Solr_AddMaterial($site, $id);
        } else {
            //удаляем индекс
            Solr_DeleteMaterial($site, $id);
        }

        MaterialTypesResetActive($site);
    }

    CreateOpenDataCsvMaterial($site, $id, $type_id);
    CreateOpenDataCsvList($site, $type_id);
});

$site->hook('materialconnect', function ($hook_state) use ($site) {
    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "parent_id={$hook_state->parent_id}, child_id={$hook_state->child_id}");

    delMatCache($site, $hook_state->parent_id);
    delMatCache($site, $hook_state->child_id);
});

$site->hook('materialconnectionreorder', function ($hook_state) use ($site) {
    //чистим кэши материалов
    delMatCache($site, $hook_state->parent_id);
    delMatCache($site, $hook_state->child_id);
});

$site->hook('materialedit', function ($hook_state) use ($site) {

    $material = $hook_state->material;

    if ($material['status_id'] == STATUS_ACTIVE &&
            (!is_null($hook_state->subscriptionDaily) ||
            !is_null($hook_state->subscriptionCancel) ||
            !is_null($hook_state->subscriptionImmediately))) {

        $site->wpf_include('modules/subscription/functions.php');

        if( is_null($hook_state->subscriptionCancel) )
        {

            if(!is_null($hook_state->subscriptionDaily) && constant('SUBSCRIPTION_ENABLED'))
                subscription_queue_item_add($site, $material['type_id'], $material['id'], SUBSCRIPTION_SEND_DAILY);

            if(!is_null($hook_state->subscriptionImmediately) && constant('SUBSCRIPTION_ENABLED'))
                subscription_queue_item_add($site, $material['type_id'], $material['id'], SUBSCRIPTION_SEND_IMMEDIATE);

        }else{
            remove_future_items_subscription_queue($site, $material['id']);
        }
    }
});
