<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$site = $this;

$site->hook('materialtypedeletable', function ($hook_state) use ($site) {
    $type_id = $hook_state->id;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id");

    if (materialTypeHasChildren($site, $type_id)) {
        $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id has children");
        $hook_state->cancel = true;
        $hook_state->message = 'нет возможности удалить тип материала - у него есть подтипы. Удалите сначала их!';
    }
});

$site->hook('beforematerialtypedelete', function ($hook_state) use ($site) {
    $type_id = $hook_state->id;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id");

    $query = <<<EOS
UPDATE "materials"
SET
    "status_id" = $1,
    "type_id" = 0
WHERE "type_id" = $2
RETURNING "materials"."id"
EOS;

    $res = $this->dbquery($query, array(STATUS_DELETED, $type_id), true);
    if ( ! empty($res)) {
        $site->beginBatch();
        foreach ($res as $item) {
            $site->callHook('materialdelete', array(
                'id' => $item['id'],
                'physical' => false,
            ));
        }
        $site->endBatch();
    }
});

$site->hook('materialtypedelete', function ($hook_state) use ($site) {
    $type_id = $hook_state->id;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id");

    $site->data->material_types = GepTypeParentTypes($site, 0);
    ArrCacheWrite($site, $site->data->material_types, 'material_types_'.$site->data->language);

    MaterialTypesResetActive($site);
});

$site->hook('materialtypecreate', function ($hook_state) use ($site) {
    $type_id = $hook_state->id;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id");
});

// Добавлен или обновлен существующий тип материала
$site->hook('materialtypesave', function ($hook_state) use ($site) {
    $type_id = $hook_state->id;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id");

    $site->data->material_types = GepTypeParentTypes($site, 0);
    ArrCacheWrite($site, $site->data->material_types, 'material_types_'.$site->data->language);
});

$site->hook('materialtypechange', function ($hook_state) use ($site) {
    $type_id = $hook_state->id;
    // $id_char = $hook_state->id_char;

    $site->getLogger()->write(LOG_INFO, __FILE__.':'.__LINE__, "type_id=$type_id");
});