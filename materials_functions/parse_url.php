<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


//функция разбора URL строки в типы/подтипы материалов, материалы
//логика работы - проверяем на наличае char_id типа материала, если есть - считаем что выбран тип, иначе проверяем на id материала,
//одновременно перестраиваем хлебные крошки и названия шаблонов.
//После окончания разбора, формируем массив типов материалов, формируем переменные для совместимости со старым разбором
//НУЖНО БУДЕТ ПЕОЕВЕРСТАТЬ ШАБЛОНЫ, где отображались подтипы материала.

function ParseOIVURL(\iSite $khabkrai)
{
    //разбираем переданную строку запроса
    //главные переменные
    $requestarr = explode('/', $khabkrai->values->requeststr);

    $rcnt = count($requestarr) - 1;


    $moreands='';
    if ($rcnt >= 0) {
        $len = mb_stripos($requestarr[$rcnt], '&', null, 'UTF8');

        //вырезаем с последнего &-ы
        if (($len === 0)||($len)) {
            if($len == 0)
            {
                $moreands=$requestarr[$rcnt];
                $last=mb_substr($requestarr[$rcnt], 0, $len, 'UTF8');
                $moreands=mb_substr($requestarr[$rcnt], $len, null, 'UTF8');
                $requestarr[$rcnt]=$last;
                unset($requestarr[$rcnt]);
                //echo '0 позиция';
            }
            else{
                //echo 'позиция в тексте';
                $last=mb_substr($requestarr[$rcnt], 0, $len, 'UTF8');
                $moreands=mb_substr($requestarr[$rcnt], $len, null, 'UTF8');
                $requestarr[$rcnt]=$last;
            }
        }
    }

    if ($moreands != '') {
        $moreandsarr=explode('&',$moreands);
        if (count($moreandsarr) > 0) {
            foreach ($moreandsarr as $andel) {
                $andelarr=explode('=',$andel);
                if (count($andelarr) == 2) {
                    $khabkrai->values->$andelarr[0] = $andelarr[1];
                    $_REQUEST[$andelarr[0]]= $andelarr[1];
                }
            }
        }
    }

    //последнюю строку разбираем еще раз
    $rvarscnt = count($requestarr);

    $morerequestarr = $requestarr[($rvarscnt-1)];

    $temparr = explode('?',$morerequestarr);

    if(isset($temparr[0])&&($temparr[0]!=''))
    {
        $requestarr[($rvarscnt-1)]=$temparr[0];
    }
    else
    {
        unset($requestarr[($rvarscnt-1)]);
    }

    if($khabkrai->values->menu=='')
    {
        $khabkrai->values->menu='materials';
    }


    $allowed_modules = allowed_modules_oiv();

    //хаки для перевода главных страниц разделов

    if ((!isset($requestarr[0])||($requestarr[0] == ''))&&(!in_array($khabkrai->values->menu,$allowed_modules))) {
        $khabkrai->values->menu = 'materials';
        $khabkrai->values->mainname = 'События';
        $khabkrai->values->maintype = 1;
    }

    //запускаем процесс разбора ссылки

    $khabkrai->data->matURLArr = Array();
    $khabkrai->data->matURLIdsArr = Array();

    $khabkrai = ParseSiteURL_Step($khabkrai,$requestarr,1);

    //второстепенные переменные
    if (isset($temparr[1])) {
        $morerequestarr = $temparr[1];

        $morerequestarr = explode('&',$morerequestarr);

        foreach ($morerequestarr as $morequest) {
            $newval=explode('=',$morequest);
            if(($morequest[0] != '')&&($morequest[1] != '')) {
                $khabkrai->values->$morequest[0] = $morequest[1];
            }
        }
    }

    return($khabkrai);
}

function ParseSiteURL(\iSite $khabkrai)
{
    if ( ! empty($khabkrai->values->menu))
        return $khabkrai;

    //разбираем переданную строку запроса
    //главные переменные
    $requestarr = explode('/', $khabkrai->values->requeststr);

    $rcnt = count($requestarr) - 1;

    $moreands='';
    if($rcnt >= 0)
    {
        $len = mb_stripos($requestarr[$rcnt], '&', null, 'UTF8');

        //вырезаем с последнего &-ы
        if (($len === 0)||($len)) {
            if ($len == 0) {
                $moreands=$requestarr[$rcnt];
                $last=mb_substr($requestarr[$rcnt], 0, $len, 'UTF8');
                $moreands=mb_substr($requestarr[$rcnt], $len, null, 'UTF8');
                $requestarr[$rcnt]=$last;
                unset($requestarr[$rcnt]);
            } else {
                $last=mb_substr($requestarr[$rcnt], 0, $len, 'UTF8');
                $moreands=mb_substr($requestarr[$rcnt], $len, null, 'UTF8');
                $requestarr[$rcnt]=$last;
            }
        }
    }

    if($moreands != '')
    {
        $moreandsarr=explode('&',$moreands);
        if(count($moreandsarr) > 0)
        {
            foreach($moreandsarr as $andel)
            {
                $andelarr=explode('=',$andel);
                if(count($andelarr) == 2)
                {
                    $khabkrai->values->$andelarr[0] = $andelarr[1];
                    $_REQUEST[$andelarr[0]]= $andelarr[1];
                }
            }
        }
    }

    //последнюю строку разбираем еще раз
    $rvarscnt=count($requestarr);

    $morerequestarr=$requestarr[($rvarscnt-1)];

    $temparr=explode('?',$morerequestarr);

    if(isset($temparr[0])&&($temparr[0]!='')) {
        $requestarr[($rvarscnt-1)]=$temparr[0];
    } else {
        unset($requestarr[($rvarscnt-1)]);
    }

    if($khabkrai->values->menu == '')
    {
        $khabkrai->values->menu = 'materials';
    }

    $allowed_modules = allowed_modules_khabkrai();

    //хаки для перевода главных страниц разделов

    if (empty($requestarr[0]) && ! in_array($khabkrai->values->menu, $allowed_modules)) {
        if ($khabkrai->data->language == LANG_RU) {
            $khabkrai->values->menu = 'materials';
            $khabkrai->values->mainname = 'События';
        } else {
            header('Location: /governor');
        }
    }

    //Заменить на свойства типов материалов - сделать поле переадресации================================================

    RedirectKhabkrai($khabkrai, $requestarr);
    BreadcrumbsHide($khabkrai, $requestarr);

    if(isset($requestarr[0])&&($requestarr[0] == 'authorities')&&((isset($requestarr[1]))&&($requestarr[1] == 'Pravitelstvo'))&&((isset($requestarr[2]))&&($requestarr[2] != '')))
    {
        $khabkrai->data->ifflag=false;
    }

    if(isset($requestarr[0])&&($requestarr[0] == 'civil-service')&&((isset($requestarr[1]))&&($requestarr[1] == 'competitions-vacancy'))&&((isset($requestarr[2]))&&($requestarr[2] != '')))
    {
        $khabkrai->data->ifflag=false;
    }

    //запускаем процесс разбора ссылки

    $khabkrai->data->matURLArr = Array();
    $khabkrai->data->matURLIdsArr = Array();

    $khabkrai = ParseSiteURL_Step($khabkrai,$requestarr,1);

    //второстепенные переменные
    if(isset($temparr[1]))
    {
        $morerequestarr = $temparr[1];
        $morerequestarr = explode('&',$morerequestarr);

        foreach($morerequestarr as $morequest)
        {
            $newval=explode('=',$morequest);
            if(($morequest[0] != '')&&($morequest[1] != ''))
            {
                $khabkrai->values->$morequest[0] = $morequest[1];
            }
        }
    }

    return $khabkrai;
}

/**
 * @param iSite $khabkrai
 * @param $requestarr
 */
function BreadcrumbsHide(\iSite $khabkrai, $requestarr)
{
    // @todo выпилить, перенести в шаблон(ы)

    if (isset($requestarr[0])
        && ($requestarr[0] == 'Gossluzhba')
        && ((isset($requestarr[1])) && ($requestarr[1] == 'Usloviya-prohozhdeniya'))
        && ((isset($requestarr[2])) && ($requestarr[2] == 'Normativno-pravovaya-baza'))) {

        $khabkrai->data->hidebread = true;
    }

    if (isset($requestarr[0])
        && ($requestarr[0] == 'officially')
        && ((isset($requestarr[1])) && ($requestarr[1] == 'Gosudarstvennye-i-municipalnye-uslugi')) && ((isset($requestarr[2])) && ($requestarr[2] == 'Gosudarstvennye-uslugi'))) {

        $khabkrai->data->hidebread = true;

    }

    if (isset($requestarr[0])
        && ($requestarr[0] == 'contacts')
        && ((isset($requestarr[1])) && ($requestarr[1] == 'contacts-governments'))) {

        $khabkrai->data->hidebread = true;

    }

    if (isset($requestarr[0])
        && ($requestarr[0] == 'civil-service')
        && ((isset($requestarr[1])) && ($requestarr[1] == 'competitions-vacancy'))) {

        $khabkrai->data->hidebread = true;
    }

    if(isset($requestarr[0])&&($requestarr[0] == 'authorities')
        &&((isset($requestarr[1]))&&($requestarr[1] == 'Pravitelstvo'))
        &&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->data->hidebread=true;
    }
}

function ParseSiteURL_Step(\iSite $khabkrai, $requestarr, $number)
{
    $i = $number - 1;

    if ( ! empty($requestarr[$i])) {
        $astring = $requestarr[$i];

        //проверяем строку на тип материала

        if ( ! empty($khabkrai->data->languages)) {
            $languages = $khabkrai->data->languages;
        } else {
            $languages = array('en');
        }

        $lang_names = array();
        foreach ($languages as $lang_name) {
            $lang_names[] = $lang_name.'_name';
        }

        $lang_names = implode(',', $lang_names);

        $query = <<<EOS
SELECT
    "id",
    "name",
    $lang_names,
    "parent_id"
FROM "material_types"
WHERE "id_char" = $1
EOS;
        $res = $khabkrai->dbquery($query, array($astring));

        if ( ! empty($res)) {
            if (count($res) && ! empty($res[0]['id'])) {
                $record = $res[0];

                if ($khabkrai->data->language == LANG_RU) {
                    $khabkrai->data->matURLArr[$i]['name'] = $record['name'];
                } else {
                    $lang_name = $khabkrai->data->languagename.'_name';
                    $khabkrai->data->matURLArr[$i]['name'] = $record[$lang_name];
                }

                $khabkrai->data->matURLArr[$i]['id'] = $record['id'];
                $khabkrai->data->matURLArr[$i]['type'] = 'type';
                $khabkrai->data->matURLArr[$i]['char'] = $requestarr[$i];

                //для обратной совместимости подставляем значения старых переменных
                SetLegacyRequestParams($khabkrai, $number);
            } else {
                if (strval((int)$astring) == $astring) {
                    $query='SELECT "id", "name" FROM "materials" WHERE "id" = '.$astring;
                }
                else{
                    $query='SELECT "id", "name" FROM "materials" WHERE "id_char" = '."'".$astring."'";
                }

                if ($res=$khabkrai->dbquery($query)) {
                    if (count($res) > 0) {
                        $khabkrai->data->matURLArr[$i]['id'] = $res[0]['id'];
                        $khabkrai->data->matURLArr[$i]['type'] = 'mat';
                        $khabkrai->data->matURLArr[$i]['name'] = $res[0]['name'];
                        $khabkrai->data->matURLArr[$i]['char'] = $requestarr[$i];

                        $khabkrai->data->matURLIdsArr[]=$khabkrai->data->matURLArr[$i];


                        //для обратной совместимости подставляем значения старых переменных
                        $khabkrai->values->id=$res[0]['id'];
                        $khabkrai->values->idname=$res[0]['name'];
                    } else {
                        $khabkrai->values->menu = '404';
                    }
                } else {
                    $khabkrai->values->menu = '404';
                }
            }
        }  else {
            if (strval((int)$astring) == $astring) {
                $query='SELECT "id", "name" FROM "materials" WHERE "id" = '."'".$astring."'";

                $odstring=str_replace('2700000786-','',$astring);

                $queryopendata='SELECT "id", "name" FROM "materials" WHERE "id_char" = '."'".$odstring."'";

                //разбираем строку - отделяем название и расширение
                $filenamearr=explode('.',$astring);

                if(count($filenamearr) == 2)
                {
                    $filename=$filenamearr[0];
                    $fileext=$filenamearr[1];

                    $queryopendatafile='SELECT "id" FROM "files" WHERE "extension" = '."'".$fileext."'".' AND "name" = '."'".$filename."'";
                } else {
                    $queryopendatafile='';
                }
            }
            else{
                $query='SELECT "id", "name" FROM "materials" WHERE "id_char" = '."'".$astring."'";

                $odstring=str_replace('2700000786-','',$astring);

                $queryopendata='SELECT "id", "name" FROM "materials" WHERE "id_char" = '."'".$odstring."'";
            }

            $res = $khabkrai->dbquery($query);

            if( ! empty($res))
            {
                if(count($res) > 0)
                {
                    $khabkrai->data->matURLArr[$i]['id'] = $res[0]['id'];
                    $khabkrai->data->matURLArr[$i]['type'] = 'mat';
                    $khabkrai->data->matURLArr[$i]['name'] = $res[0]['name'];
                    $khabkrai->data->matURLArr[$i]['char'] = $requestarr[$i];

                    $khabkrai->data->matURLIdsArr[]=$khabkrai->data->matURLArr[$i];


                    //для обратной совместимости подставляем значения старых переменных
                    $khabkrai->values->id=$res[0]['id'];
                    $khabkrai->values->idname=$res[0]['name'];
                }
                else{
                    $khabkrai->values->menu='404';
                }
            } elseif($res=$khabkrai->dbquery($queryopendata)) {
                //разбор для открытых данных - ссылка на паспорт набора
                if(count($res) > 0)
                {
                    $khabkrai->data->matURLArr[$i]['id'] = $res[0]['id'];
                    $khabkrai->data->matURLArr[$i]['type'] = 'mat';
                    $khabkrai->data->matURLArr[$i]['name'] = $res[0]['name'];
                    $khabkrai->data->matURLArr[$i]['char'] = $requestarr[$i];

                    $khabkrai->data->matURLIdsArr[]=$khabkrai->data->matURLArr[$i];


                    //для обратной совместимости подставляем значения старых переменных
                    $khabkrai->values->id=$res[0]['id'];
                    $khabkrai->values->idname=$res[0]['name'];
                }
                else{
                    $khabkrai->values->menu='404';
                }
            }
            //разбор для скачивания файлов открытых данных
            elseif(!empty($queryopendatafile))
            {
                if($res=$khabkrai->dbquery($queryopendatafile))
                {
                    if(count($res) > 0)
                    {
                        if($res[0]['id'] != '')
                        {
                            $khabkrai->values->menu = 'getfile';
                            $khabkrai->values->id=$res[0]['id'];
                            $khabkrai->values->tryupdatedownloads=1;
                        }
                        else
                        {
                            $khabkrai->values->menu = '404';
                        }
                    }
                    else
                    {
                        $khabkrai->values->menu='404';
                    }
                }
                else{
                    $khabkrai->values->menu='404';
                }
            }
            else{
                $khabkrai->values->menu='404';
            }
        }
    }

    //проверяем след. значение

    ++$i;

    if ((isset($requestarr[$i]))&&($requestarr[$i] != ''))
    {
        $number = $i + 1;
        $khabkrai = ParseSiteURL_Step($khabkrai, $requestarr, $number);
    }


    //заносим пустые старые переменные
    if ( ! isset($khabkrai->values->id))
    {
        $khabkrai->values->id='';
        $khabkrai->values->idname='';
    }

    if( ! isset($khabkrai->values->maintype))
    {
        $khabkrai->values->maintype = '';
        $khabkrai->values->mainname = '';
        $khabkrai->values->maintypechar = '';
    }

    if ( ! isset($khabkrai->values->sectype))
    {
        $khabkrai->values->sectype='';
        $khabkrai->values->sectypename='';
        $khabkrai->values->sectypechar='';
    }

    if ( ! isset($khabkrai->values->thirdtype))
    {
        $khabkrai->values->thirdtype='';
        $khabkrai->values->thirdtypename='';
        $khabkrai->values->thirdtypechar='';
    }

    if (!isset($khabkrai->values->fourtype)) {
        $khabkrai->values->fourtype = '';
        $khabkrai->values->fourtypename = '';
        $khabkrai->values->fourtypechar = '';
    }

    if (!isset($khabkrai->values->fivetype)) {
        $khabkrai->values->fivetype = '';
        $khabkrai->values->fivetypename = '';
        $khabkrai->values->fivetypechar = '';
    }

    return $khabkrai;
}

function SetLegacyRequestParams(\iSite $khabkrai, $number)
{
    $i = $number - 1;

    switch($number) {
        case '1':
            $khabkrai->values->maintype = $khabkrai->data->matURLArr[$i]['id'];
            $khabkrai->values->mainname = $khabkrai->data->matURLArr[$i]['name'];
            $khabkrai->values->maintypechar = $khabkrai->data->matURLArr[$i]['char'];

            break;

        case '2':

            $khabkrai->values->sectype = $khabkrai->data->matURLArr[$i]['id'];
            $khabkrai->values->sectypename = $khabkrai->data->matURLArr[$i]['name'];
            $khabkrai->values->sectypechar = $khabkrai->data->matURLArr[$i]['char'];

            break;

        case '3':

            $khabkrai->values->thirdtype=$khabkrai->data->matURLArr[$i]['id'];
            $khabkrai->values->thirdtypename=$khabkrai->data->matURLArr[$i]['name'];
            $khabkrai->values->thirdtypechar=$khabkrai->data->matURLArr[$i]['char'];

            break;

        case '4':

            $khabkrai->values->fourtype = $khabkrai->data->matURLArr[$i]['id'];
            $khabkrai->values->fourtypename = $khabkrai->data->matURLArr[$i]['name'];
            $khabkrai->values->fourtypechar = $khabkrai->data->matURLArr[$i]['char'];

            break;

        case '5':

            $khabkrai->values->fivetype = $khabkrai->data->matURLArr[$i]['id'];
            $khabkrai->values->fivetypename = $khabkrai->data->matURLArr[$i]['name'];
            $khabkrai->values->fivetypechar = $khabkrai->data->matURLArr[$i]['char'];

            break;
    }
}

function allowed_modules_oiv()
{
    return array(// $adminarr
        '404',
        'admin',
        'ajax',

        'cacheinfo',
        'candidates_app',
        'candidates',
        'editmaterials',
        'emailsend',
        'exit',
        'export',

        'getfile',
        'getvideo',
        'imagecaches',
        'logger',
        'projexp',
        'projimp',

        'main',
        'materials_ajax',
        'material_solrindexall',
        'material_types',
        'matsync',
        'news_parser',

        'opendata_viewer',
        'omsu_parser',
        'oivstat',
        'opendata_viewer',
        'oiv_update_db',
        'oiv_update_files',

        'playvideo',
        'remotecontrol',
        'requests',
        'rss',

        'searchlogs',
        'settings',
        'sitemap_builder',
        'sjsync',
        'standarttemplates',
        'sysoptions',
        'addmdb',
	    'zerofiles',

        'testrequest',

        'underconstruction',
        'updatedb',
        'usersettings',
        'users',
        'xlsexport',
        'appealsstat',

        'anketexport',

        'units',
        'authors',

        'savefile',
        'authorsStatistic',
    );
}

function allowed_modules_khabkrai()
{
    return array(// $adminarr
        '404',
        'admin',
        'ajax',

        'cacheinfo',
        'copyActiveDataToTest',

        'editmaterials',
        'exit',
        'export',

        'getfile',
        'getvideo',

        'imagecaches',
        'logger',

        'materials_ajax',
        'material_solrindexall',
        'material_types',
        'main',// *
        'news_parser',
        'oivstat',
        'oiv_update_db',
        'oiv_update_files',
        'oiv_update_alldb',
        'omsu_parser',
        'opendata',
        'opendata_viewer',

        'parsemembook',
        'playvideo',
        'remotecontrol',
        'rss',

        'sitemap_builder',
        'standarttemplates',
        'searchlogs',
        'settings',
        'schedule',
        'sysoptions',
        'subscription',

        'testrequest',

        'underconstruction',
        'updatedb',
        'users',
        'usersettings',
        'xlsexport',
        'appealsstat',

        'anketexport',

        'savefile',
        'authorsStatistic',
    );
}

function RedirectKhabkrai(\iSite $khabkrai, $requestarr)
{
    if (isset($requestarr[0])&&($requestarr[0] == 'events')
        &&(!isset($requestarr[1])||$requestarr[1] == ''))
    {
        if($khabkrai->data->language == LANG_RU) {
            $khabkrai->redirect('/');
        }
    }

    if (isset($requestarr[0])&&($requestarr[0] == 'civil-service')
        &&((!isset($requestarr[1]))||($requestarr[1] == '')))
    {
        $khabkrai->redirect('/civil-service/competitions-vacancy');
    }

    if(isset($requestarr[0])
        &&($requestarr[0] == 'Gossluzhba')
        &&((!isset($requestarr[1]))||($requestarr[1] == '')))
    {
        $khabkrai->redirect('/Gossluzhba/Usloviya-prohozhdeniya/Kvalifikacionnye-trebovaniya');
    }

    if(isset($requestarr[1])
        &&($requestarr[1] == 'Usloviya-prohozhdeniya')
        &&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/Gossluzhba/Usloviya-prohozhdeniya/Kvalifikacionnye-trebovaniya');
    }

    if(isset($requestarr[1])&&($requestarr[1] == 'Podgotovka-kadrov')&&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/Gossluzhba/Podgotovka-kadrov/Rezerv-upravlencheskih-kadrov');
    }

    if(isset($requestarr[1])
        &&($requestarr[1] == 'Profilaktika-korrupcii')
        &&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/Gossluzhba/Profilaktika-korrupcii/Normativnye-pravovye-i-inye-akty');
    }

    if(isset($requestarr[0])
        &&($requestarr[0] == 'officially')
        &&((!isset($requestarr[1]))||($requestarr[1] == '')))
    {
        $khabkrai->redirect('/officially/Gosudarstvennye-programmy/Dokumenty-strategicheskogo-planirovaniya');
    }

    if(isset($requestarr[1])
        &&($requestarr[1] == 'Gosudarstvennye-i-municipalnye-uslugi')
        &&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/officially/Gosudarstvennye-i-municipalnye-uslugi/Gosudarstvennye-uslugi');
    }

    // --
    if ((isset($requestarr[0])&&($requestarr[0] == 'governor'))
        &&(isset($requestarr[1])&&($requestarr[1] == 'documents'))
        &&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/governor/documents/Rasporyazheniya');
    }

    if(isset($requestarr[1])&&($requestarr[1] == 'Ekonomika')&&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/officially/Ekonomika/EconomDoc');
    }


    if(isset($requestarr[1])&&($requestarr[1] == 'Gosudarstvennye-programmy')&&((!isset($requestarr[2]))||($requestarr[2] == '')))
    {
        $khabkrai->redirect('/officially/Gosudarstvennye-programmy/Dokumenty-strategicheskogo-planirovaniya');
    }

    if(isset($requestarr[0])&&($requestarr[0] == 'authorities')
        &&((!isset($requestarr[1]))||($requestarr[1] == '')))
    {
        $khabkrai->redirect('/authorities/Pravitelstvo');
    }

    if(isset($requestarr[0])&&($requestarr[0] == 'contacts')&&((!isset($requestarr[1]))||($requestarr[1] == '')))
    {
        $khabkrai->redirect('/contacts/Obratnaya-svyaz');
    }
}