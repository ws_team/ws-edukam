<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

// функция поиска у материала дополнительного поля с адресом сайта для синхронизации
function syncmatoiv_get_oiv_param_url($params)
{
    foreach ($params as $param) {
        if ($param['name'] == 'Сайт ОИВ' || $param['name'] == 'Сайт') {
            if ( ! empty($param['value'])) {
                return $param['value'];
            }
        }
    }

    return false;
}

/**
 * @param iSite $site
 * @param $material
 * @return bool
 */

// синхронизация материалов ОИВов и ОМСУ на другие ОИВы и ОМСУ (их список берется из материалов соответсвующего типа)
function SyncMatOivToOiv(\iSite $site, $material){

    // если у материала нет связанных материалов, то отменяем синхронизацию
    // (связанные материалы должен представлять ОИВ или ОМСУ, с которым надо синхронизировать материал)
    if (empty($material['connected_materials'])) {
        return false;
    }

    /*------------------------------------
	настройки синхронизации
	------------------------------------*/

    // символьные идентификаторы синхронизируемых типов материала
    $syncMaterialTypeCharIds = array(
        'Novosti',
    );

    // символьный идентификатор типа материала, который представляет список сайтов для синхронизации
    $syncSiteMaterialTypeCharId = 'sync-sites';

    /*------------------------------------
	конец настроек синхронизации
	------------------------------------*/

    // получаем символьный идентификатор типа материала
    $matTypeCharIdArray = getMaterialTypeCharIdById($site, $material['type_id']);

    // проверяем, входит ли тип данного материала в список синхронизируемых типов
    if ( ! in_array($matTypeCharIdArray['id_char'], $syncMaterialTypeCharIds)) {
        return false;
    }

    // логирование
    $site->getLogger()->write(LOG_DEBUG, __FUNCTION__,
        "запуск синхронизации материала;site={$site->data->title};id={$material['id']};type_id={$material['type_id']}"
    );

    // получаем id типа материала, который содержит ссылку на сайты для синхронизации
    $syncSiteMaterialTypeIdArray = getMaterialTypeIdByCharId($site, $syncSiteMaterialTypeCharId);

    // ищем в связанных материалах материал со ссылкой для синхронизации и синхронизируем материал
    foreach ($material['connected_materials'] as $cmat) {

        if($cmat['type_id'] != $syncSiteMaterialTypeIdArray[0]['id']){
            continue;
        }

        // получаем дополнительные поля материала
        $params = GetMaterialParams($site, $cmat['id']);

        // среди доп полей ищем поле с адресом сайта
        $url = syncmatoiv_get_oiv_param_url($params);

        if ( ! $url)
            continue;

        $site->logWrite(LOG_DEBUG, __FUNCTION__, 'url='.$url);

        $postvalues = Array();

        // далее формируем параметры запроса на синхронизацию

        // название модуля, который будет обрабатывать отправленные данные
        $postvalues['menu'] = 'matsync';

        // время синхронизации
        $time = time();
        $postvalues['time'] = $time;

        // код для верификации, будет проверяться в модуле, обрабатывающем отправленные данные
        $code = md5('khbSynchronize343Sdb5'.$time);
        $postvalues['code'] = $code;

        // символьный идентификатор типа материала на текущем сайте должен соответствовать симв. идент. на
        // сайтах-получателях данных
        $material['typechar'] = $matTypeCharIdArray['id_char'];

        // путь к директории текущего сайта (для синхронизации файлов)
        $postvalues['fromroot'] = $site->settings->path;

        // сериализуем данные материала
        $data = urlencode(serialize($material));
        $postvalues['data'] = $data;

        // инициализируем curl
        $curl = curl_init();

        $site->getLogger()->write(
            LOG_INFO,
            __FUNCTION__,
            'dst='.$url.
            ';id='.$material['id'].
            ';type_id='.$material['type_id'].
            ';typechar="'.$material['typechar'].'"'
        );

        // если модуль curl не подключен, то выходим
        if ($curl === false) {
            return false;
        }

        // устанавливаем настройки передачи данных
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_CONNECTTIMEOUT => 2,
                CURLOPT_POSTFIELDS => $postvalues,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_MAXREDIRS => 3,
                CURLOPT_POSTREDIR => 7,
            )
        );

        // передаем данные
        $resp = curl_exec($curl);

        //print_r(html_entity_decode($resp));

        // логируем результат передачи
        if ($resp === false) {
            $site->getLogger()->write(LOG_ERR, __FUNCTION__, 'Failed to request to '.$url);
        } else {
            $site->getLogger()->write(LOG_INFO, __FUNCTION__,
                'Request '.$url.' status '.curl_getinfo($curl, CURLINFO_HTTP_CODE).' Body: '.$resp
            );
        }

        // закрываем соединенние
        curl_close($curl);
    }

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'Done');

    return true;
}

// синхронизация материалов с главного сайта (Хабкрая) на ОИВы и ОМСУ
function SyncMatToOIV(\iSite $site, $material)
{
    // логирование
    $site->getLogger()->write(LOG_DEBUG, __FUNCTION__,
        "id={$material['id']};type_id={$material['type_id']}"
    );

    // типы материалов, которые представляют ОИВы и ОМСУ, у всех этих типов есть поле - ссылка на сайт,
    // на который надо отправить материалы
    $oivs = Array(46, 91, 162, 21);

    // типы материалов на Хабкрае, которые надо синхронизировать
    $mattype_dst_typechar = array(
        2 => 'Novosti',
        84 => 'Novosti',
        47 => 'Vakansii',
        83 => 'Vakansii',
        48 => 'Objavlenija',
        284 => 'Objavlenija',
        49 => 'Rezultaty-konkursov',
        285 => 'Rezultaty-konkursov',
    );

    // получаем масив с id синхронизируемых материалов
    $syncharr = array_keys($mattype_dst_typechar);

    // проверяем, относится ли текущий материал к синхронизируемым материалам
    if ( ! in_array($material['type_id'], $syncharr)) {
        return false;
    }

    // если у материала нет связанных материалов, то отменяем синхронизацию
    // (связанные материал должен представлять ОИВ или ОМСУ, с которым надо синхронизировать материал)
    if (empty($material['connected_materials'])) {
        return false;
    }

    // ищем в связанных материалах тип, который относится к материалам-представителям ОМСУ или ОИВ,
    // и синхронизируем материал
    foreach ($material['connected_materials'] as $cmat) {

        if ( ! in_array($cmat['type_id'], $oivs)) {
            continue;
        }

        // получаем дополнительные поля материала
        $params = GetMaterialParams($site, $cmat['id']);

        // среди доп полей ищем поле с адресом сайта
        $url = syncmatoiv_get_oiv_param_url($params);

        if ( ! $url)
            continue;

        $site->logWrite(LOG_DEBUG, __FUNCTION__, 'url='.$url);

        $postvalues = Array();

        // далее формируем параметры запроса на синхронизацию

        // название модуля, который будет обрабатывать отправленные данные
        $postvalues['menu'] = 'matsync';

        // время синхронизации
        $time = time();
        $postvalues['time'] = $time;

        // код для верификации, будет проверяться в модуле, обрабатывающем отправленные данные
        $code = md5('khbSynchronize343Sdb5'.$time);
        $postvalues['code'] = $code;

        // если для текущего типа материала не задан символьный идентификатор в массиве синхронизируемых материалов,
        // то по умолчанию выставляется "Novosti"
        $material['typechar'] = isset($mattype_dst_typechar[$material['type_id']]) ?
            $mattype_dst_typechar[$material['type_id']] :
            'Novosti';

        // сериализуем данные материала
        $data = urlencode(serialize($material));
        $postvalues['data'] = $data;

        // инициализируем curl
        $curl = curl_init();

        $site->getLogger()->write(
            LOG_INFO,
            __FUNCTION__,
            'dst='.$url.
            ';id='.$material['id'].
            ';type_id='.$material['type_id'].
            ';typechar="'.$material['typechar'].'"'
        );

        // если модуль curl не подключен, то выходим
        if ($curl === false) {
            return false;
        }

        // устанавливаем настройки передачи данных
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_CONNECTTIMEOUT => 2,
                CURLOPT_POSTFIELDS => $postvalues,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_MAXREDIRS => 3,
                CURLOPT_POSTREDIR => 7,
            )
        );

        // передаем данные
        $resp = curl_exec($curl);

        // логируем результат передачи
        if ($resp === false) {
            $site->getLogger()->write(LOG_ERR, __FUNCTION__, 'Failed to request to '.$url);
        } else {
            $site->getLogger()->write(LOG_INFO, __FUNCTION__,
                'Request '.$url.' status '.curl_getinfo($curl, CURLINFO_HTTP_CODE).' Body: '.$resp
            );
        }

        // закрываем соединенние
        curl_close($curl);
    }

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'Done');

    return true;
}