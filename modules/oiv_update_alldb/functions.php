<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


/**
 * Расширяет сжатую версию списка ОИВ.
 *
 * @param array $list  Список ОИВ в человекоориентированном формате
 * @return array  Тот же список в машиноориентированном формате
 */
function oivupdatealldb_expand_oivlist($list)
{
    $ret = array();

    $url_template = null;
    $default_params = array();

    if (isset($list['_default'])) {
        $default_params = $list['_default'];
        unset($list['_default']);
    }

    foreach ($list as $key => $params) {
        if (is_numeric($key)) {
            $key = $params;
            $params = array();
        }

        if (is_string($params)) {
            $params = array(
                'url' => $params,
            );
        }

        if ($key[0] == '_') {// "_ <url_tem{pl}ate>"
            if ($key[1] == ' ') {
                $url_template = trim(substr($key, 2));
            }

            continue;
        }

        if (empty($params['url']) && ! empty($params[0])) {
            $params['url'] = $params[0];
        }

        if (empty($params['url'])) {
            if ( ! empty($url_template)) {
                $params['url'] = str_replace('{name}', $key, $url_template);
            }
        }

        if (empty($params['url']) || ! preg_match('/^https?:\/\//i', $params['url']))
            continue;

        $params = array_merge($default_params, $params);
        $ret[$key] = $params;
    }

    return $ret;
}

function oivupdatealldb_get_forwarded_params($src_params, $module_prefix)
{
    $ret = array();

    foreach ($src_params as $key => $val) {
        if (strncmp($key, $module_prefix, strlen($module_prefix)) == 0) {
            $key = substr($key, strlen($module_prefix));
            $ret[$key] = $val;
        }
    }

    return $ret;
}