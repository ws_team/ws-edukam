<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$outarr=array(
    '_ https://{name}.khabkrai.ru',

    'bdd',
    'gchp',
    'gku',
    'forum',
    'komcit' => 'https://cit.khabkrai.ru',
    'komgrz' => 'https://grz.khabkrai.ru',
    'komgzk' => 'https://gz.khabkrai.ru',
    'kominv' => 'https://potreb.khabkrai.ru',
    'kompimk' => 'https://pmk.khabkrai.ru',
    'komsud' => 'https://sud.khabkrai.ru',
    'komtek' => 'https://tek.khabkrai.ru',
    'komtrud' => 'https://trud.khabkrai.ru',
    'minec',
    'minfin',
    'minio' => 'https://mizip.khabkrai.ru',
    'minit' => 'https://mits.khabkrai.ru',
    'minjkh' => 'https://mingkh.khabkrai.ru',
    'minkult',
    'minobr',
    'minpt',
    'minsh',
    'minsport',
    'minstr',
    'minszn',
    'minyust',
    'minzdrav',
    'mpr',
    'ogs',
    'predstav',
    'uprarch' => 'https://arch.khabkrai.ru',
    'uprles' => 'https://les.khabkrai.ru',
    'uprovr' => 'https://ovr.khabkrai.ru',
    'uprvet' => 'https://vet.khabkrai.ru',
    'uprzags' => 'https://komza.khabkrai.ru',
    'nasledie',
    'kmp',
    'kontrol' => 'kontrol.khabkrai.ru',

    '_ https://{name}adm.khabkrai.ru',

    'amursk',
    'ayan',
    'bikin',
    'chumikan',
    'elban',
    'khb' => 'https://khabarovskadm.khabkrai.ru',
    'kms',
    'lazo',
    'nanraion',
    'nikol',
    'ohotsk',
    'osipenko' => 'https://raionosipenkoadm.khabkrai.ru',
    'raionkhb',
    'raionkms',
    'solnechniy',
    'sovgav',
    'udskoe' => 'https://udskoe.khabkrai.ru',
    'ulchi',
    'vanino',
    'vbr',
    'vyazemskiy',
    'svyatogorskoelazo',

    'oiv_mvp' => 'mvp.khabkrai.ru'
);

//получаем список сайтов из админки
$query='SELECT * FROM sites';
if($res=$this->dbquery($query))
{
    foreach ($res as $row)
    {
        $domain=explode(';',$row['domains']);
        $domain=$domain[0];

        $outarr[$row['dbname']]='https://'.$domain;
    }
}

$outarr['_default'] = array(
    'rc_params' => array(
        'username' => 'system.update',
        'password' => 'j8npyl1Ob0qT92cuxeCAlG77YzwsbvGhgOWHpn5hTEbVngLO',
    ),
);

echo '<pre>';
print_r($outarr);
echo '</pre>';

//krumo($outarr);

return $outarr;
