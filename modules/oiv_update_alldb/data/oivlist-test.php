<?php
/**
 * Список тестовых ОИВ-сайтов для распространения изменений.
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

return array(
    '_ https://{name}-test.khabkrai.ru',

    'bdd',
    'forum',
    'minit',
    'ogs',
    'uprzags' => 'https://komza-test.khabkrai.ru',

    '_ https://{name}adm-test.khabkrai.ru',

    'bikin',
    'khb' => 'https://khabarovskadm-test.khabkrai.ru',
    'udskoe' => 'https://udskoe-test.khabkrai.ru',

    '_default' => array(
        'rc_params' => array(
            'username' => 'system.update',
            'password' => 'j8npyl1Ob0qT92cuxeCAlG77YzwsbvGhgOWHpn5hTEbVngLO',
            'mlogin' => '_admin',
            'mpass' => 'wFs_12g3kQ4_5x7',
        ),
    ),
);