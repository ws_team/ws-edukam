<?php

unset($_SESSION['autorize']['autorized']);
unset($_SESSION['autorize']['username']);
unset($_SESSION['autorize']['userlogin']);
unset($_SESSION['autorize']['userid']);
unset($_SESSION['autorize']['userrole']);
unset($_SESSION['autorize']['useractions']);
unset($_SESSION['autorize']['email']);

unset($_SESSION['autorize']);

unset($_SESSION);

    header("Location: /");
    die();