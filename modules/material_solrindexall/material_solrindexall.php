<?php
/**
 * @var iSite $this
 */


if (!$this->getUser()->isAdmin())
    exit;

$this->data->title = 'Индексирование в Solr всей базы -'.$this->data->title;
$this->data->iH1 = 'Индексирование в Solr всей базы';
$this->data->keywords = 'индексирование, solr';
$this->data->description = 'индексирование, solr';
$this->data->breadcrumbsmore = '';
$this->data->errortext = '';

$this->GetPostValues(array('m_page', 'm_count', 'm_type'));

// проверяем, что индексируем
if ($this->values->m_type != 'material' && $this->values->m_type != 'file') {
    echo "<p>Неправильно указаны параметры для запроса</p>";
    return;
}

$m_page = 0;
$m_count = 100;

// определяем страницу
if($this->values->m_page != null && is_numeric($this->values->m_page) && intval($this->values->m_page) >= 0)
{
    $m_page = $this->values->m_page;
}

// определяем количество индексов
if($this->values->m_count != null && is_numeric($this->values->m_page) && intval($this->values->m_count) >= 50)
{
    $m_count = $this->values->m_count;
}

// определяем начальный и конечный индекс
$start_index = $m_page * $m_count;
$end_index = ($m_page + 1) * $m_count;

echo "<p>Индексирование. Начальный индекс - ".$start_index.", конечный индекс - ".$end_index.'</p>';

if ($this->values->m_type == 'material') {

    $wrongtypes='219,59,60,218,161,55,56,57,21,118,149,82,5,20,87,242,221,159,234,238,235,236,239,237,216,217,58,17';

    $add_material_query='SELECT "id" FROM "materials" WHERE "status_id" = 2 AND "type_id" NOT IN ('.$wrongtypes.') AND "id" >= '.$start_index.' AND "id" < '.$end_index;
    $delete_material_query='SELECT "id" FROM "materials" WHERE "status_id" = 2 AND "type_id" IN ('.$wrongtypes.') AND "id" >= '.$m_page * $m_count.' AND "id" < '.$end_index;

    echo "<p>Добавление в индекс материалов</p>";


    if($res=$this->dbquery($add_material_query))
    {
        if(count($res) == 0)
            echo "<p>Материалов на добавление нет</p>";

        foreach($res as $row)
        {
            $id=$row['id'];
            echo '<br>try id - '.$id.' ';

            if ($test = Solr_AddMaterial($this, $id)) {
                echo 'Материал '.$id.' - добавлен ok; ';
            } else {
                echo 'error...';
            }
        }
    }

    echo "<p>Удаление из индекса лишних материалов</p>";

    if($res=$this->dbquery($delete_material_query))
    {

        foreach($res as $row)
        {
            $id=$row['id'];

            echo '<br>try id - '.$id.' ';

            if($test=Solr_DeleteMaterial($this, $id))
            {
                echo 'Материал '.$id.' - удален ok; ';
            }
            else{
                echo 'error...';
            }
        }
    }
}
else if ($this->values->m_type == 'file') {
    $add_file_query='SELECT "id" FROM "files" WHERE "type_id" = $1 AND "id" >= $2 AND "id" < $3';
    $q_add_params = array(FILETYPE_DOCUMENT, $start_index, $end_index);
    $delete_file_query='SELECT "id" FROM "files" WHERE "type_id" <> $1 AND "id" >= $2 AND "id" < $3';
    $q_del_params = array(FILETYPE_DOCUMENT, $start_index, $end_index);

    echo "<p>Добавление файлов в индекс</p>";

    if($res=$this->dbquery($add_file_query, $q_add_params))
    {
        foreach($res as $row)
        {
            $id=$row['id'];
            echo '<br>try id - '.$id.' ';

            if ($test = Solr_AddFile($this, $id))
            {
                echo 'Файл '.$id.' - добавлен в индекс ';
            }
            else{
                echo 'error...';
            }
        }
    }

    echo "<p>Удаление из индекса лишних файлов</p>";

    if($res=$this->dbquery($delete_file_query, $q_del_params))
    {
        foreach($res as $row)
        {
            $id=$row['id'];
            echo '<br>try id - '.$id.' ';

            if ($test=Solr_DeleteFile($this, $id))
            {
                echo 'Файл '.$id.' - удален из индекса; ';
            }
            else{
                echo 'error...';
            }
        }
    }
}

$query='SELECT MAX("id") "cnt" FROM "files"';
if($res=$this->dbquery($query))
{
    echo '<br><br>Максимальный id файлов: '.$res[0]['cnt'];
}

