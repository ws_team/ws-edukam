<?php

$this->data->ajaxhtml = '-1';


//определяем что выводить
switch($this->values->imenu)
{
    //получаем жилые комплексы
    case 'JKShow':

        $materials=Array();

        $this->GetPostValues(Array('city','house','etap','sort','order'));



        //формируем список id жилых комплексов
        if(($this->values->city != '')||($this->values->house != '')||($this->values->etap != ''))
        {

            //echo $this->values->city;

            $ids='';
            $query="SELECT m.id FROM materials m WHERE type_id = 1";

            if($this->values->city != '')
            {
                //получаем список id городов и их подгородов
                $cids=$this->values->city;
                $cids.=GetChildMaterials($this,$cids,false);

            $query.=" AND m.id IN (

            SELECT c.material_id FROM contacts c WHERE c.city IN ($cids)

            )";

            }

            if($this->values->house != '')
            {
                $query.=" AND m.id IN (

                SELECT mc.material_parent FROM material_connections mc WHERE mc.material_child IN (

SELECT e.material_id FROM extra_materials e WHERE e.type_id = 22 AND e.valuemat IN (
SELECT ee.material_id FROM extra_materials ee WHERE ee.type_id = 23 AND ee.valuemat = ".$this->values->house.")

)

UNION ALL

SELECT mc.material_child FROM material_connections mc WHERE mc.material_parent IN (

SELECT e.material_id FROM extra_materials e WHERE e.type_id = 22 AND e.valuemat IN (
SELECT ee.material_id FROM extra_materials ee WHERE ee.type_id = 23 AND ee.valuemat = ".$this->values->house.")

)

                )";
            }

            if($this->values->etap != '')
            {
                $query.=" AND m.id IN (

                SELECT mcc.material_parent FROM material_connections mcc WHERE mcc.material_child IN (

SELECT t1.material_id FROM
(SELECT m1.material_id FROM extra_materials m1 WHERE m1.selector_id = ".$this->values->etap." AND m1.type_id = 5 AND NOW() >= m1.valuedate AND m1.valuedate IS NOT NULL) t1 INNER JOIN (SELECT m2.material_id FROM extra_materials m2 WHERE m2.selector_id = ".$this->values->etap." AND m2.type_id = 6 AND NOW() <= m2.valuedate AND m2.valuedate IS NOT NULL) t2 ON t1.material_id = t2.material_id

)

UNION ALL

SELECT mcc.material_child FROM material_connections mcc WHERE mcc.material_parent IN (

SELECT t1.material_id FROM
(SELECT m1.material_id FROM extra_materials m1 WHERE m1.selector_id = ".$this->values->etap." AND m1.type_id = 5 AND NOW() >= m1.valuedate AND m1.valuedate IS NOT NULL) t1 INNER JOIN (SELECT m2.material_id FROM extra_materials m2 WHERE m2.selector_id = ".$this->values->etap." AND m2.type_id = 6 AND NOW() <= m2.valuedate AND m2.valuedate IS NOT NULL) t2 ON t1.material_id = t2.material_id

)

                )";
            }

            //print($query);

            if($res=$this->dbquery($query))
            {
                if(count($res) > 0)
                {
                    $mids='';
                    foreach($res as $row)
                    {
                        if($mids != '')
                        {
                            $mids.=',';
                        }
                        $mids.=$row['id'];
                    }


                    //MaterialList($site, $type = '', $page, $perpage, $status='', $ids='', $idstype='')
                    $materials=MaterialList($this, 1, 1, 1000, 2, $mids);

                }
            }

        }
        else
        {
            $materials=MaterialList($this, 1, 1, 1000, 2);
        }


        //добавляем в материалы площадь, дату здачи, трансляцию, координаты, тип домов, общий процент завершения
        if(isset($materials[0]) && count($materials[0]) > 0)
        {
            $i=-1;
            foreach($materials as $material)
            {
                ++$i;


                $housetype=0;

                $query="
	SELECT em1.valuemat mid FROM extra_materials em1 WHERE em1.type_id = 23 AND em1.material_id IN
	(
	SELECT em.valuemat FROM extra_materials em WHERE em.type_id = 22 AND em.material_id IN
		(
		SELECT m.id FROM materials m WHERE m.type_id = 2 AND m.id IN
			(
			SELECT mc.material_parent FROM material_connections mc WHERE mc.material_child = ".$material['id']."

			UNION ALL

			SELECT mc.material_child FROM material_connections mc WHERE mc.material_parent = ".$material['id']."
			)
		)
	) LIMIT 0,1
	";

                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $housetype=$res[0]['mid'];
                    }
                }

                $materials[$i]['housetype']=$housetype;


                //город
                $city='';
                $query="SELECT m.name FROM materials m, contacts c
WHERE m.id = c.city AND c.material_id = $material[id]";

                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $city=$res[0]['name'];
                    }
                }

                $materials[$i]['city']=$city;

                //завершенность
                $complited=0;


                //кол-во домов*5 hn = всего процентов - 100%
                //сумма по всем этапам всех домов ac - кол-во сделанного - x%
                //завершенность = round(100*$ac/$hn)

                $hn=0;
                $query="SELECT count(id) hn FROM materials m WHERE m.status_id = 2 AND m.type_id = 2 AND m.id IN (
SELECT mc.material_parent FROM material_connections mc WHERE material_child = $material[id]

UNION ALL

SELECT mc.material_child FROM material_connections mc WHERE material_parent = $material[id])";

                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $hn=$res[0]['hn'];
                    }
                }

                if($hn > 0)
                {
                    $hn=$hn*500;
                    $ac=0;
                    $query="SELECT SUM(em.valuename) ac FROM extra_materials em WHERE type_id = 9 AND em.material_id IN
(
SELECT m.id FROM materials m WHERE m.status_id = 2 AND m.type_id = 2 AND m.id IN (
SELECT mc.material_parent FROM material_connections mc WHERE material_child = $material[id]

UNION ALL

SELECT mc.material_child FROM material_connections mc WHERE material_parent = $material[id])
)";

                    if($res=$this->dbquery($query))
                    {
                        if(count($res) > 0)
                        {
                            $ac=$res[0]['ac'];
                        }
                    }

                    $complited=round(100*$ac/$hn);

                }


                $materials[$i]['complited']=$complited;


                $square=0;

                //площадь
                $query="SELECT m.valuename, c.material_parent hid FROM extra_materials m, extra_materials p, material_connections c
                WHERE m.type_id = 4 AND p.valuemat = m.material_id AND c.material_parent = p.material_id AND c.material_child = ".$material['id']."

                UNION ALL

SELECT m.valuename, c.material_child hid FROM extra_materials m, extra_materials p, material_connections c
                WHERE m.type_id = 4 AND p.valuemat = m.material_id AND c.material_child = p.material_id AND c.material_parent = ".$material['id'];



                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        foreach($res as $row)
                        {
                            $square+=(int)$row['valuename'];
                        }
                    }
                }

                $materials[$i]['square']=$square;


                $enddate='';
                //дата здачи
                $query="SELECT UNIX_TIMESTAMP(max(t.valuedate)) enddate FROM (
SELECT e.valuedate FROM extra_materials e, material_connections c
WHERE e.selector_id IS NOT NULL AND e.type_id IN (6,8) AND e.material_id = c.material_parent AND c.material_child = ".$material['id']."

UNION ALL

SELECT e.valuedate FROM extra_materials e, material_connections c
WHERE e.selector_id IS NOT NULL AND e.type_id IN (6,8) AND e.material_id = c.material_child AND c.material_parent = ".$material['id'].") t";


                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $enddate=$res[0]['enddate'];
                    }
                }

                if($enddate != "")
                {
                    $materials[$i]['enddate']=date_from_bad_date3(date('Y-m-d H:i:s',$enddate));
                    $materials[$i]['unixdate']=(int)$enddate;
                }
                else
                {
                    $materials[$i]['enddate'] = "неизвестно";
                }

                if(!isset($materials[$i]['unixdate']))
                {
                    $materials[$i]['unixdate']=0;
                }

                //флаг трансляции
                $translation=0;

                $query="SELECT f.id FROM files f, file_connections c WHERE f.type_id = 5 AND c.file_id = f.id AND c.material_id = ".$material['id'];

                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $translation=$res[0]['id'];
                    }
                }

                $materials[$i]['translation']=$translation;

                //координаты
                $lat='';
                $long='';

                $query="SELECT latitude, longitude FROM contacts WHERE material_id = ".$material['id'];
                if($res=$this->dbquery($query))
                {
                    if(count($res) > 0)
                    {
                        $lat=$res[0]['latitude'];
                        $long=$res[0]['longitude'];
                    }
                }

                $materials[$i]['lat']=$lat;
                $materials[$i]['long']=$long;


            }
        }

        //сортировка массива
        function cmpASC($a, $b)
        {
            if($a['complited'] == $b['complited'])
            {
                return 0;
            }
            elseif($a['complited'] > $b['complited'])
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        function cmpDESC($a, $b)
        {
            if($a['complited'] == $b['complited'])
            {
                return 0;
            }
            elseif($a['complited'] < $b['complited'])
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        function dmpASC($a, $b)
        {
            if($a['unixdate'] == $b['unixdate'])
            {
                return 0;
            }
            elseif($a['unixdate'] > $b['unixdate'])
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        function dmpDESC($a, $b)
        {
            if($a['unixdate'] == $b['unixdate'])
            {
                return 0;
            }
            elseif($a['unixdate'] < $b['unixdate'])
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }


        if($this->values->sort == '2')
        {
            if($this->values->order == '1')
            {
                usort($materials, "cmpDESC");
            }
            else
            {
                usort($materials, "cmpASC");
            }
        }
        else
        {
            if($this->values->order == '1')
            {
                usort($materials, "dmpDESC");
            }
            else
            {
                usort($materials, "dmpASC");
            }
        }



        $this->data->ajaxhtml=json_encode($materials);



        break;

    //формируем options файлов для привязки к материалу
    case 'giveMeAFilesByMaterial':

        $typewhere='';

        if($this->values->id != '')
        {
            $typewhere=' AND "c"."material_id" = '.$this->values->id;
        }

        $query='SELECT DISTINCT "f"."id", "f"."name", "t"."name" "tname", "f"."ordernum", "f"."type_id"
        FROM "files" "f", "file_types" "t", "file_connections" "c"
        WHERE "f"."type_id" = "t"."id" AND "c"."file_id" = "f"."id" '.$typewhere.' ORDER BY "t"."name" ASC, "f"."ordernum" ASC';
        //echo $query;
        if($res=$this->dbquery($query))
        {
            $this->data->ajaxhtml='';
            if(count($res) > 0)
            {
                $atype='';
                foreach($res as $row)
                {

                    if($atype != $row['tname'])
                    {
                        $atype=$row['tname'];
                        $this->data->ajaxhtml.='<option disabled><b>'.$row['tname'].'</b></option>';

                    }



                    $this->data->ajaxhtml.="<option value='$row[id]'>&nbsp;&nbsp;&nbsp;$row[name]</option>";
                }
            }
        }

        break;

    //формируем options материалов переданного типа материала
    case 'giveMeAMaterialsByType':

        $this->GetPostValues('hidden', 'query');
        $whereCond = array(
            '"status_id" = $1',
        );
        $params = array($this->values->hidden ? STATUS_HIDDEN : STATUS_ACTIVE);
        $params[] = $this->data->language;
        $whereCond[] = '"language_id" = $'.count($params);

        if($this->values->id != '')
        {
            $params[] = $this->values->id;
            $whereCond[] = '"type_id" = $'.count($params);
        }

        if (!empty($this->values->query)) {
            $params[] = '%'.$this->values->query.'%';
            $whereCond[] = '"name" LIKE $'.count($params);
        }

        $whereCond = implode(' AND ', $whereCond);
        $limit = 300;
        $query='SELECT "id", "name" FROM "materials" WHERE '.$whereCond.' ORDER BY "date_event" DESC LIMIT '.$limit;

        if (($res=$this->dbquery($query, $params)) !== false)
        {
            $this->data->ajaxhtml='';
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    $value = $row['id'];
                    $text = $row['name'];
                    /*
                    if (($len = mb_strlen($text)) > 50) {
                        $text = mb_substr($text, 0, 35).'[...]'.mb_substr($text, $len - 15, 15);
                    }
                    */
                    $this->data->ajaxhtml.="<option value=\"$value\">".htmlspecialchars($text).'</option>';
                }
            }
        }
        break;

    //выводим анонс
    case 'ShowAnons':

        $material=MaterialGet($this, $this->values->id);

        print('<!--
        ');
        print_r($material);
        print('
        -->');

        $contact='';
        if(isset($material['contacts'][0]))
        {
            $addr=$material['contacts'][0]['addr'].' '.$material['contacts'][0]['building'];
            $contact="<p style='padding-top:5px;'><img src='/templates/khabkrai/img/whitecont.png' width='14px' height='20px;' align='left' style='padding: 5px;'>$addr</p>";
        }

        $this->data->ajaxhtml="<div style='position: absolute; width: 978px; height: 300px;'>
            <div class='anons_header'>
            <p style='font-size: 36px; line-height: 32px;'>$material[eventtime]</p>
            <p style='font-size: 20px; line-height: 46px;'>$material[eventdatename]</p>
            <hr />
            $contact
            </div>
            <div class='anons_body'>
                <p style='font-size: 24px;'>$material[name]</p>
                <p>&nbsp;</p>
                <p>$material[content]</p>
            </div>
        </div>";

        break;
}



//$time = microtime(true) - $start;
//printf('Скрипт выполнялся %.4F сек.', $time);
?>
