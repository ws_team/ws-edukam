<?php
//переназначаем некоторые настройки сайта=============================================
$this->data->title = '404 запрошенной страницы не существует! - ' . $this->data->title;
//$this->data->keywords='тут, ключевые, слова';
//$this->data->description='тут описание раздела';


//выполняем нужные бизнесс-процессы===================================================

$temptext= ob_get_contents();
//ob_end_clean();

ob_start();
header("HTTP/1.0 404 Not Found");
header("expires: mon, 26 jul 2000 05:00:00 Gmt"); //Дата в прошлом
header("cache-control: no-cache, must-revalidate"); // http/1.1
header("pragma: no-cache"); // http/1.1
header("last-modified: " . gmdate("d, d m y h:i:s") . " Gmt");
header('Content-Type: text/html; charset=utf-8');
$this->headers = ob_get_contents();
ob_end_clean();

ob_start();
echo $temptext;