<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $this->getPostValues(Array('emailFeedback','title','keywords','description','yandexMetrikaIdCounter', 'siteColorTheme'));

    settings_set($this, 'site.colorTheme', $this->values->siteColorTheme);
    $this->data->siteColorTheme = $this->values->siteColorTheme;

    settings_set($this, 'site.title', $this->values->title);
    $this->data->title = $this->values->title;

    settings_set($this, 'site.keywords', $this->values->keywords);
    $this->data->keywords = $this->values->keywords;

    settings_set($this, 'site.description', $this->values->description);
    $this->data->description = $this->values->description;

    settings_set($this, 'yandex_metrika_id_counter', $this->values->yandexMetrikaIdCounter);
    $this->data->yaMetrikaIdCounter = $this->values->yandexMetrikaIdCounter;

    settings_set($this, 'email.feedback', $this->values->emailFeedback);
    $this->data->feedbackEmail = $this->values->emailFeedback;

    $status  = 'success';
    $message = 'Данные сохранены';

}

$this->data->iH1 = 'Общие настройки сайта';