<?php
/**
 * Редактирование пользовательских настроек в админке.
 * Настройки представлены в человекоориентированной форме.
 *
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 * @release 2.14
 */


defined('_WPF_') or die();

$this->getPostValues(array('action'));

$valid_actions = array(
    'general',
);

$action = $this->values->action;

if (empty($this->values->action))
    $this->redirect('/?menu='.$this->values->menu.'&action='.$valid_actions[0]);

if ( ! in_array($action, $valid_actions))
    $this->badRequest();

if ( ! $this->requireAuthUser()->can('usersettings:'.$action)) {
    $this->forbidden();
}

$this->wpf_include('modules/settings/functions.php');

if ( ! function_exists('settings_get')) {
    $status = 'error';
    $message = 'Не установлен/неправильно сконфигурирован модуль "<code>settings</code>". Обратитесь к разработчикам для решения проблемы.';
    $errorInfo = 'код ошибки 1, номер релиза 2.14, модуль "usersettings".';
    return;
}

include(__DIR__.'/action/'.$action.'.php');
