<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

global $message, $status;

$valid_actions = array(
    'list',
    'view',
    'add',
    'edit',
    'del',
    'addextraselect',
    'delextraselect',
    'addextratype',
    'editextratype',
    'delextratype',
    'test',
);

$this->getPostValues(array('action', 'imenu'));
$action = $this->values->action;

if ( ! empty($action) && ! in_array($action, $valid_actions))
    $this->badRequest();

if (0 && $_SERVER['REQUEST_METHOD'] == 'POST' && ! $this->validateCsrfToken())// @todo добавить csrf-tokens во всех формы/ссылки
    $this->badRequest();

if ( ! $this->requireAuthUser()->can('admin.material_types')) {
    $this->forbidden();
}

require_once(__DIR__.'/functions.php');

$this->data->title = 'Типы материалов -'.$this->data->title;
$this->data->iH1 = 'Типы материалов';
$this->data->keywords = 'тут, ключевые, слова';
$this->data->description = 'тут описание раздела';
$this->data->breadcrumbsmore = '';
$this->data->errortext = '';


//die($action);


if ( ! empty($action))
    include(__DIR__.'/action/'.$action.'.php');


switch($this->values->imenu)
{
    case 'add':
        $this->data->iH1 = 'Добавление типа материала';
        break;

    case 'edit':
        $this->data->iH1 = 'Изменение типа материала';
        break;
}

//если есть - получаем данные материала
$this->data->amattype = Array();
$this->data->extratypes = Array();

include(__DIR__.'/action/view.php');
