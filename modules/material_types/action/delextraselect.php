<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->getPostValues(array('esid'));

if (empty($this->values->esid)) {
    $status = 'warning';
    $message = ' - не задан ID типа материала характеристики';
    return;
}

list($success, $message) = MaterialTypeExtraSelectorDelete($this, $this->values->esid);
$status = $success ? 'success' : 'error';

