<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues('iid');

if (empty($this->values->id) || empty($this->values->iid)) {
    $status = 'error';
    $error = 'Ошибка - не передан тип материала или идентификатор характеристики на удаление';
    return;
}

list($success, $message) = MaterialTypeExtraDelete($this, $this->values->iid);
$status = $success ? 'success' : 'error';
