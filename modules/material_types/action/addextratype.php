<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $success, $message;

$this->getPostValues(array(
    'id',
    'name',
    'valuetype',
    'valuemattype',
));

if (empty($this->values->name) || empty($this->values->valuetype)) {
    $success = false;
    $status = 'warning';

    if (empty($this->values->name))
        $message[] = ' - не задано наименование характеристики';
    if (empty($this->values->valuetype))
        $message[] = ' - не указан тип характеристики';

    $message = implode("\n", $message);
    return;
}

list($success, $message) = MaterialTypeExtraAdd(
    $this,
    $this->values->id,
    $this->values->name,
    $this->values->valuetype,
    $this->values->valuemattype
);

$status = $success ? 'success' : 'error';