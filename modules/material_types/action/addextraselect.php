<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->getPostValues(array('id', 'type_id'));

if ( ! empty($this->values->type_id)) {
    list($success, $message) = MaterialTypeExtraSelectorAdd($this, $this->values->id, $this->values->type_id);
    $status = $success ? 'success' : 'error';
} else {
    $status = 'warning';
    $message = ' - не выбран тип материала';
}