<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$err = false;
$errtext = '';

$type_id = $this->values->id;
if (empty($type_id))
    $this->badRequest();

list($success, $errtext) = deleteMaterialType($this, $type_id);

$this->setFlash('status', $success ? 'success' : 'error');
$this->setFlash('message', $errtext);
$this->redirect('/?menu=material_types&imenu=list&action=list');
