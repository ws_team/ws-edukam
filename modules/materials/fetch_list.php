<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


global $materials;

$mquery = null;

if (empty($this->values->sectype)) {
    $this->data->atype = $this->values->maintype;
} else {
    if (empty($this->values->thirdtype)) {
        $this->data->atype = $this->values->sectype;
    } else {
        if ( empty($this->values->fourtype)) {
            $this->data->atype = $this->values->thirdtype;
        } else {
            if ( ! empty($this->values->fivetype)) {
                $this->data->atype = $this->values->fivetype;
            } else {
                $this->data->atype = $this->values->fourtype;
            }
        }
    }
}

if (isset($this->settings->filters)
    && ! empty($this->settings->filters['apply_list_template'])) {
    include('filters.php');
}

if ( ! $nolist) {
    if (empty($this->values->perpage)) {
        $res = $this->dbquery(
            'SELECT perpage FROM material_types WHERE id = $1',
            array($this->data->atype)
        );
        if ( ! empty($res))
            $this->values->perpage = intval($res[0]['perpage']);

        if (empty($this->values->perpage))
            $this->values->perpage = 10;
    }

    $materials = MaterialList($this,
        $this->data->atype,
        $this->values->page,
        $this->values->perpage,
        STATUS_ACTIVE,
        '',
        '',
        $mquery);
}

$keycontent='';

if (isset($materials[0])) {
    foreach($materials as $material) {
        $keycontent.=' '.$material['name'].' '.$material['anons'];
    }
}
