<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$this->GetPostValues(
    array(
        'tag',
        'date',
        'single_calendar_month',
        'single_calendar_year',
        'single_calendar_day',
        'list_date_filter_datestart',
        'list_date_filter_dateend',

    ),
    1
);
$filterCond = array();

if (!empty($this->values->tag)) {
    $tagCond = array();
    foreach (array_map('trim', explode(',', $this->values->tag)) as $tag) {
        $tag = mb_strtolower($tag, 'utf-8');
        $tagCond[] = 'lower("extra_text1") LIKE \'%'.pg_escape_string($tag).'%\'';
    }
    $filterCond[] = implode(' AND ', $tagCond);
}

$dateParams = array(
    'list_date_filter_datestart',
    'list_date_filter_dateend',
    'single_calendar_month',
    'single_calendar_year',
    'single_calendar_day'
);
foreach ($dateParams as $param) {
    if (!empty($this->values->$param)) {
        $timeBiasRevert = '';
        if (defined('DB_TIME_BIAS_HOURS') && ! empty(constant('DB_TIME_BIAS_HOURS'))) {
            $tsSign = DB_TIME_BIAS_HOURS < 0 ? '-' : '+';
            $tsVal = abs(DB_TIME_BIAS_HOURS);
            $timeBiasRevert = ' '.$tsSign.' INTERVAL \''.$tsVal.' HOUR\'';
        }
        break;
    }
}

if (!empty($this->values->list_date_filter_datestart) || !empty($this->values->list_date_filter_dateend)) {
    if (!empty($this->values->list_date_filter_datestart)) {
        $filterCond[] = 'DATE(date_event'.$timeBiasRevert.') >= \''.pg_escape_string(
                implode('-', array_reverse(explode('.', $this->values->list_date_filter_datestart)))).'\'';
    }
    if (!empty($this->values->list_date_filter_dateend)) {
        $filterCond[] = 'DATE(date_event'.$timeBiasRevert.') <= \''.pg_escape_string(
                implode('-', array_reverse(explode('.', $this->values->list_date_filter_dateend)))).'\'';
    }
    foreach (array('date', 'single_claendar_month', 'single_calendar_year', 'single_calendar_day') as $arg)
        $this->values->$arg = '';
} elseif (!empty($this->values->single_calendar_year)
    || !empty($this->values->single_calendar_month)
    || !empty($this->values->single_calendar_day)) {
    $now = explode('.', date('d.m.Y'));
    $singleDate = array(
        !empty($this->values->single_calendar_day) ? sprintf('%02d', intval($this->values->single_calendar_day)) :
            $now[0],
        !empty($this->values->single_calendar_month) ? sprintf('%02d', intval($this->values->single_calendar_month)) :
            $now[1],
        !empty($this->values->single_calendar_year) ? $this->values->single_calendar_year : $now[2],
    );
    $this->values->date = implode('.', $singleDate);
    $filterCond[] = 'DATE(date_event'.$timeBiasRevert.') = \''.pg_escape_string(
        implode('-', array_reverse($singleDate))).'\'';
}

if (!empty($filterCond)) {
    $mquery = 'SELECT "id" FROM materials'.
        ' WHERE type_id='.$this->data->atype.
        ' AND status_id='.STATUS_ACTIVE.
        ' AND '.implode(' AND ', $filterCond);
}
