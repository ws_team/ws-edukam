<?php


global $material;
global $materials;

$starttime = microtime(true);


$this->data->breadcrumbs='<a href="/">Главная</a>';

$this->data->keywords='';
//сохраняем файл
function savefile($site,$mid)
{
    //пробуем добавить файл (если он передан)
    //пробуем сохранить файл
    $file = new iImgFile($site);
    $file->Save('imagefile', null, null, null, 3, $mid);

    $file->Resize(true, 'jpg', 800);

    if($small=$file->Resize(false, false, 200))
    {
        unset($file);
        //$files = new iImgFile($this, $small);
        $file = new iImgFile($site, $small);
    }

    unset($file);
}

$this->GetPostValues(array('page', 'perpage'));
$this->values->page = intval($this->values->page);
if ($this->values->page < 1)
    $this->values->page = 1;

$this->values->perpage = intval($this->values->perpage);

$materials = Array();

$nolist = false;

$nolistarray = Array(1);

$typescnt = count($this->data->matURLArr);

if ($typescnt > 0) {
    $lasttype = $this->data->matURLArr[($typescnt-1)]['id'];
} else {
    $lasttype = '-1';
}

if(in_array($lasttype,$nolistarray))
{
    $nolist=true;
}

//определение активного типа материала
if (empty($this->values->id)) {
    include('fetch_list.php');
} else {//выводим материал
    include('fetch_single.php');
}