<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ($this->values->id != '') {
    $subquery = '';

    $this->GetPostValues(Array('email','fio','role_id','pass','status'));

    $q_params = array();

    if ($this->values->email != '')
    {
        //$q_params[] = $this->values->email;
        $subquery.=' email = '."'".$this->values->email."'";
    }

    if ($this->values->fio != '') {
        if($subquery != '')
        {
            $subquery.=',';
        }
        //$q_params[] = $this->values->fio;
        $subquery .= ' fio = '."'".$this->values->fio."'";

        $subquery.=',';
        //$q_params[] = $this->values->firstname;
        //$subquery .= ' "firstname" = $'.count($q_params);
        $subquery .= ' firstname = '."'".$this->values->firstname."'";
        $subquery .= ',';
        //$q_params[] = $this->values->middlename;
        //$subquery.=' "middlename" = $'.count($q_params);
        $subquery .= ' middlename = '."'".$this->values->middlename."'";
        $subquery.=',';
        //$q_params[] = $this->values->unit;
        //$subquery.=' "unit" = $'.count($q_params);
        $subquery .= ' unit = '."'".$this->values->unit."'";
        $subquery.=',';
        //$q_params[] = $this->values->post;
        //$subquery.=' "post" = $'.count($q_params);
        $subquery .= ' post = '."'".$this->values->post."'";
    }

    if($this->values->role_id != '')
    {
        if($subquery != '')
        {
            $subquery.=',';
        }
        //$q_params[] = $this->values->role_id;
        //$subquery.=' "role_id" = $'.count($q_params);
        $subquery .= ' role_id = '.$this->values->role_id;
    }

    if ($this->values->pass != '')
    {
        if (checkpass($this->values->pass))
        {
            if ($subquery != '')
            {
                $subquery.=',';
            }

            $passdays = time() + PASS_EXPIRE_TIME;

            //$this->values->pass = md5($this->values->pass);

            $pass=$this->values->pass.'f6hal_'.$this->values->email.'fGh73c'.$this->values->pass;
            $pass=hash('sha256', $pass);

            //$q_params[] = $pass;
            //$subquery.=' "pass" = $'.count($q_params);
            $subquery .= ' pass = '."'".$pass."'";
            $subquery.=',';
            //$q_params[] = $passdays;
            $subquery.=' passtime = '.$passdays;
        }
        else
        {
            $this->data->errortext.='Новый пароль не соответствует требованиям безопасности (длинна не менее 6 символов, наличае и цифр и букв)';
        }
    }

    if ($this->values->status !== '') {
        if ($subquery != '') {
            $subquery.=',';
        }
        //$q_params[] = intval($this->values->status);
        //$subquery.=' "status" = $'.count($q_params);
        $subquery.=' status = '.intval($this->values->status);
    }

    //$omsu = !empty($this->values->omsu) ? $this->values->omsu : null;
    if($subquery != '')
    {
        $subquery.=',';
    }
    //$q_params[] = $omsu;
    //$subquery .= ' "omsu" = $'.count($q_params);
    if($this->values->omsu == '')
    {
        $omsu = 'NULL';
}
    else
    {
        $omsu = intval($this->values->omsu);
    }
    $subquery.=' omsu = '.$omsu;

    //$q_params[] = $this->values->id;

    $query = 'UPDATE users SET '.$subquery.' WHERE id = '.$this->values->id;

    if ($res = $this->dbquery($query))
    {
        $this->data->errortext.='<p style="color: green;">Пользователь изменен!</p>';
    }
    else
    {
        $this->data->errortext.='<p style="color: #ff0000;">Ошибка изменения пользователя (не удалось записать изменения в БД). Сообщите в службу поддержки сайт, email пользователя и какие поля пробовали менять.</p>';
    }
}