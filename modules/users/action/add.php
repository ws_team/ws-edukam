<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if(($this->values->email != '')&&($this->values->pass != '')) {
    $err = validate_user_email($this, $this->values->email);
    if ($err !== true) {
        $this->data->errortext = '<p style="color:#F00;">'.strval($err).'</p>';
        return;
    }

    //проверка пароля на соответствие требованиям безопасности
    if (checkpass($this->values->pass)) {
        $mid = '';

        $query = 'SELECT MAX("id")+1 "mid" FROM "users"';
        if ($res = $this->dbquery($query)) {
            $mid = $res[0]['mid'];
        }

        if($mid == '')
            $mid = 1;

        $email = $this->values->email;
        $fio = $this->values->fio;

        $post = $this->values->post;
        $firstname = $this->values->firstname;
        $middlename = $this->values->middlename;
        $unit = $this->values->unit;

        $role_id = $this->values->role_id;

        if($role_id == '')
        {
            $role_id=2;
        }

        $content = '';
        $pass = md5($this->values->pass);

        $pass=$this->values->pass.'f6hal_'.$email.'fGh73c'.$this->values->pass;
        $pass=hash('sha256', $pass);

        $status = $this->values->status;

        if($status == '')
        {
            $status=1;
        }

        $passdays = time() + PASS_EXPIRE_TIME;

        $query = 'INSERT INTO "users"(
                "id",
                "email",
                "fio",
                "role_id",
                
                "content",
                "pass",
                "status",
                "firstname",
                
                "middlename",
                "unit",
                "post",
                "passtime"
            )
            VALUES (
                $1,
                $2,
                $3,
                $4,
                
                $5,
                $6,
                $7,
                $8,
                
                $9,
                $10,
                $11,
                $12
            )';
        
        $q_params = array(
            $mid,
            $email,
            $fio,
            $role_id,

            $content,
            $pass,
            $status,
            $firstname,

            $middlename,
            $unit,
            $post,
            $passdays,
        );

        if($res=$this->dbquery($query, $q_params))
        {
            $this->data->errortext='Пользователь добавлен!';
        }
        else
        {
            $this->data->errortext='Ошибка записи в БД!';
            //krumo($q_params);
        }
    }
    else
    {
        $this->data->errortext='Слишком короткий пароль! Длинна пароля не должна быть меньше 6-ти символов.';
    }
}
else
{
    $this->data->errortext='Не заполнены email или пароль!';
}