<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

global $users;

//добявляем переменные
$this->data->errortext='';

$this->GetPostValues(Array(
    'fio',
    'firstname',
    'middlename',
    'unit',
    'post',
    'pass',
    'email',
    'role_id',
    'status',
    'omsu',
));

function validate_user_email($site, $email)
{
    $email_rx = '/[a-z][\w._-]+@([a-z][a-z0-9-]+\.)+[a-z]+/i';
    if (!preg_match($email_rx, $email))
        return 'Некорректный email';

    $q = 'SELECT COUNT(*) num FROM users WHERE email=$1';
    $res = $site->dbquery($q, array($email));

    if (empty($res))
        return 'Внутренняя ошибка';

    if (!empty($res[0]['num']))
        return 'Такой email уже зарегистрирован';

    return true;
}

//проверяем версию таблицы пользователей
$query = 'SELECT MAX(firstname) FROM users';
if (!$res=$this->dbquery($query)) {
    //обновляем таблицу
    $query='ALTER TABLE users ADD firstname VARCHAR(127)';
    $res=$this->dbquery($query);

    $query='ALTER TABLE users ADD middlename VARCHAR(127)';
    $res=$this->dbquery($query);

    $query='ALTER TABLE users ADD unit VARCHAR(127)';
    $res=$this->dbquery($query);

    $query='ALTER TABLE users ADD post VARCHAR(127)';
    $res=$this->dbquery($query);

    $query='ALTER TABLE users ADD tryes numeric(1,0)';
    $res=$this->dbquery($query);

    $query='ALTER TABLE users ADD bloctime numeric(11,0)';
    $res=$this->dbquery($query);

    $query='ALTER TABLE users ADD passtime numeric(11,0)';
    $res=$this->dbquery($query);

}

if (!empty($this->values->action)) {
    $controller_script = dirname(__FILE__).'/action/'.
        str_replace(array('.', '/', '~', '*', '?'), '', $this->values->action).'.php';
    if (file_exists($controller_script)) {
        include($controller_script);
    } else {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        exit;
    }
}

//выполняем действия

function printUserRoleOptions($site, $id)
{
    $query='SELECT * FROM "user_roles" ORDER BY "id" ASC';
    if (($res=$site->dbquery($query)) !== false)
        return html_print_select_options($res, $id);

    return '';
}

function printUserStatusOptions($site, $id)
{
    $statuses = array();

    $statuses[0]['id']=0;
    $statuses[0]['name']='Выкл';
    $statuses[1]['id']=1;
    $statuses[1]['name']='Вкл';

    return html_print_select_options($statuses, $id);
}

function html_print_select_options($items, $selectedId = null)
{
    $options = array();
    foreach ($items as $row) {
        if ($row['id'] == $selectedId) {
            $selected = 'SELECTED';
        } else {
            $selected = '';
        }

        $options[] = "<option value=\"{$row['id']}\" $selected>{$row['name']}</option>";
    }

    return implode("\n", $options);
}

function users_get_list(\iSite $site)
{
    $users = array();
    //получаем список пользователей
    $query = <<<EOS
SELECT
        "u".*,
        "r"."name" "rolename"
    FROM
        "users" "u"
        LEFT JOIN "user_roles" "r" ON "u"."role_id" = "r"."id"
    ORDER BY
        "u"."status" DESC,
        "u"."email",
        "r"."name" ASC
EOS;

    $res = $site->dbquery($query);
    if ( ! empty($res)) {
        foreach($res as $row) {
            $mattypes = array();
            $uid = $row['id'];

            //получаем разрешенные разделы
            $queryr = 'SELECT
                    "m"."id",
                    "m"."name"
                FROM
                    "user_rights" "r"
                    INNER JOIN "material_types" "m" ON "m"."id" = "r"."type_id"
                WHERE
                    "r"."goal_id" = $1';

            $resr = $site->dbquery($queryr, array($uid));
            if ( ! empty($resr)) {
                $mattypes = $resr;
            }

            $row['materials'] = $mattypes;
            $users[] = $row;
        }
    }

    return $users;
}

$users = users_get_list($this);

$this->data->iH1 = 'Администрирование пользователей';

