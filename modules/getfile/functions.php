<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


define('MATTYPE_OPENSET', 234);
define('MATTYPE_OPENVERSION', 236);
define('MATTYPE_OPENSTRUCTURE', 237);

define('OPENDATA_FILE_FRESHNESS_LIFETIME', 3600 * 24 * 30);

function file_force_download(\iSite $site, $file, $filename) {
    if (file_exists($file)) {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        // если этого не сделать файл будет читаться в память полностью!
        if (ob_get_level()) {
            ob_end_clean();
        }

        // заставляем браузер показать окно сохранения файла
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'. $filename.'";');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        // читаем файл и отправляем его пользователю
        readfile($file);
        $site->endRequest();
    }

    $site->notFound();
}

function getfile_download_group(\iSite $site, $files)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'files='.$files);
    if ($filesarr = explode(',', $files)){
        $realkey = MakeFileGroupKey($site->values->files);

        if ($realkey == $site->values->key) {
            $file = $site->settings->path.'/photos/'.$realkey.'.zip';

            unlink($file);

            //проверка на существование архива переданных файлов
            if ( ! file_exists($file)) {
                //создаём файл
                if($zip = new ZipArchive()) {
                    if ($zip->open($file, ZIPARCHIVE::CREATE) !== true) {
                        fwrite(STDERR, "Error while creating archive file");
                        exit(1);
                    }

                    $i=-1;

                    foreach ($filesarr as $doc) {
                        ++$i;
                        if (file_exists($site->settings->path.'/photos/'.$doc)) {

                            $query = 'SELECT "name", "extension" FROM "files" WHERE "id" = '.$doc;
                            if ($res = $site->dbquery($query)) {
                                if(count($res) > 0)
                                {
                                    $res[0]['name']=str_replace(' ','_',$res[0]['name']);
                                    $res[0]['name']=htmlspecialchars($res[0]['name']);
                                    $res[0]['name']=strip_tags($res[0]['name']);

                                    if(mb_strlen($res[0]['name'],UTF8) > 20)
                                    {
                                        $res[0]['name']=mb_substr($res[0]['name'],0,20).'_'.$i;
                                    }

                                    $filename=$doc.'_'.$res[0]['name'].'.'.$res[0]['extension'];

                                    $zip->addFile($site->settings->path.'/photos/'.$doc, mb_transliterate($filename));
                                }
                            }
                        }
                    }

                    $zip->close();
                } else {
                    return false;
                }
            }

            //выдача архива
            $temptext = ob_get_contents();

            $filename = str_replace(',','_',$site->values->files).'_arc.zip';

            file_force_download($site, $file, $filename);

            ob_start();
            $site->headers = ob_get_contents();
            ob_end_clean();

            ob_start();
            echo $temptext;
            return true;
        }
    }
}

function getfile_download(\iSite $site, $file_id)
{
    //переназначаем некоторые настройки сайта=============================================
    //$site->data->title = '404 запрошенной страницы не существует! - ' . $site->data->title;
    //$site->data->keywords='тут, ключевые, слова';
    //$site->data->description='тут описание раздела';
    //выполняем нужные бизнесс-процессы===================================================

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'file_id='.$file_id);

    $temptext = ob_get_contents();
    //ob_end_clean();

    $filename = '';

    $file_local_path = $site->settings->path.'/photos/'.$file_id;

    $file_id_sv=$file_id;
    $file_id=(int)$file_id;

    //получаем файл из базы
    $query = 'SELECT * FROM "files" WHERE "id" = $1';
    $res = $site->dbquery($query, array($file_id));

    if ( ! empty($res)){
        $filename = $res[0]['name'];
        //$filename = str_replace(array('"', '\''), "", $filename);

        $filename = str_replace(' ','_',$filename);
        $filename = htmlspecialchars($filename);
        $filename = strip_tags($filename);

        $filename = mb_transliterate($filename);

        if (mb_strlen($filename, 'UTF-8') > 50) {
            $filename = mb_substr($filename,0,50).'_1';
        }



        if($file_id_sv !== $file_id)
        {
            $fileext='doc';
        }
        else
        {
            $fileext = $res[0]['extension'];
        }

        if ( ! empty($fileext)) {
            $filename .= '.'.$fileext;
        }

        if ( ! file_exists($file_local_path)) {
            getfile_preload_opendata($site, $res[0], $file_local_path);
        }
    }

    if ( ! empty($filename)) {
        if ( ! isset($site->values->tryupdatedownloads))
        {
            $site->values->tryupdatedownloads = '';
        }

        //обновляем кол-во скачиваний файлов ОД
        if( ! empty($site->values->tryupdatedownloads)) {
            //получаем материал версии открытых данных
            $query = <<<EOS
SELECT "id"
FROM "materials"
WHERE
    "type_id" = $1
    AND "id" IN (
        SELECT "material_id" FROM "file_connections" WHERE "file_id" = $2
    )
    AND status_id = $3
EOS;
            $q_params = array(236, $file_id, STATUS_ACTIVE);
            $res = $site->dbquery($query, $q_params);

            if ( ! empty($res)) {
                $version_id = $res[0]['id'];

                //получаем id открытых данных

                $versionmat = MaterialGet($site, $version_id);

                if (isset($versionmat['params'][49]['value'])) {
                    $nabormat = MaterialGet($site, $versionmat['params'][49]['value']);

                    if (isset($nabormat['params'][46]['value'])) {
                        $downloads = $nabormat['params'][46]['value'] + 1;

                        //обновляем кол-во скачек
                        $query = <<<EOS
UPDATE "extra_materials"
SET "valuename" = $1
WHERE "type_id" = $2 AND "material_id" = $3
EOS;
                        $q_params = array($downloads, 46, $nabormat['id']);
                        $res = $site->dbquery($query, $q_params);

                        if ( ! empty($res))
                        {
                            //слыыыыыышь! все записалось!
                        }
                    }
                }
            }
        }

        getfile_output($site, $file_local_path, $filename);
    }

    ob_start();
    $site->headers = ob_get_contents();
    ob_end_clean();

    ob_start();
    echo $temptext;

    return true;
}

/**
 * @param \iSite $site
 * @param string $file_path
 */
function getfile_direct_output(\iSite $site, $file_path)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'file_path="'.$file_path.'"');

    if (file_exists($file_path)) {

        if (ob_get_level())
            ob_end_clean();

        $fi = finfo_open(FILEINFO_MIME_TYPE);
        $file_type = finfo_file($fi, $file_path);
        if ($file_type === false)
            $file_type = 'application/octet-stream';

        $ostream = fopen($file_path, 'rb');

        $site->setResponseHeader('content-type', $file_type);
        $site->sendResponse($ostream);
        $site->endRequest();
    }

    $site->notFound();
}

function getfile_opendata(\iSite $site)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, '_');

    $uri = getenv('REQUEST_URI');
    $uri_segs = explode('/', $uri);

    if (strpos($uri, 'opendata') !== false) {
        $file_path = substr($uri, strpos($uri, '/', 1) + 1);
    } else {
        $file_path = array();

        if (isset($site->values->set)) {
            $set = preg_replace('/[^\w-]+/', '-', $site->values->set);
            $set = preg_replace('/-{2,}/', '-', $set);
            $set = trim($set, '-');
            if ( ! empty($set))
                $file_path[] = $set;
        }

        if (isset($site->values->data)) {
            $data = preg_replace('/[^\w-]+/', '-', $site->values->data);
            $data = preg_replace('/-{2,}/', '-', $data);
            $data = trim($data, '-');
            if ( ! empty($data))
                $file_path[] = 'data-'.$data;
        }

        if (isset($site->values->struct)) {
            $struct = preg_replace('/[^\w-]+/', '-', $site->values->struct);
            $struct = preg_replace('/-{2,}/', '-', $struct);
            $struct = trim($struct, '-');
            if ( ! empty($struct))
                $file_path[] = 'structure-'.$struct;
        }

        $last_uri_seg = $uri_segs[count($uri_segs) - 1];
        if (substr($last_uri_seg, -4) == '.csv'
            && in_array(substr($last_uri_seg, 0, -4), array('list', 'meta', 'opendatalist'))) {
            $file_path[] = $last_uri_seg;
        }

        $file_path = implode('/', $file_path);
        $file_path .= '.csv';
    }

    $file_name = basename($file_path);
    $file_path = $site->settings->path.'/opendatafiles/'.$file_path;

    //echo $file_path;
    //die();

    $site->logWrite(LOG_DEBUG, __FUNCTION__, 'file_path="'.$file_path.'";'.
        'file_name="'.$file_name.'"');

    if ( ! file_exists($file_path) || time() - filemtime($file_path) > OPENDATA_FILE_FRESHNESS_LIFETIME) {
        $local_dir = dirname($file_path);
        if ( ! is_dir($local_dir)) {
            if (!mkdir($local_dir, 0755, true)) {
                $site->logWrite(LOG_ERR, __FUNCTION__, 'failed to mkdir('.$local_dir.')');
                $site->internalServerError();
            }
        }

        $uri_orig = 'https://open27.ru'.$uri;

        //echo $uri_orig;
        //die();

        $fd_orig = fopen($uri_orig, 'rb');
        if ($fd_orig === false) {
            $site->logWrite(LOG_ERR, __FUNCTION__, 'fopen "'.$uri_orig.'" failed');
        }
        if ($fd_orig !== false) {
            $fd_local = fopen($file_path, 'wb');
            if ($fd_local === false) {
                $site->logWrite(LOG_ERR, __FUNCTION__, 'fopen '.$file_path.' failed');
            }
            if ($fd_local !== false) {
                stream_copy_to_stream($fd_orig, $fd_local);
                fclose($fd_local);
                $site->logWrite(LOG_DEBUG, __FUNCTION__, 'copied '.$uri_orig.' -> '.$file_path);
            }
            fclose($fd_orig);
        }
    }

    getfile_output($site, $file_path, $file_name);
}

function getfile_preload_opendata(\iSite $site, $file_rec, $local_path)
{
    $site->logWrite(LOG_DEBUG, __FUNCTION__, "'$file_rec', '$local_path'");

    $query = <<<EOS
SELECT
    m.id_char
FROM file_connections fc
    INNER JOIN materials m ON m.id = fc.material_id
WHERE
    m.type_id IN($1)
    AND fc.file_id = $2
EOS;

    $q_params = array(
        array(MATTYPE_OPENSET, MATTYPE_OPENVERSION, MATTYPE_OPENSTRUCTURE),
        $file_rec['id']
    );

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $res = $site->dbquery($query, $q_params);

    if (empty($res)) {
        $site->logWrite(LOG_WARNING, __FUNCTION__, '..owner record not found');
    }

    if ( ! empty($res)) {
        $mat = $res[0];

        $openset_id = $mat['id_char'];

        $base_url = 'https://open27.ru/opendata/'.$openset_id.'/';
        $url = $base_url.$res['name'].'.'.$res['extension'];

        $fin = fopen($url, 'rb');
        if ($fin === false) {
            $site->logWrite(LOG_ERR, __FUNCTION__, '..failed to connect to remote url');
        } else {
            $fout = fopen($local_path, 'wb');

            if ($fout === false) {
                $site->logWrite(LOG_ERR, __FUNCTION__, '..failed to open local file');
            } else {
                stream_copy_to_stream($fin, $fout);
                fclose($fout);
            }
            fclose($fin);
        }
    }
}

function getfile_output($site, $file_local_path, $filename = null)
{
    if ( ! empty($site->values->view)) {
        getfile_direct_output($site, $file_local_path);
    } else {
        file_force_download($site, $file_local_path, $filename);
    }
}
