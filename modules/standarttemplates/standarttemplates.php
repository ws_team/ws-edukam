<?php


//добьявляем переменные
$this->data->errortext='';

//получаем переменные
$this->GetPostValues(Array('id','action','name','file_name','content','materiallist','template_id'));


//действия
switch($this->values->action){
    //удаление шаблона
    case 'delete':
        if($this->values->template_id != '')
        {
            $query='DELETE FROM standart_shablons WHERE id = '.$this->values->template_id;
            if($res=$this->dbquery($query))
            {
                $this->data->errortext='Шаблон удален!';
            }
            else{
                $this->data->errortext='Не удалось удалить шаблон';
            }
        }
        break;
    //изменение шаблона
    case 'edit':
        if(($this->values->name != '')&&($this->values->file_name != '')&&($this->values->materiallist != '')&&($this->values->template_id != ''))
        {

            $this->values->materiallist=(int)$this->values->materiallist;

            //$query='INSERT INTO "standart_shablons" ("id","author","name","file_name","content","materiallist")
            //VALUES('.$sid.', '."'".$this->autorize->userlogin."'".', '."'".$this->values->name."'".', '."'".$this->values->file_name."'".', '."'".$this->values->content."'".', '.$this->values->materiallist.')';

            $query='UPDATE "standart_shablons"
            SET "name" = '."'".$this->values->name."'".',
            "file_name" = '."'".$this->values->file_name."'".',
            "content" = '."'".$this->values->content."'".',
            "materiallist" = '."'".$this->values->materiallist."'".'
            WHERE "id" = '.$this->values->template_id;

            if($res=$this->dbquery($query))
            {
                $this->data->errortext='Шаблон изменен!';
            }
            else{
                $this->data->errortext='Не удалось сохранить шаблон';
            }

        }else{
            $this->data->errortext='Введены не все обязательные поля';
        }
        break;

    //добавление шаблона
    case 'add':
        if(($this->values->name != '')&&($this->values->file_name != '')&&($this->values->materiallist != ''))
        {

            $this->values->materiallist=(int)$this->values->materiallist;

            //получаем новый id записи
            $query='SELECT MAX("id")+1 "sid" FROM "standart_shablons"';
            $sid='';
            if($res=$this->dbquery($query))
            {
                if(count($res) > 0)
                {
                    $sid=$res[0]['sid'];
                }
            }

            if($sid == '')
            {
                $sid=1;
            }

            //добавляем шаблон
            $query='INSERT INTO "standart_shablons" ("id","author","name","file_name","content","materiallist")
            VALUES('.$sid.', '."'".$this->autorize->userlogin."'".', '."'".$this->values->name."'".', '."'".$this->values->file_name."'".', '."'".$this->values->content."'".', '.$this->values->materiallist.')';

            if($res=$this->dbquery($query))
            {
                $this->data->errortext='Шаблон добавлен!';
            }
            else{
                $this->data->errortext='Не удалось сохранить шаблон';
            }

        }
        else{
            $this->data->errortext='Введены не все обязательные поля';
        }
        break;
}

$this->data->iH1='Стандартные шаблоны';