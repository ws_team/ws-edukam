<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\modules\oiv_update_db;

defined('_WPF_') or die();

$this->getPostValues(array('continue', 'updatedb', 'updatedb_from'));

if (basename(dirname($_SERVER['DOCUMENT_ROOT'])) == 'oiv') {
    if (strncmp(basename($_SERVER['DOCUMENT_ROOT']), 'omsu_', 5) == 0)
        $sitetype = 'omsu';
    else
        $sitetype = 'oiv';
} else {
    $sitetype = '';
}

$continue = explode(':', $this->values->continue);

$output = '';

if ( ! empty($this->values->updatedb)) {
    ob_start();

    $this->wpf_include('modules/updatedb/functions.php');
    if (function_exists('wpf\modules\updatedb\execute')) {
        $from = intval($this->values->updatedb_from);
        \wpf\modules\updatedb\execute($this, $from);
    } else {
        printf("Задан параметр 'updatedb', но не удалось подключить модуль 'updatedb'\n");
    }
    $output = ob_get_clean();
}

$siteid = preg_replace('/_test\d*$/', '', $this->settings->DB->name);
oiv_update_db($this, $sitetype, $siteid, $continue);

if (strlen($output))
    $this->html .= "\n".$output;