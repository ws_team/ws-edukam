<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

namespace wpf\modules\oiv_update_db;

require_once(WPF_CLASSROOT.'/migration/Migration.php');

function oiv_update_db(\iSite $site, $sitetype, $siteid, $continue = null)
{
    $migration = new \wpf\migration\Migration($site);
    $migration->begin();

    oivupdate_create_templates($migration, $sitetype, $siteid, $continue);
    oivupdate_update_mattypes($migration, $sitetype, $siteid, $continue);
    oivupdate_update_materials($migration, $sitetype, $siteid, $continue);

    $migration->end();
}

function oivupdate_create_templates(\wpf\migration\Migration $migration, $sitetype, $siteid)
{
    $templatesarr = oivupdate_collect_data($sitetype, $siteid, 'templates');

    foreach($templatesarr as $ts)
        $migration->createTemplate($ts);
}

function oivupdate_collect_data($sitetype, $siteid, $datatype)
{
    $ret = array();

    $sources = array(
        'any',
    );

    if ( ! empty($sitetype)) {
        if ($sitetype == 'oiv' || $sitetype == 'omsu') {
            $sources[] = 'anyoiv';
        }
        $sources[] = $sitetype;
    }

    $sources[] = $siteid;

    $sources = array_unique($sources);

    foreach ($sources as $src) {
        $source_path = __DIR__."/data/$src/$datatype.php";
        if (file_exists($source_path)) {
            $ret = array_merge($ret, (array)include($source_path));
        }
    }

    return $ret;
}

function oivupdate_update_mattypes(\wpf\migration\Migration $migration, $sitetype, $siteid)
{
    $mattypesarr = oivupdate_collect_data($sitetype, $siteid, 'material_types');

    foreach($mattypesarr as $newtype)
        $migration->createMaterialType($newtype);
    foreach($mattypesarr as $newtype)
        $migration->addMaterialTypeParams($newtype);

    $updatetypesarr = oivupdate_collect_data($sitetype, $siteid, 'material_types_upd');
    foreach($updatetypesarr as $newtype)
        $migration->alterMaterialType($newtype);

    $hidemattypes = oivupdate_collect_data($sitetype, $siteid, 'material_types_hide');
    foreach($hidemattypes as $hidemattype)
        $migration->deleteMaterialType($hidemattype);
}

function oivupdate_update_materials(\wpf\migration\Migration $migration, $sitetype, $siteid)
{
    $matsarr = oivupdate_collect_data($sitetype, $siteid, 'materials');
    foreach($matsarr as $mat)
        $migration->addMaterial($mat);
}
