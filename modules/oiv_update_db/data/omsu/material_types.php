<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$ret = array();

/*
$ret[] = array(
    'name' => 'Карта сайта',
    'en_name' => 'Sitemap',
    'id_char' => 'Karta-sajta',
    'parent_char' => '0',
    'showinmenu' => '',
    'ordernum' => '0',
    'template_list_char' => 'sitemap',
    'template_char' => '',
    'perpage' => '0',
    'hide_anons' => '',
    'hide_parent_id' => '1',
    'hide_files' => '',
    'hide_content' => '',
    'hide_extra_number' => '1',
    'hide_conmat' => '',
    'hide_date_event' => '',
    'hide_contacts' => '1',
    'mat_as_type' => '',
    'hide_tags' => '',
    'hide_lang' => '',
    'params' => Array(),
);
*/
/*
$ret[]= array(
    'name' => 'Подписка',
    'en_name' => 'subscription',
    'id_char' => 'subscription',
    'template_list_char' => 'subscription',
    'parent_char' => null,
    'showinmenu' => '0',
);

$ret[]= array(
    'name' => 'Календарь знаменательных дат',
    'en_name' => 'Calendar of important dates',
    'id_char' => 'Kalendar-znamenatelnyh-dat',
    'template_list_char' => 'events_calendar',
    'parent_char' => null,
    'showinmenu' => '1',
);

$ret[] = array(
    'name' => 'Типы дат в календаре',
    'en_name' => 'Calendar date types',
    'id_char' => 'Tipy-dat-v-kalendare',
    'parent_char' => 'Kalendar-znamenatelnyh-dat',
    'showinmenu' => '0',
);*/

/*$ret[]= array(
    'name' => 'Защита персональных данных',
    'en_name' => 'Protection of personal information',
    'id_char' => 'protection',
    'parent_char' => 'Rukovodstvo',
    'showinmenu' => '1',
    'template_list_char' => 'document_list',
    'template_char' => 'news_detail',
    'ordernum' => 100,
);

$ret[]= array(
    'name' => 'Ссылки в подвале Навигация и возможности',
    'en_name' => 'Footerlinks',
    'id_char' => 'Footerlinks',
    'parent_char' => null,
    'showinmenu' => '0',
    'template_list_char' => null,
    'template_char' => null,
    'ordernum' => 0,
);

$ret[]= array(
    'name' => 'Боковые ссылки',
    'en_name' => 'Side links',
    'id_char' => 'sidelinks',
    'parent_char' => null,
    'showinmenu' => '0',
    'template_list_char' => null,
    'template_char' => null,
    'ordernum' => 0,
);*/

// {{INSERT}}

return $ret;
