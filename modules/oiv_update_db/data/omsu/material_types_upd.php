<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

$ret = array();

/*
$ret[] = array(
    'id_char' => 'Kalendar-znamenatelnyh-dat',
    'params' => array(
        'name' => 'Тип даты',
        'valuetype' => 'valuemat',
        'matchar' => 'Tipy-dat-v-kalendare',
    ),
);
*/
/*
$ret[]= array(
    'id_char' => 'subscription',
    'template_list_char' => 'subscription',
);

$ret[]= array(
    'id_char' => 'Kalendar-znamenatelnyh-dat',
    'template_list_char' => 'events_calendar',
    'params' => array(
        array(
            'name' => 'Тип даты',
            'valuetype' => 'valuemat',
            'matchar' => 'Tipy-dat-v-kalendare',
        )
    )
);*/

$ret[]= array(
    'id_char' => 'Footerlinks',
    'params' => array(
        array(
            'name' => 'URL',
            'valuetype' => 'valuename'
        )
    )
);

$ret[]= array(
    'id_char' => 'sidelinks',
    'params' => array(
        array(
            'name' => 'URL',
            'valuetype' => 'valuename'
        )
    )
);

// {{INSERT}}

return $ret;
