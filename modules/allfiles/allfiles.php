<?php
/**
 * @var \iSite $this
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

defined('_WPF_') or die();

//if ( ! $this->requireAuthUser()->can('admin.sysoptions')) {
//	$this->endRequest(403);
//}

//die('step1');

$this->data->title = 'Загруженные файлы';
$this->data->iH1 = 'Загруженные файлы';
$this->data->keywords = '';
$this->data->description = 'Загруженные файлы';
$this->data->breadcrumbsmore = '';
$this->data->errortext = '';

//получаем список сайтов
global $sites, $stypes, $files, $wrongfiles;
$sites=Array();
$files=Array();
$wrongfiles=Array();

$sites=Array();
$sites[]='/home/habkrai/data/www/html.3';

$moresites=scandir('/home/habkrai/data/www/html.3/oiv');

$warr=Array('.','..','photos','template_1','template_3','tempoiv'
,'omsu_bikin_lermontovka1','omsu_bikin_lermontovka2','omsu_bikin_lermontovka3','omsu_bikin_lermontovka4','omsu_bikin_lermontovka5','omsu_bikin_lermontovka6',
    'omsu_bikin_lermontovka7','omsu_de-kastri');

foreach ($moresites as $ms)
{
    if(!in_array($ms,$warr))
    {
        $sites[]='/home/habkrai/data/www/html.3/oiv/'.$ms;
    }
}

//получаем список папок и время создания photos
//$oivs=scandir('/var/www/html/oiv/');


        //получаем список файлов
	    foreach ($sites as $site)
	    {

            //echo '<br>'.$site;

            //die('step3');

	        //устанавливаем соединение с БД сайта
            //$siteobj=$this;
            //$siteodj->settings->DB->name=$site['dbname'];
            //$siteodj->settings->DB->login=$site['dblogin'];
            //$siteodj->settings->DB->pass=$site['dbpass'];

            //$siteodj->dbclose();

            if($site == '/home/habkrai/data/www/html.3')
            {
                $siteodj=$this;
            }
            else
            {
                unset($siteodj);
                unset($setup);
                unset($versions);

                //die('step4');

                include_once($site.'/settings.php');
                include_once($site.'/versions.php');

                //print_r($setup);

                //die('step5');



                $siteodj = new iSite($setup,$versions, 0);

                //die('step6');
            }

            if($siteodj)
            {

                //die('step7');

                //получаем список вложэений файлов, привязанных к активным материалам
                $query='SELECT f.* FROM files f WHERE f.type_id IN (1,4) AND f.id IN ( SELECT fc.file_id FROM file_connections fc WHERE fc.material_id IN 
                    (SELECT m.id FROM materials m WHERE m.date_add >= \'2018-04-08\'))';
                if($res = $siteodj->dbquery($query))
                {
                    //die('step8');

                    //krumo($res);

                    if(count($res) > 0){
                        foreach ($res as $dbfile)
                        {
                            $putfileinfo=true;
                            $filemat='';
                            $filesize='';
                            $filedatecreate='';
                            $filedateedit='';
                            $fileexist=true;

                            //получаем характеристики файла на диске (размер, дата редактирования, дата создания)
                            $filepatch=$site.'/photos/'.$dbfile['id'];
                            if(file_exists($filepatch))
                            {
                                $filesize=filesize($filepatch);
                                //if($filesize == 0)
                                //{
                                    $putfileinfo=true;
                                    $filedatecreate=date("d.m.Y H:i:s", filectime($filepatch));
                                    $filedateedit=date("d.m.Y H:i:s", filemtime($filepatch));
                                    //$filesize=0;
                                //}
                            }
                            else
                            {
                                $fileexist=false;
                                //$putfileinfo=true;
                                $filedatecreate='';
                                $filedateedit='';
                                $filesize='';
                            }

                            //die('step9');

                            if($putfileinfo)
                            {

                                //вносим новые данные о файле
                                $dbfile['site']=$siteodj->data->title;

                                $dbfile['domain']='';

                                $dbfile['filedatecreate']=$filedatecreate;
                                $dbfile['filedateedit']=$filedateedit;
                                $dbfile['filesize']=$filesize;

                                //получаем материал и пользователя изменившего материал с данным файлом
                                /*$query='SELECT m.id, m.name, m.type_id, m.date_add, m.date_edit, m.user_id,
                                (SELECT u.fio FROM users u WHERE u.id = m.user_id) fio,
                                (SELECT u.email FROM users u WHERE u.id = m.user_id) email  
                                FROM materials m 
                                WHERE m.id IN 
                                    (SELECT fc.material_id FROM file_connections fc WHERE fc.file_id = '.$dbfile['id'].')';
                                if($mres=$siteodj->dbquery($query))
                                {
                                    //die('step10');

                                    $dbfile['materialid']=$mres[0]['id'];
                                    $dbfile['materialtypeid']=$mres[0]['type_id'];
                                    $dbfile['materialname']=$mres[0]['name'];
                                    $dbfile['materialuserid']=$mres[0]['user_id'];
                                    $dbfile['materialuseremail']=$mres[0]['email'];
                                    $dbfile['materialuserfio']=$mres[0]['fio'];
                                    $dbfile['materialdate_add']=$mres[0]['date_add'];
                                    $dbfile['materialdate_edit']=$mres[0]['date_edit'];



                                }*/

                                $pa=$site;
                                $pa=str_replace('/home/habkrai/data/www/html.3','',$pa);
                                $pa.='/photos/'.$dbfile['id'];

                                if(file_exists('/var/www/html'.$pa))
                                {
                                    $pa='/var/www/html'.$pa;
                                }
                                else
                                {
                                    $pa='/home/habkrai/data/www/html.3'.$pa;
                                }

                                $dbfile['patch']=$pa;
                                //$dbfile['patch']=$site;

                                //сохраняем в массив ошибочных файлов
                                $wrongfiles[]=$dbfile;
                                //krumo($dbfile);
                            }

                        }
                    }
                }

            }

            //die('step11');

        }


//die('step12');