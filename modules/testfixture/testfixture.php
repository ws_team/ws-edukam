<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 * @release 2.15
 */


defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.testfixture'))
    $this->forbidden(403);

require_once(__DIR__.'/functions.php');

$this->getPostValues('up');

if ( ! empty($this->values->up)) {
    $fixtures = explode(',', $this->values->up);
    foreach ($fixtures as $fixture_name)
        testfixture_up($this, $fixture_name);
}

if ( ! empty($this->values->down)) {
    $fixtures = explode(',', $this->values->down);
    foreach ($fixtures as $fixture_name)
        testfixture_down($this, $fixture_name);
}
