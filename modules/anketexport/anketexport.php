<?php

defined('_WPF_') or die();

$this->getPostValues(array('datestart','dateend','aid','mode','groupby'));

//получаем список анкет

if($this->values->aid === '')
{
    die();
}
else
    {

        //создаем недостающие таблицы
        //таблица анкет
        $query = <<<EOS
CREATE TABLE IF NOT EXISTS survey_ankets(
    id BIGINT NOT NULL,
    survey_id BIGINT NOT NULL,
    dateadd BIGINT NOT NULL,
    PRIMARY KEY(id)
)
EOS;

        $this->dbquery($query);


        //таблица ответов
        $query = <<<EOS
CREATE TABLE IF NOT EXISTS survey_anketanswers(
    anket_id BIGINT NOT NULL,
    question_id BIGINT NOT NULL,
    answer_id BIGINT,
    answer_text CHARACTER VARYING (1000),
    PRIMARY KEY(anket_id,question_id,answer_id)
)
EOS;

        $this->dbquery($query);


        //правка таблицы ответов - расширение ключа, чтобы сохранялся множественный выбор ответа на вопрос
        $query = <<<EOS
ALTER TABLE survey_anketanswers DROP CONSTRAINT survey_anketanswers_pkey
EOS;
        $this->dbquery($query);



        require_once(WPF_ROOT.'/classes/PHPExcel.php');
        require_once(WPF_ROOT.'/classes/PHPExcel/Writer/Excel5.php');

        //получаем заголовок анкетирования
        $survey=MaterialGet($this, $this->values->aid);

        //получаем массивы вопросов и вариантов ответов анкетирования
        $questinos=$survey['connected_materials'];
        $normal_questions=array();
        $all_answers=array();

        foreach ($questinos as $quest)
        {
            $normal_questions[$quest['id']]=$quest;

            $answers_all=MaterialList($this, '', 1, '1000', 2, $quest['cids']);
            $answers=array();
            $normal_answers=array();

            foreach ($answers_all as $answer)
            {
                if($answer['type_name'] != 'Анкеты')
                {
                    $answers[]=$answer;
                }
            }

            //krumo($answers);
            //die();

            foreach ($answers as $answer)
            {
                $normal_answers[$answer['id']]=$answer;
                $all_answers[$answer['id']]=$answer;
            }

            $normal_questions[$quest['id']]['answers']=$normal_answers;
        }

        //krumo($survey);
        //die();

        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();



        $sheet->setTitle('Анкетирование_'.$this->values->aid);

        //die('step1');

        $header=array();

        switch ($this->values->mode)
        {
            //Все данные анкет
            case 'all':

                //die('test');

                //формируем заголовки выгрузки
                $header[]='№анкеты';
                $header[]='дата';

                foreach ($questinos as $quest)
                {
                    $header[]=$quest['name'];
                }

                $j=-1;
                $rownum=1;
                foreach ($header as $col)
                {
                    ++$j;
                    $sheet->setCellValueByColumnAndRow($j,$rownum,$col);
                }

                //формируем тело выгрузки
                $query="SELECT * FROM survey_ankets WHERE survey_id = ".$this->values->aid." ORDER BY id ASC";
                if($res=$this->dbquery($query))
                {

                    foreach ($res as $anket)
                    {
                        ++$rownum;

                        //данные анкеты
                        $sheet->setCellValueByColumnAndRow(0,$rownum,$anket['id']);
                        $sheet->setCellValueByColumnAndRow(1,$rownum,date('d.m.Y H:i:s',$anket['dateadd']));

                        //ответы на вопросы анкеты
                        $queryans="SELECT * FROM survey_anketanswers WHERE anket_id = ".$anket['id'];
                        if($resans = $this->dbquery($queryans))
                        {
                            $col = 1;
                            $normalanswers=array();
                            foreach ($resans as $ans)
                            {
                                $normalanswers[$ans['question_id']][]=$ans;
                            }

                            //рисуем данные в таблицу ваыгрузки
                            foreach ($questinos as $quest)
                            {
                                $rowcontent='';
                                /*if($normalanswers[$quest['id']] != 0)
                                {
                                    $rowcontent.=$all_answers[$normalanswers[$quest['id']]['answer_id']]['name'];
                                }
                                if($normalanswers[$quest['id']]['answer_text'] != '')
                                {
                                    if($rowcontent != '')
                                    {
                                        $rowcontent.='; ';
                                    }
                                    $rowcontent.='свой ответ: '.$normalanswers[$quest['id']]['answer_text'];
                                }
                                */

                                foreach ($normalanswers[$quest['id']] as $nanswer)
                                {
                                    if($rowcontent != '')
                                    {
                                        $rowcontent.='; ';
                                    }

                                    if($nanswer['answer_id'] != 0)
                                    {
                                        $rowcontent.=$all_answers[$nanswer['answer_id']]['name'];
                                    }
                                    if($nanswer['answer_text'] != '')
                                    {
                                        $rowcontent.='свой ответ: '.$nanswer['answer_text'];
                                    }
                                }
                                /*echo "<br>";
                                krumo($col);
                                krumo($rownum);
                                krumo($rowcontent);*/

                                ++$col;
                                $sheet->setCellValueByColumnAndRow($col,$rownum,$rowcontent);

                            }
                        }
                    }
                }

                break;

            //анкет за период с группировкой
            case 'groupanddate':

                //выгрузка анкет без группировки
                if($this->values->groupby == '')
                {
                    //формируем заголовки
                    $header[]='Вопрос';
                    $header[]='Ответ';
                    $header[]='Колличество';


                    $j=-1;
                    $rownum=1;
                    foreach ($header as $col)
                    {
                        ++$j;
                        $sheet->setCellValueByColumnAndRow($j,$rownum,$col);
                    }


                    //формирууем тело
                    foreach ($normal_questions as $quest)
                    {
                        foreach ($quest['answers'] as $answer)
                        {
                            ++$rownum;
                            $sheet->setCellValueByColumnAndRow(0,$rownum,$quest['name']);
                            $sheet->setCellValueByColumnAndRow(1,$rownum,$answer['name']);
                            //подсчитываем кол-во
                            $querycnt='SELECT count(a.anket_id) acnt FROM survey_anketanswers a WHERE a.question_id = '.$quest['id'].' AND a.answer_id = '.$answer['id'];

                            //вставляем в запрос проверку на даты
                            if($this->values->datestart != '')
                            {
                                $timestampstart=MakeTimeStampFromStr($this->values->datestart);
                                $querycnt.=' AND a.anket_id IN (SELECT id FROM survey_ankets an WHERE an.dateadd >= '.$timestampstart.')';
                            }

                            if($this->values->dateend != '')
                            {
                                $timestampend=MakeTimeStampFromStr($this->values->dateend);
                                $querycnt.=' AND a.anket_id IN (SELECT id FROM survey_ankets an WHERE an.dateadd <= '.$timestampend.')';
                            }

                            $acnt='';
                            if($resans=$this->dbquery($querycnt))
                            {
                                if(count($resans) > 0)
                                {
                                    $acnt=$resans[0]['acnt'];
                                }
                            }
                            if($acnt == '')
                            {
                                $acnt=0;
                            }
                            $sheet->setCellValueByColumnAndRow(2,$rownum,$acnt);
                        }
                    }

                }
                //группировка по вопросу
                else
                {

                    //формируем заголовки
                    $header[]='Вопрос (группировка)';
                    $header[]='Ответ (группировка)';
                    $header[]='Вопрос';
                    $header[]='Ответ';
                    $header[]='Колличество';


                    $j=-1;
                    $rownum=1;
                    foreach ($header as $col)
                    {
                        ++$j;
                        $sheet->setCellValueByColumnAndRow($j,$rownum,$col);
                    }

                    $groupquest=$normal_questions[$this->values->groupby];

                    foreach ($groupquest['answers'] as $groupanswer)
                    {

                        //все вопросы
                        foreach ($normal_questions as $quest)
                        {
                            if($groupquest['id'] != $quest['id'])
                            {
                                foreach ($quest['answers'] as $answer)
                                {
                                    ++$rownum;
                                    $sheet->setCellValueByColumnAndRow(0,$rownum,$groupquest['name']);
                                    $sheet->setCellValueByColumnAndRow(1,$rownum,$groupanswer['name']);
                                    $sheet->setCellValueByColumnAndRow(2,$rownum,$quest['name']);
                                    $sheet->setCellValueByColumnAndRow(3,$rownum,$answer['name']);
                                    //подсчитываем кол-во
                                    $querycnt='SELECT count(a.anket_id) acnt FROM survey_anketanswers a WHERE a.question_id = '.$quest['id'].' AND a.answer_id = '.$answer['id'].'
                                AND a.anket_id IN (SELECT g.anket_id FROM survey_anketanswers g WHERE g.question_id = '.$groupquest['id'].' AND g.answer_id = '.$groupanswer['id'].')';

                                    //вставляем в запрос проверку на даты
                                    if($this->values->datestart != '')
                                    {
                                        $timestampstart=MakeTimeStampFromStr($this->values->datestart);
                                        $querycnt.=' AND a.anket_id IN (SELECT id FROM survey_ankets an WHERE an.dateadd >= '.$timestampstart.')';
                                    }

                                    if($this->values->dateend != '')
                                    {
                                        $timestampend=MakeTimeStampFromStr($this->values->dateend);
                                        $querycnt.=' AND a.anket_id IN (SELECT id FROM survey_ankets an WHERE an.dateadd <= '.$timestampend.')';
                                    }

                                    $acnt='';
                                    if($resans=$this->dbquery($querycnt))
                                    {
                                        if(count($resans) > 0)
                                        {
                                            $acnt=$resans[0]['acnt'];
                                        }
                                    }
                                    if($acnt == '')
                                    {
                                        $acnt=0;
                                    }
                                    $sheet->setCellValueByColumnAndRow(4,$rownum,$acnt);
                                }
                            }
                        }

                    }

                }

                break;
        }

        /*krumo('end');
        exit;*/

        $fileName = "Анкеты";

        //https://minkult.khabkrai.ru/?menu=anketexport&aid=2675&mode=groupanddate
        //выводим файл
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=$fileName" );

        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');

        $this->endRequest();
    }

