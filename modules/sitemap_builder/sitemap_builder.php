<?php
/**
 * Created by PhpStorm.
 * User: Raven
 * Date: 06.03.2015
 * Time: 9:54
 */

// Создать файл с перечнем файлов sitemap.xml по разделам
function create_sitemap_index($site, $sitemap_file_path, $sitemap_url) {
    $xmlWriter = new XMLWriter();
    $xmlWriter->openMemory();
    $xmlWriter->startDocument("1.0", "utf-8");
    $xmlWriter->startElement("sitemapindex");
    $xmlWriter->writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

    $query = 'SELECT "id_char" FROM "material_types" WHERE ("parent_id" is null) and ("showinmenu" is not null and "showinmenu" = 1)';

    $res = $site->dbquery($query);

    // для каждого корневого раздела создаем секцию
    if (count($res) > 0) {
        foreach($res as $record) {
            $xmlWriter->startElement("sitemap");
            $xmlWriter->writeElement("loc", $sitemap_url . '/sitemap/' .root_type_sitemap_filename($record['id_char']));
            $xmlWriter->endElement();
        }

    }

    $xmlWriter->endElement();
    $xmlWriter->endDocument();

    $xml_file = $xmlWriter->outputMemory();

    file_put_contents($sitemap_file_path . "/sitemap.xml", $xml_file);
}

// создаем sitemap-файлы для всех корневых разделов
function create_sitemap($site, $sitemap_file_path, $sitemap_url) {
    $query = 'SELECT "id", "id_char" FROM "material_types" WHERE ("parent_id" is null) and ("showinmenu" is not null and "showinmenu" = 1)';

    $res = $site->dbquery($query);

    if (count($res) > 0) {
        foreach($res as $root_type) {
            create_sitemap_for_root_type($site, $root_type['id'], $root_type['id_char'],
                $sitemap_file_path, $sitemap_url);
        }
    }
}

// создать файл sitemap для корневого раздела
function create_sitemap_for_root_type($site, $type_id, $type_name, $sitemap_file_path, $sitemap_url) {

    $xml_filename = $sitemap_file_path . '/' . root_type_sitemap_filename($type_name);

    file_put_contents($xml_filename, '');

    $xmlWriter = new XMLWriter();
    $xmlWriter->openMemory();
    $xmlWriter->startDocument("1.0", "utf-8");
    $xmlWriter->startElement("urlset");
    $xmlWriter->writeAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

    $xmlWriter->startElement("url");
    $xmlWriter->writeElement("loc", $sitemap_url . GetMaterialTypeUrl($site, $type_id));
    $xmlWriter->endElement();

    $query = 'SELECT "id", "mat_as_type" FROM "material_types" WHERE ("parent_id" is not null and "parent_id" = '
        . $type_id . ') and ("showinmenu" is not null and "showinmenu" = 1)';

    $res = $site->dbquery($query);

    if (count($res) > 0) {
        foreach($res as $record) {
            write_type_in_sitemap($site, $record["id"], $xmlWriter, $xml_filename, $sitemap_url, $record["mat_as_type"]);
        }
    }

    $xmlWriter->endElement();
    $xmlWriter->endDocument();

    file_put_contents($xml_filename, $xmlWriter->flush(true), FILE_APPEND);
}

// создать записи в sitemap для раздела
function write_type_in_sitemap($site, $type_id, $xmlWriter, $filename, $sitemap_url, $mat_as_type) {
    $xmlWriter->startElement("url");
    $xmlWriter->writeElement("loc", $sitemap_url . GetMaterialTypeUrl($site, $type_id));
    $xmlWriter->endElement();

    write_materials_in_sitemap($site, $type_id, $xmlWriter, $filename, $sitemap_url);

    if($mat_as_type != null && $mat_as_type) {
        $query = 'SELECT "id", "mat_as_type" FROM "material_types" WHERE ("parent_id" is not null and "parent_id" = '
            . $type_id . ') and ("showinmenu" is not null and "showinmenu" = 1)';

        $res = $site->dbquery($query);

        if (count($res) > 0) {
            foreach($res as $record) {
                write_type_in_sitemap($site, $record["id"], $xmlWriter, $filename, $sitemap_url,
                    $record["mat_as_type"]);
            }
        }
    }

    file_put_contents($filename, $xmlWriter->flush(true), FILE_APPEND);
}

// записать страницы материалов в sitemap
function write_materials_in_sitemap($site, $type_id, $xmlWriter, $filename, $sitemap_url) {

    $flush_index = 0;
    $flush_increment = 200;

    // выбираем только активные материалы на русском языке
    $query = 'SELECT "id" FROM "materials" WHERE "type_id" = '
        . $type_id.' and "status_id" = 2 and "language_id" = 1 OFFSET '
        .$flush_index . ' ROWS FETCH NEXT '. $flush_increment .' ROWS ONLY';

    $res = $site->dbquery($query);

    while(count($res) > 0) {
        foreach($res as $record) {
            $xmlWriter->startElement("url");
            $xmlWriter->writeElement("loc", $sitemap_url . GetMaterialTypeUrl($site, $type_id, $record['id'])
                . '/' . $record['id']);
            $xmlWriter->endElement();
        }

        file_put_contents($filename, $xmlWriter->flush(true), FILE_APPEND);

        $flush_index += $flush_increment;

        $query = 'SELECT "id" FROM "materials" WHERE "type_id" = '
            . $type_id.' and "status_id" = 2 and "language_id" = 1 OFFSET '
            .$flush_index . ' ROWS FETCH NEXT '. $flush_increment .' ROWS ONLY';

        $res = $site->dbquery($query);
    }

    file_put_contents($filename, $xmlWriter->flush(true), FILE_APPEND);
}

function root_type_sitemap_filename($type_name) {
    return 'sitemap_'.$type_name.'.xml';

}

$sitemap_file_path = '/var/www/html/sitemap';
$sitemap_url = 'http://' . $_SERVER['HTTP_HOST'];

create_sitemap_index($this, $sitemap_file_path, $sitemap_url);
create_sitemap($this, $sitemap_file_path, $sitemap_url);