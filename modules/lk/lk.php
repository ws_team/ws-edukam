<?php
//переназначаем некоторые настройки сайта=============================================
$this->data->title='Личный кабинет - '.$this->data->title;
$this->data->keywords='личный, кабинет, пользователь';
$this->data->description='Личный кабинет пользователя';
$this->data->breadcrumbsmore='&rarr;<strong>Личный кабинет</strong>';
$this->data->iH1='Личный кабинет';


//выполняем нужные бизнесс-процессы===================================================
//получаем пользовательские данные
if($this->autorize->autorized == 1)
    {
        
        
    //действия
    switch($this->values->action)
        {
        case 'editcontacts':
        $this->getPostValues(Array('email','emailsec','tel'));
        
        $doQuery=false;
        
        if(($this->values->email != '')&&($this->values->email != $_SESSION['userdata']['email']))
            {
            //генерируем код подтверждения
            $new_email=$this->values->email;
            $new_email_approve=random_string(32, 'lower,upper,numbers');
            $doQuery=true;
            }
        else
            {
            $new_email=$_SESSION['userdata']['email'];
            $new_email_approve=$_SESSION['userdata']['email_approve'];
            }
            
        if(($this->values->emailsec != '')&&($this->values->emailsec != $_SESSION['userdata']['emailsec']))
            {
            //генерируем код подтверждения
            $new_emailsec=$this->values->emailsec;
            $new_emailsec_approve=random_string(32, 'lower,upper,numbers');
            $doQuery=true;
            }
        else
            {
            $new_emailsec=$_SESSION['userdata']['emailsec'];
            $new_emailsec_approve=$_SESSION['userdata']['emailsec_approve'];
            }
        
        if(($this->values->tel != '')&&($this->values->tel != $_SESSION['userdata']['tel']))
            {
            //генерируем код подтверждения
            $new_tel=$this->values->tel;
            $new_tel_approve=random_string(5, 'lower,numbers');
            $doQuery=true;
            }
        else
            {
            $new_tel=$_SESSION['userdata']['tel'];
            $new_tel_approve=$_SESSION['userdata']['emailsec_approve'];
            }
        
        if($doQuery)
            {
            $query="UPDATE rs_users SET tel='$new_tel', tel_approve='$new_tel_approve', email='$new_email', email_approve='$new_email_approve', emailsec='$new_emailsec', emailsec_approve='$new_emailsec_approve' WHERE id = ".$_SESSION['userdata']['id'];
            $this->dbquery($query);
            }
        break;
        }
        
        
    $query="SELECT * FROM rs_users WHERE id = ".$this->autorize->userid;
    if($res=$this->dbquery($query))
        {
        if(count($res) > 0)
            {
            $this->data->userdata=$res[0];
            $_SESSION['userdata']=$res[0];
            }
        }
    
    }
else
    {
    header("Location: /");
    die();
    }