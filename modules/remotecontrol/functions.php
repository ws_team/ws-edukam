<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


require_once(WPF_ROOT.'/classes/wpf/auth/PasswordAuthenticator.php');

function remotecontrol_auth_peer(\iSite $site)
{
    $auth_list = remotecontrol_get_authenticator_list($site);
    foreach ($auth_list as $auth) {
        $userId = $auth->authenticate($site);
        if ($userId instanceof \wpf\auth\IdentityInterface) {
            $site->getUser()->login($userId);
            return true;
        }
    }

    $site->endRequest(403);
}

function remotecontrol_get_authenticator_list(\iSite $site)
{
    $ret = array();
    $ret[] = new \wpf\auth\PasswordAuthenticator($site);
    return $ret;
}

function remotecontrol_process_request(\iSite $site)
{
    $extracted = array();

    $is_post = $_SERVER['REQUEST_METHOD'] == 'POST';

    foreach (array_keys($_REQUEST) as $key) {
        if (strncmp($key, 'rc__', 4) == 0) {
            $val = $_REQUEST[$key];
            $extr_key = substr($key, 4);
            if ( ! isset($extracted[$extr_key])) {
                $extracted[$extr_key] = $val;
                $_REQUEST[$extr_key] = $val;
                if ($is_post)
                    $_POST[$extr_key] = $val;
                else
                    $_GET[$extr_key] = $val;
            }
        }
        if ( ! isset($extracted[$key])) {
            unset($_REQUEST[$key]);
            if ($is_post) {
                unset($_POST[$key]);
            } else {
                unset($_GET[$key]);
            }
        }
    }

    $site->resetValues();
    $site->resetError();

    $site->getPostValue('menu');

    $site->makeData();

    if (empty($site->error->flag)) {
        // $site->echoHtml();
    }

    if ($site->getUser()->isAdmin()) {
        $site->showWarning();
        if ( ! empty($site->error->flag))
            $site->showError();
    }
}

/**
 * @param string $remote_url  URL сайта
 * @param array $rc_params  Параметры аутентификации и прочее
 * @param string $module  Модуль для вызова на удаленном сервере
 * @param array $module_params  Параметры для передачи в вызываемый модуль
 * @return bool|mixed  Ответ удаленного сервера при успехе, false при неуспехе
 */
function remotecontrol_call($remote_url, $rc_params, $module, $module_params = array())
{
    $ch = curl_init($remote_url);
    if ( ! is_resource($ch)) {
        return false;
    }

    $module_params['menu'] = $module;
    $post_params = array();

    foreach ($module_params as $attr => $val)
        $post_params['rc__'.$attr] = $val;

    $post_params['menu'] = 'remotecontrol';
    foreach ($rc_params as $attr => $val)
        $post_params[$attr] = $val;

    $postdata = array();
    foreach ($post_params as $param => $val)
        $postdata[] = $param.'='.urlencode($val);

    $postdata = implode('&', $postdata);

    $headers = array(
        'Accept: application/json, *.*;q=0.1',
        'Content-type: application/x-www-form-urlencoded',
    );

    curl_setopt_array($ch, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_MAXREDIRS => 5,

        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $postdata,
        CURLOPT_HTTPHEADER => $headers,
    ));

    $resp = curl_exec($ch);

    curl_close($ch);

    return $resp;
}