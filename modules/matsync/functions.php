<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function matsync_process_files(\iSite $site, $data, $material, $froot=false)
{
    $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'syncing files, total '. count($data['allfiles']));

    foreach ($data['allfiles'] as $file) {
        //проверка на наличае файла в материале
        $file_name = $file['name'].'.'.$file['extension'];
        $file_type = $file['type_id'];

        if (isset($file['size'])) {
            $file_size = intval($file['size']);
        } else {
            $file_size = '';
        }

        $file_content = $file['content'];

        $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'syncing files '. $file_type.', '.$file_name);

        $doadd = true;

        if(isset($material['allfiles'])){

            foreach ($material['allfiles'] as $matfile) {
                if(!isset($matfile['size'])) {
                    $matfile['size'] = '';
                }

                $matfile['size'] = intval($matfile['size']);

                if ($matfile['size'] == $file_size
                    && $matfile['type_id'] == $file_type
                    && $matfile['content'] == $file_content) {
                    $doadd = false;
                    $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'same file already exists');
                }
            }

        }

        //загрузка файла
        if ($doadd) {
            $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'adding file '. $file_type.', '.$file_name);

            if($froot)
            {
            	$fileroot=$froot.'/photos/'.$file['id'];
            }
            else
            {
	            $fileroot=WPF_ROOT.'/photos/'.$file['id'];
            }

            //формируем массивов файла (какбудто он передан формой POST-ом)
            $_FILES['filedata'] = Array();
            $_FILES['filedata']['name'] = $file_name;
            $_FILES['filedata']['size'] = $file['size'];
            $_FILES['filedata']['tmp_name'] = $fileroot;


            $site->values->file_type_id = $file_type;
            $site->values->file_content = $file['content'];
            $site->values->file_name_en = $file['name_en'];
            $site->values->date_event = $file['date_event'];
            $site->values->author = $file['author'];
            $site->values->ordernum = $file['ordernum'];

            //пишем файл
            $nfile = new MFile($site);
            $nfile->DoSaveFile();
            unset($nfile);
        }
    }
}

/**
 * @param \iSite $site
 * @param array $oivsettings
 * @param array $param
 * @return array
 */
function matsync_extra_param_value(\iSite $site, $oivsettings, $param)
{
    $type_id = null;

    $value = isset($param['value']) ? $param['value'] : null;

    $site->getLogger()->write(LOG_INFO, __FUNCTION__,
        'Processing param: name="'.$param['name'].'";value="'.$param['value'].'"');

    switch($param['name']) {
        case 'Заработная плата':

            $type_id = $oivsettings['vacancy_salaryparam'];
            $site->getLogger()->write(LOG_DEBUG, __FUNCTION__, 'salary type='.$type_id.';value='.$value);
            break;

        case 'Госслужба или нет':

            $value = null;

            if (mb_substr(mb_strtolower($param['valuename']), 0, 3) == 'не ') {
                break;
            }

            $type_id = $oivsettings['vacancy_typeparam'];
            $value = matsync_matid_byname($site, $oivsettings['vacancytypes'], $param['valuename']);

            $site->getLogger()->write(LOG_DEBUG, __FUNCTION__, 'state service type='.$type_id.';value='.$value);

            break;

        case 'Муниципальная служба или нет':

            $value = null;

            if (mb_substr(mb_strtolower($param['valuename']), 0, 3) == 'не ') {
                break;
            }

            $type_id = $oivsettings['vacancy_typeparam'];
            $value = matsync_matid_byname($site, $oivsettings['vacancytypes'], $param['valuename']);

            $site->getLogger()->write(LOG_DEBUG, __FUNCTION__, 'municipal service type='.$type_id.';value='.$value);

            break;

        case 'Приём документов до':

            $type_id = $oivsettings['vacancy_datetoparam'];
            $site->getLogger()->write(LOG_DEBUG, __FUNCTION__, 'term type='.$type_id.';value='.$value);
            break;
    }

    return array($value, $type_id);
}

function matsync_matid_byname(\iSite $site, $type_id, $name)
{
    $value = null;

    $query = <<<EOS
SELECT id FROM materials
WHERE
    type_id = $1
    AND lower(name) LIKE lower($2)
EOS;
    $q_params = array($type_id, $name);
    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {
        $value = $res[0]['id'];
    }

    if (empty($value))
        $site->getLogger()->write(LOG_DEBUG, __FUNCTION__, $query.'?'.serialize($q_params));

    return $value;
}

/**
 * @param iSite $site
 * @param array $material
 * @param array $sync_data
 * @param array $oivsettings
 */
function matsync_process_extra_params(\iSite $site, $material, $sync_data, $oivsettings)
{
    //удаляем предыдущие значения
    $mat_id = $material['id'];
    $mattype = $material['type_id'];

    $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'material #'. $mat_id .', deleting params');
    
    $query = 'DELETE FROM "extra_materials" WHERE "material_id" = $1 AND "selector_id" IS NULL';
    $q_params = array($mat_id);
    $site->dbquery($query, $q_params);

    $site->getLogger()->write(LOG_INFO, __FUNCTION__, 'material #'. $mat_id .', deleted params');

    $is_vacancy = $mattype == $oivsettings['vacancytype'];
    $selector = null;
    $job_type = null;

    foreach ($sync_data['params'] as $param) {

        list($value, $param_id) = matsync_extra_param_value($site, $oivsettings, $param);

        if (is_null($param_id) || is_null($value)) {
            $site->getLogger()->write(LOG_WARNING, __FUNCTION__,
                "Failed to get param {$param['name']} value");
            continue;
        }

        matsync_extra_param_set($site, $mat_id, $param_id, $param['valuetype'], $value);

        if ($param['name'] == 'Муниципальная служба или нет') {
            $job_type = 'municipal';
        } elseif ($param['name'] == 'Госслужба или нет') {
            $job_type = 'state';
        }
    }

    if ($is_vacancy && is_null($job_type)) {
        matsync_extra_param_set($site, $mat_id, $oivsettings['vacancy_typeparam'], 'valuemat',
            matsync_matid_byname($site, $oivsettings['vacancytypes'], 'Другое')
        );
    }
}

/**
 * @param iSite $site
 * @param int $mat_id
 * @param int $param_id
 * @param string $param_type
 * @param mixed $value
 * @return bool
 */
function matsync_extra_param_set(\iSite $site, $mat_id, $param_id, $param_type, $value)
{
    $selector = null;

    $q_upsert = <<<EOS
UPDATE "extra_materials"
SET
    "valuename" = $4,
    "valuetext" = $5,
    "valuedate" = $6,
    "valuemat" = $7
WHERE
    "type_id" = $1
    AND "material_id" = $2
    AND "selector_id" = $3
RETURNING "type_id", "material_id", "selector_id", "valuename", "valuetext", "valuedate", "valuemat"
EOS;
    $query = <<<EOS
WITH upsert("type_id", "material_id", "selector_id", "valuename", "valuetext", "valuedate", "valuemat") AS ($q_upsert)
INSERT INTO "extra_materials"("type_id", "material_id", "selector_id", "valuename", "valuetext", "valuedate", "valuemat")
SELECT $1, $2, $3, $4, $5, $6, $7
    WHERE NOT EXISTS(SELECT 1 FROM upsert);
EOS;

    switch ($param_type) {
        case 'valuetext':
            $q_params = array($param_id, $mat_id, $selector, null, $value, null, null);
            break;

        case 'valuedate':

            $q_params = array($param_id, $mat_id, $selector, null, null, $value, null);
            break;

        case 'valuemat':

            $valuename = null;
            $q_params = array($param_id, $mat_id, $selector, $valuename, null, null, $value);
            break;

        default:

            $q_params = array($param_id, $mat_id, $selector, $value, null, null, null);
            break;
    }

    $res = $site->dbquery($query, $q_params);

    if ($res === false) {
        $site->getLogger()->write(
            LOG_ERR,
            __FUNCTION__,
            'Failed to insert extra param '.$mat_id.'@'.$param_id.'('.$param_type.'), details:'.pg_last_error()
        );
    }

    return $res !== false;
}

function matsync_get_local_id(\iSite $site, $data)
{
    $query = 'SELECT id FROM material_synch WHERE foreignid = $1';
    $res = $site->dbquery($query, array($data['id']));

    if ( ! empty($res)) {
        return $res[0]['id'];
    }

    $type_id = findTypeIdByChar($site, $data['typechar']);

    $query = <<<EOS
SELECT m.id
FROM materials m
    LEFT JOIN material_synch s ON s.id = m.id 
WHERE
    m.type_id = $1
    AND "status_id" <> $2
    AND name = $3
    AND (EXTRACT(EPOCH FROM date_add) - $6 - EXTRACT(EPOCH FROM to_timestamp($4, 'YYYY-MM-DD HH24:MI:SS'))) < $5
    AND s.id IS NULL
EOS;
    $time_treshold = 3600 * 6;

    // в БД дата хранится без зоны (= зона +0000),
    // поэтому при timestamp-операциях дата получает смещение текущей зоны,
    // н., сохранено 12:00:00 текущая временная зона +1000 при timestamp-операциях будет считаться за 22:00:00
    // тут мы компенсируем это смещение

    $db_timezone_offset = 10 * 3600;


    $q_params = array($type_id, STATUS_DELETED, $data['name'], $data['date_add'], $time_treshold, $db_timezone_offset);

    $res = $site->dbquery($query, $q_params);

    if ( ! empty($res)) {
        $id = $res[0]['id'];
        $site->dbquery('INSERT INTO material_synch(foreignid, id) VALUES($1, $2)', array($data['id'], $id));
        return $id;
    }
}
