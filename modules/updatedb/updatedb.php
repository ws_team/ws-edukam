<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\modules\updatedb;

defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.updatedb')) {
    $this->endRequest(403);
}

header('content-type: text/plain');

require_once(__DIR__.'/functions.php');

$this->setResponseHeader('content-type', 'text/plain');

$this->getPostValues(array('from'));

$from = intval( ! empty($this->values->from) ? $this->values->from : date('Ymd'));

execute($this, $from);
