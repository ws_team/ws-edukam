<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


namespace wpf\modules\updatedb;

class Executor {
    private $site;

    public function __construct(\iSite $site)
    {
        $this->site = $site;
    }

    public function runScript($filePath)
    {
        include($filePath);
    }

    public function dbquery($query, $params = array(), $clobs = array(), $blobs = array())
    {
        return $this->site->dbquery($query, $params, $clobs, $blobs);
    }

    protected function resetError()
    {
        $this->site->resetError();
    }
}

function execute(\iSite $site, $from)
{
    $executor = new Executor($site);
    $updatedb_scripts = array();

    $path_prefix = __DIR__.'/data/';

    foreach ((new \GlobIterator($path_prefix.'*/*.php')) as $script) {
        if (empty($from)) {
            $updatedb_scripts[] = $script;
            continue;
        }
        $script_rel = substr($script, strlen($path_prefix));
        $script_date = intval(substr($script_rel, 0, strpos($script_rel, '/')));

        if ($script_date < $from)
            continue;

        $updatedb_scripts[] = $script;
    }

    sort($updatedb_scripts);

    foreach ($updatedb_scripts as $script) {
        printf("apply %s..", implode('/', array_slice(explode('/', $script), -2, 2)));

        try {
            $executor->runScript($script);
        } catch (\Exception $ex) {
            printf("failed\n(%d) %s\n", $ex->getCode(), $ex->getMessage());
        }

        printf("ok\n");
    }
}
