<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$query = <<<EOS
CREATE TABLE IF NOT EXISTS survey(
    question_id BIGINT NOT NULL,
    answer_id BIGINT,
    answer_text CHARACTER VARYING (1000),
    num_answers INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY(question_id, answer_id, answer_text)
)
EOS;

$this->dbquery($query);