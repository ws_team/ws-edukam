<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$query = 'ALTER TABLE survey ADD survey_id BIGINT';

$this->dbquery($query);

$survey_mattype = FindTypeIdByChar($this, 'survey');

$query = <<<EOS
UPDATE survey
SET survey_id = (
    SELECT m.id
    FROM materials m
    WHERE 
        m.type_id = $1
        AND survey.question_id IN(
            SELECT mc.material_child FROM material_connections mc WHERE mc.material_parent = m.id
            UNION
            SELECT mc.material_parent FROM material_connections mc WHERE mc.material_child = m.id
        )
    LIMIT 1
)
WHERE
    survey_id IS NULL
EOS;

$q_params = array($survey_mattype);

$this->dbquery($query, $q_params);

$query = 'ALTER TABLE survey ALTER COLUMN survey_id SET NOT NULL';
$this->dbquery($query);

$query = 'ALTER TABLE survey DROP CONSTRAINT survey_pkey';
$this->dbquery($query);

$query = 'ALTER TABLE survey ADD PRIMARY KEY (survey_id, question_id, answer_id, answer_text)';
$this->dbquery($query);
