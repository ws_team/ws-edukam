<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $cacheinfo_typename;

$this->getPostValues(array(
    'type',
    'key',
));

$valid_types = array(
    'all',
    'arr',
    'html',
    'linksarr',
    'locality',
    'material',
    'oivsettings',
);

$cache_type = $this->values->type;
if (empty($cache_type) || ! in_array($cache_type, $valid_types))
    $this->badRequest();

if ($cache_type == 'all') {
    foreach ($valid_types as $cache_type)
        if ($cache_type != 'all')
            cacheinfo_flush($this, $cache_type);
} else {
    cacheinfo_flush($this, $cache_type);
}

$this->setFlash('status', 'success');
$message = $cache_type == 'all' ? 'Кэш полностью очищен' : 'Очишен кэш '.$cacheinfo_typename[$cache_type];
$this->setFlash('message', $message);

$this->redirect(array(
    'cacheinfo',
    'action' => 'list',
), 302);
