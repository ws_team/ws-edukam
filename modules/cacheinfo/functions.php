<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function cacheinfo_flush(\iSite $site, $type)
{
    $cache = $site->getCache();
    if ($type == 'linksarr') {
        $cache->deleteGlobal($type);
    } else {
        $cache->deleteByPrefix($type.':');
    }
}

function cacheinfo_get_items_count(\iSite $site, $type)
{
    if ($type == 'liknsarr') {
        $ret = $site->getCache()->hasGlobal($type) ? 1 : 0;
    } else {
        $ret = $site->getCache()->countByPrefix($type.':');
    }

    return $ret;
}