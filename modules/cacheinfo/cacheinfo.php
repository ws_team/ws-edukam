<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.cacheinfo'))
    $this->forbidden();

if ($_SERVER['REQUEST_METHOD'] == 'POST')
    if ( ! $this->validateCsrfToken())
        $this->badRequest();

$this->getPostValues(array('action'));

$valid_actions = array('list', 'flush');
$action = $this->values->action;

if (empty($action)) {
    $this->redirect(array(
        'cacheinfo',
        'action' => 'list',
    ));
}

if ( ! in_array($action, $valid_actions))
    $this->badRequest();

require_once(__DIR__.'/defines.php');
require_once(__DIR__.'/functions.php');

$action_controller = __DIR__.'/action/'.$action.'.php';

if (file_exists($action_controller))
    include($action_controller);

