<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $cacheinfo_typename;

$cacheinfo_typename = array(
    'arr' => 'меню и прочих данных',
    'html' => 'cтраниц',
    'locality' => 'геокодирования',
    'linksarr' => 'подвальных ссылок',
    'material' => 'материалов',
    'oivsettings' => 'настроек ОИВ',
);