<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $uflag;

if ($this->getUser()->isAdmin()) {

    //выполняем действие
    $this->GetPostValues('toggle');

    if ($this->values->toggle === 'on' || $this->values->toggle === '1') {
        $this->setState('maintenance', 1);
    } else {
        $this->resetState('maintenance');
    }

    //получаем флаг
    $uflag = $this->getState('maintenance');
}