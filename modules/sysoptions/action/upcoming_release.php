<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.sysoptions'))
    $this->endRequest(403);

$this->getPostValues(array('toggle', 'all'));

// @todo реализовать работу с глобальным состоянием
// 1) установка глобавльного "upcoming_release"
// 2) снятие глобального "upcoming_release" (локальные "upcoming_release" становятся активными)
// 3) принудительное глобальное снятие "upcoming_release" (локальные "upcoming_release" также снимаются)

if ($this->values->toggle === 'on' || $this->values->toggle === '1') {
    if ( ! empty($this->values->all)) {
        $this->setGlobalState('upcoming_release', 1);
    } else {
        $this->setState('upcoming_release', 1);
    }
} else {
    if ( ! empty($this->values->all)) {
        $this->resetGlobalState('upcoming_release');
    } else {
        $this->resetState('upcoming_release');
    }
}

//получаем флаг
$uflag = $this->getState('upcoming_release');