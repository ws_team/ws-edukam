<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if ( ! $this->requireAuthUser()->can('admin.sysoptions')) {
    $this->endRequest(403);
}

$this->getPostValues(array(
    'action',
));

$available_actions = array('maintenance', 'upcoming_release');

if ( ! empty($this->values->action)) {
    if ( ! in_array($this->values->action, $available_actions)) {
        $this->endRequest(400);
    }
    $controller = 'action/'.$this->values->action.'.php';
    include($controller);
}
