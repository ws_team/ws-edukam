<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues(Array(
    'surname',
    'first_name',
    'second_name',
    'post',
    'unit_id',
));

$resultCheckRequiredFields = $checkRequiredFields($this);

if ($resultCheckRequiredFields['success']) {

    if(!checkFieldValueInDB($this, 'id', $this->values->unit_id, 'units')){
        $this->data->errortext = '<p style="color:#F00;">В базе данных не найдено указанное подразделение</p>';
        return;
    }

    $mid = '';

    $query = 'SELECT MAX("id")+1 "mid" FROM "authors"';
    if ($res = $this->dbquery($query))
        $mid = $res[0]['mid'];

    if($mid == '')
        $mid = 1;

    $query = 'INSERT INTO "authors"(
            "id",
            "surname",
            "first_name",
            "second_name",
            "post",
            "unit_id"
        )
        VALUES (
            $1,
            $2,
            $3,
            $4,
            $5,
            $6
        )';

    $q_params = array(
        $mid,
        $this->values->surname,
        $this->values->first_name,
        $this->values->second_name,
        $this->values->post,
        $this->values->unit_id,
    );

    $res = $this->dbquery($query, $q_params);

    if($res) {
        $this->data->errortext='Автор добавлен!';
    }
    else {
        $this->data->errortext='Ошибка записи в БД!';
    }
}
else {
    $this->data->errortext = '<p style="color:#F00;">Не заполнены поля: '. $resultCheckRequiredFields['emptyFields'] .'</p>';
}