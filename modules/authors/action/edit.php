<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues(Array(
    'id',
    'surname',
    'first_name',
    'second_name',
    'post',
    'unit_id',
));

$resultCheckRequiredFields = $checkRequiredFields($this);

if ($resultCheckRequiredFields['success']) {
    $subquery = '';

    if(checkFieldValueInDB($this, 'id', $this->values->unit_id, 'units')){
        $subquery.=' unit_id = '."'".$this->values->unit_id."'";
        $subquery.= ', ';
    }
    else{
        $this->data->errortext = '<p style="color:#F00;">В базе данных не найдено указанное подразделение</p>';
        return;
    }

    if ($this->values->surname != ''){
        $subquery.=' surname = '."'".$this->values->surname."'";
        $subquery.= ', ';
    }

    if ($this->values->first_name != ''){
        $subquery.=' first_name = '."'".$this->values->first_name."'";
        $subquery.= ', ';
    }

    if ($this->values->second_name != ''){
        $subquery.=' second_name = '."'".$this->values->second_name."'";
        $subquery.= ', ';
    }

    if ($this->values->post != ''){
        $subquery.=' post = '."'".$this->values->post."'";
        $subquery.= ', ';
    }

    $subquery = substr($subquery, 0, -2);

    $query = 'UPDATE authors SET '.$subquery.' WHERE id = '.$this->values->id;

    $res = $this->dbquery($query);

    if ($res) {
        $this->data->errortext.='<p style="color: green;">Изменения сохранены!</p>';
    }
    else{
        $this->data->errortext.='<p style="color: #ff0000;">Ошибка изменения автора (не удалось записать изменения в БД).</p>';
    }
}
else {
    $this->data->errortext = '<p style="color:#F00;">Не заполнены поля: '. $resultCheckRequiredFields['emptyFields'] .'</p>';
}