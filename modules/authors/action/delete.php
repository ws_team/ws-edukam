<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->GetPostValues(Array(
    'id'
));

if ($this->values->id != '') {

    $query = 'SELECT "id" FROM "materials" WHERE "author_id" = $1';

    $q_params = array(
        $this->values->id
    );

    $res = $this->dbquery($query, $q_params);

    if(!empty($res)){
        $this->data->errortext='<p style="color:#F00;">Нельзя удалить автора, пока есть материалы, которые связаны с ним.</p>';
        return;
    }

    $query = 'DELETE FROM "authors" WHERE "id" = $1';

    $q_params = array(
        $this->values->id
    );

    $res = $this->dbquery($query, $q_params);

    if($res){
        $this->data->errortext='Запись удалена!';
    }

}