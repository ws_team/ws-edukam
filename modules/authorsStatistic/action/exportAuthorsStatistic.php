<?php

defined('_WPF_') or die();

$statData = getAuthorsStat($this);

if( ! $statData) {
    exit('Статистика по авторам отсутствует');
}

require_once(WPF_ROOT.'/classes/PHPExcel.php');
require_once(WPF_ROOT.'/classes/PHPExcel/Writer/Excel5.php');

$xls = new PHPExcel();

// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);

// Получаем активный лист
$sheet = $xls->getActiveSheet();

// Подписываем лист
$sheet->setTitle('Статистика по авторам');

$sheet->getRowDimension(1)->setRowHeight(20);

$sheet->setCellValue("A1", 'Дата');
$sheet->getStyle('A1')->getFont()->setBold(true);
$sheet->getColumnDimension('A')->setWidth(20);

$sheet->setCellValue("B1", 'ФИО');
$sheet->getStyle('B1')->getFont()->setBold(true);
$sheet->getColumnDimension('B')->setWidth(32);

$sheet->setCellValue("C1", 'Подразделение');
$sheet->getStyle('C1')->getFont()->setBold(true);
$sheet->getColumnDimension('C')->setWidth(40);

$sheet->setCellValue("D1", 'Название материала');
$sheet->getStyle('D1')->getFont()->setBold(true);
$sheet->getColumnDimension('D')->setWidth(40);

$sheet->setCellValue("E1", 'Ссылка на материал');
$sheet->getStyle('E1')->getFont()->setBold(true);
$sheet->getColumnDimension('E')->setWidth(32);

for($row = 0; $row < count($statData); $row++){

    $sheet->getRowDimension($row+2)->setRowHeight(20);

    $sheet->setCellValueByColumnAndRow(0, $row+2, $statData[$row]['material_date_add']);
    $sheet->setCellValueByColumnAndRow(1, $row+2, $statData[$row]['author_surname'] . ' ' . $statData[$row]['author_first_name'] . ' ' . $statData[$row]['author_second_name']);
    $sheet->setCellValueByColumnAndRow(2, $row+2, $statData[$row]['author_unit']);
    $sheet->setCellValueByColumnAndRow(3, $row+2, $statData[$row]['material_name']);
    $sheet->setCellValueByColumnAndRow(4, $row+2, $statData[$row]['material_url']);
}

$fileName = "Статистика материалов по авторам c {$this->values->startdate} до {$this->values->enddate}.xlc";

header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
header ( "Cache-Control: no-cache, must-revalidate" );
header ( "Pragma: no-cache" );
header ( "Content-type: application/vnd.ms-excel" );
header ( "Content-Disposition: attachment; filename=$fileName" );

$objWriter = new PHPExcel_Writer_Excel5($xls);
$objWriter->save('php://output');

$this->endRequest();
