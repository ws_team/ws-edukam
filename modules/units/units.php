<?php
/**
 * @var \iSite $this
 */


defined('_WPF_') or die();

global $units;

$this->data->errortext='';

$this->GetPostValues(Array(
    'id',
    'name'
));

if (!empty($this->values->action)){
    $controller_script = dirname(__FILE__).'/action/'.
        str_replace(array('.', '/', '~', '*', '?'), '', $this->values->action).'.php';
    if (file_exists($controller_script)) {
        include($controller_script);
    } else {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        exit;
    }
}

$units = getUnitsList($this);

$this->data->iH1 = 'Администрирование подразделений';

