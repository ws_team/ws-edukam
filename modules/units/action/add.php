<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();


if ($this->values->name != '') {
    $mid = '';

    $query = 'SELECT MAX("id")+1 "mid" FROM "units"';
    if ($res = $this->dbquery($query))
        $mid = $res[0]['mid'];

    if($mid == '')
        $mid = 1;

    $name = $this->values->name;

    $query = 'INSERT INTO "units"(
            "id",
            "name"
        )
        VALUES (
            $1,
            $2
        )';

    $q_params = array(
        $mid,
        $name
    );

    $res = $this->dbquery($query, $q_params);

    if($res) {
        $this->data->errortext='Подразделение добавлено!';
    }
    else {
        $this->data->errortext='Ошибка записи в БД!';
    }
}
else {
    $this->data->errortext = '<p style="color:#F00;">'.strval("Не заполнено название подразделения!").'</p>';
}