<?php
/**
 * @var \iSite $this
 * @var array $header
 * @var array $outputItems
 * @var string out $fileExt
 * @var string out $mediaType
 * @var string in $sheetName
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

require_once(WPF_ROOT.'/classes/PHP_XLSXWriter-master/xlsxwriter.class.php');

$headerXls = array();
foreach ($header as $col => $colName) {
    if (isset($encoding) && $encoding != 'utf-8')
        $colName = iconv('utf-8', $encoding, $colName);
    //$colName = $col;
    $headerXls[$colName] = $col == 'date' ? 'datetime' : 'string';
}

$xlsOutputItems = array();
foreach ($outputItems as $i => $item) {
    $xlsItem = array();
    foreach ($header as $col => $colName) {
        $dataVal = isset($item[$col]) ? $item[$col] : '';
        if (is_array($dataVal))
            $dataVal = implode("\r\n", $dataVal);
        $dataVal = strip_tags($dataVal);
        $xlsItem[] = $dataVal;
    }
    $xlsOutputItems[] = $xlsItem;
}

$writer = new XLSXWriter();
$writer->setAuthor('wpf');

if ( ! isset($sheetName))
    $sheetName = 'Sheet';

$writer->writeSheet($xlsOutputItems, $sheetName.' '.date('d.m.Y'), $headerXls);
$writer->writeToStdout();

$mediaType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
$fileExt = '.xlsx';

return array($mediaType, $fileExt);