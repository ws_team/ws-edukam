<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->getPostValue('id');

if (empty($this->values->id)) {
    $this->redirect(array(
        'export',
        'action' => 'index',
        'item' => 'survey',
    ));
}

$mattype_id = FindTypeIdByChar($this, 'survey');

require_once(WPF_ROOT.'/templates/khabkrai/partial/survey/functions.php');

$survey_id = $this->values->id;
$survey = MaterialGet($this, $survey_id);
$results = survey_get_results($this, $survey);

$header = array(
    'number' => 'Номер',
    'title' => 'Вопрос/Вариант ответа',
    'num_answers' => 'Количество ответов',
);

$data = array();

$n_question = 1;
foreach ($results as $question) {
    $data[] = array(
        'number' => 'Вопрос №'.$n_question,
        'title' => $question['name'],
        'num_answers' => $question['total_answers'],
    );

    $n_answer = 1;
    foreach ($question['answers'] as $answer) {
        $data[] = array(
            'number' => 'Вариант ответа №'.$n_answer,
            'title' => $answer['answer_text'],
            'num_answers' => $answer['num_answers'],
        );

        ++$n_answer;
    }

    ++$n_question;
}

$sheet_name = 'Survey #'.$survey['id'];

$file_name_base = str_replace(' ', '_', $survey['name']);

return array($header, $data, $sheet_name);
