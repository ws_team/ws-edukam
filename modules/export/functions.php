<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


/**
 * @param iSite $site
 * @return array(header[], data[][])
 */
function export_fetch_subscribers(\iSite $site)
{
    $header = array(
        'id' => 'ID',
        'email' => 'E-mail',
        'time' => 'Дата регистрации',
    );
    $query = 'SELECT "id","email",to_char("time", $1) "time" FROM "subscribers" ORDER BY "email"';
    $q_params = array('DD.MM.YYYY HH:MI');

    $res = $site->dbquery($query, $q_params);
    $items = array();

    if ($res !== false) {
        $items = $res;
    }

    return array($header, $items, 'Подписчики');
}

function export_fetch_survey(\iSite $site, $survey_id)
{
    // @todo
}