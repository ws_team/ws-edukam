<?php
/**
 * Модуль подписок на информационные рассылки.
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */

// если вдруг перестанет работать рассылка, то проверить константу SUBSCRIPTION_TRUSTED_IPADDRS
/*echo $_SERVER['REMOTE_ADDR'];
exit;*/

global
    $subscription_status,
    $error,
    $errorCode;

$this->GetPostValue('action');

require_once(dirname(__FILE__).'/functions.php');
require_once('Subscription_Exception.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

$action = $this->values->action;
try {
    switch ($action) {
        case 'subscribe':
            $subscription_status = subscription_subscribe($this);
            break;
        case 'unsubscribe':// @todo drop
            $subscription_status = subscription_unsubscribe($this);
            break;
        case 'confirm':
            $subscription_status = subscription_confirm($this);
            break;
        case 'unsubscribe_confirm':
            $subscription_status = subscription_unsubscribe_confirm($this);
            break;
        case 'lists':
            break;
        case 'send':
            subscription_check_action_access($this, $action);
            subscription_send($this);
            ob_end_clean();
            exit;
        default:
            $subscription_status = subscription_is_subscribed() ? 'subscribed' : 'not_subscribed';
    }
} catch (Subscription_Exception $ex) {
    $subscription_status = 'error';
    $error = $ex->getMessage();
    $errorCode = $ex->getCode();
}
$this->getLogger()->write(LOG_ERR, __FUNCTION__, 'Error status'.$subscription_status.'), error='.$error.'.');

