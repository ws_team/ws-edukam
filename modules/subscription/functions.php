<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


require_once(__DIR__.'/defines.php');
include_once(WPF_ROOT.'/modules/settings/functions.php');
include_once(__DIR__.'/Subscription_Exception.php');

/**
 * @param iSite $site
 * @return string
 */
function subscription_subscribe(\iSite $site)
{
    $site->GetPostValues(array('email', 'list_id'));
    $email_rx = '/^[-\w.~]+@([a-z][a-z0-9-]*[a-z0-9]\.)+[a-z][a-z0-9-]*[a-z0-9]$/';

    if (empty($site->values->email)) {
        throw new Subscription_Exception('Необходимо указать адрес электронной почты');
    }

    $email = $site->values->email;
    if ( ! preg_match($email_rx, $email)) {
        throw new Subscription_Exception('Необходимо указать корректный адрес электронной почты');
    }

    subscription_validate_csrf_token($site);

    $token_secret = subscription_token_secret($site);
    $token = subscription_subscribe_token_generate($token_secret, $email);
    $url_confirm = subscription_url($site, 'confirm', array(
        'token' => $token,
        'list_id' => $site->values->list_id
    ));

    list($subject, $body) = subscription_get_subscribe_email_data($site, $url_confirm);

    $emailSender = subscription_get_sender($site);

    $emailSender->send($email, $subject, $body);

    return 'subscribe_wait_confirm';
}

function subscription_subscribe_token_generate($token_secret, $email, $list_id = null)
{
    $value = $email.( ! empty($list_id) ? '*'.$list_id : '');
    return subscription_token_generate($token_secret, 'subscribe', $value);
}

function subscription_get_csrf_token(\iSite $site)
{
    return $site->generateCsrfToken();
}

function subscription_validate_csrf_token(\iSite $site)
{
    if ( ! $site->validateCsrfToken())
        subscription_error_bad_request();
}

function subscription_generate_csrf_token(\iSite $site)
{
    return $site->generateCsrfToken();
}

function subscription_get_sender(\iSite $site)
{
    return $site->getMailer();
}

/**
 * @param \iSite $site
 * @return array|string
 */
function subscription_token_secret(\iSite $site)
{
    if ( ! isset($site->settings->subscription) || empty($site->settings->subscription['token_secret']))
        throw new \RuntimeException('Token secret unset');
    return $site->settings->subscription['token_secret'];
}

/**
 * @param string|array $token_secret
 * @param string $type
 * @param string $value
 * @return string
 */
function subscription_token_generate($token_secret, $type, $value)
{
    if (is_array($token_secret))
        $token_secret = $token_secret[0];

    $nonce_len = 18;
    $nonce = openssl_random_pseudo_bytes($nonce_len);
    $token_raw = $nonce.$token_secret.$type.':'.$value;
    $token_enc = sha1($token_raw, true);
    $token_enc_text = base64_encode(pack('C', $nonce_len).$nonce.$token_enc).':'.$type.':'.$value;

    return $token_enc_text;
}

function subscription_token_parse($tokenSecret, $token)
{
    $parts = explode(':', $token);
    $token = base64_decode($parts[0]);
    $nonceLen = unpack('C', substr($token, 0, 1));

    if ( ! is_array($nonceLen) || empty($nonceLen[1]))
        return false;

    $nonceLen = $nonceLen[1];
    $nonce = substr($token, 1, $nonceLen);

    $hashSize = 20;

    foreach ((array)$tokenSecret as $ts) {
        $tokenRaw = $nonce.$ts.$parts[1].':'.$parts[2];
        $tokenRecalc = sha1($tokenRaw, true);
        $tokenIn = substr($token, 1 + $nonceLen, $hashSize);

        if ($tokenIn == $tokenRecalc) {
            return array(
                'type' => $parts[1],
                'value' => $parts[2],
            );
        }
    }

    return false;
}

function subscription_get_subscribe_email_data(\iSite $site, $url_confirm)
{
    $mailBody = subscription_render_template($site, 'subscribe', array(
        'confirmUrl' => $url_confirm,
    ));

    return array(
        'Подписка на новостную рассылку',
        $mailBody,
    );
}

function subscription_render_template(\iSite $site, $name, $params = array())
{
    $template_file = $site->locateTemplate('/mail/subscription/'.$name);

    if ( ! is_file($template_file)) {
        throw new \RuntimeException("Шаблон письма '$name' не существует");// HTTP 500
    }

    extract($params);
    ob_start();
    include($template_file);
    return ob_get_clean();
}

function subscription_unsubscribe(\iSite $site)// @todo drop?
{
    $ss_id = subscription_subscriber_current($site);
    if (empty($ss_id))
        subscription_error_bad_request();
    
    $subscriber = subscription_subscriber_get($site, $ss_id);

    if ( ! empty($subscriber)) {
        $email = $subscriber['email'];
        
        $token = subscription_token_unsubscribe_get($site, $subscriber);

        list($subject, $body) = subscription_get_unsubscribe_mail_data($site, $token);

        $email_sender = subscription_get_sender($site);
        $email_sender->send($email, $subject, $body, array());
    }

    return 'unsubscribe_wait_confirm';
}

/**
 * @param iSite $site
 * @param int|array $subscriber
 * @return string
 */
function subscription_token_unsubscribe_get(\iSite $site, $subscriber)
{
    return subscription_token_get($site, $subscriber, 'unsubscribe');
}

/**
 * @param iSite $site
 * @param $subscriber_id
 * @return bool
 */
function subscription_subscriber_get(\iSite $site, $subscriber_id)
{
    if ( ! $subscriber_id)
        return false;

    $query = 'SELECT * FROM '.SUBSCRIPTION_TABLE.' WHERE id = $1';
    $q_params = array(intval($subscriber_id));
    $res = $site->dbquery($query, $q_params);

    return ! empty($res) ? $res[0] : false;
}

function subscription_subscriber_find(\iSite $site, $email)
{
    $query = 'SELECT * FROM '.SUBSCRIPTION_TABLE.' WHERE "email" = $1';
    $q_params = array($email);
    $res = $site->dbquery($query, $q_params);
    return ! empty($res) ? $res[0] : false;
}

function subscription_token_subscribe_get(\iSite $site, $subscriber)
{
    return subscription_token_get($site, $subscriber, 'subscribe');
}

function subscription_token_generate_store(\iSite $site, $type, $email)
{
    $token = subscription_token_generate(subscription_token_secret($site), $type, $email);

    $column = $type == 'confirm' ? 'subscribe' : str_replace('_confirm', '', $type);
    $column = 'token_'.$column;

    $table = SUBSCRIPTION_TABLE;
    $query = <<<EOS
UPDATE "$table" SET "$column" = $1 WHERE email = $2
EOS;

    $q_params = array(
        $token,
        $email,
    );
    $site->dbquery($query, $q_params);
    return $token;
}

function subscription_get_unsubscribe_mail_data(\iSite $site, $token, $list_id = null)
{
    $url_params = array('token' => $token);
    if ( ! empty($list_id))
        $url_params['list_id'] = $list_id;

    $confirm_url = subscription_url($site, 'unsubscribe_confirm', $url_params);
    $body = subscription_render_template($site, 'unsubscribe', array(
        'confirmUrl' => $confirm_url,
    ));

    return array(
        'Прекращение подписки - подтверждение',
        $body,
    );
}

function subscription_url(\iSite $site, $action = null, $params = array())
{
    $args = array();

    if ($action)
        $args[] = 'action='.$action;

    foreach ($params as $name => $val)
        $args[] = $name.'='.rawurlencode($val);

    return $site->getAbsoluteUrl('/subscription?'.implode('&', $args));
}

/**
 * @param \iSite $site
 * @return string
 */
function subscription_unsubscribe_confirm(\iSite $site)
{
    $site->GetPostValue('token');

    if (empty($site->values->token)) {
        subscription_error_bad_request();
    }

    $token_parsed = subscription_token_parse(subscription_token_secret($site), $site->values->token);
    if ($token_parsed === false)
        subscription_error_bad_request();

    if ($token_parsed['type'] != 'unsubscribe')
        subscription_error_bad_request();

    $list_id = null;
    $email = explode('*', $token_parsed['value']);
    if (count($email) > 1)
        $list_id = $email[1];
    $email = $email[0];

    if (empty($list_id)) {
        $site->getPostValue('list_id');
        $list_id = $site->values->list_id;
    }

    $subscriber = subscription_subscriber_find($site, $email);

    //var_dump($subscriber);exit;

    $status = 'unsubscribed';

    if ( ! empty($subscriber)) {
        $ss_id = $subscriber['id'];

        if ( ! empty($list_id)) {
            $status = 'list_subscription';
            if ( ! subscription_list_subscriber_remove($site, $list_id, $ss_id)) {
                $status = 'error';
            }
        } else {
            subscription_subscriber_remove($site, $ss_id);
        }
    }

    /*if (empty($list_id))
        subscription_cookie($site, null);*/

    return $status;
}

function subscription_cookie(\iSite $site, $value)
{
    $site->addCookie(
        SUBSCRIPTION_COOKIE,
        $value,
        time() + (is_null($value) ?  -1 : 3600 * 24 * 180),
        '/'
    );

    if (is_null($value))
        unset($_COOKIE[SUBSCRIPTION_COOKIE]);
    else
        $_COOKIE[SUBSCRIPTION_COOKIE] = $value;
}

function subscription_error($message)
{
    throw new Subscription_Exception($message);
}

function subscription_error_bad_request()
{
    subscription_error('Система выявила некорректный запрос');
}

function subscription_confirm(\iSite $site)
{
    $site->GetPostValue('token');
    if (empty($site->values->token)) {
        subscription_error_bad_request();
    }

    $token_orig = $site->values->token;
    $token = subscription_token_parse(subscription_token_secret($site), $token_orig);
    if ($token === false)
        subscription_error_bad_request();
    if ($token['type'] != 'subscribe')
        subscription_error_bad_request();

    $value = explode('*', $token['value']);
    $email = $value[0];
    $list_id = count($value) > 1 ? $value[1] : null;

    if (empty($list_id)) {
        $site->getPostValue('list_id');
        $list_id = $site->values->list_id;
    }

    //var_dump(__FILE__,__LINE__,$token, $email);exit;

    $table = SUBSCRIPTION_TABLE;

    $query = <<<EOS
INSERT INTO "$table"("email")
SELECT CAST($1 AS CHARACTER VARYING)
WHERE NOT EXISTS(
    SELECT 1 FROM "$table" WHERE "email" = CAST($1 AS CHARACTER VARYING)
)
EOS;

    $q_params = array($email);

    $site->dbquery($query, $q_params);

    $query = <<<EOS
SELECT "id" FROM "$table" WHERE "email" = CAST($1 AS CHARACTER VARYING)
EOS;

    $res = $site->dbquery($query, array($email));

    if (empty($res)) {
        $site->getLogger()->write(LOG_ERR, __FUNCTION__, 'failed to insert subscriber record('.$email.')');
        return 'error';
    }

    $ss_id = $res[0]['id'];

    $query = <<<EOS
UPDATE "$table" SET token_subscribe = $1 WHERE id = $2
EOS;
    $q_params = array($token_orig, $ss_id);
    $site->dbquery($query, $q_params);

    $status = 'subscribed';

    if ($list_id) {
        $site->values->list_id = $list_id;
        $status = subscription_list_subscriber_add($site, $list_id, $ss_id) ? 'list_subscription' : 'error';
    }

    subscription_cookie($site, $token_orig);

    return $status;
}

function subscription_list_subscriber_add(\iSite $site, $list_id, $ss_id)
{
    $query = <<<EOS
INSERT INTO list_subscribers(list_id, subscriber_id)
SELECT $1, $2
    WHERE NOT EXISTS(SELECT 1 FROM list_subscribers WHERE list_id = $1 AND subscriber_id = $2)
EOS;

    $q_params = array(intval($list_id), intval($ss_id));

    $res = $site->dbquery($query, $q_params);
    return ! empty($res);
}

function subscription_list_subscriber_remove(\iSite $site, $list_id, $ss_id)
{
    $query = 'DELETE FROM '.SUBSCRIPTION_TABLE_LIST_SUBSCRIBER.
        ' WHERE subscriber_id = $1 AND list_id = $2';
    $q_params = array($ss_id, $list_id);
    $res = $site->dbquery($query, $q_params);
    return $res !== false;
}

function subscription_subscriber_remove(\iSite $site, $ss_id)
{
    $site->dbquery('DELETE FROM '.SUBSCRIPTION_TABLE.' WHERE id = $1', array($ss_id));
    // @todo сделать на триггерах:
    $site->dbquery('DELETE FROM '.SUBSCRIPTION_TABLE_SENDQUEUE.' WHERE subscriber_id = $1', array($ss_id));
    $site->dbquery('DELETE FROM '.SUBSCRIPTION_TABLE_LIST_SUBSCRIBER.' WHERE subscriber_id = $1', array($ss_id));
}

/**
 * Высылает письмо подписчику на список рассылки.
 *
 * @param iSite $site
 * @param array $list_info
 * @param array $items
 * @param string $email
 * @param string $token_unsub
 */
function subscription_send_mail(\iSite $site, $list_info, $items, $email, $token_unsub, $token_sub)
{
    list($subject, $mailBody) = subscription_mail_get_data($site, $list_info, $items, $token_unsub, $token_sub);

    $mailSender = subscription_get_sender($site);
    $mailSender->send($email, $subject, $mailBody);
}

/**
 * @param iSite $site
 * @param array $list_info
 * @param array $items
 * @param string $token_unsub
 * @param string $token_sub
 *
 * @return array(subject, body)
 */
function subscription_mail_get_data(\iSite $site, $list_info, $items, $token_unsub, $token_sub)
{
    $subject = !empty($list_info['params']['subject']) ?
        $list_info['params']['subject'] :
        subscription_settings($site, 'subject', $site->settings->email->signature);

    $url_params = array(
        'token' => $token_unsub,
    );
    if ( ! empty($list_info) && ! empty($list_info['id']))
        $url_params['list_id'] = $list_info['id'];

    $unsub_url = subscription_url($site, 'unsubscribe_confirm', $url_params);

    $tpl_name = isset($list_info['params']['body_template']) ?
        $list_info['params']['body_template'] :
        'news';
    $bodyTpl = subscription_render_template($site, $tpl_name, array(
        'items' => $items,
        'unsubscribeUrl' => $unsub_url,
        'list' => $list_info,
    ));

    $body = strtr($bodyTpl, array('{UBSUBSCRIBE_URL}' => htmlspecialchars($unsub_url)));
    return array($subject, $body);
}

function subscription_is_subscribed()
{
    return ! empty($_COOKIE[SUBSCRIPTION_COOKIE]);
}

function subscription_get_unsubscribe_url(\iSite $site)
{
    return subscription_url($site, 'unsubscribe', array(
        'csrf' => subscription_get_csrf_token($site),
    ));
}

/**
 * @param iSite $site
 * @param int $mattype_id
 * @return bool|int
 */
function subscription_list_match(\iSite $site, $mattype_id)
{
    $table = SUBSCRIPTION_TABLE_LIST;
    $query = <<<EOS
SELECT id, params FROM $table ORDER BY "priority"
EOS;

    $lists = $site->dbquery($query);

    foreach ($lists as $list_info) {
        $params = unserialize($list_info['params']);
        $mattypes = (array)$params['material_types'];
        if (in_array($mattype_id, $mattypes))
            return $list_info['id'];
    }

    $def_list = subscription_list_default_info($site);
    $mattypes = $def_list['params']['material_types'];
    if (in_array($mattype_id, $mattypes))
        return 0;

    return false;
}

/**
 * @param \iSite $site
 * @param int $type_id
 * @param int $material_id
 * @param int $schedule_type
 */
function subscription_queue_item_add(\iSite $site, $type_id, $material_id, $schedule_type)
{

    $list_id = subscription_list_match($site, $type_id);

    if ($list_id === false)
        return;

    $schedule_time  = subscription_list_item_schedule_time($site, $list_id, $schedule_type);
    $table          = SUBSCRIPTION_TABLE;
    $table_sendq    = SUBSCRIPTION_TABLE_SENDQUEUE;

    // запрос на изменение записи будущей рассылки

    $query = <<<EOS
UPDATE $table_sendq
SET schedule_time = $3
WHERE
    list_id = $1
    AND material_id = $2
    AND subscriber_id IN(SELECT id FROM $table)
    AND schedule_time > $3
EOS;
    $q_params = array($list_id, $material_id, $schedule_time->format('Y-m-d H:i:s'));

    $site->dbquery($query, $q_params);

    // запрос на вставку новой записи, если рассылка этого материала не осуществлялась

    $query = <<<EOS
INSERT INTO $table_sendq(list_id, material_id, subscriber_id, schedule_time)
SELECT $1, $2, id, $3
FROM $table s
WHERE NOT EXISTS(
    SELECT 1 FROM $table_sendq WHERE list_id = $1 AND material_id = $2 AND subscriber_id = s.id
)
EOS;

    $site->dbquery($query, $q_params);

    //krumo($query);
    //krumo($q_params);

    //exit;

}

/**
 * @param iSite $site
 * @param int $list_id
 * @param int $schedule_type
 * @return \DateTime
 */
function subscription_list_item_schedule_time(\iSite $site, $list_id, $schedule_type)
{
    $tz_app = new DateTimeZone(date_default_timezone_get());
    $tz_db = new DateTimeZone(DB_TIMEZONE);
    $now = new DateTime('now', $tz_app);
    $schedule_time = $now;

    switch ($schedule_type) {
        case SUBSCRIPTION_SEND_DAILY:
        default:
            $list_info = subscription_list_info($site, $list_id);

            //krumo($list_info);

            $time_hr = intval($now->format('H'));

            $send_at = $list_info['params']['schedule'];
            sort($send_at);

            $send_at[] = $next_send_hr = $send_at[0] + 24;

            foreach ($send_at as $hr) {
                $next_send_hr = $hr;
                if ($hr > $time_hr)
                    break;
            }

            $diff_hr = ($next_send_hr - $time_hr) % 24;
            $diff_min = 0;

            if ($diff_hr > 0) {
                $min = intval($now->format('i'));
                if ($min) {
                    $diff_hr--;
                    $diff_min = 60 - $min;
                }
            }

            $schedule_time->add(new DateInterval('PT'.$diff_hr.'H'.(isset($diff_min) ? $diff_min.'M' : '')));

            break;
        case SUBSCRIPTION_SEND_IMMEDIATE:
            break;
    }

    $schedule_time->setTimezone($tz_db);

    return $schedule_time;
}

/**
 * Рассылка запланированных материалов.
 * Выбирается не более SUBSCRIPTION_SEND_BATCH_SIZE материалов из очереди.
 * Выбранные материалы группируются по подписчикам и по спискам рассылки, для каждой комбинации список/подписчик
 * высылается письмо со всеми отобранными материалами, не более SUBSCRIPTION_MAIL_MAX_ITEMS в одном письме.
 * Те материалы, которые попали в письмо, помечаются как "доставленные" (просто удаляются из очереди).
 * Если материал элемента очереди в статусе "удален", он сразу удаляется из очереди.
 *
 * @param iSite $site
 */
function subscription_send(\iSite $site)
{
	//$hour=(int)date('H');

	//return false;

	/*if($hour > 7 && $hour < 22)
	{
		return false;
	}*/

    $site->getPostValues(array('limit', 'filter', 'list'));

    $limit = ! empty($site->values->limit) ? $site->values->limit : SUBSCRIPTION_SEND_BATCH_SIZE;
    if ($limit > SUBSCRIPTION_SEND_BATCH_SIZE)
        $limit = SUBSCRIPTION_SEND_BATCH_SIZE;

    $table_queue        = SUBSCRIPTION_TABLE_SENDQUEUE;
    $table_subscribers  = SUBSCRIPTION_TABLE;
    $sqlIntervalQuery   = "INTERVAL '" . SUBSCRIPTION_SEND_INTERVAL_MINUTES . " MINUTE'" ;

    $q_params = array();

    $query_filter = array('');

    if ( ! empty($site->values->filter)) {
        $q_params[] = '%'.$site->values->filter.'%';
        $query_filter[] = 's.email LIKE $'.count($q_params);
    }

    $query_filter = implode(' AND ', $query_filter);

    $query = <<<EOS
SELECT
    s.id "subscriber_id",
    s."email",
    s."token_subscribe",
    s."token_unsubscribe",
    q."list_id",
    q."material_id",
    m."name",
    m."id_char",
    m."type_id",
    m."date_event",
    m."date_edit",
    m."anons",
    m."content",
    m."status_id",
    t.name "type_name"
 FROM
    "$table_queue" "q"
     INNER JOIN "$table_subscribers" "s" ON s.id = q.subscriber_id
     INNER JOIN materials m ON m.id = q.material_id
     INNER JOIN material_types t ON t.id = m.type_id
 WHERE
    q.schedule_time <= NOW() AND
    q.schedule_time >= NOW() - $sqlIntervalQuery
    $query_filter
 ORDER BY q.schedule_time ASC
 LIMIT $limit
EOS;

    $data = $site->dbquery($query, $q_params);

    $delivery = array();
    $materials = array();

    $tz = new DateTimeZone(DB_TIMEZONE);

    file_put_contents(WPF_ROOT.'/../drop/query-'.date('Ymd').'.sql', $query);
    
    foreach ($data as $item) {

        /*if($item['email']!='n.samoilenko@white-soft.ru' && $item['email']!='samoilenkonikita@mail.ru'){
            continue;
        }*/

        $ss_id = $item['subscriber_id'];
        $list_id = $item['list_id'];

        if ($item['status_id'] != STATUS_ACTIVE) {
            if ($item['status_id'] == STATUS_DELETED) {
                subscription_queue_item_remove($site, $ss_id, $item['material_id'], $list_id);
            }
            continue;
        }

        if ( ! isset($delivery[$list_id]))
            $delivery[$list_id] = array();

        if ( ! isset($delivery[$list_id][$ss_id])) {
            if (empty($item['token_unsubscribe']))
                $item['token_unsubscribe'] = subscription_token_unsubscribe_get($site, $item);
            if (empty($item['token_subscribe']))
                $item['token_subscribe'] = subscription_token_subscribe_get($site, $item);

            $delivery[$list_id][$ss_id] = array(
                'email' => $item['email'],
                'token' => $item['token_unsubscribe'],// @todo drop
                'token_subscribe' => $item['token_subscribe'],
                'token_unsubscribe' => $item['token_unsubscribe'],
                'items' => array(),
            );
        }
        if (count($delivery[$list_id][$ss_id]['items']) > SUBSCRIPTION_MAIL_MAX_ITEMS)
            continue;

        $matId = $item['material_id'];

        if ( ! isset($materials[$matId])) {
            $itemText = strip_tags(! empty($item['anons']) ? $item['anons'] : $item['content']);
            $itemText = susbcription_cut_item_text($itemText);
            $itemImage = null;
            $itemCategory = array(
                'url' => $site->getAbsoluteUrl(GetMaterialTypeUrl($site, $item['type_id'])),
                'name' => $item['type_name'],
            );

            $item_time = new \DateTime($item['date_event'], $tz);
            $item_time_update = new \DateTime($item['date_edit'], $tz);

            $materials[$matId] = array(
                'id' => $matId,
                'title' => $item['name'],
                'url' => $itemCategory['url'].'/'.$item['id_char'],
                'text' => $itemText,
                'image' => $itemImage,
                'category' => $itemCategory,
                'time' => $item_time,
                'time_update' => $item_time_update,
                'params' => MaterialGetParams($site, $item['type_id'], $matId),
            );
        }

        $delivery[$list_id][$ss_id]['items'][] = $materials[$matId];
    }

    $num_sent = 0;

    foreach ($delivery as $list_id => $subscribers) {
        $list_info = subscription_list_info($site, $list_id);
        if ($list_info === false)
            continue;

        foreach ($subscribers as $ss_id => $del_info) {

            subscription_send_mail($site,
                $list_info,
                $del_info['items'],
                $del_info['email'],
                $del_info['token_unsubscribe'],
                $del_info['token_subscribe']
            );

            $items_id = array();
            foreach ($del_info['items'] as $mat)
                $items_id[] = $mat['id'];

            // раскомментировать для удаления записей в таблице subscription_sendqueue после отправки
            //subscription_mark_delivered($site, $ss_id, $items_id, $list_id);

            // так как писем может быть очень много, я задал очень большой лимит - 20000
            if (++$num_sent >= SUBSCRIPTION_SEND_MAIL_LIMIT)
                break;
        }
    }
}

/**
 * @param iSite $site
 * @param $list_id
 * @return bool
 */
function subscription_list_info(\iSite $site, $list_id)
{
    if (empty($list_id)) {
        $ret = subscription_list_default_info($site);
    } else {
        $table = SUBSCRIPTION_TABLE_LIST;
        $query = <<<EOS
SELECT name, params FROM $table WHERE id=$1
EOS;
        $q_params = array($list_id);
        $res = $site->dbquery($query, $q_params);

        if ($res === false) {
            $site->getLogger()->write(LOG_WARNING, __FUNCTION__, "List #$list_id not found");
            return false;
        }

        $ret = array(
            'id' => $list_id,
            'name' => $res[0]['name'],
            'params' => unserialize($res[0]['params']),
        );
    }

    if (empty($ret['params']['schedule'])) {
        $ret['params']['schedule'] = isset($site->settings->subscription['send_at_hours']) ?
            $site->settings->subscription['send_at_hours'] :
            array(10, 18);
    }

    return $ret;
}

function subscription_list_default_info(\iSite $site)
{
    $schedule = subscription_settings($site, 'schedule');
    if (empty($schedule) && isset($site->settings->subscription['send_at_hours']))
        $schedule = $site->settings->subscription['send_at_hours'];

    $materialtypes = subscription_settings($site, 'material_types');

    $params = array(
        'subject' => subscription_settings($site, 'subject'),
        'body_template' => 'news',
        'schedule' => $schedule,
        'material_types' => $materialtypes,
    );

    $params = subscription_list_params_normalize($site, $params);

    return array(
        'id' => '0',
        'name' => 'Новости',
        'description' => 'Последние события, новости',
        'params' => $params,
    );
}

function subscription_queue_item_remove(\iSite $site, $ss_id, $mat_id, $list_id)
{
    $table = SUBSCRIPTION_TABLE_SENDQUEUE;
    $query = <<<EOS
DELETE FROM $table
WHERE
    subscriber_id = $1
    AND material_id IN($2)
EOS;

    $q_params = array($ss_id, $mat_id);

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $site->dbquery($query, $q_params);
}

function remove_future_items_subscription_queue(\iSite $site, $mat_id)
{
    $table  = SUBSCRIPTION_TABLE_SENDQUEUE;
    $now    = time();
    $query  = <<<EOS
DELETE FROM $table
WHERE material_id IN($1) AND schedule_time > $2
EOS;

    $q_params = array($mat_id, date('Y-m-d H:i:s', $now));

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    $site->dbquery($query, $q_params);
}

function get_subscribe_send_schedule(\iSite $site, $mat_id)
{
    $table = SUBSCRIPTION_TABLE_SENDQUEUE;
    $query = <<<EOS
SELECT DISTINCT schedule_time FROM $table
WHERE material_id = $1
ORDER BY schedule_time DESC
EOS;

    $q_params = array($mat_id);

    list($query, $q_params) = $site->dbqueryExpand($query, $q_params);

    return $site->dbquery($query, $q_params);
}

/**
 * Удаляет из очереди "устаревшие" элементы - с запланированной датой в прошлом за порогом устаревания.
 * @param iSite $site
 */
function subscription_queue_purge(\iSite $site)
{
    $treshold = 3600 * 24 * 7;
    if (isset($site->settings->subscription['queue_obsolescence_treshold']))
        $treshold = $site->settings->subscription['queue_obsolescence_treshold'];
    $query = <<<EOS
DELETE FROM subscription_sendqueue
WHERE EXTRACT(EPOCH FROM schedule_time) < (EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) - $1)
EOS;

    $q_params = array($treshold);
    $site->dbquery($query, $q_params);
}

function susbcription_cut_item_text($text)
{
    $srcLen = mb_strlen($text);
    if ($srcLen > SUBSCRIPTION_ITEM_TEXT_MAXLEN) {

        $text = preg_replace('/\s{2,}/', ' ', $text);

        $partial = array();
        $partialLen = 0;

        $srcParts = explode(' ', $text);

        foreach ($srcParts as $i => $part) {
            $partLen = mb_strlen($part);
            if ($partialLen + $partLen > SUBSCRIPTION_ITEM_TEXT_MAXLEN)
                break;
            $partial[] = $part;
            $partialLen += $partLen;
        }
        $text = implode(' ', $partial).'...';
    }
    return $text;
}

function subscription_mark_delivered(\iSite $site, $subscriber_id, $mat_id, $list_id)
{
    subscription_queue_item_remove($site, $subscriber_id, $mat_id, $list_id);
}

function subscription_settings(\iSite $site, $name, $fallback = null)
{
    $ret = $fallback;

    if (isset($site->settings->subscription[$name]))
        $ret = $site->settings->subscription[$name];

    if (function_exists('settings_get'))
        $ret = settings_get($site, 'subscription.'.$name, $ret);

    if (is_array($fallback) && ! is_array($ret)) {
        $ret = is_string($ret) ? explode(',', $ret) : (array)$ret;
    }

    return $ret;
}

function subscription_is_eligible_item(\iSite $site, $material)
{
    return subscription_list_match($site, $material['type_id']) !== false;
}

function subscription_is_candidate_item(\iSite $site, $material)
{
    if ($material['status_id'] == STATUS_HIDDEN && $material['status_id'] == STATUS_DELETED)
        return false;

    $tz = new DateTimezone(DB_TIMEZONE);
    $now = new DateTime('now', $tz);

    if ( ! empty($material['date_event'])) {
        $date = new DateTime($material['date_event'], $tz);
    } else {
        $date = new DateTime($material['date_add'], $tz);
    }

    $diff = $date->diff($now);

    if ($diff->invert)
        return false;

    if ($diff->y || $diff->m || $diff->d > 3)
        return false;

    return true;
}

/**
 * Завершает скрипт, если нет прав на выполнение действия.
 *
 * @param iSite $site
 * @param string $action
 */
function subscription_check_action_access(\iSite $site, $action)
{
    // если скрипт вызывается из командной строки, то все норм
    if (php_sapi_name() == 'cli')
        return;

    // для всех сайтов определен только один разрешенный ip адрес - это 127.0.0.1
    // если скрипт вызван с локального хоста, то все норм
    if (defined('SUBSCRIPTION_TRUSTED_IPADDRS')) {
        $trustedAddrs = explode(',', constant('SUBSCRIPTION_TRUSTED_IPADDRS'));
        $ipAddr = $_SERVER['REMOTE_ADDR'];
        if (in_array($ipAddr, $trustedAddrs))
            return;
    }

    $site->GetPostValue('api_key');

    // если скрипт вызван не с локального хоста и не из командной строки, то проверяем API KEY
    // если API KEY неправильный, то завершаем программу
    if (empty($site->values->api_key)
        || !subscription_validate_api_key($site, $site->values->api_key, $action)) {
        if (php_sapi_name() != 'cli') {
            $site->endRequest(403);
        }
        exit;
    }
}

function subscription_validate_api_key(\iSite $site, $apiKey, $action)
{
    return $site->settings->subscription['api_key'] == $apiKey;
}

function subscription_url_list_subscribe(\iSite $site, $list_id, $email = null)
{
    if (is_null($email)) {
        $token = subscription_token_get(
            $site,
            subscription_subscriber_get($site,
                subscription_subscriber_current($site)
            ),
            'subscribe'
        );
    } else {
        $token = subscription_token_generate(subscription_token_secret($site), 'confirm', $email);
    }

    $url_params = array(
        'token' => $token
    );
    if ( ! empty($list_id))
        $url_params['list_id'] = $list_id;

    return subscription_url($site, 'confirm', $url_params);
}

/**
 * @param iSite $site
 * @param array|int $subscriber
 * @param string $type
 * @return bool|string
 */
function subscription_token_get(\iSite $site, $subscriber, $type)
{
    if (empty($subscriber) && $type == 'subscribe') {
        if (isset($_COOKIE[SUBSCRIPTION_COOKIE]) && ! is_numeric($_COOKIE[SUBSCRIPTION_COOKIE]))
            return $_COOKIE[SUBSCRIPTION_COOKIE];
    }

    if ( ! is_array($subscriber)) {
        $subscriber = subscription_subscriber_get($site, $subscriber);
        if (empty($subscriber)) {
            return false;
        }
    }

    $column = 'token_'.str_replace('_confirm', '', $type);
    if (empty($subscriber[$column])) {
        $subscriber[$column] = subscription_token_generate_store($site, $type, $subscriber['email']);
    }

    return $subscriber[$column];
}

function subscription_list_subscribe_action(\iSite $site, $action)
{// @todo
    list($list_id, $subscriber_id) = subscription_list_subscribe_action_params($site);

    $table_list_ss = SUBSCRIPTION_TABLE_LIST_SUBSCRIBER;

    if ($action == 'subscribe') {
        $query = <<<EOS
INSERT INTO $table_list_ss(list_id, subscriber_id)
SELECT $1, $2 WHERE NOT EXISTS(
    SELECT 1 FROM $table_list_ss WHERE list_id = $1 AND subscriber_id = $2
)
EOS;
    } else {
        $query = <<<EOS
DELETE FROM $table_list_ss WHERE list_id = $1 AND subscriber_id = $2
EOS;
    }

    $q_params = array($list_id, $subscriber_id);
    $res = $site->dbquery($query, $q_params);

    if ($res === false) {
        $log_id = $site->getLogger()->write(LOG_ERR, __FUNCTION__,
            sprintf('Failed to %s subscriber %d to/from list %d', $action, $subscriber_id, $list_id));
        $message = "Сбой на сервере. Техничские детали: *J{$log_id}*";
        throw new \RuntimeException($message);
    }

    return 'list_subscription';
}

function subscription_subscriber_current(\iSite $site)
{
    static $ss_id;

    if (is_null($ss_id)) {
        $ss_id = false;
        if ( ! empty($_COOKIE[SUBSCRIPTION_COOKIE])) {
            $token = $_COOKIE[SUBSCRIPTION_COOKIE];

            if (is_numeric($token) && time() < 1472000000) {
                $ss_id = $token;
                $token = subscription_token_get($site, $ss_id, 'subscribe');
                subscription_cookie($site, $token);
                return $ss_id;
            }

            $query = 'SELECT id FROM '.SUBSCRIPTION_TABLE.' WHERE token_subscribe = $1';
            $q_params = array($token);
            $res = $site->dbquery($query, $q_params);
            if ( ! empty($res)) {
                $ss_id = $res[0]['id'];
            }
        }
    }

    return $ss_id;
}

function subscription_list_subscribe_action_params(\iSite $site)
{
    if ( ! $site->validateCsrfToken())
        $site->badRequest();

    $site->getPostValue('list_id');
    $list_id = intval($site->values->list_id);
    if (empty($list_id))
        $site->badRequest();

    $subscriber_id = intval(subscription_subscriber_current($site));
    if ( ! $subscriber_id)
        $site->badRequest();

    return array($list_id, $subscriber_id);
}

function subscription_get_lists(\iSite $site)
{
    return subscription_list_getall($site);
}

function subscription_list_getall(\iSite $site)
{
    $ret = array();
    if (subscription_settings($site, 'default_list', true)) {
        $ret[] = subscription_list_default_info($site);
    }

    $table = SUBSCRIPTION_TABLE_LIST;
    $query = <<<EOS
SELECT id, name, description, "params" FROM $table ORDER BY "priority"
EOS;
    $res = $site->dbquery($query);
    if ( ! empty($res)) {
        foreach ($res as $item) {
            if (empty($item['params'])) {
                $item['params'] = array();
            } else {
                $item['params'] = unserialize($item['params']);
            }

            $item['params'] = subscription_list_params_normalize($site, $item['params']);

            $ret[] = $item;
        }
    }

    return $ret;
}

function subscription_list_params_normalize(\iSite $site, $params)
{
    if ( ! isset($params['schedule']))
        $params['schedule'] = array(10, 18);

    if ( ! isset($params['material_types']))
        $params['material_types'] = array(MATTYPE_EVENT, MATTYPE_NEWS, MATTYPE_ANNOUNCE);

    if ( ! isset($params['subject'])) {
        $at_site_name = isset($site->settings->subscribe) && !empty($site->settings->subscribe['at_site_name']) ?
            $site->settings->subscribe['at_site_name'] :
            '';
        $params['subject'] = str_replace('{at_site_name}', $at_site_name, SUBSCRIPTION_DAILY_SUBJECT);
    }

    return $params;
}

/**
 * @param iSite $site
 * @param $list_id
 * @return bool
 */
function subscription_list_subscribed(\iSite $site, $list_id)
{
    $ss_id = subscription_subscriber_current($site);
    if (empty($ss_id))
        return false;

    if ( ! is_null($list_id) && intval($list_id) == 0)
        return true;

    $table = SUBSCRIPTION_TABLE_LIST_SUBSCRIBER;
    $query = "SELECT 1 FROM $table WHERE list_id = $1 AND subscriber_id = $2";
    $res = $site->dbquery($query, array($list_id, $ss_id));
    return ! empty($res);
}

/**
 * @param iSite $site
 * @return bool
 */
function subscription_nondefault(\iSite $site)
{
    return ! (bool)subscription_settings($site, 'default_list', true)
        || count(subscription_list_getall($site)) > 1;
}

/**
 * @param iSite $site
 * @param $list_id
 * @return array|bool
 */
function subscription_list(\iSite $site, $list_id)
{
    $table = SUBSCRIPTION_TABLE_LIST;
    $query = <<<EOS
SELECT * FROM "$table" WHERE id = $1
EOS;
    $q_params = array($list_id);
    $ret = $site->dbquery($query, $q_params);

    if (empty($ret))
        return false;

    return $ret[0];
}

/**
 * @param $site
 * @param ?int $list_id
 * @return string
 */
function subscription_url_unsubscribe($site, $list_id = null)
{
    return subscription_url_subscribe_action($site, 'unsubscribe', $list_id);
}

/**
 * @param $site
 * @param ?int $list_id
 * @return string
 */
function subscription_url_unsubscribe_confirm($site, $list_id = null)
{
    return subscription_url_subscribe_action($site, 'unsubscribe', $list_id);
}

function subscription_url_subscribe_action(\iSite $site, $action, $list_id = null)
{
//    var_dump(__FILE__,__LINE__,subscription_subscriber_current($site));exit;
    $token = subscription_token_get($site, subscription_subscriber_current($site), $action);

    $url_params = array(
        'token' => $token,
    );
    if ( ! empty($list_id))
        $url_params['list_id'] = $list_id;

    return subscription_url($site, $action == 'subscribe' ? 'confirm' : $action.'_confirm', $url_params);
}

function subscription_url_subscribe(\iSite $site, $list_id = null)
{
    return subscription_url_subscribe_action($site, 'subscribe', $list_id);
}
