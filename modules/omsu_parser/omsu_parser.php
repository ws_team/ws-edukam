<?php
/**
 * Created by PhpStorm.
 * User: Raven
 * Date: 11.02.2015
 * Time: 16:23
 */



function exists_settle_with_oktmo($site, $oktmo, $settle_type_id, $settle_oktmo_param_id) {
    $query = 'SELECT count("id") "m_count" FROM "materials" WHERE "type_id" = '.$settle_type_id.
        ' AND "id" IN (SELECT "material_id" FROM "extra_materials" WHERE "type_id" = '.$settle_oktmo_param_id.
        ' AND "valuename" = \''.$oktmo.'\')';

    if($res = $site->dbquery($query)) {
        return $res[0]['m_count'] !=  0;
    }

    return true;
}

function get_omsu_by_region_id($site, $omsu_type_id, $region_id) {
    $query = 'SELECT "id" FROM "materials" WHERE "type_id" = '.$omsu_type_id.' AND ("id" IN
	(SELECT "material_parent" FROM "material_connections" WHERE "material_child" = '.$region_id.')
	OR "id" IN (SELECT "material_child" FROM "material_connections" WHERE "material_parent" = '.$region_id.'))';

    if($res = $site->dbquery($query)) {
        if(count($res) > 0) {
            return intval($res[0]["id"]);
        }
    }

    return null;
}

function get_material_id_by_name($site, $material_type_id, $name) {
    $query = 'SELECT "id" FROM "materials" WHERE "type_id"='.$material_type_id.' AND "language_id" = 1 AND "name" = \''
        .$name.'\'';

    if($res = $site->dbquery($query)) {
        if(count($res) > 0) {
            return intval($res[0]["id"]);
        }
    }

    return null;
}

function get_material_id_by_oktmo($site, $material_oktmo, $material_type_id, $material_oktmo_param_id) {
    $query = 'SELECT "id" FROM "materials" WHERE "type_id" = '.$material_type_id.
        ' AND "id" IN (SELECT "material_id" FROM "extra_materials" WHERE "type_id" = '.$material_oktmo_param_id.
        ' AND "valuename" = \''.$material_oktmo.'\')';

    if($res = $site->dbquery($query)) {
        if(count($res) > 0) {
            return intval($res[0]["id"]);
        }
    }

    return null;
}

//функция добавления контакта
function DoAddCont($site){

    $err=false;
    $errtext='';

    if($site->values->addr != '')
    {

        $cid='';
        //получаем id
        $query='SELECT MAX("id")+1 "cid" FROM "contacts"';
        if($res=$site->dbquery($query))
        {
            if(count($res) > 0)
            {
                $cid=$res[0]['cid'];
            }
        }

        if($cid == '')
        {
            $cid = 1;
        }

        //заносим контакт
        $query='INSERT INTO "contacts" ("id","material_id","addr","tel","email","fax","building","office","city","latitude","longitude")
        VALUES('.$cid.', '.$site->values->id.', '."'".$site->values->addr."'".','."'".$site->values->tel."'".',
        '."'".$site->values->email."'".','."'".$site->values->fax."'".',
        '."'".$site->values->building."'".','."'".$site->values->office."'".','."'".$site->values->city."'".',
        '."'".$site->values->latitude."'".','."'".$site->values->longitude."'".')';


        if($res=$site->dbquery($query))
        {
            $err=false;
            $errtext=' - контакт успешно сохранен!';
        }


    }
    else
    {
        $err=true;
        $errtext=' - ошибка: не заполнен адресс контакта';
    }

    return Array(!$err,$errtext);

}

function parse_omsus($site, $filepath) {

    $omsu_type_id = 162;

    $region_type_id = 21;
    $region_oktmo_param_id = 28;

    $settle_type_id = 209;
    $settle_oktmo_param_id = 32;

    $latitude_param_id = 30;
    $longtitude_param_id = 31;

    if(($file_handler = fopen($filepath, "r")) !== false) {
        while(($row_data = fgetcsv($file_handler, 1000, ';')) !== false) {
            $num = count($row_data);

            $oktmo = $row_data[0];

            echo "<p>$oktmo</p>";

//            if(!exists_settle_with_oktmo($site, $oktmo, $settle_type_id, $settle_oktmo_param_id)) {
            $parent_region_id = get_material_id_by_oktmo($site, $row_data[1], $region_type_id,
                $region_oktmo_param_id);


            if ($parent_region_id == null) {
                print_r("Родительский район не найден");
                break;
            } else print_r($parent_region_id);

            $parent_omsu_id = get_omsu_by_region_id($site, $omsu_type_id, $parent_region_id);

            if ($parent_omsu_id == null) {
                print_r("ОМСУ не найдено");
                break;
            }

            $settle_id = get_material_id_by_name($site, $settle_type_id, $row_data[2]);

            if ($settle_id == null) {
                $site->values->id = '';
                $site->values->matforconnect = '';
                $site->values->id_char = '';

                $site->values->name = $row_data[2];
                $site->values->type_id = $settle_type_id;
                $site->values->language_id = 1;
                $site->values->content = '';
                $site->values->date_add = '';
                $site->values->status_id = 2;
                $site->values->anons = '';

                createMaterial($site);

                print_r("Поселение создано");





            } else {
                print_r("Поселение уже создано");
            }

            $settle_id = get_material_id_by_name($site, $settle_type_id, $row_data[2]);

            // добавляем связь с районом

            $site->values->matforconnect = $parent_region_id;
            $site->values->id = $settle_id;

            DoConnectMaterial($site);

            print_r("Связь с районом добавлена");

            $site->values->matforconnect = $parent_omsu_id;
            $site->values->id = $settle_id;

            DoConnectMaterial($site);

            print_r("Связь с ОМСУ добавлена");

            $delete_params_query = 'DELETE FROM "extra_materials" WHERE "material_id" = '.$settle_id;

            $site->dbquery($delete_params_query);

            $oktmo_query = 'INSERT INTO "extra_materials" ("valuename", "material_id", "type_id") VALUES (\''
                . $oktmo . '\', ' . $settle_id . ', ' . $settle_oktmo_param_id . ')';

            $site->dbquery($oktmo_query);

            $latitude_query = 'INSERT INTO "extra_materials" ("valuename", "material_id", "type_id") VALUES (\''
                . $row_data[11] . '\', ' . $settle_id . ', ' . $latitude_param_id . ')';

            $site->dbquery($latitude_query);

            $longtitude_query = 'INSERT INTO "extra_materials" ("valuename", "material_id", "type_id") VALUES (\''
                . $row_data[12] . '\', ' . $settle_id . ', ' . $longtitude_param_id . ')';

            $site->dbquery($longtitude_query);

            $delete_contacts_query = 'DELETE FROM "contacts WHERE "material_id = "'.$settle_id;
            $site->dbquery($delete_contacts_query);

            $phones =  Array();
            $phone_strings = explode(',', $row_data[5]);


            foreach($phone_strings as $phone_row) {
                $phone_parts = explode(':', $phone_row);
                if(trim($phone_parts[0]) == 'факс') {
                    $phones['fax'] = $phone_parts[1];
                }
                else {
                    $phones['tel'] = $phone_parts[1];
                }
            }

            $site->values->id = $settle_id;
            $site->values->addr = $row_data[6].', '.$row_data[7].','.$row_data[8];
            $site->values->email = $row_data[4];
            $site->values->building = $row_data[9];
            $site->values->office = $row_data[10];
            $site->values->tel = $phones['tel'];
            $site->values->fax = $phones['fax'];
            $site->values->city = 10;

            DoAddCont($site);

            print_r("Контакты добавлены");

//            }


        }
    }
    else {
        echo "File not found";
    }
}

function parse_chiefs($site, $filepath) {
    $chief_type_id = 119;
    $chief_post_param_id = 8;

    $settle_type_id = 209;
    $settle_oktmo_param_id = 32;

    if(($file_handler = fopen($filepath, "r")) !== false) {
        while(($row_data = fgetcsv($file_handler, 1000, ';')) !== false) {
            $num = count($row_data);

            $oktmo = $row_data[0];
            $fio = "$row_data[1] $row_data[2] $row_data[3]";
            $post = $row_data[4];

            echo "<p>$oktmo $fio $post</p>";

            $settle_id = get_material_id_by_oktmo($site, $oktmo, $settle_type_id, $settle_oktmo_param_id);

            if($settle_id == null) {
                echo "<p>Поселения с заданным ОКТМО не существует</p>";
                continue;
            }

            $chief_id = get_material_id_by_name($site, $chief_type_id, $fio);

            if($chief_id == null) {
                $site->values->id = '';
                $site->values->matforconnect = '';
                $site->values->id_char = '';

                $site->values->name = $fio;
                $site->values->type_id = $chief_type_id;
                $site->values->language_id = 1;
                $site->values->content = '';
                $site->values->date_add = '';
                $site->values->status_id = 2;
                $site->values->anons = '';

                createMaterial($site);

                echo "<p>Глава поселения создан</p>";
            }

            else {
                echo "<p>Глава поселения уже создан</p>";
            }

            $chief_id = get_material_id_by_name($site, $chief_type_id, $fio);

            if($chief_id == null) {
                echo "<p>Ошибка создания главы поселения</p>";
                continue;
            }

            $site->values->matforconnect = $settle_id;
            $site->values->id = $chief_id;

            DoConnectMaterial($site);

            echo "<p>Связка руководителя с поселением</p>";

            $delete_params_query = 'DELETE FROM "extra_materials" WHERE "material_id" = '.$chief_id;

            $site->dbquery($delete_params_query);

            $post_query = 'INSERT INTO "extra_materials" ("valuename", "material_id", "type_id") VALUES (\''
                . $post . '\', ' . $chief_id . ', ' . $chief_post_param_id . ')';

            $site->dbquery($post_query);

            echo "<p>Пост добавлен</p>";
        }
    }
}

parse_omsus($this, '/var/www/html/dev/modules/omsu_parser/omsus.csv');

parse_chiefs($this, '/var/www/html/dev/modules/omsu_parser/chiefs.csv');