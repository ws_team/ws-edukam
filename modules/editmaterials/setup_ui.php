<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


global $file_type_selector,
       $file_par_selector,
       $file_pars,
       $status_selector,
       $language_selector,
       $type_id;

//селектор языка
$language_selector = '';
$query = 'SELECT * FROM "languages" ORDER BY "id" ASC';
if ($res = $this->dbquery($query))
{
    foreach ($res as $row) {

        if ((isset($this->values->language_id))&&($this->values->language_id == $row['id'])) {
            $selected = 'SELECTED';
        } else {
            $selected = '';
        }

        $language_selector.="<option value='$row[id]' $selected>$row[name]</option>";
    }
}

//селектор статуса
$status_selector = '';
$query = 'SELECT * FROM "material_statuses" ORDER BY "id" ASC';
if ($res = $this->dbquery($query)) {
    foreach ($res as $row) {

        if ((isset($this->values->status_id))&&($this->values->status_id == $row['id'])) {
            $selected='SELECTED';
        }
        else
        {
            $selected='';
        }

        $status_selector.="<option value='$row[id]' $selected>$row[name]</option>";
    }
}


//селектор типов файлов
$file_type_selector = '';
$query = 'SELECT * FROM "file_types" ORDER BY "id" ASC';
if ($res = $this->dbquery($query)) {
    foreach ($res as $row) {
        if ((isset($this->values->file_type_id)) && ($this->values->file_type_id == $row['id'])) {
            $selected = 'SELECTED';
        } else {
            $selected = '';
        }
        $file_type_selector .= "<option value='$row[id]' $selected>$row[name]</option>";
    }
}

//подключаем tinymce

$this->data->morejs = <<<EOS
<script type="text/javascript" src="/js/tinymce/tinymce.js"></script>
<link rel="stylesheet" type="text/css" href="/css/pickmeup.css" />
<script type="text/javascript" src="/js/jquery.pickmeup.min.js"></script>
EOS;

if (!empty($this->values->id)) {

    //список изображений для редактора tinyMCE
    if(!isset($material)){
        $material=MaterialGet($this, $this->values->id);
    }

    $imagesftm=Array();

    //проверяем наличае прикрепленных изображений
    if(is_array($material['allfiles']) && count($material['allfiles']) > 0){
        foreach($material['allfiles'] as $file){
            if($file['type_id'] == FILETYPE_IMAGE){
                $imagesftm[]=$file;
            }
        }
    }

    $itm='';

    if(count($imagesftm) > 0){
        $itm=", image_list: [";

        $tmi=0;
        foreach($imagesftm as $item){

            ++$tmi;
            if($tmi > 1){
                $itm.=',';
            }

            $itm.="
            {title: '$item[name]', value: 'https://".$_SERVER['SERVER_NAME']."/photos/$item[id]_x922.jpg'}";
        }

        $itm.="]";
    }

}

$this->data->jsdocready = <<<EOS
    $('#date_event').datetimepicker({
        lang:'ru',
        timepicker:true,
        format:'d.m.Y H:i'
    });

    $('.dateinput').datetimepicker({
        lang:'ru',
        timepicker:true,
        format:'d.m.Y H:i'
    });

    tinymce.init({
        selector: "textarea:not(.notiny)",
        mobile: {
            theme: 'mobile'
        },
        language : 'ru',
        plugins: [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime table contextmenu paste image"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        autosave_ask_before_unload: false,
        height : 200,
        relative_urls: false,
        remove_script_host : false,
        valid_styles: {
        "*": "text-align,vertical-align,text-decoration,color,background-color,border-color,border,padding,margin,padding-top,padding-bottom,padding-left,padding-right,width,height,border-width,border-collapse"}
        $itm,
        images_upload_url: '/?menu=ajax&imenu=saveUploadedFileTinyMCE',
        init_instance_callback: function (editor) {
            editor.on('change', function (e) {
                $('#checkEditMaterialField').val('edited');
            });
        }
    });
EOS;

//формируем хлебные крошки
$this->data->breadcrumbs.='&rarr;<a href="/?menu=admin">Администрирование</a>';

$this->values->typename='';
//определяем выбранный тип материала
if($this->values->imenu != '') {
    $query='SELECT "name" FROM "material_types" WHERE "id" = $1';
    if($res=$this->dbquery($query, array($this->values->imenu)))
    {
        if(count($res) > 0)
        {
            $this->values->typename=$res[0]['name'];
            if($this->values->action == 'list')
            {
                $this->data->breadcrumbs.='&rarr;<b>'.$res[0]['name'].'</b>';
            }
            else
            {
                $this->data->breadcrumbs.='&rarr;<a href="/?menu=editmaterials&action=list&imenu='.$this->values->imenu.'">'.$res[0]['name'].'</a>';
            }
        }
    }
}

if($this->values->action == 'add')
{
    $this->data->breadcrumbs.='&rarr;<b>Добавление материала (шаг 1)</b>';
}
elseif(($this->values->action == 'edit')&&($this->values->id != ''))
{
    $this->data->breadcrumbs.='&rarr;<b>'.
        (!empty($material['name']) ? $material['name'] : '').
        ' (изменение)</b>';
}

//параграфы/разделы файлов
$file_par_selector='<option value="">-</option>';
$file_pars='';
if (!empty($this->values->id)) {
    $query = <<<EOS
SELECT * FROM "file_pars"
WHERE "material_id" = $1 ORDER BY orderby ASC NULLS LAST, "name" ASC
EOS;

    if ($res=$this->dbquery($query, array($this->values->id))) {
        foreach ($res as $row) {

            $row['name'] = htmlspecialchars($row['name']);

            $file_par_selector.="<option value='$row[id]'>$row[name]</option>";

            if($row['orderby'] == '')
            {
                $row['orderby']=0;
            }

            //$actions="<a href=\"#files\" onclick=\"deleteFilePar($row[id],".$this->values->imenu.", ".$this->values->id.");\"><img class='svg delete-icon' src='/images/trash.svg' /></a>";
            //$actions.="&nbsp;<a onclick=\"editFileParForm($row[id],'$row[name]', $row[orderby]);\"><img class='svg edit-icon' src='/images/edit.svg' /></a>";

            $actions="
            <button 
            type=\"button\" 
            class=\"btn btn-danger showtooltip small\" 
            data-toggle=\"tooltip\" 
            data-placement=\"bottom\" 
            title=\"Удалить раздел вложений\" 
            onclick=\"if(checkEditInputs()){ deleteFilePar($row[id],".$this->values->imenu.", ".$this->values->id.") } else{ showEditWarning(event, this) }\">
            <span class=\"glyphicon glyphicon-remove-sign\"></span></button>";
            $actions.="&nbsp;
            <button 
            onclick=\"if(checkEditInputs()){ editFileParForm($row[id], '$row[name]', $row[orderby]) } else{ showEditWarning(event, this) }\" 
            type=\"button\" class=\"btn btn-default showtooltip small\" 
            data-toggle=\"tooltip\" 
            data-placement=\"bottom\" 
            title=\"Форма редактирования вложения\" >
            <span class=\"glyphicon glyphicon-pencil\"></span></button>";


            $file_pars.="<tr>
    <td>$row[orderby] - $row[name]</td>
    <td>$actions</td>
</tr>";
        }
    }
}