<?php
/**
 * @var \iSite $this
 */

$max_uploaded_size = "8M";
error_reporting(7);
ini_set("display_errors","1");
ini_set("upload_max_filesize", $max_uploaded_size);
ini_set("post_max_size", $max_uploaded_size);
ini_set('max_execution_time', 3000);

global
    $type_id,
    $fulltypename,
    $language_selector,
    $status_selector,
    $file_type_selector,
    $file_par_selector,
    $file_pars,
    $extratypes;

// @todo csrf

include_once(dirname(__FILE__).'/functions.php');

//обьявляем переменные
$this->data->errortext = '';

//тип материала
$fulltypename = '';

if (isset($this->values->imenu)) {
    $type_id = $this->values->imenu;
}

$fulltypename = GetTypeName($this, $type_id);


$this->GetPostValues(array(
    'id',
    'id_char',
    'type_id',
    'language_id',
    'author_id',

    'name',
    'anons',
    'content',
    'date_add',

    'date_edit',
    'date_event',
    'status_id',
    'extra_number',

    'extra_number2',
    'extra_text1',
    'extra_text2',
    'file_name',

    'file_name_en',
    'file_type_id',
    'file_par',
    'file_content',

    'doaddfile',
    'doconnectmaterial',
    'matforconnect',
    'doaddcont',
    'dosynctooiv',

    'addr',
    'tel',
    'email',
    'fax',

    'building',
    'office',
    'doaddshed',
    'sname',

    'scontent',
    'mat',
    'dfile',
    'dpar',

    'dcont',
    'parent_id',
    'city',
    'latitude',

    'longitude',
    'orderby',
    'ordersort',
    'search',

    'page',
    'ordernum',
    'author',
    'file_id',

    'doeditfile',
    'deletefilegroup',
    'deleteconmatgroup',
    'fileforconnect',

    'clonematerial',
    'doaddfilepar',
    'filepar',
    'doeditfilepar',

    'fileparid',
    'acont',
    'doeditcont',
    'ChangeOrdernumToConMat',

    'parentid',
    'childid',
    'cordernum',
    'subscriptionDaily',
    'subscriptionCancel',
    'subscriptionImmediately',
    'redirect_to',

	'materialdetailed_vidjet',
	'matdetailed_photovideo'
    ),
    1
);

if (empty($this->values->action))
    $this->values->action = 'list';

$action_permission = 'editmaterials:'.$this->values->imenu;
if ($this->values->action == 'edit')
    $action_permission .= ':'.$this->values->id;

if ( ! $this->requireAuthUser()->can($action_permission)) {
    $this->redirect('/');
}

//рисуем заголовки
switch ($this->values->action) {
    case 'add':
    case 'edit':
    case 'list':
        include(dirname(__FILE__).'/action/'.$this->values->action.'.php');
        break;

    default:
        $this->endRequest(400);
}

include(dirname(__FILE__).'/setup_ui.php');
