<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


$this->data->iH1 = 'Добавление материала (шаг 1 из 2)';

//получаем связанные характеристики
$extratypes = GetExtraTypes($this, $this->values->imenu);

$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);

//получаем селекторы
$selectors = GetSelectors($this, $this->values->imenu);

$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
//обработка добавления
if ( ! empty($this->values->name)) {
    $resarr = createMaterial($this);
    $this->data->errortext = $resarr[1];

$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
    if ($resarr[0]) {
        $this->values->action='edit';
        $this->values->id=$resarr[0];
        $this->data->iH1='Изменение материала';

$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
        //формируем массив материала
        $material = MaterialGet($this, $this->values->id);


$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
        //связка материала
        if (($this->values->doconnectmaterial == '1')&&($this->values->matforconnect != '')) {
            DoConnectMaterial($this);
            //формируем массив материала
$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
            $material = MaterialGet($this, $this->values->id);
        }

        $this->logWrite(LOG_DEBUG, __FILE__, __LINE__);

        //добавляем значения характеристик
        if ($extratypes[0] != '') {
            DoAddExtraMaterials($this, $extratypes, $this->values->id);
$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
        }

        $this->logWrite(LOG_DEBUG, __FILE__, __LINE__);

        if (count($selectors) > 0) {
            DoAddSelector($this, $selectors, $this->values->id);
        }

        $this->logWrite(LOG_DEBUG, __FILE__, __LINE__);

        $temptext = ob_get_contents();

        $this->redirect(array(
            'editmaterials',
            'imenu' => $this->values->imenu,
            'id' => $this->values->id,
            'action' => 'edit',
        ));
$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);

        ob_start();

        $this->headers = ob_get_contents();
        ob_end_clean();

        ob_start();
        echo $temptext;
    }
}

$this->logWrite(LOG_DEBUG, __FILE__, __LINE__);
