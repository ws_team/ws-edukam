<?php
/**
 * @var \iSite $this
 *
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

global $ids,
    $idstype,
    $mquery;

$mquery = '';
$ids = '';
$idstype = '';

$omsuHackArray = Array(164, 165, 211, 94, 212);

$this->data->iH1 = 'Администрирование материалов';

if (empty($this->values->imenu)) {
    $this->redirect('?menu=editmaterials&action=list&imenu=1');
}

$query = 'SELECT "name" FROM "material_types" WHERE "id" = $1';
$res = $this->dbquery($query, array($this->values->imenu));

if ( ! empty($res)) {
    $this->data->iH1 .= ' &laquo;'.$res[0]['name'].'&raquo;';
}

if (empty($this->values->page))
{
    $this->values->page = 1;
}

$this->values->perpage = 50;
$this->data->atype = $this->values->imenu;

/*
$allowedMatTypes = array();
$idsList = array();

$query = <<<EOS
WITH RECURSIVE sub AS(
	SELECT type_id id FROM user_rights WHERE goal_id = $1
	UNION
	SELECT mt.id FROM material_types mt INNER JOIN sub ON sub.id = mt.parent_id
)
SELECT id FROM sub
EOS;

$q_params = array($this->autorize->userid);

$userMaterialTypes = $this->dbquery($query, $q_params);

if (!empty($userMaterialTypes)) {
    $allowedMatTypes = array(0);
    foreach ($userMaterialTypes as $umt) {
        $allowedMatTypes[] = $umt['id'];
    }
}

if (!empty($this->autorize->omsu)) {
    $idsList[] = 0;

    $allowedMatTypes = array_merge($allowedMatTypes, $omsuHackArray);

    if (in_array($this->values->imenu, $allowedMatTypes)) {
        $idsList[] = $this->autorize->omsu;
    }

    $query = <<<EOS
SELECT "cm"."material_child" "id" FROM "material_connections" "cm"
    WHERE "cm"."material_parent" = $1

    UNION ALL

    SELECT "pm"."material_parent" "id" FROM "material_connections" "pm"
    WHERE "pm"."material_child" = $1
EOS;

    // $hackArray = Array(164, 165, 211, 94, 212);// @todo разобраться, возможно выпилить
    // if (in_array($this->values->imenu, $hackArray))

    $res = $this->dbquery($query, array($this->autorize->omsu));
    if ($res) {
        if (count($res) > 0) {
            foreach ($res as $row) {
                $idsList[] = $row['id'];
            }
        }
    }
}

if (count($allowedMatTypes) == 1 || count($idsList) == 1) {
    $materials = array();
    $mquery = 'NULL';
    return;
}

if (!empty($allowedMatTypes) && !in_array($this->values->imenu, $allowedMatTypes)) {
    $materials = array();
    $mquery = 'NULL';
    return;
}

if (!empty($idsList)) {
    $ids = implode(',', $idsList);
    $idstype = $this->values->imenu;
}
*/

$region = intval($this->getUser()->getAttribute('reg'));
$bound_mats = $this->getUser()->getAttribute('bound_materials');

if ( ! empty($bound_mats)) {
    $bound_mat_list = implode(',', $bound_mats);
    $mquery = <<<EOS
SELECT m.id FROM materials m
WHERE
    m.type_id = {$this->values->imenu}
    AND m.id IN(
        SELECT "cm"."material_child" "id"
        FROM "material_connections" "cm"
        WHERE "cm"."material_parent" IN($bound_mat_list)
    
        UNION ALL
    
        SELECT "pm"."material_parent" "id"
        FROM "material_connections" "pm"
        WHERE "pm"."material_child" IN($bound_mat_list)
    )
EOS;
    $ids = '';
    $idstype = '';
}

$this->data->materials = MaterialList(
    $this,
    $this->values->imenu,
    $this->values->page,
    $this->values->perpage,
    $this->values->status_id,
    $ids,
    $idstype,
    $mquery
);
