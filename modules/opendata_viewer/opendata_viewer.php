<?php

/**
 * Получение полей заголовка для файла открытых данных
 * @param $structure_material - ID материала структуры открытых данных
 * @param $header_field_name - Название поля структуры, по которому определяется название заголовка
 * @return array|null список полей заголовка или null, если файл отсутсвует, пуст или заданное поле не найдено
 */
function GetOpenDataHeaderFields($structure_material, $header_field_name, $strlink) {

    // открываем файл структуры
    if(count($structure_material["documents"]) > 0) {

        $structure_file_id = $structure_material["documents"][0]["id"];

        //$structure_file = fopen("https://www.khabkrai.ru". "/?menu=getfile&id=" . $structure_file_id, "r");

        $strlink='https://www.khabkrai.ru'.$strlink;


        $structure_file = fopen($strlink, "r");


        // определяем индекс поля заголовка
        $header_field_name_index = -1;


        if(!feof($structure_file)) {

            $header_array = fgetcsv($structure_file);

            if(count($header_array) > 0) {
                $header_field_name_index = array_search($header_field_name, $header_array);
            }
        }



        // нет такого поля
        if(!$header_field_name_index) {
            return null;
        }

        // считываем названия полей
        $russian_field_names = array();

        while(!feof($structure_file)) {
            $field_array = fgetcsv($structure_file);

            if(count($field_array) > $header_field_name_index) {
                array_push($russian_field_names, $field_array[$header_field_name_index]);
            }
        }

        return $russian_field_names;
    }

    return null;
}

/**
 * Вывод CSV-файла версиии как html-таблицы
 * @param $site - Экземпляр сайта
 * @param $opendata_material_id - ID материала открытых данных
 */
function ViewOpenDataAsTable($site, $opendata_material_id, $strlink, $link) {
    $opendata_material = MaterialGet($site, $opendata_material_id);

    $version_ids = GetParamMaterials($site, $opendata_material["id"]);

    // тянем информацию по текущей версии
    $current_version = null;

    // если не прикреплено ни одной версии
    if($version_ids == 0) {
        return "<p>Отсутствует информация</p>";
    }

    // тянем текущую версию и ее структуру
    $current_version = ! empty($version_ids[0]) ?
        MaterialGet($site, $version_ids[0]["id"]) :
        null;
    $current_structure = $current_version
        && ! empty($current_version['params'][50])
        && ! empty($current_version['params'][50]['value']) ?
        MaterialGet($site, $current_version["params"][50]["value"]) :
        null;

    $header_fields = GetOpenDataHeaderFields($current_structure, "russian description", $strlink);

    echo "<table border='1'>";

    // выводим заголовок
    if($header_fields != null && count($header_fields) > 0) {
        echo "<thead><tr>";

        foreach($header_fields as $header_field) {
            echo "<th>$header_field</th>";
        }

        echo "</tr></thead>";
    }

    // выводим строки
    if(count($current_version["documents"]) > 0) {
        echo "<tbody>";

        $version_file_id = $current_version["documents"][0]["id"];

        //$version_file = fopen("http://khabkrai.ru". "/?menu=getfile&id=" . $version_file_id, "r");

        //echo $link;
        //die();

        $link=str_replace('https://khabkrai.ru','https://golos27.ru',$link);

        //$versionfile=file_get_contents($link);
        //echo $versionfile;

        $version_file = fopen($link, "r");



        fgets($version_file);

        while(!feof($version_file)) {
            $row = fgetcsv($version_file);

            echo "<tr>";

            foreach($row as $elem) {
                if(empty($elem)) {
                    echo "<td>&nbsp;</td>";
                }
                else{
                    echo "<td>$elem</td>";
                }
            }

            echo "</tr>";

        }

        echo "</tbody>";
    }



    echo "</table>";


    //обновляем кол-во просмотров
    //krumo($opendata_material);
    $views = isset($opendata_material['params'][45])
        && isset($opendata_material['params'][45]['value']) ?
        $opendata_material['params'][45]['value'] :
        0;
    ++$views;

    $query='UPDATE "extra_materials" SET "valuename" = '."'".$views."'".' WHERE "type_id" = 45 AND "material_id" = '.$opendata_material['id'];
    if($res=$site->dbquery($query))
    {
        //вроде обновили - радуемся!
    }


}

$this->GetPostValues(Array('id','strlink','link'));

ViewOpenDataAsTable($this, $this->values->id, $this->values->strlink, $this->values->link);

