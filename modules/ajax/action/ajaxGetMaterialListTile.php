<?php

defined('_WPF_') or die();

$this->GetPostValues(array(
    'materialTypeId',
    'perpage',
    'pageNumber',
));

$remainingAmount   = '';
$responseHtml      = '';
$nextPageNumber    = $this->values->pageNumber + 1;

$materials = MaterialList(
    $this,
    $this->values->materialTypeId,
    $nextPageNumber,
    $this->values->perpage,
    STATUS_ACTIVE
);

if($materials){

    $allMaterials = MaterialList(
        $this,
        $this->values->materialTypeId,
        1,
        10000,
        STATUS_ACTIVE
    );

    $materialsCount  = count($allMaterials);
    $remainingAmount = $materialsCount - $nextPageNumber * $this->values->perpage;

    if($remainingAmount <= 0){
        $paginatorBtnText = '';
    }
    elseif($remainingAmount > $this->values->perpage){
        $paginatorBtnText = "Загрузить еще {$this->values->perpage} из $remainingAmount";
    }
    else{
        $paginatorBtnText = "Загрузить еще $remainingAmount";
    }
}
else{
    $paginatorBtnText = '';
}

ob_start();

foreach ($materials as $materialsItem):

    $currentDateNewsPeriod = formatDateNewsPeriod($materialsItem['date_event']);

    // плитка состоит из 7 материалов, каждые семь материалов плитка выводится заново

    // также новая плитка выводится в случае, если текущая новость опубликована в месяце,
    // который не совпадает с месяцем предыдущей новости

    if($preventDateNewsPeriod != $currentDateNewsPeriod){
        $preventDateNewsPeriod = $currentDateNewsPeriod;
        echo '</div>';
        echo '<div class="subtitle-4">'. $preventDateNewsPeriod .'</div>';
        echo '<div class="news-tile">';
        $i = 1;
    }
    elseif($i == 1) {
        echo '<div class="news-tile">';
    }

    printMaterialTileItem($this, $materialsItem);

    if($i == 7 || ($j + 1) == $materialsCount) {
        echo '</div>';
        $i = 0;
    }

    $i++;
    $j++;

endforeach;

$responseHtml = ob_get_contents();

ob_end_clean();

$this->data->ajaxhtml = json_encode(array(
    'status'            => 'success',
    'remainingAmount'   => $remainingAmount,
    'pageNumber'        => $nextPageNumber,
    'html'              => $responseHtml,
    'paginatorBtnText'  => $paginatorBtnText,
));

