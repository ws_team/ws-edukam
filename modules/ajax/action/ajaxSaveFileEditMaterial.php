<?php

defined('_WPF_') or die();

$this->GetPostValues(array(
    'id',
    'file_type_id',
    'file_name',
    'name_off',
    'file_name_en',
    'file_content',
    'filedata',
    'filedata_sv',
    'file_par',
    'date_event',
    'author',
    'ordernum',
    'doaddfile',
    'current_imenu',
),
    1
);

ob_start();

$nfile  = new MFile($this);
$resarr = $nfile->DoSaveFile();

unset($nfile);

delMatCache($this, $this->values->id);

ob_end_clean();

if($resarr[0]){
    $status = 'success';

    ob_start();

    $material     = MaterialGet($this, $this->values->id);
    $filesIdArray = array_column($material['allfiles'], 'id');

    rsort($filesIdArray, SORT_NUMERIC);

    $maxFileId = $filesIdArray[0];

    foreach ($material['allfiles'] as $fileData){
        if($fileData['id'] == $maxFileId){
            $file = $fileData;
            break;
        }
    }

    $ccnt  = 0;
    $query = 'SELECT count("material_id") "ccnt" FROM "file_connections" WHERE "file_id" = '.$file['id'];

    if($res=$this->dbquery($query))
    {
        if(count($res) > 0)
        {
            $ccnt=$res[0]['ccnt'];
        }
    }

    $ilink  = '';
    $iclass = 'class="img-thumbnail"';

    switch($file['type_id'])
    {
        case FILETYPE_IMAGE:
            $icon='/photos/'.$file['id'].'_xy64x64.jpg?' . time('u') ;
            $ilink=" href='/photos/$file[id]_x600.jpg? " . time('u')  .  "' class='imggroup'";
            break;
        case FILETYPE_VIDEO:
            $icon='/images/filetypes/video.png';
            break;
        case FILETYPE_AUDIO:
            $icon='/images/filetypes/audio.png';
            break;
        case FILETYPE_DOCUMENT:
            $icon='/images/filetypes/document.png';
            $ilink=" href='/?menu=getfile&amp;id={$file['id']}'";
            break;
        case FILETYPE_BROADCAST:
            $icon='/images/filetypes/broadcast.png';
            break;
        default:
            $icon='/images/filetypes/file.png';
            break;
    }

    $icon = "<img src='$icon' width=\"42\" height=\"42\" $iclass border='0'/>";

    if($ilink != '')
    {
        $icon = "<a $ilink>".$icon.'</a>';
    }

    $actions          = '';
    $imenu            = $this->values->current_imenu;
    $id               = $this->values->id;
    $file['name']     = str_replace("'",'',$file['name']);
    $file['name_off'] = isset($file['name_off']) ? $file['name_off'] : '';

    $actions = "
<button type=\"button\" 
class=\"btn btn-danger showtooltip small\" 
data-toggle=\"tooltip\" 
data-placement=\"bottom\" 
title=\"Удалить вложение\" 
onclick=\"if(checkEditInputs()){ deleteMatFile($file[id],$ccnt,$imenu,$id) } else{ showEditWarning(event, this) }\">
<span class=\"glyphicon glyphicon-remove-sign\"></span></button>";

    $actions.="
&nbsp;<button 
onclick=\"if(checkEditInputs()){ editFileForm($file[id],'$file[typename]','$file[name]','$file[name_en]','$file[content]','$file[date_event]','$file[author]','$file[ordernum]','$file[par_id]','$file[name_off]') } else{ showEditWarning(event, this) }\" 
type=\"button\" 
class=\"btn btn-default showtooltip small\" 
data-toggle=\"tooltip\" 
data-placement=\"bottom\" 
title=\"Форма редактирования вложения\" >
<span class=\"glyphicon glyphicon-pencil\"></span></button>";

    $typename = $file['typename'];

    if ($file['type_id'] == FILETYPE_DOCUMENT){
        $typename .= ' ('.$file['fextension'].')';
    }

    ?>

    <tr>
        <td><?=$icon?></td>
        <td><?=$file['name']?> / <?=$file['par_name']?></td>
        <td><?=$typename?></td>
        <td><?php if(isset($file['cecutient']) && $file['cecutient']){echo 'загружен';}else{echo 'нет';}
            echo "<div style='display: none;'>"; print_r($file); echo "</div>"; ?></td>
        <td><?=$file['ordernum']?></td>
        <td><?=$actions?></td>
        <td><input type='checkbox' value='1' name='dfile_<?=$file['id']?>' /></td>
    </tr>

    <?php

    $responseHtml = ob_get_contents();

    ob_end_clean();
}
else{
    $status         = 'fail';
    $responseHtml   = '';
}

$this->data->ajaxhtml = json_encode(array(
    'status'  => $status,
    'message' => $resarr[1],
    'html'    => $responseHtml,
));

