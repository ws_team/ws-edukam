<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

$this->data->ajaxhtml = '';

$region = (int)$this->values->id;

$fromOIV = $this->settings->DB->name != 'habkrai';
if ($fromOIV) {
    $regionchar='';

    $query = 'SELECT id_char FROM materials WHERE id = $1';
    $res = $this->dbquery($query, array($region));
    if (!empty($res)) {
        $regionchar=$res[0]['id_char'];
    }

    global $setup;

    $setupSaved = $setup;

    include(WPF_ROOT.'/settings.php');

    $versions = Array();

    $versions['site']='2.5'; //site release version
    $versions['materials']='1.91kh';//materials module release version

    $realkhabkrai = new iSite($setup,$versions, 0);

    //получаем регион хабкрая

    $areg = Array();

    $query = "SELECT id FROM materials WHERE id_char = $1 AND type_id = $2";

    if ($res=$realkhabkrai->dbquery($query, array($regionchar, 21))) {
        if (count($res) > 0) {
            $khbregion=$res[0]['id'];
            $areg = GetRegionInfo($realkhabkrai, $khbregion);
            $areg['adm_url']='http://khabkrai.ru'.$areg['adm_url'];
        }
    }

    unset($realkhabkrai);
} else {
    $areg = GetRegionInfo($this, $region);
}

if (!empty($areg)) {

    ?>
    <div class="omsulist_info-top">
        <div class="omsulist_logo">
            <?php

            $areg['logo'] = str_replace('/templates/khabkrai/img/', '/img/', $areg['logo']);

            ?>
            <div class="xmedia s124 mt-image">
                <img src="<?= $areg['logo'] ?>" alt="<?= $areg['name'] ?>" width="124"
                     class="media-content">
                    <span class="media-textview"><?=
                        htmlspecialchars($areg['name']) ?> &ndash; герб</span>
            </div>

        </div>
        <div class="omsulist_regioninfo">
            <h3><?php echo $areg['name']; ?></h3>

            <p>&nbsp;</p>
            <?php

            if (isset($areg['adm_head_name'])) {
                ?>
                <div class='omsulins_regioninfo_row'>
                    <span class='omsulins_regioninfo_leftcol'>Глава:</span>
                    <span class='omsulins_regioninfo_rightcol'>
                        <a href="<?= $areg['adm_url'] ?>/Rukovoditel"><?= $areg['adm_head_name'] ?></a>
                    </span>
                </div>
                <?php
            }
            if (isset($areg['center']['name'])) {
                echo "<div class='omsulins_regioninfo_row'><span class='omsulins_regioninfo_leftcol'>Админ. центр:</span><span class='omsulins_regioninfo_rightcol'><a href='" . $areg['center']['url'] . "'>" . $areg['center']['name'] . "</a></span></div>";
            }
            if ((isset($areg['villagesnum'])) && ($areg['villagesnum'] > 0)) {
                echo "<div class='omsulins_regioninfo_row'><span class='omsulins_regioninfo_leftcol'>Включает:</span><span class='omsulins_regioninfo_rightcol'><a href='" . $areg['adm_url'] . "/Poseleniya'>" . get_correct_str($areg['villagesnum'], 'муниципальное образование', 'муниципальных образования', 'муниципальных образований') . "</a></span></div>";
            }
            if ((isset($areg['birth'])) && ($areg['birth'] != '')) {
                echo "<div class='omsulins_regioninfo_row'><span class='omsulins_regioninfo_leftcol'>Образован:</span><span class='omsulins_regioninfo_rightcol'>" . $areg['birth'] . "</span></div>";
            }
            if ((isset($areg['population'])) && ($areg['population'] != '')) {
                echo "<div class='omsulins_regioninfo_row'><span class='omsulins_regioninfo_leftcol'>Население:</span><span class='omsulins_regioninfo_rightcol'>" . $areg['population'] . "</span></div>";
            }
            if ((isset($areg['square'])) && ($areg['square'] != '')) {
                echo "<div class='omsulins_regioninfo_row'><span class='omsulins_regioninfo_leftcol'>Площадь:</span><span class='omsulins_regioninfo_rightcol'>" . $areg['square'] . "</span></div>";
            }

            ?>
        </div>
    </div>
    <a href="<?php echo $areg['adm_url']; ?>" class="new_style_link link-btn">Подробнее</a>
    <?php
} else {
    ?><p class="text-info">Информация в данное время недоступна</p><?php
}

//получаем последние новости района
$this->getPostValue('showmode');

if ($this->values->showmode == 'fromOIV') {
    $types = '43';

    $news = listNews($this, 1, 2, '2', $types, $region, '', '', '');

    if (count($news) > 0) {
        //выводим новости
        ?><h3 class="omsulist_caption">Результаты обсуждений района:</h3><?php

        $adate = '';
        foreach ((array)$news as $new) {
            if (empty($new['unixtime']))
                continue;

            $date = russian_datetime_from_timestamp($new['unixtime'], false, false, true);

            //выводим дату
            if($adate != $date)
            {
                $adate = $date;

                ?><p class="omsulist_news_data"><?= $adate ?></p><?php
            }

            //выводим новость
            ?>
            <p class="omsulist_news_name">
                <a href="<?= $new['url'] ?>"><?= $new['name'] ?></a>
            </p>
            <p class="omsulist_news_anons"><?= $new['anons'] ?></p>
            <?php
        }

        //кнопка - все новости
        ?><a href="/Oschestvennoe-obsuzhdenie-gosudarstvennyh-programm/Rezultaty-obsuzhdenij/" class="new_style_link">Все результаты обсуждений</a><?php
    }

} else {

    $types = '84, 94, 164, 211, 2';
    $news = listNews($this, 1, 2, '2', $types, $region, '', '', '');

    if (count($news) > 0) {
        //выводим новости
        ?><h3 class="omsulist_caption">Новости:</h3><?php

        $adate='';

        foreach ($news as $new) {

            $date = russian_datetime_from_timestamp($new['unixtime'], false, false, true);

            //выводим дату
            if ($adate != $date) {
                $adate = $date;

                ?><p class="omsulist_news_data"><?= $adate ?></p><?php
            } else {
                ?><p>&nbsp;</p><p>&nbsp;</p><?php
            }

            //выводим новость
            ?>
            <p class="omsulist_news_name">
                <a href="<?= $new['url'] ?>"><?= $new['name'] ?></a>
            </p>
            <p class="omsulist_news_anons"><?= $new['anons'] ?></p>
            <?php
        }

        //кнопка - все новости
        ?>
    <a href="<?= $areg['adm_url'] ?>/OMSU-Novosti" class="new_style_link link-btn">Все новости</a><?php
    }
}

exit;