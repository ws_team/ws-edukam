<?php

defined('_WPF_') or die();

$this->GetPostValues(array(
    'materialTypeId',
    'perpage',
    'pageNumber',
));

$remainingAmount   = '';
$responseHtml      = '';
$nextPageNumber    = $this->values->pageNumber + 1;

$materials = MaterialList(
    $this,
    $this->values->materialTypeId,
    $nextPageNumber,
    $this->values->perpage,
    STATUS_ACTIVE
);

if($materials){

    $allMaterials = MaterialList(
        $this,
        $this->values->materialTypeId,
        1,
        10000,
        STATUS_ACTIVE
    );

    $materialsCount  = count($allMaterials);
    $remainingAmount = $materialsCount - $nextPageNumber * $this->values->perpage;

    if($remainingAmount <= 0){
        $paginatorBtnText = '';
    }
    elseif($remainingAmount > $this->values->perpage){
        $paginatorBtnText = "Загрузить еще {$this->values->perpage} из $remainingAmount";
    }
    else{
        $paginatorBtnText = "Загрузить еще $remainingAmount";
    }
}
else{
    $paginatorBtnText = '';
}

ob_start();

foreach ($materials as $materialsItem){
    printMaterialListItemWithImage($this, $materialsItem);
}

$responseHtml = ob_get_contents();

ob_end_clean();

$this->data->ajaxhtml = json_encode(array(
    'status'            => 'success',
    'remainingAmount'   => $remainingAmount,
    'pageNumber'        => $nextPageNumber,
    'html'              => $responseHtml,
    'paginatorBtnText'  => $paginatorBtnText,
));

