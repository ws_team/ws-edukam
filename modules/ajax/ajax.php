<?php


ini_set('display_errors', true);
error_reporting(E_ALL);

$this->data->ajaxhtml = '-1';

$action = $this->values->imenu;
//определяем что выводить
switch ($action) {

	//добавление группы изображений в материал
	case 'doAddGroupImages';

		$this->getPostValues(array('files'));

		if($this->values->id != '' && $this->values->files != '')
		{
			//$sendData = json_decode(htmlspecialchars_decode($this->values->files));

			$files=explode(';',$this->values->files);

            $maxFileOrderNumArray = getMaterialMaxFileOrderNum($this, $this->values->id);

            if($maxFileOrderNumArray){
                $maxFileOrderNum = $maxFileOrderNumArray['max'];
            }
            else{
                $maxFileOrderNum = 0;
            }

			$allsize  = 0;
			$allsized = 0;

			foreach ($files as $file)
			{
				$fileall=explode('/',$file);
				$cnt=count($fileall);
				$file=$fileall[($cnt-1)];

				++$maxFileOrderNum;
				$_FILES['filedata']=Array();
				$_FILES['filedata']['name']=$file;
				$_FILES['filedata']['tmp_name']=$this->settings->path.'/files/'.$file;
				$thumbnail=$this->settings->path.'/files/thumbnail/'.$file;
				$_FILES['filedata']['size']=filesize($_FILES['filedata']['tmp_name']);

				$allsize=$_FILES['filedata']['size'];

				//если размер загруженных файлов меньше 10Мб
				if($allsize <= 10*1024*1024)
				{
					//$allsized+=$_FILES['filedata']['size'];
					//$allsize=$allsized;

					if(@is_array(getimagesize($_FILES['filedata']['tmp_name']))){
						$this->values->file_type_id = 1;
					} else {
						$this->values->file_type_id = 4;
					}
					$this->values->ordernum = $maxFileOrderNum;
					$this->values->file_name = $file;
					$this->values->file_name_en = $file;
					$this->values->file_content = "";
					$this->values->date_event=date('d.m.Y H:i:s');
					$this->values->author='';
					$this->values->file_par='';
					$this->values->nameoff='';

					$nfile = new MFile($this);
					$resarr = $nfile->DoSaveFile();
					unset($nfile);

					delMatCache($this, $this->values->id);

					//формируем массив материала
					$material = MaterialGet($this, $this->values->id);

				}

				//очищаем темповые файлы
				unlink($_FILES['filedata']['tmp_name']);
				if(file_exists($thumbnail))
				{
					unlink($thumbnail);
				}

			}

			$this->data->ajaxhtml=1;

		}

	break;

    //добавление группы изображений в материал
    case 'saveUploadedFileTinyMCE';

        if(!empty($_FILES['file'])) {

            if($_FILES['file']['size'] <= 10*1024*1024){

                $_FILES['filedata']             = Array();
                $_FILES['filedata']['name']     = $_FILES['file']['name'];
                $_FILES['filedata']['type']     = $_FILES['file']['type'];
                $_FILES['filedata']['size']     = $_FILES['file']['size'];
                $_FILES['filedata']['tmp_name'] = $_FILES['file']['tmp_name'];
                $_FILES['filedata']['error']    = $_FILES['file']['error'];


                $nfile  = new MFile($this);
                $resarr = $nfile->DoSaveFile();
                unset($nfile);

                echo json_encode(array(
                    'location' => '/photos/' . $resarr[2]
                    )
                );

            }
            else{
                echo json_encode(array('error_message' => 'Размер загруженной картинки должен быть не более 10 Мб'));
            }
        }

        exit;

    //блок инф-ции по региону в список ОМСУ
    case 'OMSUListSelectOMSU':

        include('action/'.$action.'.php');

        break;

    //листинг новостей
    case 'ajax_pagenator_news':

        $this->GetPostValues(array(
            'page',
            'region',
            'keywords',
            'list_date_filter_datestart',
            'list_date_filter_dateend',
            'single_calendar_month',
            'single_calendar_year'
        ));

        if ($this->values->page == '') {
            $this->values->page = 1;
        }

        $news_count = coutNews(
            $this,
            '2',
            '84,
            94,
            164,
            211,
            2',
            $this->values->region,
            $this->values->keywords,
            $this->values->list_date_filter_datestart,
            $this->values->list_date_filter_dateend
        );

        $news = listNews($this,
            $this->values->page,
            10,
            '2',
            '84,
            94,
            164,
            211,
            2',
            $this->values->region,
            $this->values->keywords,
            $this->values->list_date_filter_datestart,
            $this->values->list_date_filter_dateend
        );

        break;

    //список годов, за которые есть материалы переданных типов
    case 'giveMeAMaterialYears':

        $this->GetPostValues('types');

        if($this->values->types != '')
        {

            $query='SELECT DISTINCT TO_NUMBER(TO_CHAR("date_event",'."'YYYY'".')) "year"
            FROM "materials" WHERE "type_id" IN ('.$this->values->types.')
            ORDER BY "year" ASC';

            if($res=$this->dbquery($query))
            {



                if(count($res) > 0)
                {

                    $years=Array();

                    foreach($res as $row)
                    {
                        $years[]=$row['year'];
                    }

                    $this->data->ajaxhtml = json_encode($years);
                }


            }

        }

        break;

    //формируем options файлов для привязки к материалу
    case 'giveMeAFilesByMaterial':

        $typewhere='';

        if($this->values->id != '')
        {
            $typewhere=' AND "c"."material_id" = '.$this->values->id;
        }

        $query='SELECT DISTINCT "f"."id", "f"."name", "t"."name" "tname", "f"."ordernum", "f"."type_id"
        FROM "files" "f", "file_types" "t", "file_connections" "c"
        WHERE "f"."type_id" = "t"."id" AND "c"."file_id" = "f"."id" '.$typewhere.' ORDER BY "t"."name" ASC, "f"."ordernum" ASC';
        //echo $query;
        if($res=$this->dbquery($query))
        {
            $this->data->ajaxhtml='';
            if(count($res) > 0)
            {
                $atype='';
                foreach($res as $row)
                {

                    if($atype != $row['tname'])
                    {
                        $atype=$row['tname'];
                        $this->data->ajaxhtml.='<option disabled><b>'.$row['tname'].'</b></option>';

                    }



                    $this->data->ajaxhtml.="<option value='$row[id]'>&nbsp;&nbsp;&nbsp;$row[name]</option>";
                }
            }
        }

        break;

    //формируем options материалов переданного типа материала
    case 'giveMeAMaterialsByType':

        $typewhere='';

        if($this->values->id != '')
        {
            $typewhere=' AND "type_id" = '.$this->values->id;
        }

        $limit = 100;
        $query='SELECT "id", "name" FROM "materials" WHERE "status_id" = 2 '.$typewhere.
            ' ORDER BY "date_event" DESC'.
            ' LIMIT '.$limit;

        if($res=$this->dbquery($query))
        {
            $this->data->ajaxhtml='';
            if(count($res) > 0)
            {
                foreach($res as $row)
                {
                    $this->data->ajaxhtml.="<option value='$row[id]'>$row[name]</option>";
                }
            }
        }

        break;

    //выводим анонс
    case 'ShowAnons':

        $material=MaterialGet($this, $this->values->id);

        $contact='';
        if(isset($material['contacts'][0]))
        {
            $addr=$material['contacts'][0]['addr'].' '.$material['contacts'][0]['building'];
            $contact="<p style='padding-top:5px;'><img src='/templates/khabkrai/img/whitecont.png' width='14px' height='20px;' align='left' style='padding: 5px;'>$addr</p>";
        }

        $this->data->ajaxhtml="
            <div class='anons_header'>
            <p style='font-size: 36px; line-height: 32px;'>$material[eventtime]</p>
            <p style='font-size: 20px; line-height: 46px;'>$material[eventdatename]</p>
            <hr />
            $contact
            </div>
            <div class='anons_body'>
                <p style='font-size: 24px;'>$material[name]</p>
                <p>&nbsp;</p>
                <p>$material[content]</p>";

        $this->data->ajaxhtml.='<div style="position:relative; display: inline-block;">';

        //выводим документы
        $files=vjt_print_documents($material['documents']);

        if($files != '')
        {
            $this->data->ajaxhtml.='<hr style="margin-bottom:10px;" />'.$files;
        }

        $this->data->ajaxhtml.="
            </div>
        ";

        break;

    // возвращаем в JSON тексты наиболее популярных поисковых запросов в количестве count,
    // начинающихся с phrase
    case 'GetPopularSearchPhrases' :
        $this->GetPostValues(Array('phrase', 'count'));
        $results = GetTopSearchRequests($this, $this->values->phrase, $this->values->count);
        $this->data->ajaxhtml = json_encode($results);

        break;

    case 'village_summary' :

        $this->data->ajaxhtml = '';

        $this->GetPostValues('village_id');
        $village = GetVillageInfo($this, $this->values->village_id);

        $contactInfo = $village['contacts'][0];

        $contacts_block = '<div class="village-info-contacts">';

            if (array_key_exists('tel', $contactInfo) && $contactInfo['tel'] != '' && $contactInfo['tel'] != null) {
                $contacts_block.= '<div class="region-head-phone row"><div class="title">Телефон:</div><div class="value">'.$contactInfo['tel'].'</div></div>';
            }
            if (array_key_exists('fax', $contactInfo) && $contactInfo['fax'] != '' && $contactInfo['fax'] != null) {
                $contacts_block.= '<div class="region-head-fax row"><div class="title">Факс:</div><div class="value">'.$contactInfo['fax'].'</div></div>';
            }
            if (array_key_exists('email', $contactInfo) && $contactInfo['email'] != '' && $contactInfo['email'] != null) {
                $contacts_block.= '<div class="region-head-email row"><div class="title">E-mail:</div><div class="value"><a href="mailto:'.$contactInfo['email'].'">'.$contactInfo['email'].'</a></div></div>';
            }
            if (array_key_exists('site', $contactInfo) && $contactInfo['site'] != '' && $contactInfo['site'] != null) {
                $contacts_block.= '<div class="region-head-site row"><div class="title">Сайт:</div><div class="value"><a href="'.$contactInfo['site'].'">'.$contactInfo['site'].'</a></div></div>';
            }
            if (array_key_exists('fulladdr', $contactInfo) && $contactInfo['fulladdr'] != '' && $contactInfo['fulladdr'] != null) {
                $contacts_block.= '<div class="region-head-address row"><div class="title">Адрес:</div><div class="value">'.$contactInfo['fulladdr'].'</div></div>';
            }

            $contacts_block.= '<a href="'.$village['url'].'" class="village-link">Подробнее о поселении</a>';

        $contacts_block.= '</div>';





        // $golos_server = 'http://192.168.0.252:6080/';
        $golos_server = 'http://172.18.209.26:6080/';

        if ($_SERVER['SERVER_NAME'] == '192.168.0.248') {
            $golos_server = 'http://golos27.ru:6080/';
        }

        $golos_request = 'api/ratings/?oktmo=';
        $oktmo = '';

        $oktmo = (array_key_exists('oktmo', $village) && $village['oktmo'] != '') ? $village['oktmo'] : '8701000';

        $url = $golos_server.$golos_request.$oktmo;

        $golos_response = new \StdClass();

        $golos_response = cache_get_contents($this, $url, 86400);

        $golos_response = json_decode($golos_response);

        if ($golos_response == '' || $golos_response === false) {
            $this->data->ajaxhtml.= $contacts_block;
            break;
        }

        $golos_block = '';

        $golos_block .= '<div class="village-info-golos-stats">';
        $golos_block .= '<h4>Рейтинг руководителя</h4>';
        $golos_block .= '<p class="golos-reference">По данным <a href="'.strip_tags($golos_response->cardUrl).'" title="Портал Голос27">golos27.ru</a></p>';

        $diagram_id = 'village-diagram-circle-'.$village['id'];

        $golos_block .= '<div class="global-rating">';
        $golos_block .= '<div class="chart-caption"><span class="name">'.$village['adm_head_name'].'</span><span class="post">'.$village['adm_head_post'].'</span></div>';
        $golos_block .= '<div class="chart-total">'.number_format($golos_response->inquiryRating->average, 2, ",", " ").'</div>';

        $chart_values = '';


        foreach ($golos_response->inquiryRating->answerCounts as $num => $answer) {
            $chart_values .= ' data-value-'.$num.'="'.$answer->count.'"';
        }

        $golos_block .= '<div id="'.$diagram_id.'" class="village-chart"'.$chart_values.'></div>';
        $golos_block .= '</div>';

        $golos_response->startDate = russian_datetime_from_timestamp(strtotime($golos_response->startDate), $needtime=false, $needdaytoday=false, $needyear=true);
        $golos_response->endDate = russian_datetime_from_timestamp(strtotime($golos_response->endDate), $needtime=false, $needdaytoday=false, $needyear=true);


        $golos_block.= '<a href="http://golos27.ru/?action=auth" class="vote-link" target="_blank">Оценить главу поселения</a>';


        $golos_block.= '</div>';


        $this->data->ajaxhtml.= $contacts_block;
        $this->data->ajaxhtml.= $golos_block;


        break;

    case 'getAllVillages':
        $this->data->ajaxhtml = json_encode(GetAllVilages($this));
        break;

    case 'memorybook':
        $this->GetPostValues('q');
        $q = mb_strtolower($this->values->q, 'utf-8');
        // $q = strtolower($this->values->q);

        // $this->data->ajaxhtml = $q;

        $query = 'SELECT "name" FROM "materials" WHERE "status_id" = 2 AND "type_id" = 241 AND lower("name") LIKE ' . "'" . $q . "%'" . ' ORDER BY "name"';
        $result = $this->dbquery($query);

        $surnames = array();

        foreach ($result as $num => $item) {
            $surname = preg_replace("/\s.*/", '', $item['name']);
            if (!in_array($surname, $surnames)) {
                $surnames[] = $surname;
            }

        }

        $this->data->ajaxhtml = json_encode($surnames);
        break;

    case 'getServerTime':
            $this->GetPostValues(array('t', 'tz'));
            $clientTime = $this->values->t;
            $clientTimeZoneOffsetMinutes = $this->values->tz;

            $clientTime = $clientTime + $clientTimeZoneOffsetMinutes * 60 * 1000;

            $serverTimeMs = round(microtime(true) * 1000) + 10*60*60*1000;

            $clientServerTimeDiff = $serverTimeMs - $clientTime;

            $this->data->ajaxhtml = $clientServerTimeDiff;
        break;

    default:

        $actionFilePath = dirname(__FILE__)."/action/$action.php";

        if(file_exists($actionFilePath)){
            include($actionFilePath);
        }
        else{
            $this->data->ajaxhtml = json_encode(array(
                'status' => 'fail',
                'message' => 'Не найден файл ' . $actionFilePath
            ));
        }

        echo $this->data->ajaxhtml;

        // завершаем скрипт таким путем, чтобы движок сайта не успел вывести инфу о времени выполнения, warnings и т.д.
        exit;
}
