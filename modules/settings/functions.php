<?php
/**
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


function settings_set(\iSite $site, $name, $value)
{
    $res = $site->dbquery('SELECT 1 FROM settings WHERE name = $1', array($name));
    if (!empty($res)) {
        $ret = $site->dbquery('UPDATE settings SET value = $1 WHERE name = $2', array($value, $name));
    } else {
        $ret = $site->dbquery('INSERT INTO settings(name, value) VALUES($1, $2)', array($name, $value));
    }
    return $ret !== false;
}

function settings_delete(\iSite $site, $name)
{
    $res = $site->dbquery('DELETE FROM settings WHERE name = $1', array($name));
    return $res !== false;
}

function settings_get_count(\iSite $site)
{
    $res = $site->dbquery('SELECT COUNT(*) num FROM settings');
    return empty($res) ? 0 : intval($res[0]['num']);
}
