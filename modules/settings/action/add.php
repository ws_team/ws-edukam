<?php
/**
 * @var \iSite $this
 * @author Dmitriy Tkach <d.tkach@white-soft.ru>
 */


defined('_WPF_') or die();

if (!$this->getUser()->can('admin.settings.add')) {
    $this->error->flag = 1;
    $this->error->text = 'Действие не разрешено';
    return;
}

if (include(dirname(__FILE__).'/setparam.php'))
    $this->error->text = 'Параметр добавлен';
