<?php

$this->getUser()->logout();

$_SESSION['siteuserid'] = false;
$_SESSION['siteuserauthorized'] = false;

unset($_SESSION);

header("Location: /");
die();