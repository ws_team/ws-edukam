<?php

global $uflag;

if(($this->autorize->autorized == 1)&&($this->autorize->userrole > 0))
{

    //выполняем действие
    $this->GetPostValues('action');

    if($this->values->action == 'toggle')
    {
        ToggleUnderConstructionFlag($this);
    }

    //получаем флаг
    $uflag=GetUnderConstructionFlag($this);

}