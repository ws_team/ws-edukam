<?php

function specialver_select_stylesheets(\iSite $site, $path_prefix = '/')
{
    if (specialver_is_active($site) && isset($_SESSION['special_fontsize'])) {

        switch ($_SESSION['special_fontsize']){
            case '2x':
                $site->AttachCSS($path_prefix.'css/edukamCSS/specialVersion2x.min.css');
                break;
            case '3x':
                $site->AttachCSS($path_prefix.'css/edukamCSS/specialVersion3x.min.css');
                break;
        }
    }
}

function specialver_body_classname(\iSite $site)
{
    $ret = array();

    if (specialver_is_active($site)) {
        $ret[] = 'special xassist';

        if (specialver_option('toggle_media'))
            $ret[] = 'hidemedia';
        else
            $ret[] = 'nohidemedia';

        if (specialver_option('toggle_sidebar'))
            $ret[] = 'hidesidebar';
        else
            $ret[] = 'nohidesidebar';

        if (specialver_option('showhint'))
            $ret[] = 'assist-showhint';

        $ret[] = 'fontsize-'.specialver_option('fontsize', 'normal');
        $ret[] = 'color-'.specialver_option('color', 'normal');
    }

    return implode(' ', $ret);
}

function specialver_header_toggle_link(\iSite $site)
{
    $href = specialver_toggle_link_url($site);
    $is_special = specialver_is_active($site);
    $class = 'topmenu1-icon glasses-mode'.($is_special ? ' active' : '');
    $tooltip = $is_special ? 'обычная версия' : 'версия для слабовидящих';
    return '<a href="'.htmlspecialchars($href).'" class="'.$class.'" data-tooltip="'.
        htmlspecialchars($tooltip).'"></a>';
}

function specialver_is_active(\iSite $site)
{
    $adminPages = array(
        'admin',
        'editmaterials',
        'material_types',
        'users',
        'standarttemplates'
    );
    
    if (isset($site->values->menu) && in_array($site->values->menu, $adminPages))
        return false;

    return isset($site->data->special) && $site->data->special == 1;
}

function specialver_option($name, $fallback = null)
{
    return isset($_SESSION['special_'.$name]) ?
        $_SESSION['special_'.$name] :
        $fallback;
}

function specialver_toggle_link_url(\iSite $site)
{
    $query = getenv('QUERY_STRING');
    $query = preg_replace('/&?\bversion=\w*/', '', $query);
    $query = '?'.($query ? $query.'&' : '');
    $query .= 'version='.(specialver_is_active($site) ? 'normal' : 'special');
    return $query;
}

function specialver_toggle_link_text(\iSite $site)
{
    return specialver_is_active($site) ?
        'Обычная версия сайта' :
        'Версия для слабовидящих';
}

function specialver_footer_toggle_link(\iSite $site, $localize)
{
    $linkUrl = specialver_toggle_link_url($site);
    $linkText = specialver_toggle_link_text($site);
    $linkText = localize($linkText, $site->data->language, $localize);

    if (specialver_is_active($site)) {
        return '<div class="footeroption">
            <a href="'.htmlspecialchars($linkUrl).'">'.
                htmlspecialchars($linkText).
            '</a></div>';
    } else  {
        return '<div class="footeroption">'.
            '<a href="'.htmlspecialchars($linkUrl).'">'.
                htmlspecialchars($linkText).
            '</a></div>';
    }
}

function specialver_url($option, $value)
{
    $arg = 'special_'.$option;
    $url = getenv('REQUEST_URI');
    $rx = '/&*\b'.$arg.'=\w*/';

    $url = preg_replace($rx, '', $url);
    $url .= strpos($url, '?') === false ? '?' : '&';
    $url .= $arg.'='.urlencode($value);

    return $url;
}